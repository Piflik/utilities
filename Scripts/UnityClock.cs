﻿using System;
using UnityEngine;

namespace Utilities {
	public class UnityClock : MonoBehaviour {
		private static UnityClock _instance;

		public static UnityClock instance {
			get {
				if (_instance == null) {
					GameObject go = new GameObject("Clock");
					_instance = go.AddComponent<UnityClock>();
				}

				return _instance;
			}
		}

		public static bool isRunning => _instance != null;

		public event Action onUpdate;
		public event Action onFixedUpdate;
		public event Action onLateUpdate;
		public event Action<bool> onApplicationFocus;
		public event Action<bool> onApplicationPause;
		public event Action onApplicationQuit;
		public event Action onDrawGizmos;
		public event Action onPreRender;
		public event Action onPostRender;
		public event Action onGui;

		private void OnDestroy() {
			_instance = null;
		}

		private void Update() {
			onUpdate?.Invoke();
		}

		private void FixedUpdate() {
			onFixedUpdate?.Invoke();
		}

		private void LateUpdate() {
			onLateUpdate?.Invoke();
		}

		private void OnApplicationFocus(bool hasFocus) {
			onApplicationFocus?.Invoke(hasFocus);
		}

		private void OnApplicationPause(bool pauseStatus) {
			onApplicationPause?.Invoke(pauseStatus);
		}

		private void OnApplicationQuit() {
			onApplicationQuit?.Invoke();
		}

		private void OnDrawGizmos() {
			onDrawGizmos?.Invoke();
		}

		private void OnPreRender() {
			onPreRender?.Invoke();
		}

		private void OnPostRender() {
			onPostRender?.Invoke();
		}

		private void OnGUI() {
			onGui?.Invoke();
		}
	}
}
