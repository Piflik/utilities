﻿using UnityEngine;

namespace Utilities {
	[RequireComponent(typeof(CharacterController))]
	public class FPSPlayer : MonoBehaviour {

		[SerializeField]
		Camera _cam;

		[SerializeField]
		float _verticalAngleMin = -85;

		[SerializeField]
		float _verticalAngleMax = 85;

		[SerializeField]
		float _mouseSensitivity = 15;

		float _rotationX;
		float _rotationY;
		Quaternion _rotation;
		Quaternion _camRotation;

		CharacterController _controller;

		[SerializeField]
		float _moveSpeed = 2;
		Vector3 _moveDir;

		float _gravity = 1;

		void Start() {
			if (!_cam) {
				_cam = GetComponentInChildren<Camera>();
			}

			if (_cam) {
				_camRotation = _cam.transform.localRotation;
			}

			Renderer rend = GetComponent<Renderer>();
			if (rend) rend.enabled = false;

			_rotation = transform.localRotation;

			_controller = GetComponent<CharacterController>();
		}

		void Update() {

			//mouse look
			_rotationX += Input.GetAxis("Mouse X") * _mouseSensitivity;
			_rotationY += Input.GetAxis("Mouse Y") * _mouseSensitivity;

			_rotationX = ClampAngle(_rotationX, -360f, 360f);
			_rotationY = ClampAngle(_rotationY, _verticalAngleMin, _verticalAngleMax);

			Quaternion xQuat = Quaternion.AngleAxis(_rotationX, Vector3.up);
			Quaternion yQuat = Quaternion.AngleAxis(_rotationY, -Vector3.right);

			transform.localRotation = _rotation * xQuat;

			if (_cam) {
				_cam.transform.localRotation = _camRotation * yQuat;
			}

			//gravity
			_moveDir.y -= _gravity * Time.deltaTime;

			_moveDir.x = Input.GetAxis("Horizontal") * _moveSpeed;
			_moveDir.z = Input.GetAxis("Vertical") * _moveSpeed;

			_moveDir = transform.TransformDirection(_moveDir);

			_controller.Move(_moveDir * Time.deltaTime);
		}

		public static float ClampAngle(float angle, float min, float max) {
			if (angle < -360f)
				angle += 360f;
			if (angle > 360f)
				angle -= 360f;
			return Mathf.Clamp(angle, min, max);
		}

		public void SetRotation(Quaternion q) {
			Vector3 euler = q.eulerAngles;

			_rotationX = euler.y;
			_rotationY = 0;

			Quaternion xQuat = Quaternion.AngleAxis(_rotationX, Vector3.up);
			Quaternion yQuat = Quaternion.AngleAxis(_rotationY, -Vector3.right);

			transform.localRotation = _rotation * xQuat;

			if (_cam) {
				_cam.transform.localRotation = _camRotation * yQuat;
			}
		}
	}
}