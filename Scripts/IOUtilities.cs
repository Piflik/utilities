﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

#if UNITY_ANDROID && !UNITY_EDITOR || NET_4_6
using UnityEngine.Networking;
#endif

// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static class IOUtilities {
		public static Uri FindNextAvailableFilename(Uri directory, string name, string format) {
			string file = name;
			Uri ret;

			for (int i = 1; File.Exists((ret = new Uri($"file://{directory.LocalPath}{file}")).LocalPath); ++i) {
				file = string.Format(format, i);
			}
			return ret;
		}

		/// <summary>
		/// returns true for an absolute path
		/// </summary>
		public static bool IsFullPath(string path) {
#if UNITY_ANDROID && !UNITY_EDITOR
			return !string.IsNullOrEmpty(path)
				&& path.IndexOfAny(System.IO.Path.GetInvalidPathChars().ToArray()) == -1
				//&& path.StartsWith("/")
				&& path.ToLower().Contains("/emulated/");
#else
			return !string.IsNullOrEmpty(path)
				&& path.IndexOfAny(System.IO.Path.GetInvalidPathChars().ToArray()) == -1
				&& System.IO.Path.IsPathRooted(path)
				&& !System.IO.Path.GetPathRoot(path).Equals(System.IO.Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal);
#endif
		}

		/// <summary>
		/// Load a sprite from disc
		/// </summary>
		public static Sprite LoadSprite(string path, Rect? rect = null, FilterMode filterMode = FilterMode.Point, int pixelsPerUnit = 100, SpriteMeshType meshType = SpriteMeshType.FullRect, Vector4 border = default(Vector4)) {
			Texture2D tex = LoadTexture(path, filterMode);
			if (tex == null) return null;
			Rect spriteRect = rect ?? new Rect(0, 0, tex.width, tex.height);
			Sprite sprite = Sprite.Create(tex, spriteRect, new Vector2(0.5f, 0.5f), pixelsPerUnit, 0, meshType: meshType, border: border);
			sprite.name = System.IO.Path.GetFileNameWithoutExtension(path);

			return sprite;
		}

		public static bool TryLoadSprite(string path, out Sprite sprite, Rect? rect = null, FilterMode filterMode = FilterMode.Point, int pixelsPerUnit = 100, SpriteMeshType meshType = SpriteMeshType.FullRect) {
			sprite = LoadSprite(path, rect, filterMode, pixelsPerUnit, meshType);
			return sprite != null;
		}

		/// <summary>
		/// Load a multisprite from disc
		/// </summary>
		public static Sprite[] LoadMultiSprite(string path, FilterMode filterMode = FilterMode.Point, int pixelsPerUnit = 100, SpriteMeshType meshType = SpriteMeshType.FullRect, params Rect[] rects) {
			if (rects == null || rects.Length < 1) {
				return new[] { LoadSprite(path, filterMode: filterMode, pixelsPerUnit: pixelsPerUnit) };
			}

			Sprite[] sprites = new Sprite[rects.Length];

			for (int i = 0; i < rects.Length; ++i) {
				sprites[i] = LoadSprite(path, rects[i], filterMode, pixelsPerUnit, meshType);
			}

			return sprites;
		}

		public static bool TryLoadMultiSprite(string path, out Sprite[] sprites, FilterMode filterMode = FilterMode.Point, int pixelsPerUnit = 100, SpriteMeshType meshType = SpriteMeshType.FullRect, params Rect[] rects) {
			try {
				sprites = LoadMultiSprite(path, filterMode, pixelsPerUnit, meshType, rects);
			} catch (Exception e) {
				UnityEngine.Debug.LogError(e.Message);
				sprites = new Sprite[0];
				return false;
			}

			return sprites.Length > 0;
		}

		/// <summary>
		/// Load a texture from disc
		/// </summary>
		public static Texture2D LoadTexture(string path, FilterMode filterMode = FilterMode.Point, bool mipChain = false) {
			byte[] fileData;

			try {
				fileData = File.ReadAllBytes(IsFullPath(path) ? path : Utils.CreateAssembledPath(Utils.GetRootPath(), path));
			} catch (Exception e) {
				Debug.Log(e.Message);
				return null;
			}

			Texture2D tex = new Texture2D(2, 2, TextureFormat.RGBA32, mipChain) { filterMode = filterMode };
			if (!tex.LoadImage(fileData)) {
				Debug.LogError($"Could not load {path}: Filetype not supported");
				return null;
			}

			return tex;
		}

		public static bool TryLoadTexture(string path, out Texture2D tex, FilterMode filterMode = FilterMode.Point, bool mipChain = false) {
			tex = LoadTexture(path, filterMode, mipChain);
			return tex != null;
		}

		public static Texture2D[] LoadTextures(string path, FilterMode filterMode = FilterMode.Point, bool mipChain = false) {

			string[] texturePaths = Directory.GetFiles(path).Where(Utils.IsImage).ToArray();
			Texture2D[] textures = new Texture2D[texturePaths.Length];

			for (int i = 0; i < texturePaths.Length; ++i) {
				string texturePath = texturePaths[i];

				using (FileStream file = File.Open(texturePath, FileMode.Open)) {
					byte[] bytes = new byte[file.Length];
					file.Read(bytes, 0, (int)file.Length);
					Texture2D tex = new Texture2D(2, 2, TextureFormat.RGBA32, mipChain) { filterMode = filterMode };
					tex.LoadImage(bytes);
					textures[i] = tex;
				}
			}

			return textures;
		}

		public static bool TryLoadTextures(string path, out Texture2D[] textures, FilterMode filterMode = FilterMode.Point, bool mipChain = false) {
			textures = LoadTextures(path, filterMode, mipChain);
			return textures.Length > 0;
		}

		public static async Task<Texture2D[]> LoadTexturesAsync(string path, FilterMode filterMode = FilterMode.Point, bool mipChain = false) {

			string[] texturePaths = Directory.GetFiles(path);
			Texture2D[] textures = new Texture2D[texturePaths.Length];

			for (int i = 0; i < texturePaths.Length; ++i) {
				string texturePath = texturePaths[i];

				using (FileStream file = File.Open(texturePath, FileMode.Open)) {
					byte[] bytes = new byte[file.Length];
					await file.ReadAsync(bytes, 0, (int)file.Length);
					Texture2D tex = new Texture2D(2, 2, TextureFormat.RGBA32, mipChain) { filterMode = filterMode };
					tex.LoadImage(bytes);
					textures[i] = tex;
				}
			}

			return textures;
		}

		public static bool WritePNG(Texture2D tex, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;
			return WriteBytesRaw(tex.EncodeToPNG(), path, overwriteMode);
		}

		public static bool WriteJPG(Texture2D tex, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;
			return WriteBytesRaw(tex.EncodeToJPG(), path, overwriteMode);
		}

		public static bool WriteTGA(Texture2D tex, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;
			return WriteBytesRaw(tex.EncodeToTGA(), path, overwriteMode);
		}

		public static bool WriteEXR(Texture2D tex, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;
			return WriteBytesRaw(tex.EncodeToEXR(), path, overwriteMode);
		}

		public static bool WritePNG(RenderTexture tex, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;
			return WritePNG(tex.ToTexture2D(), path, overwriteMode);
		}

		public static bool WriteJPG(RenderTexture tex, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;
			return WriteJPG(tex.ToTexture2D(), path, overwriteMode);
		}

		public static bool WriteTGA(RenderTexture tex, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;
			return WriteTGA(tex.ToTexture2D(), path, overwriteMode);
		}

		public static bool WriteEXR(RenderTexture tex, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;
			return WriteEXR(tex.ToTexture2D(), path, overwriteMode);
		}

		/// <summary>
		/// Load Audio file from disc
		/// </summary>
		public static AudioClip LoadSound(string path) {

#if NET_4_6
			AudioType at;
			switch (System.IO.Path.GetExtension(path)?.ToLower()) {
				case ".mp3":
					at = AudioType.MPEG;
					break;
				case ".ogg":
					at = AudioType.OGGVORBIS;
					break;
				case ".wav":
					at = AudioType.WAV;
					break;
				default:
					at = AudioType.UNKNOWN;
					break;
			}

			//TODO upgrade Utilities to C# 8.0
			using (UnityWebRequest uwr = UnityWebRequestMultimedia.GetAudioClip($"file://{(IsFullPath(path) ? path : Utils.CreateAssembledPath(Utils.GetRootPath(), path))}", at)) {
				UnityWebRequestAsyncOperation request = uwr.SendWebRequest();

				while (!request.isDone) {
					Thread.Sleep(10);
				}
#if UNITY_2020_1_OR_NEWER
				return uwr.result == UnityWebRequest.Result.Success ? DownloadHandlerAudioClip.GetContent(uwr) : null;
#else
					return !uwr.isHttpError && !uwr.isNetworkError ? DownloadHandlerAudioClip.GetContent(uwr) : null;
#endif
			}
#else
				WWW www = new WWW(string.Format("file://{0}", IsFullPath(path) ? path : CreateAssembledPath(GetRootPath(), path)));

				while (!www.isDone) {
					Thread.Sleep(10);
				}

				return !string.IsNullOrEmpty(www.error) ? null : www.GetAudioClip(false, false);
#endif
		}

		public static bool TryLoadSound(string path, out AudioClip audio) {
			audio = LoadSound(path);
			return audio != null;
		}

		/// <summary>
		/// Copy a Directory with subdirectories
		/// </summary>
		public static bool CopyDirectory(string srcPath, string dstPath, OverwriteMode overwriteMode = OverwriteMode.Always) {
			DirectoryInfo diSource = new DirectoryInfo(srcPath);
			DirectoryInfo diTarget = new DirectoryInfo(dstPath);

			return CopyDirectory(diSource, diTarget, overwriteMode);
		}

		/// <summary>
		/// Copy a Directory with subdirectories
		/// </summary>
		public static bool CopyDirectory(DirectoryInfo src, DirectoryInfo dest, OverwriteMode overwriteMode = OverwriteMode.Always) {

			if (!src.Exists) return false;

			bool success = true;

			Directory.CreateDirectory(dest.FullName);

			foreach (FileInfo fi in src.GetFiles()) {
				success &= CopyFile(fi, new FileInfo(System.IO.Path.Combine(dest.FullName, fi.Name)), overwriteMode);
			}

			// Copy each subdirectory using recursion.
			foreach (DirectoryInfo diSourceSubDir in src.GetDirectories()) {
				DirectoryInfo nextTargetSubDir = dest.CreateSubdirectory(diSourceSubDir.Name);
				success &= CopyDirectory(diSourceSubDir, nextTargetSubDir, overwriteMode);
			}

			return success;
		}

		public static bool CopyFile(string srcPath, string dstPath, OverwriteMode overwriteMode = OverwriteMode.Always) {
			FileInfo src = new FileInfo(srcPath);
			FileInfo dst = new FileInfo(dstPath);

			return CopyFile(src, dst, overwriteMode);
		}

		public static bool CopyFile(FileInfo src, FileInfo dst, OverwriteMode overwriteMode = OverwriteMode.Always) {

			if (!src.Exists) return false;

			if (dst.Exists) {
				DateTime lastWriteSrc = src.LastWriteTime;
				DateTime lastWriteDst = dst.LastWriteTime;


				switch (overwriteMode) {
					case OverwriteMode.Never:
					case OverwriteMode.Newer when lastWriteSrc <= lastWriteDst:
					case OverwriteMode.Different when lastWriteSrc == lastWriteDst:
						return true;
				}
			}

			if (!Directory.Exists(dst.DirectoryName)) Directory.CreateDirectory(dst.DirectoryName);

			src.CopyTo(dst.FullName, true);
			return true;
		}

		public static bool WriteBytes(object obj, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {

			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;

			string directory = System.IO.Path.GetDirectoryName(path);
			if (!Directory.Exists(directory)) {
				Directory.CreateDirectory(directory);
			}

			BinaryFormatter formatter = new BinaryFormatter();
			using (FileStream file = File.Open(path, FileMode.Create)) {
				formatter.Serialize(file, obj);
			}

			return true;
		}

		public static bool WriteBytesRaw(byte[] bytes, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;

			string directory = System.IO.Path.GetDirectoryName(path);
			if (!Directory.Exists(directory)) {
				Directory.CreateDirectory(directory);
			}

			File.WriteAllBytes(path, bytes);
			return true;
		}

		public static bool ReadBytes<T>(string path, out T obj) {
			if (!File.Exists(path)) {
				obj = default;
				return false;
			}

			BinaryFormatter formatter = new BinaryFormatter();
			using (FileStream file = File.Open(path, FileMode.Open)) {
				object o = formatter.Deserialize(file);

				if (o is T t) {
					obj = t;
					return true;
				}
			}

			obj = default;
			return false;
		}

		public static bool ReadBytesRaw(string path, out byte[] data) {
			if (!File.Exists(path)) {
				data = default;
				return false;
			}

			using (FileStream file = File.Open(path, FileMode.Open)) {
				data = new byte[file.Length];
				file.Read(data, 0, (int)file.Length);

				return true;
			}
		}

		public static bool ReadBytesParallel<T>(string path, int numPackets, out T obj) {

			if (!ReadBytesParallelRaw(path, numPackets, out byte[] bytes)) {
				obj = default;
				return false;
			}

			using (MemoryStream mStream = new MemoryStream(bytes)) {
				BinaryFormatter formatter = new BinaryFormatter();
				object o = formatter.Deserialize(mStream);

				if (o is T t) {
					obj = t;
					return true;
				}

				obj = default;
				return false;
			}
		}

		public static bool ReadBytesParallelRaw(string path, int numPackets, out byte[] data) {
			if (!File.Exists(path)) {
				data = default;
				return false;
			}

			using (FileStream file = File.Open(path, FileMode.Open)) {
				int frameWidth = (int)(file.Length / numPackets);
				int lastFrameWidth = (int)(file.Length % frameWidth);

				Task[] tasks = new Task[lastFrameWidth > 0 ? numPackets + 1 : numPackets];
				data = new byte[file.Length];


				for (int i = 0; i < numPackets; ++i) {
					tasks[i] = file.ReadAsync(data, i * frameWidth, frameWidth);
				}

				if (lastFrameWidth > 0) {
					tasks[numPackets] = file.ReadAsync(data, numPackets * frameWidth, lastFrameWidth);
				}

				Task.WaitAll(tasks);

				return true;
			}
		}

#if UNITY_2021_3_OR_NEWER
		public static async Task<bool> WriteBytesAsync<T>(T obj, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;

			await Task.Run(() => {

				string directory = System.IO.Path.GetDirectoryName(path);
				if (!Directory.Exists(directory)) {
					Directory.CreateDirectory(directory);
				}

				BinaryFormatter formatter = new BinaryFormatter();
				using (FileStream file = File.Open(path, FileMode.Create)) {
					formatter.Serialize(file, obj);
				}
			});

			return true;
		}

		public static async Task<bool> WriteBytesAsyncRaw(byte[] bytes, string path, OverwriteMode overwriteMode = OverwriteMode.Always) {
			if (File.Exists(path) && overwriteMode == OverwriteMode.Never) return false;

			string directory = System.IO.Path.GetDirectoryName(path);
			if (!Directory.Exists(directory)) {
				Directory.CreateDirectory(directory);
			}

			await File.WriteAllBytesAsync(path, bytes);
			return true;
		}
#endif

		public static async Task<(bool success, T obj)> ReadBytesAsync<T>(string path) {

			(bool success, byte[] data) = await ReadBytesAsyncRaw(path);

			if (!success) {
				return (false, default);
			}

			T obj = default;
			await Task.Run(() => {
				using (MemoryStream mStream = new MemoryStream(data)) {
					BinaryFormatter formatter = new BinaryFormatter();
					object o = formatter.Deserialize(mStream);

					if (o is T t) {
						obj = t;
						return;
					}

					success = false;
				}
			});

			return (success, obj);
		}

		public static async Task<(bool success, byte[] data)> ReadBytesAsyncRaw(string path) {
			if (!File.Exists(path)) {
				return (false, default);
			}

			using (FileStream file = File.Open(path, FileMode.Open)) {
				byte[] data = new byte[file.Length];
				await file.ReadAsync(data, 0, (int)file.Length);

				return (true, data);
			}
		}

		public enum OverwriteMode {
			Never,
			Always,
			Newer,
			Different,
		}

		/// <summary>
		/// Try to copy a file from StreamingAssets
		/// </summary>
		/// <param name="srcFile">Source path relative to StreamingAssets</param>
		/// <param name="dstFile">Destination path relative to application root</param>
		/// <param name="overwrite">Determine if destination should be overwritten if it already exists</param>
		/// <returns>Copy success</returns>
		public static bool TryCopyFromStreamingAssets(string srcFile, string dstFile, OverwriteMode overwrite = OverwriteMode.Never) {
			string dstPath = Utils.CreateAssembledPath(Utils.GetRootPath(), dstFile);
			string srcPath = Utils.CreateAssembledPath(Application.streamingAssetsPath, srcFile);

			bool dstExists = File.Exists(dstPath);

			if (dstExists && overwrite == OverwriteMode.Never) {
				return false;
			}

			string metaData = System.IO.Path.ChangeExtension(dstPath, ".meta");

			if (dstExists) {
#if UNITY_EDITOR
				DateTime dstTime = File.GetLastWriteTime(dstPath);
				DateTime srcTime = File.GetLastWriteTime(srcPath);

				if (overwrite != OverwriteMode.Never && srcTime < dstTime && File.Exists(srcPath)) {
					Debug.LogWarning($"File {dstFile} seems to have been modified, but is marked to be overwritten from StreamingAssets. If you want to modify the file, edit the file in the StreamingAssets folder", new UnityEngine.Color(1, 0.5f, 0, 1));
				}

				if ((overwrite == OverwriteMode.Newer && srcTime <= dstTime)
					|| (overwrite == OverwriteMode.Different && srcTime == dstTime)) {
					return false;
				}
#else
					if (!File.Exists(metaData) || !Version.TryParse(File.ReadAllText(metaData), out Version dstVersion)) dstVersion = default;
					if (!Version.TryParse(Application.version, out Version srcVersion)) srcVersion = default;
					if (overwrite == OverwriteMode.Newer && srcVersion <= dstVersion
						|| overwrite == OverwriteMode.Different && srcVersion == dstVersion) return false;
#endif
			}

			//TODO WebGL?
#if UNITY_ANDROID && !UNITY_EDITOR
				UnityWebRequest request = UnityWebRequest.Get(srcPath);
				request.SendWebRequest();

				while (!request.isDone) { }
#if UNITY_2020_1_OR_NEWER
				if (request.result != UnityWebRequest.Result.Success) return false;
#else
				if (!string.IsNullOrEmpty(request.error)) return false;
#endif
				if (request.downloadHandler.data != null) {

				string dstDir = System.IO.Path.GetDirectoryName(dstPath);
				if (!Directory.Exists(dstDir)) {
					Directory.CreateDirectory(dstDir);
				}

					File.WriteAllBytes(path: dstPath, bytes: request.downloadHandler.data);
				}
#elif !UNITY_WEBGL || UNITY_EDITOR
			if (!File.Exists(srcPath)) {
				return false;
			}

			string dstDir = System.IO.Path.GetDirectoryName(dstPath);
			if (!Directory.Exists(dstDir)) {
				Directory.CreateDirectory(dstDir);
			}

			File.Copy(srcPath, dstPath, true);
#endif
			File.WriteAllText(metaData, Application.version);

			return true;
		}
	}
}
