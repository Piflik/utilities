﻿#if !NO_SQL
using System.Collections.Generic;
using Mono.Data.Sqlite;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static class SQLExtensions {
		public static bool HasColumn(this SqliteConnection connection, string tableName, string columnName) {
			using (SqliteCommand cmd = new SqliteCommand($"PRAGMA table_info({tableName})", connection))
			using (SqliteDataReader dr = cmd.ExecuteReader()) {
				while (dr.Read()) { //loop through the various columns and their info
					object value = dr.GetValue(1); //column 1 from the result contains the column names
					if (columnName.Equals(value)) {
						return true;
					}
				}

				return false;
			}
		}

		public static List<string> Columns(this SqliteConnection connection, string tableName) {
			using (SqliteCommand cmd = new SqliteCommand($"PRAGMA table_info({tableName})", connection))
			using (SqliteDataReader dr = cmd.ExecuteReader()) {
				List<string> columns = new List<string>();

				while (dr.Read()) {
					columns.Add((string)dr.GetValue(1));
				}

				return columns;
			}
		}
	}
}
#endif
