﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public struct Range<T> : IEquatable<Range<T>>, IComparable<Range<T>> where T : struct, IEquatable<T>, IComparable<T> {
		public T min { get; set; }
		public T max { get; set; }

		public Range(T min, T max) : this() {
			if (min.CompareTo(max) <= 0) {
				this.min = min;
				this.max = max;
			} else {
				this.min = max;
				this.max = min;
			}
		}

		public bool valid => min.CompareTo(max) <= 0;

		public Range<T> Validate() {
			return valid ? this : new Range<T> { min = max, max = min };
		}

		public bool Contains(T value) {
			return min.CompareTo(value) <= 0 && max.CompareTo(value) >= 0;
		}

		public bool Contains(Range<T> range) {
			return Contains(range.min) && Contains(range.max);
		}

		public bool Intersects(Range<T> range) {
			return Contains(range.min) && max.CompareTo(range.max) < 0
			|| Contains(range.max) && min.CompareTo(range.min) > 0;
		}

		public Range<T> Apply(Func<T, T> function) {
			return new Range<T> { min = function(min), max = function(max) };
		}

		public int CompareTo(Range<T> range) {
			if (min.CompareTo(range.min) < 0 && max.CompareTo(range.max) < 0) return -1;
			if (min.CompareTo(range.min) > 0 && max.CompareTo(range.max) > 0) return 1;
			return 0;
		}

		public override bool Equals(object obj) {
			if (obj is null) return false;
			return obj is Range<T> range && Equals(range);
		}

		public override int GetHashCode() {
			unchecked {
				return (min.GetHashCode() * 397) ^ max.GetHashCode();
			}
		}

		public bool Equals(Range<T> range) {
			return min.Equals(range.min) && max.Equals(range.max);
		}
	}

	[Serializable]
	public struct FloatRange : IEquatable<FloatRange>, IComparable<FloatRange>, IComparable<float> {
		public float min;
		public float max;

		public FloatRange(float min, float max) : this() {
			this.min = Mathf.Min(min, max);
			this.max = Mathf.Max(min, max);
		}

		public FloatRange(float value) : this() {
			min = max = value;
		}

		public static readonly FloatRange zero = new FloatRange();
		public static readonly FloatRange one = new FloatRange(1, 1);

		public float center => (max + min) * 0.5f;
		public float size => max - min;
		public float random => Random.Range(min, max);
		public bool valid => min <= max;

		public void Validate() {
			if (valid) return;
			float tmp = min;
			min = max;
			max = tmp;
		}

		public FloatRange Intersect(FloatRange other) {
			return !Intersects(other) ? zero : new FloatRange(Mathf.Max(min, other.min), Mathf.Min(max, other.max));
		}

		public FloatRange Combine(FloatRange other) {
			return !Intersects(other) ? zero : new FloatRange(Mathf.Min(min, other.min), Mathf.Max(max, other.max));
		}

		public bool Contains(float value) {
			return value >= min && value <= max;
		}

		public bool Contains(FloatRange range) {
			return Contains(range.min) && Contains(range.max);
		}

		public bool Intersects(FloatRange range) {
			return Contains(range) || range.Contains(this)
				|| range.min < min && Contains(range.max)
				|| range.max > max && Contains(range.min);
		}

		public FloatRange Apply(Func<float, float> function) {
			return new FloatRange { min = function(min), max = function(max) };
		}

		public float Clamp(float value) {
			if (value < min) return min;
			if (value > max) return max;
			return value;
		}

		public float Lerp(float alpha, bool clamp = true) {
			if (clamp) {
				if (alpha < 0) return min;
				if (alpha > 1) return max;
			}

			return min + alpha * (max - min);
		}

		public int LerpToInt(float alpha, bool clamp = true) {
			if (clamp) {
				if (alpha < 0) return (int)min;
				if (alpha > 1) return (int)max;
			}

			return (int)(alpha * max + (1 - alpha) * min);
		}

		public float Remap(float value, FloatRange dst, bool clamp = true) {
			if (size.Approximates(0) || dst.size.Approximates(0)) return dst.min;

			if (clamp) {
				if (value < min) return dst.min;
				if (value > max) return dst.max;
			}

			return dst.min + (value - min) / (max - min) * (dst.max - dst.min);
		}

		public int RemapToInt(float value, FloatRange dst, bool clamp = true) {
			if (size.Approximates(0) || dst.size.Approximates(0)) return (int)dst.min;

			if (clamp) {
				if (value < min) return (int)dst.min;
				if (value > max) return (int)dst.max;
			}

			return (int)(dst.min + (value - min) / (max - min) * (dst.max - dst.min));
		}

		public float Remap(float value, IntRange dst, bool clamp = true) {
			if (size.Approximates(0) || dst.size == 0) return dst.min;

			if (clamp) {
				if (value < min) return dst.min;
				if (value > max) return dst.max;
			}

			return dst.min + (value - min) / (max - min) * (dst.max - dst.min);
		}

		public int RemapToInt(float value, IntRange dst, bool clamp = true) {
			if (size.Approximates(0) || dst.size == 0) return dst.min;

			if (clamp) {
				if (value < min) return dst.min;
				if (value > max) return dst.max;
			}

			return (int)(dst.min + (value - min) / (max - min) * (dst.max - dst.min));
		}

		public float RelativeValue(float value, bool clamp = true) {
			if (size.Approximates(0)) return 0;
			if (clamp) {
				if (value < min) return 0;
				if (value > max) return 1;
			}

			return (value - min) / (max - min);
		}

		public float SmoothstepInterpolation(float alpha) {
			if (alpha < 0) return min;
			if (alpha > 1) return max;

			alpha = alpha * alpha * (3 - 2 * alpha);

			return alpha * max + (1 - alpha) * min;
		}

		public float Sigmoid(float x, float e) {
			float r = max - min;
			float q = 1 + Mathf.Exp(-e * x);
			return min + r / q;
		}

		public int CompareTo(FloatRange other) {
			if (this > other) return 1;
			if (this < other) return -1;
			return 0;
		}

		public int CompareTo(float other) {
			if (this > other) return 1;
			if (this < other) return -1;
			return 0;
		}

		public override bool Equals(object obj) {
			if (obj is null) return false;
			return obj is FloatRange range && Equals(range);
		}

		public override int GetHashCode() {
			unchecked {
				return (min.GetHashCode() * 397) ^ max.GetHashCode();
			}
		}

		public override string ToString() {
			return $"[{min}, {max}]";
		}

		public bool Equals(FloatRange other) {
			return min.Equals(other.min) && max.Equals(other.max);
		}

		public static bool operator ==(FloatRange a, FloatRange b) {
			return a.min.Equals(b.min) && a.max.Equals(b.max);
		}

		public static bool operator !=(FloatRange a, FloatRange b) {
			return !a.min.Equals(b.min) || !a.max.Equals(b.max);
		}

		public static bool operator <(FloatRange a, FloatRange b) {
			return a.min < b.min && a.max < b.max;
		}

		public static bool operator <=(FloatRange a, FloatRange b) {
			return a.min <= b.min && a.max <= b.max;
		}

		public static bool operator >(FloatRange a, FloatRange b) {
			return a.min > b.min && a.max > b.max;
		}

		public static bool operator >=(FloatRange a, FloatRange b) {
			return a.min >= b.min && a.max >= b.max;
		}

		public static bool operator <(FloatRange a, float b) {
			return a.min < b && a.max < b;
		}

		public static bool operator <=(FloatRange a, float b) {
			return a.min <= b && a.max <= b;
		}

		public static bool operator >(FloatRange a, float b) {
			return a.min > b && a.max > b;
		}

		public static bool operator >=(FloatRange a, float b) {
			return a.min >= b && a.max >= b;
		}

		public static bool operator <(float a, FloatRange b) {
			return a < b.min && a < b.max;
		}

		public static bool operator <=(float a, FloatRange b) {
			return a <= b.min && a <= b.max;
		}

		public static bool operator >(float a, FloatRange b) {
			return a > b.min && a > b.max;
		}

		public static bool operator >=(float a, FloatRange b) {
			return a >= b.min && a >= b.max;
		}

		public static FloatRange operator +(FloatRange a, FloatRange b) {
			return new FloatRange { min = a.min + b.min, max = a.max + b.max };
		}

		public static FloatRange operator +(FloatRange a, float b) {
			return new FloatRange { min = a.min + b, max = a.max + b };
		}

		public static FloatRange operator +(float a, FloatRange b) {
			return new FloatRange { min = b.min + a, max = b.max + a };
		}

		public static FloatRange operator -(FloatRange a, FloatRange b) {
			return new FloatRange { min = a.min - b.min, max = a.max - b.max };
		}

		public static FloatRange operator -(FloatRange a, float b) {
			return new FloatRange { min = a.min - b, max = a.max - b };
		}

		public static FloatRange operator -(float a, FloatRange b) {
			return new FloatRange { min = b.min - a, max = b.max - a };
		}

		public static FloatRange operator -(FloatRange a) {
			return new FloatRange { min = -a.max, max = -a.min };
		}

		public static FloatRange operator *(FloatRange a, FloatRange b) {
			return new FloatRange { min = a.min * b.min, max = a.max * b.max };
		}

		public static FloatRange operator *(FloatRange a, float b) {
			return new FloatRange { min = a.min * b, max = a.max * b };
		}

		public static FloatRange operator *(float a, FloatRange b) {
			return new FloatRange { min = b.min * a, max = b.max * a };
		}

		public static FloatRange operator /(FloatRange a, FloatRange b) {
			return new FloatRange { min = a.min / b.min, max = a.max / b.max };
		}

		public static FloatRange operator /(FloatRange a, float b) {
			return new FloatRange { min = a.min / b, max = a.max / b };
		}

		public static explicit operator IntRange(FloatRange value) {
			return new IntRange { min = (int)value.min, max = (int)value.max };
		}

		public string compilerCode => $"new {nameof(FloatRange)}({min:0.###}f, {max:0.###}f)";
	}

	[Serializable]
	public struct IntRange : IEquatable<IntRange>, IComparable<IntRange>, IComparable<float> {
		public int min;
		public int max;

		public IntRange(int min, int max) : this() {
			this.min = Mathf.Min(min, max);
			this.max = Mathf.Max(min, max);
		}

		public IntRange(int value) : this() {
			min = max = value;
		}

		public static readonly IntRange zero = new IntRange();
		public static readonly IntRange one = new IntRange(1, 1);

		public float center => (max + min) * 0.5f;
		public int size => max - min;
		public int random => Random.Range(min, max + 1);
		public bool valid => min <= max;

		public void Validate() {
			if (valid) return;
			int tmp = min;
			min = max;
			max = tmp;
		}

		public IntRange Intersect(IntRange other) {
			return !Intersects(other) ? zero : new IntRange(Mathf.Max(min, other.min), Mathf.Min(max, other.max));
		}

		public IntRange Combine(IntRange other) {
			if (!Intersects(other) && Mathf.Max(min, other.min) - Mathf.Min(max, other.max) > 1) return zero;

			return new IntRange(Mathf.Min(min, other.min), Mathf.Max(max, other.max));
		}

		public bool Contains(int value) {
			return value >= min && value <= max;
		}

		public bool Contains(IntRange range) {
			return Contains(range.min) && Contains(range.max);
		}

		public bool Intersects(IntRange range) {
			return Contains(range) || range.Contains(this)
				|| range.min < min && Contains(range.max)
				|| range.max > max && Contains(range.min);
		}

		public IntRange Apply(Func<int, int> function) {
			return new IntRange { min = function(min), max = function(max) };
		}

		public int Clamp(int value) {
			if (value < min) return min;
			if (value > max) return max;
			return value;
		}

		public int Clamp(float value) {
			if (value < min) return min;
			if (value > max) return max;
			return (int)value;
		}


		public float Lerp(float alpha, bool clamp = true) {
			if (clamp) {
				if (alpha < 0) return min;
				if (alpha > 1) return max;
			}

			return alpha * max + (1 - alpha) * min;
		}

		public int LerpToInt(float alpha, bool clamp = true) {
			if (clamp) {
				if (alpha < 0) return min;
				if (alpha > 1) return max;
			}

			return (int)(alpha * max + (1 - alpha) * min);
		}

		public float Remap(float value, FloatRange dst, bool clamp = true) {
			if (size == 0 || dst.size.Approximates(0)) return dst.min;

			if (clamp) {
				if (value < min) return dst.min;
				if (value > max) return dst.max;
			}

			return dst.min + (value - min) / (max - min) * (dst.max - dst.min);
		}

		public int RemapToInt(float value, FloatRange dst, bool clamp = true) {
			if (size == 0 || dst.size.Approximates(0)) return (int)dst.min;

			if (clamp) {
				if (value < min) return (int)dst.min;
				if (value > max) return (int)dst.max;
			}

			return (int)(dst.min + (value - min) / (max - min) * (dst.max - dst.min));
		}

		public float Remap(float value, IntRange dst, bool clamp = true) {
			if (size == 0 || dst.size == 0) return dst.min;

			if (clamp) {
				if (value < min) return dst.min;
				if (value > max) return dst.max;
			}

			return dst.min + (value - min) / (max - min) * (dst.max - dst.min);
		}

		public int RemapToInt(float value, IntRange dst, bool clamp = true) {
			if (size == 0 || dst.size == 0) return dst.min;

			if (clamp) {
				if (value < min) return dst.min;
				if (value > max) return dst.max;
			}

			return (int)(dst.min + (value - min) / (max - min) * (dst.max - dst.min));
		}

		public float RelativeValue(float value, bool clamp = true) {
			if (clamp) {
				if (value < min) return 0;
				if (value > max) return 1;
			}

			return (value - min) / (max - min);
		}

		public float SmoothstepInterpolation(float alpha) {
			if (alpha < 0) return min;
			if (alpha > 1) return max;

			alpha = alpha * alpha * (3 - 2 * alpha);

			return alpha * max + (1 - alpha) * min;
		}

		public int SmoothstepInterpolationToInt(float alpha) {
			if (alpha < 0) return min;
			if (alpha > 1) return max;

			alpha = alpha * alpha * (3 - 2 * alpha);

			return (int)(alpha * max + (1 - alpha) * min);
		}

		public float Sigmoid(float x, float e) {
			float r = max - min;
			float q = 1 + Mathf.Exp(-e * x);
			return min + r / q;
		}

		public int SigmoidToInt(float x, float e) {
			float r = max - min;
			float q = 1 + Mathf.Exp(-e * x);
			return (int)(min + r / q);
		}

		public int CompareTo(IntRange other) {
			if (this > other) return 1;
			if (this < other) return -1;
			return 0;
		}

		public int CompareTo(float other) {
			if (this > other) return 1;
			if (this < other) return -1;
			return 0;
		}

		public override bool Equals(object obj) {
			if (obj is null) return false;
			return obj is IntRange range && Equals(range);
		}

		public override int GetHashCode() {
			unchecked {
				return (min * 397) ^ max;
			}
		}

		public override string ToString() {
			return $"[{min}, {max}]";
		}

		public bool Equals(IntRange other) {
			return min == other.min && max == other.max;
		}

		public static bool operator ==(IntRange a, IntRange b) {
			return a.min.Equals(b.min) && a.max.Equals(b.max);
		}

		public static bool operator !=(IntRange a, IntRange b) {
			return !a.min.Equals(b.min) || !a.max.Equals(b.max);
		}

		public static bool operator <(IntRange a, IntRange b) {
			return a.min < b.min && a.max < b.max;
		}

		public static bool operator <=(IntRange a, IntRange b) {
			return a.min <= b.min && a.max <= b.max;
		}

		public static bool operator >(IntRange a, IntRange b) {
			return a.min > b.min && a.max > b.max;
		}

		public static bool operator >=(IntRange a, IntRange b) {
			return a.min >= b.min && a.max >= b.max;
		}

		public static bool operator <(IntRange a, float b) {
			return a.min < b && a.max < b;
		}

		public static bool operator <=(IntRange a, float b) {
			return a.min <= b && a.max <= b;
		}

		public static bool operator >(IntRange a, float b) {
			return a.min > b && a.max > b;
		}

		public static bool operator >=(IntRange a, float b) {
			return a.min >= b && a.max >= b;
		}

		public static bool operator <(float a, IntRange b) {
			return a < b.min && a < b.max;
		}

		public static bool operator <=(float a, IntRange b) {
			return a <= b.min && a <= b.max;
		}

		public static bool operator >(float a, IntRange b) {
			return a > b.min && a > b.max;
		}

		public static bool operator >=(float a, IntRange b) {
			return a >= b.min && a >= b.max;
		}

		public static IntRange operator +(IntRange a, IntRange b) {
			return new IntRange { min = a.min + b.min, max = a.max + b.max };
		}

		public static IntRange operator +(IntRange a, int b) {
			return new IntRange { min = a.min + b, max = a.max + b };
		}

		public static IntRange operator +(int a, IntRange b) {
			return new IntRange { min = b.min + a, max = b.max + a };
		}

		public static IntRange operator -(IntRange a, IntRange b) {
			return new IntRange { min = a.min - b.min, max = a.max - b.max };
		}

		public static IntRange operator -(IntRange a, int b) {
			return new IntRange { min = a.min - b, max = a.max - b };
		}

		public static IntRange operator -(int a, IntRange b) {
			return new IntRange { min = b.min - a, max = b.max - a };
		}

		public static IntRange operator -(IntRange a) {
			return new IntRange { min = -a.max, max = -a.min };
		}

		public static IntRange operator *(IntRange a, IntRange b) {
			return new IntRange { min = a.min * b.min, max = a.max * b.max };
		}

		public static IntRange operator *(IntRange a, int b) {
			return new IntRange { min = a.min * b, max = a.max * b };
		}

		public static IntRange operator *(int a, IntRange b) {
			return new IntRange { min = b.min * a, max = b.max * a };
		}

		public static FloatRange operator *(IntRange a, float b) {
			return new FloatRange { min = a.min * b, max = a.max * b };
		}

		public static FloatRange operator *(float a, IntRange b) {
			return new FloatRange { min = b.min * a, max = b.max * a };
		}

		public static IntRange operator /(IntRange a, IntRange b) {
			return new IntRange { min = a.min / b.min, max = a.max / b.max };
		}

		public static IntRange operator /(IntRange a, int b) {
			return new IntRange { min = a.min / b, max = a.max / b };
		}
		public static FloatRange operator /(IntRange a, float b) {
			return new FloatRange { min = a.min / b, max = a.max / b };
		}

		public static implicit operator FloatRange(IntRange value) {
			return new FloatRange { min = value.min, max = value.max };
		}

		public string compilerCode => $"new {nameof(IntRange)}({min:0}, {max:0})";
	}

	public sealed class MinMaxRangeAttribute : Attribute {
		public MinMaxRangeAttribute(float min, float max) {
			this.min = min;
			this.max = max;
		}
		public float min { get; }
		public float max { get; }
	}
}