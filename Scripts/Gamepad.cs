﻿#if !NEW_INPUT
//TODO make work with new input system?
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities {
	public class Gamepad {

		public delegate void AnalogAction(float horizontal, float vertical);

		public enum Stick {
			Left,
			Right,
		}

		public enum Axis {
			None,
			AnalogLeftHorizontal,
			AnalogLeftVertical,
			AnalogRightHorizontal,
			AnalogRightVertical,
			Triggers,
			TriggerLeft,
			TriggerRight,
			ArrowsHorizontal,
			ArrowsVertical,
		}

		public enum Button {
			None,
			StickLeft,
			StickRight,
			A,
			B,
			X,
			Y,
			ShoulderLeft,
			ShoulderRight,
			Start,
			Back,
		}

		public enum ButtonState {
			Pressed,
			Down,
			Up,
		}

		private event AnalogAction _leftStick;
		private event AnalogAction _rightStick;

		private readonly Dictionary<Axis, Action<float>> _axisActions = new Dictionary<Axis, Action<float>>();
		private readonly Dictionary<Button, Action<float>> _buttonActions = new Dictionary<Button, Action<float>>();
		private readonly Dictionary<Button, Action> _buttonDownActions = new Dictionary<Button, Action>();
		private readonly Dictionary<Button, Action> _buttonUpActions = new Dictionary<Button, Action>();

		public void RegisterStickHandler(Stick stick, AnalogAction action) {
			switch (stick) {
				case Stick.Left:
					_leftStick += action;
					break;
				case Stick.Right:
					_rightStick += action;
					break;
			}
		}

		public void UnregisterStickHandler(Stick stick, AnalogAction action) {
			switch (stick) {
				case Stick.Left:
					_leftStick -= action;
					break;
				case Stick.Right:
					_rightStick -= action;
					break;
			}
		}

		public void RegisterAxisHandler(Axis axis, Action<float> action) {
			if (_axisActions.TryGetValue(axis, out Action<float> axisAction)) {
				axisAction += action;
			} else {
				axisAction = action;
			}

			_axisActions[axis] = axisAction;
		}

		public void UnregisterAxisHandler(Axis axis, Action<float> action) {
			if (_axisActions.TryGetValue(axis, out Action<float> axisAction)) {
				axisAction -= action;
				if (axisAction == null) {
					_axisActions.Remove(axis);
				} else {
					_axisActions[axis] = axisAction;
				}
			}
		}

		public void RegisterButtonHandler(Button button, Action<float> action) {
			if (_buttonActions.TryGetValue(button, out Action<float> buttonAction)) {
				buttonAction += action;
			} else {
				buttonAction = action;
			}

			_buttonActions[button] = buttonAction;
		}

		public void UnregisterButtonHandler(Button button, Action<float> action) {
			if (_buttonActions.TryGetValue(button, out Action<float> buttonAction)) {
				buttonAction -= action;
				if (buttonAction == null) {
					_buttonActions.Remove(button);
				} else {
					_buttonActions[button] = buttonAction;
				}
			}
		}

		public void RegisterButtonDownHandler(Button button, Action action) {
			if (_buttonDownActions.TryGetValue(button, out Action buttonAction)) {
				buttonAction += action;
			} else {
				buttonAction = action;
			}

			_buttonDownActions[button] = buttonAction;
		}

		public void UnregisterButtonDownHandler(Button button, Action action) {
			if (_buttonDownActions.TryGetValue(button, out Action buttonAction)) {
				buttonAction -= action;
				if (buttonAction == null) {
					_buttonDownActions.Remove(button);
				} else {
					_buttonDownActions[button] = buttonAction;
				}
			}
		}

		public void RegisterButtonUpHandler(Button button, Action action) {
			if (_buttonUpActions.TryGetValue(button, out Action buttonAction)) {
				buttonAction += action;
			} else {
				buttonAction = action;
			}

			_buttonUpActions[button] = buttonAction;
		}

		public void UnregisterButtonUpHandler(Button button, Action action) {
			if (_buttonUpActions.TryGetValue(button, out Action buttonAction)) {
				buttonAction -= action;
				if (buttonAction == null) {
					_buttonUpActions.Remove(button);
				} else {
					_buttonUpActions[button] = buttonAction;
				}
			}
		}

		public float GetAxis(Axis axis) {
			if (_mapping.GetAxisMapping(axis, out string axisName)) return Input.GetAxis(axisName);
			return 0;
		}

		public bool GetButton(Button button) {
			return _mapping.GetButtonMapping(button, out string buttonName) && Input.GetButton(buttonName);
		}

		public bool GetButtonDown(Button button) {
			return _mapping.GetButtonMapping(button, out string buttonName) && Input.GetButtonDown(buttonName);
		}

		public bool GetButtonUp(Button button) {
			return _mapping.GetButtonMapping(button, out string buttonName) && Input.GetButtonUp(buttonName);
		}

		public readonly struct Mapping {

			public Mapping(params IDefinition[] mappings) {

				buttons = new Dictionary<Gamepad.Button, string>();
				axes = new Dictionary<Gamepad.Axis, string>();

				foreach (IDefinition map in mappings) {
					switch (map) {
						case Axis axis:
							axes[axis.axis] = axis.name;
							break;
						case Button button:
							buttons[button.button] = button.name;
							break;
					}
				}
			}

			public interface IDefinition {
				string name { get; }
			}

			public struct Axis : IDefinition {
				public string name { get; set; }
				public Gamepad.Axis axis;
			}

			public struct Button : IDefinition {
				public string name { get; set; }
				public Gamepad.Button button;
			}

			public readonly Dictionary<Gamepad.Button, string> buttons;
			public readonly Dictionary<Gamepad.Axis, string> axes;

			public bool GetButtonMapping(Gamepad.Button button, out string buttonName) => buttons.TryGetValue(button, out buttonName);
			public bool GetAxisMapping(Gamepad.Axis axis, out string axisName) => axes.TryGetValue(axis, out axisName);
		}

		private Mapping _mapping;

		private static readonly List<Gamepad> _gamepads = new List<Gamepad>();

		public static Gamepad Get(int index) {
			if (index < 0 || index >= _gamepads.Count) return null;
			return _gamepads[index];
		}

		public static Gamepad InitGamepad(Mapping buttonMapping) {

			Gamepad pad = new Gamepad {
				_mapping = buttonMapping
			};

			_gamepads.Add(pad);

			UnityClock.instance.onUpdate += pad.PollInput;

			return pad;
		}

		private void PollInput() {

			if (_mapping.GetAxisMapping(Axis.AnalogLeftHorizontal, out string horizontal) && _mapping.GetAxisMapping(Axis.AnalogLeftVertical, out string vertical))
				_leftStick?.Invoke(Input.GetAxis(horizontal), Input.GetAxis(vertical));

			if (_mapping.GetAxisMapping(Axis.AnalogRightHorizontal, out horizontal) && _mapping.GetAxisMapping(Axis.AnalogRightVertical, out vertical))
				_rightStick?.Invoke(Input.GetAxis(horizontal), Input.GetAxis(vertical));

			foreach ((Axis axis, Action<float> action) in _axisActions) {
				if (_mapping.GetAxisMapping(axis, out string axisName)) action?.Invoke(Input.GetAxis(axisName));
			}

			foreach ((Button button, Action<float> action) in _buttonActions) {
				if (_mapping.GetButtonMapping(button, out string buttonName)) action?.Invoke(Input.GetAxis(buttonName));
			}

			foreach ((Button button, Action action) in _buttonDownActions) {
				if (_mapping.GetButtonMapping(button, out string buttonName) && Input.GetButtonDown(buttonName)) action?.Invoke();
			}

			foreach ((Button button, Action action) in _buttonUpActions) {
				if (_mapping.GetButtonMapping(button, out string buttonName) && Input.GetButtonUp(buttonName)) action?.Invoke();
			}
		}

		public bool hasReceivedInputThisFrame {
			get {
				foreach ((_, string value) in _mapping.buttons) {
					if (Input.GetButton(value)) return true;
				}

				foreach ((_, string value) in _mapping.axes) {
					if (Mathf.Abs(Input.GetAxis(value)) > 0) return true;
				}

				return false;
			}
		}
	}
}
#endif
