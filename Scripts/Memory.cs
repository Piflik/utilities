﻿
// ReSharper disable UnusedMember.Global

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;

public static class MemoryExtensions {

	// ReSharper disable InconsistentNaming
	public static int b(this int number) {
		return number;
	}

	public static long kb(this int number) {
		return number * Utilities.Memory.KILO_BIT;
	}

	public static long Mb(this int number) {
		return number * Utilities.Memory.MEGA_BIT;
	}

	public static long kib(this int number) {
		return number * Utilities.Memory.KIBI_BIT;
	}

	public static long Mib(this int number) {
		return number * Utilities.Memory.MEBI_BIT;
	}

	public static long B(this int number) {
		return number * Utilities.Memory.BYTE;
	}

	public static long kB(this int number) {
		return number * Utilities.Memory.KILO_BYTE;
	}

	public static long MB(this int number) {
		return number * Utilities.Memory.MEGA_BYTE;
	}

	public static long kiB(this int number) {
		return number * Utilities.Memory.KIBI_BYTE;
	}

	public static long MiB(this int number) {
		return number * Utilities.Memory.MEBI_BYTE;
	}
	// ReSharper restore InconsistentNaming
}

namespace Utilities {
	public static class Memory {

		public const long BIT = 1;
		public const long BYTE = 8;
		public const long KILO_BIT = 1000;
		public const long MEGA_BIT = 1000000;
		public const long GIGA_BIT = 1000000000;
		public const long KILO_BYTE = 8000;
		public const long MEGA_BYTE = 8000000;
		public const long GIGA_BYTE = 8000000000;
		public const long KIBI_BIT = 1024;
		public const long MEBI_BIT = 1048576;
		public const long GIBI_BIT = 1073741824;
		public const long KIBI_BYTE = 8192;
		public const long MEBI_BYTE = 8388608;
		public const long GIBI_BYTE = 8589934592;

		public enum Unit : long {
			// ReSharper disable InconsistentNaming
			b = BIT,
			B = BYTE,
			kb = KILO_BIT,
			Mb = MEGA_BIT,
			Gb = GIGA_BIT,
			kB = KILO_BYTE,
			MB = MEGA_BYTE,
			GB = GIGA_BYTE,
			kib = KIBI_BIT,
			Mib = MEBI_BIT,
			Gib = GIBI_BIT,
			kiB = KIBI_BYTE,
			MiB = MEBI_BYTE,
			GiB = GIBI_BYTE,
			// ReSharper restore InconsistentNaming
		}

		public static long Size(long number, long multiplier) {
			return number * multiplier;
		}

		public static long Size(long number, Unit unit) {
			return number * (long)unit;
		}

		/// <summary>
		/// Creates a bitmask containing <see cref="count"/> ones
		/// </summary>
		public static int BitmaskOnes(int count) {
			return (1 << count) - 1;
		}

		public static byte[] ToBytes(this object obj) {
			UnityEngine.Debug.Assert(obj.GetType().IsSerializable, $"Type {obj.GetType()} is not marked Serializable");
			BinaryFormatter bf = new BinaryFormatter();
			using (MemoryStream ms = new MemoryStream()) {
				bf.Serialize(ms, obj);
				return ms.ToArray();
			}
		}

		public static T[] GetStructs<T>(this byte[] bytes) where T : struct {
			int size = Marshal.SizeOf(default(T));
			int len = bytes.Length / size;
			T[] result = new T[len];

			GCHandle handle = GCHandle.Alloc(result, GCHandleType.Pinned);
			try {
				IntPtr pointer = handle.AddrOfPinnedObject();
				Marshal.Copy(bytes, 0, pointer, bytes.Length);
			} finally {
				if (handle.IsAllocated)
					handle.Free();
			}

			return result;
		}

		public static byte[] GetBytes<T>(this T[] structAry, int startIndex = 0, int numElements = 0) where T : struct {
			int len = (numElements > 0 ? numElements : structAry.Length).AtMost(structAry.Length - startIndex);
			int endIndex = (startIndex + len).AtMost(structAry.Length);
			int size = Marshal.SizeOf(default(T));
			byte[] arr = new byte[size * len];

			IntPtr ptr = Marshal.AllocHGlobal(size);
			for (int i = startIndex; i < endIndex; ++i) {
				Marshal.StructureToPtr(structAry[i], ptr, true);
				Marshal.Copy(ptr, arr, i * size, size);
			}
			Marshal.FreeHGlobal(ptr);

			return arr;
		}

		/// <summary>
		/// Set a certain bit to 1 or 0
		/// </summary>
		public static byte SetBit(this byte i, int index, bool enabled) {
			if (index < 0 || index >= 8) return i;
			return (byte)(enabled ? i | (1 << index) : i & ~(1 << index));
		}

		/// <summary>
		/// Test if a bit is 1
		/// </summary>
		public static bool IsBitSet(this byte i, int index) {
			if (index < 0 || index >= 8) return false;
			return (i & (1 << index)) != 0;
		}

		/// <summary>
		/// Set a certain bit to 1 or 0
		/// </summary>
		public static sbyte SetBit(this sbyte i, int index, bool enabled) {
			if (index < 0 || index >= 8) return i;
			sbyte sb = (sbyte)(1 << index);
			return (sbyte)(enabled ? i | sb : i & ~sb);
		}

		/// <summary>
		/// Test if a bit is 1
		/// </summary>
		public static bool IsBitSet(this sbyte i, int index) {
			if (index < 0 || index >= 8) return false;
			return (i & (1 << index)) != 0;
		}

		/// <summary>
		/// Set a certain bit to 1 or 0
		/// </summary>
		public static short SetBit(this short i, int index, bool enabled) {
			if (index < 0 || index >= 16) return i;
			short s = (short)(i << index);
			return (short)(enabled ? i | s : i & ~s);
		}

		/// <summary>
		/// Test if a bit is 1
		/// </summary>
		public static bool IsBitSet(this short i, int index) {
			if (index < 0 || index >= 16) return false;
			return (i & (1 << index)) != 0;
		}

		/// <summary>
		/// Set a certain bit to 1 or 0
		/// </summary>
		public static ushort SetBit(this ushort i, int index, bool enabled) {
			if (index < 0 || index >= 16) return i;
			return (ushort)(enabled ? i | (1 << index) : i & ~(1 << index));
		}

		/// <summary>
		/// Test if a bit is 1
		/// </summary>
		public static bool IsBitSet(this ushort i, int index) {
			if (index < 0 || index >= 16) return false;
			return (i & (1 << index)) != 0;
		}

		/// <summary>
		/// Set a certain bit to 1 or 0
		/// </summary>
		public static int SetBit(this int i, int index, bool enabled) {
			if (index < 0 || index >= 32) return i;
			return enabled ? i | (1 << index) : i & ~(1 << index);
		}

		/// <summary>
		/// Test if a bit is 1
		/// </summary>
		public static bool IsBitSet(this int i, int index) {
			if (index < 0 || index >= 32) return false;
			return (i & (1 << index)) != 0;
		}

		/// <summary>
		/// Set a certain bit to 1 or 0
		/// </summary>
		public static uint SetBit(this uint i, int index, bool enabled) {
			if (index < 0 || index >= 32) return i;
			return enabled ? i | (1u << index) : i & ~(1u << index);
		}

		/// <summary>
		/// Test if a bit is 1
		/// </summary>
		public static bool IsBitSet(this uint i, int index) {
			if (index < 0 || index >= 32) return false;
			return (i & (1 << index)) != 0;
		}

		/// <summary>
		/// Set a certain bit to 1 or 0
		/// </summary>
		public static long SetBit(this long i, int index, bool enabled) {
			if (index < 0 || index >= 64) return i;
			return enabled ? i | (1L << index) : i & ~(1L << index);
		}

		/// <summary>
		/// Test if a bit is 1
		/// </summary>
		public static bool IsBitSet(this long i, int index) {
			if (index < 0 || index >= 64) return false;
			return (i & (1 << index)) != 0;
		}

		/// <summary>
		/// Set a certain bit to 1 or 0
		/// </summary>
		public static ulong SetBit(this ulong i, int index, bool enabled) {
			if (index < 0 || index >= 64) return i;
			return enabled ? i | (1UL << index) : i & ~(1UL << index);
		}

		/// <summary>
		/// Test if a bit is 1
		/// </summary>
		public static bool IsBitSet(this ulong i, int index) {
			if (index < 0 || index >= 64) return false;
			return (i & (1UL << index)) != 0;
		}

		public static int SetBits(this int i, int value, int mask) {
			i &= ~mask;
			i |= value;
			return i;
		}
	}
}