﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public abstract class SerializableDictionaryBase { } // used to be able to add the property drawer to all serialized dictionaries automatically

	public class SerializableDictionary<TKey, TValue> : SerializableDictionaryBase, IEnumerable<KeyValuePair<TKey, TValue>>, ISerializationCallbackReceiver {

		[NonSerialized] private readonly Dictionary<TKey, TValue> _dictionary = new Dictionary<TKey, TValue>();

		[SerializeField] private TKey[] _keys = default;
		[SerializeField] private TValue[] _values = default;

		public SerializableDictionary() { }

		public SerializableDictionary(IDictionary<TKey, TValue> dict) {
			_dictionary = new Dictionary<TKey, TValue>(dict.Count);
			CopyFrom(dict);
		}

		public TValue this[TKey index] {
			get => _dictionary[index];
			set => _dictionary[index] = value;
		}

		public static implicit operator Dictionary<TKey, TValue>(SerializableDictionary<TKey, TValue> serializableDictionary) {
			return serializableDictionary._dictionary;
		}

		public Dictionary<TKey, TValue>.KeyCollection keys => _dictionary.Keys;
		public Dictionary<TKey, TValue>.ValueCollection values => _dictionary.Values;

		public int count => _dictionary.Count;

		public void Add(TKey key, TValue value) {
			_dictionary.Add(key, value);
		}

		public bool Remove(TKey key) {
			return _dictionary.Remove(key);
		}

		public void Clear() {
			_dictionary.Clear();
		}

		public bool ContainsKey(TKey key) {
			return _dictionary.ContainsKey(key);
		}

		public bool ContainsValue(TValue value) {
			return _dictionary.ContainsValue(value);
		}

		public bool TryGetValue(TKey key, out TValue value) {
			return _dictionary.TryGetValue(key, out value);
		}

		private void CopyFrom(IDictionary<TKey, TValue> dict) {
			_dictionary.Clear();
			foreach ((TKey key, TValue value) in dict) {
				this[key] = value;
			}
		}

		public void OnAfterDeserialize() {
			if (_keys != null && _values != null && _keys.Length == _values.Length) {
				_dictionary.Clear();
				int n = _keys.Length;
				for (int i = 0; i < n; ++i) {
					this[_keys[i]] = _values[i];
				}
				_keys = null;
				_values = null;
			}

		}

		public void OnBeforeSerialize() {
			int n = _dictionary.Count;
			if (_keys == null || _keys.Length != n) {
				_keys = new TKey[n];
				_values = new TValue[n];
			}

			int i = 0;
			foreach ((TKey key, TValue value) in this) {
				_keys[i] = key;
				_values[i] = value;
				i++;
			}
		}

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
			return _dictionary.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
	}
}