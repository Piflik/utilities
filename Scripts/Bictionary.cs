﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Utilities {
	public class Bictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>> where TKey : IEquatable<TKey> where TValue : IEquatable<TValue> {
		public Indexer<TKey, TValue> forward { get; } = new Indexer<TKey, TValue>();
		public Indexer<TValue, TKey> reverse { get; } = new Indexer<TValue, TKey>();

		public Bictionary() { }

		public Bictionary(IDictionary<TKey, TValue> forward) {
			this.forward = new Indexer<TKey, TValue>(forward);
			reverse = new Indexer<TValue, TKey>(forward.ToDictionary(kv => kv.Value, kv => kv.Key));
		}

		public void Add(TKey key, TValue value) {
			if (forward.ContainsKey(key)) {
				throw new ArgumentException($"An item with the same key has already been added: {key}");
			}
			if (reverse.ContainsKey(value)) {
				throw new ArgumentException($"An item with the same value has already been added: {value}");
			}

			forward.Add(key, value);
			reverse.Add(value, key);
		}

		public int count => forward.count;

		public bool Remove(TKey key) {
			if (!forward.TryGetValue(key, out TValue value)) return false;
			forward.Remove(key);
			reverse.Remove(value);
			return true;
		}

		public bool RemoveValue(TValue value) {
			if (!reverse.TryGetValue(value, out TKey key)) return false;
			forward.Remove(key);
			reverse.Remove(value);
			return true;
		}

		public bool TryGetKey(TKey key, out TValue value) => forward.TryGetValue(key, out value);
		public bool TryGetValue(TValue value, out TKey left) => reverse.TryGetValue(value, out left);

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => forward.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => forward.GetEnumerator();


		public class Indexer<TIndexerKey, TIndexerValue> : IEnumerable<KeyValuePair<TIndexerKey, TIndexerValue>> {
			private readonly IDictionary<TIndexerKey, TIndexerValue> _dictionary;

			public Indexer() => _dictionary = new Dictionary<TIndexerKey, TIndexerValue>();
			public Indexer(IDictionary<TIndexerKey, TIndexerValue> dictionary) => _dictionary = dictionary;

			public TIndexerValue this[TIndexerKey index] => _dictionary[index];

			public static implicit operator Dictionary<TIndexerKey, TIndexerValue>(Indexer<TIndexerKey, TIndexerValue> indexer) => new Dictionary<TIndexerKey, TIndexerValue>(indexer._dictionary);

			internal void Add(TIndexerKey key, TIndexerValue value) => _dictionary.Add(key, value);
			internal bool Remove(TIndexerKey key) => _dictionary.Remove(key);
			internal int count => _dictionary.Count;

			public bool ContainsKey(TIndexerKey key) => _dictionary.ContainsKey(key);
			public bool TryGetValue(TIndexerKey key, out TIndexerValue value) => _dictionary.TryGetValue(key, out value);

			public IEnumerable<TIndexerKey> keys => _dictionary.Keys;
			public IEnumerable<TIndexerValue> values => _dictionary.Values;
			public Dictionary<TIndexerKey, TIndexerValue> ToDictionary() => new Dictionary<TIndexerKey, TIndexerValue>(_dictionary);

			public IEnumerator<KeyValuePair<TIndexerKey, TIndexerValue>> GetEnumerator() => _dictionary.GetEnumerator();
			IEnumerator IEnumerable.GetEnumerator() => _dictionary.GetEnumerator();
		}
	}
}
