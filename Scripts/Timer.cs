﻿using System;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public class Timer {

		public struct Notification {
			public readonly float remainingSeconds, remainingNormalized;

			public Notification(float remainingSeconds, float remainingNormalized) {
				this.remainingSeconds = remainingSeconds;
				this.remainingNormalized = remainingNormalized;
			}
		}

		private float _duration, _remaining;
		private Action _onFinish;
		private Action<Notification> _onUpdate;

		public bool running { get; private set; }

		public static Timer StartNew(float duration, Action<Notification> onUpdate = null, Action onFinish = null) {
			return new Timer(duration, onUpdate, onFinish);
		}

		public Timer(float duration, Action<Notification> onUpdate = null, Action onFinish = null, bool autoStart = true) {
			if (autoStart) {
				Start(duration, onUpdate, onFinish);
			} else {
				_onUpdate = onUpdate;
				_onFinish = onFinish;
				_duration = duration;
			}
		}

		public void Start(float duration, Action<Notification> onUpdate, Action onFinish = null) {
			_onUpdate = onUpdate;
			_onFinish = onFinish;
			Start(duration);
		}

		public void Start(float duration) {
			_duration = duration.Approximates(0) ? 0 : duration;
			Start();
		}

		public void Start() {
			if (_duration >= 0) {
				running = true;
				_remaining = _duration;
				UnityClock.instance.onUpdate -= Update;
				UnityClock.instance.onUpdate += Update;
			}
		}

		public void Cancel(bool clearEvents = false) {
			running = false;
			UnityClock.instance.onUpdate -= Update;
			
			if (clearEvents) {
				_onUpdate = null;
				_onFinish = null;
			}
		}

		private void Update() {
			_remaining = Mathf.Max(_remaining - Time.deltaTime, 0);

			_onUpdate?.Invoke(new Notification(_remaining, _duration.Approximates(0) ? 0 : _remaining / _duration));
			if (_remaining.Approximates(0)) {
				UnityClock.instance.onUpdate -= Update;
				running = false;
				_remaining = 0;
				_onFinish?.Invoke();
			}
		}
	}
}
