﻿using System;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static partial class FloatUtilities {
		/// <summary>
		/// Map value when it lies between valMin and valMax to a range outMin .. outMax
		/// Example: angle 40° .. 200° should be mapped to 0..1 
		/// value = angle, valMin = 40, valMax=200, outMin=0, outMax=1
		/// </summary>
		public static float ValueMapper(float value, float valMin, float valMax, float outMin, float outMax) {
			float r = Mathf.Clamp01((value - valMin) / (valMax - valMin));
			return r * (outMax - outMin) + outMin;
		}

		public static float ValueMapper(float value, float valMin, float valMax, float outMin, float outMax, EaseType ease) {
			return Utils.CalcLerp(valMax - valMin, value - valMin, ease, outMin, outMax);
		}

		/// <summary>
		/// Map-method for double
		/// </summary>
		public static double ValueMapper(double value, double valMin, double valMax, double outMin, double outMax, bool clamp = true) {
			double r = (value - valMin) / (valMax - valMin) * (outMax - outMin) + outMin;
			if (clamp) {
				if (r > valMax) {
					return valMax;
				}

				if (r < valMin) {
					return valMin;
				}
			}

			return r;
		}

		/// <summary>
		/// Map value when it lies between valMin and valMax to a range outMin .. outMax
		/// </summary>
		public static int ValueMapperToInt(float value, float valMin, float valMax, int outMin, int outMax) {
			return (int)Math.Round(ValueMapper(value, valMin, valMax, outMin, outMax), MidpointRounding.AwayFromZero);
		}

		/// <summary>
		/// Map value when it lies between valMin and valMax to a range outMin .. outMax with a Smoothstep
		/// </summary>
		public static float ValueMapperSmoothed(float value, float vMin, float vMax, float oMin, float oMax) {
			float r = Smoothstep((value - vMin) / (vMax - vMin));
			return r * (oMax - oMin) + oMin;
		}

		/// <summary>
		/// Smoothstep implementations, see wikipedia
		/// </summary>
		public static float Smoothstep(float f) {
			f = Mathf.Clamp01(f);
			return f * f * (3 - 2 * f);
		}

		/// <summary>
		/// Sigmoid implementation, see wikipedia
		/// </summary>
		public static float Sigmoid(float x, float min, float max, float exponent, float q = 1, float nu = 1) {
			float r = max - min;
			q = Mathf.Pow(1 + q * Mathf.Exp(-exponent * x), 1 / nu);
			return min + r / q;
		}

		/// <summary>
		/// Step function
		/// </summary>
		/// <param name="threshold">Threshold</param>
		/// <param name="value">Value to test</param>
		/// <returns>0 if value is below threshold, 1 otherwise</returns>
		public static float Step(float threshold, float value) {
			return value < threshold ? 0 : 1;
		}

		/// <summary>
		/// Evaluates a Gaussian curve at x
		/// </summary>
		/// <param name="x"></param>
		/// <param name="a">Height of the Gaussian</param>
		/// <param name="b">Position of the maximum</param>
		/// <param name="c">Standard deviation</param>
		/// <returns></returns>
		public static float Gaussian(float x, float a, float b, float c) {
			return a * Mathf.Exp(-Mathf.Pow(x - b, 2) / (2 * c * c));
		}

		/// <summary>
		/// True/False chance
		/// </summary>
		public static bool Random(float chance) {
			return UnityEngine.Random.value < chance;
		}


		public static float Tanh(float f) {
			return 1 - 2 / (Mathf.Exp(2 * f) + 1);
		}

		public static float Coth(float f) {
			return 1 + 2 / (Mathf.Exp(2 * f) - 1);
		}

		public static float Cubic(float x, float a, float b, float c, float d) => a + x * (b + x * (c + x * d));
		public static float CubicDerivative(float x, float b, float c, float d) => b + x * (2 * c + x * (3 * d));

	}
}
