﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

// ReSharper disable UnusedMember.Global

namespace Utilities {

	public interface IInitable {
		void Init();
	}

	public static class Utils {
		public const float CM_TO_INCH = 0.393700787f;
		public const float INCH_TO_CM = 2.54f;
		public const float KMH_TO_MPH = 0.621371192f;
		public const float MPH_TO_KMH = 1.609344f;
		public const float BAR_TO_PSI = 14.50377f;
		public const float PSI_TO_BAR = 0.06894757f;
		public const float METER_2_MILES = 0.000621371192f;
		public const float MILES_TO_METERS = 1609.344f;
		public const float MILES_TO_YARDS = 1760f;
		public const float MILES_TO_FEET = 5280f;
		public const float L_PER_100_KM_TO_MILES_PER_GALLON = 235.2145833f;
		public const float C_TO_F_OFFSET = 32f;
		public const float C_TO_F_MULTIPLIER = 1.8f;
		public const float F_TO_C_MULTIPLIER = 0.556f;
		public const int RENDER_QUEUE_BACKGROUND = 1000;        // unity internal render queues (https://docs.unity3d.com/Manual/SL-SubShaderTags.html)
		public const int RENDER_QUEUE_GEOMETRY = 2000;
		public const int RENDER_QUEUE_ALPHATEST = 2450;
		public const int RENDER_QUEUE_TRANSPARENT = 3000;
		public const int RENDER_QUEUE_OVERLAY = 4000;

		public static int internalDPI = 142;

		/// <summary>
		/// Add an empty gameObject as child to a parent
		/// </summary>
		public static GameObject AddChild(GameObject parent, string name = "") {
			return AddChild(parent, new GameObject(), name);
		}

		/// <summary>
		/// Add a gameObject as child to a parent
		/// </summary>
		public static GameObject AddChild(GameObject parent, GameObject go, string name = "") {
			Transform t = go.transform;
			if (parent != null) {
				t.SetParent(parent.transform, false);
				go.layer = parent.layer;
			}
			t.localPosition = Vector3.zero;
			t.localRotation = Quaternion.identity;
			t.localScale = Vector3.one;

			if (!string.IsNullOrEmpty(name)) go.name = name;

			return go;
		}

		/// <summary>
		/// Add a new gameObject to a parent
		/// </summary>
		/// <typeparam name="T">Component to add</typeparam>
		/// <param name="init">if true and T is IInitable, call <see cref="IInitable.Init"/></param>
		/// <returns>The newly added T Component</returns>
		public static T AddChild<T>(GameObject parent, string name = "", int layer = -1, bool init = true) where T : Component {
			return AddChild<T>(parent, new GameObject(), name, layer, init);
		}

		/// <summary>
		/// Add an existing gameObject to a parent
		/// </summary>
		/// <typeparam name="T">required component</typeparam>
		/// <param name="init">if true and T is IInitable, call <see cref="IInitable.Init"/></param>
		/// <returns>The newly added or already existing T Component</returns>
		public static T AddChild<T>(GameObject parent, GameObject go, string name = "", int layer = -1, bool init = true) where T : Component {

			go = AddChild(parent, go, name);

			if (string.IsNullOrEmpty(name)) go.name = typeof(T).Name;
			if (layer >= 0 && layer <= 31) go.layer = layer;

			T output = go.GetOrAddComponent<T>();
			if (init && output is IInitable initable) {
				initable.Init();
			}
			return output;
		}

		/// <summary>
		/// Returns true if 't' is a child of 'parent' (or any descendant if recursive)
		/// </summary>
		public static bool IsChildOf(Transform parent, Transform t, bool recursive = true) {
			if (parent == null || t == null) return false;

			// check if t is child of parent
			if (t.IsChildOf(parent)) {
				return true;
			}

			if (recursive) {
				foreach (Transform child in parent) {
					if (IsChildOf(child, t, true)) return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Get the Canvas on the closest ancestor of a transform (including the transform itself)
		/// </summary>
		public static Canvas GetRootCanvas(Transform t) {
			return t.GetCanvas();
		}

		/// <summary>
		/// Position an object within a rectangle
		/// </summary>
		public static void AlignInBounds(Alignment alignment, int rectWidth, int rectHeight, int centerX, int centerY, out int x, out int y) {
			float tX;
			float tY;
			switch (alignment) {
				case Alignment.Left:
				case Alignment.BottomLeft:
				case Alignment.TopLeft:
					tX = -rectWidth / 2 + centerX;
					break;
				case Alignment.Right:
				case Alignment.BottomRight:
				case Alignment.TopRight:
					tX = rectWidth / 2 - centerX;
					break;
				default:
					tX = centerX;
					break;
			}
			switch (alignment) {
				case Alignment.Top:
				case Alignment.TopLeft:
				case Alignment.TopRight:
					tY = rectHeight / 2 - centerY;
					break;
				case Alignment.Bottom:
				case Alignment.BottomRight:
				case Alignment.BottomLeft:
					tY = -rectHeight / 2 + centerY;
					break;
				default:
					tY = centerY;
					break;
			}
			x = (int)tX;
			y = (int)tY;
		}

		public static Vector2 PivotTypeToPivot(Alignment pPivotType) {
			switch (pPivotType) {
				case Alignment.BottomLeft:
					return new Vector2(0f, 0f);
				case Alignment.Bottom:
					return new Vector2(.5f, 0f);
				case Alignment.BottomRight:
					return new Vector2(1f, 0f);
				case Alignment.Left:
					return new Vector2(0f, .5f);
				case Alignment.Center:
					return new Vector2(.5f, .5f);
				case Alignment.Right:
					return new Vector2(1f, .5f);
				case Alignment.TopLeft:
					return new Vector2(0f, 1f);
				case Alignment.Top:
					return new Vector2(.5f, 1f);
				case Alignment.TopRight:
					return new Vector2(1f, 1f);
			}

			return new Vector2(0, 0);
		}


		/// <summary>
		/// Convert Pivot Alignment to Vector2
		/// </summary>
		public static Vector2 ToVec2(this Alignment pivot) {
			return PivotTypeToPivot(pivot);
		}

		public static Scene[] GetOpenScenes() {
			Scene[] scenes = new Scene[SceneManager.sceneCount];

			for (int i = 0; i < SceneManager.sceneCount; ++i) {
				scenes[i] = SceneManager.GetSceneAt(i);
			}

			return scenes;
		}

		/// <summary>
		/// Create a list of all ancestors of a transform until the next canvas or the root object
		/// </summary>
		public static int[] GetSiblingTree(Transform t, bool tillNextCanvas = true) {
			Stack<int> depthArray = new Stack<int>();

			while (t != null) {
				//store sibling index
				depthArray.Push(t.GetSiblingIndex());

				//stop if canvas was found
				if (tillNextCanvas) {
					Canvas c = t.GetComponent<Canvas>();
					if (c != null) break;
				}

				//continue with parent transform
				t = t.parent;
			}

			return depthArray.ToArray();
		}

		public static int CompareDepthTrees(int[] a, int[] b) {
			//example depth trees
			//A [1,0,0]
			//B [1,0,1]
			//C [1,0,1,1,0,1,0]
			//---> compare entries, return higher one as soon as entries dont match
			//---> A<>B  >> B wins as third entry is higher
			//---> B<>C  >> C wins as max entries are equal and C is longer

			//-1: a wins    1: b wins

			int result = 0;
			int maxLength = Mathf.Max(a.Length, b.Length);
			int i = 0;
			while (i < maxLength) {
				if (i >= a.Length) {
					//if entries were equal until now, and A has no more entries ... return B
					result = 1;
					break;
				}

				if (i >= b.Length) {
					result = -1;
					break;
					//
				}

				int diff = b[i].CompareTo(a[i]); //-1 if B[i] is smaller, 1 if A[i] is smaller
				if (diff != 0) {
					//if B[i] is smaller value, return A (-1)
					result = diff;
					break;
				}

				i++;
			}

			return result;
		}

		public class DepthTreeComparer : IComparer<int[]> {
			public int Compare(int[] a, int[] b) {
				if (a == null) return b == null ? 0 : 1;
				if (b == null) return -1;
				return CompareDepthTrees(a, b);
			}
		}

		/// <summary>
		/// Convert EaseType to String
		/// </summary>
		public static string MapEaseToString(EaseType easeType) {
			string strEase = "linear";
			switch (easeType) {
				case EaseType.SineIn:
					strEase = "sineIn";
					break;
				case EaseType.SineInOut:
					strEase = "sineInOut";
					break;
				case EaseType.SineOut:
					strEase = "sineOut";
					break;
				case EaseType.CubicIn:
					strEase = "cubicIn";
					break;
				case EaseType.CubicInOut:
					strEase = "cubicInOut";
					break;
				case EaseType.CubicOut:
					strEase = "cubicOut";
					break;
				case EaseType.QuadIn:
					strEase = "quadIn";
					break;
				case EaseType.QuadInOut:
					strEase = "quadInOut";
					break;
				case EaseType.QuadOut:
					strEase = "quadOut";
					break;
				case EaseType.QuintIn:
					strEase = "quintIn";
					break;
				case EaseType.QuintOut:
					strEase = "quintOut";
					break;
				case EaseType.QuintInOut:
					strEase = "quintInOut";
					break;
				case EaseType.ElasticIn:
					strEase = "elasticIn";
					break;
				case EaseType.ElasticOut:
					strEase = "elasticOut";
					break;
				case EaseType.ElasticInOut:
					strEase = "elasticInOut";
					break;
				case EaseType.OvershootIn:
					strEase = "overshootIn";
					break;
				case EaseType.OvershootOut:
					strEase = "overshootOut";
					break;
				case EaseType.OvershootInOut:
					strEase = "overshootInOut";
					break;
			}
			return strEase;
		}

		/// <summary>
		/// Interpolate between two Colors according to an EaseType
		/// </summary>
		public static Color TweenByLerp(Color valueStart, Color valueEnd, float tweenDur, float tweenTime, EaseType easeType) {
			float lrp = CalcLerp(tweenDur, tweenTime, easeType);
			Color value = Color.LerpUnclamped(valueStart, valueEnd, lrp);
			return value;
		}

		/// <summary>
		/// Interpolate between two Vector4s according to an EaseType
		/// </summary>
		public static Vector4 TweenByLerp(Vector4 valueStart, Vector4 valueEnd, float tweenDur, float tweenTime, EaseType easeType) {
			float lrp = CalcLerp(tweenDur, tweenTime, easeType);
			Vector4 value = Vector4.LerpUnclamped(valueStart, valueEnd, lrp);
			return value;
		}

		/// <summary>
		/// Interpolate between two Vector3s according to an EaseType
		/// </summary>
		public static Vector3 TweenByLerp(Vector3 valueStart, Vector3 valueEnd, float tweenDur, float tweenTime, EaseType easeType) {
			float lrp = CalcLerp(tweenDur, tweenTime, easeType);
			Vector3 value = Vector3.LerpUnclamped(valueStart, valueEnd, lrp);
			return value;
		}

		/// <summary>
		/// Interpolate between two Vector2s according to an EaseType
		/// </summary>
		public static Vector2 TweenByLerp(Vector2 valueStart, Vector2 valueEnd, float tweenDur, float tweenTime, EaseType easeType) {
			float lrp = CalcLerp(tweenDur, tweenTime, easeType);
			Vector2 value = Vector2.LerpUnclamped(valueStart, valueEnd, lrp);
			return value;
		}

		/// <summary>
		/// Interpolate between two floats according to an EaseType
		/// </summary>
		public static float TweenByLerp(float valueStart, float valueEnd, float tweenDur, float tweenTime, EaseType easeType) {
			float lrp = CalcLerp(tweenDur, tweenTime, easeType);
			float value = Mathf.LerpUnclamped(valueStart, valueEnd, lrp);
			return value;
		}


		/// <summary>
		/// Interpolate between two Quaternion according to an EaseType
		/// </summary>
		public static Quaternion TweenByLerp(Quaternion valueStart, Quaternion valueEnd, float tweenDur, float tweenTime, EaseType easeType) {
			float lrp = CalcLerp(tweenDur, tweenTime, easeType);
			Quaternion value = Quaternion.LerpUnclamped(valueStart, valueEnd, lrp);
			return value;
		}

		/// <summary>
		/// Calculate an interpolation according to an EaseType
		/// </summary>
		public static float CalcLerp(float total, float share, EaseType easeType, float valStart = 0f, float valEnd = 1f) {
			// t = current percentage 0..1  // share/total (time)
			float t = Mathf.Clamp01(total > 0f ? share / total : 1f);
			float lrp = t;                  // percentage
			float b = valStart;             // beginning value
			float c = valEnd - valStart;    // delta between beginning...end    

			switch (easeType) {
				//LINEAR
				case EaseType.Linear:
					lrp = c * t + b;

					break;
				//SINE_IN
				case EaseType.SineIn:
					lrp = -c * Mathf.Cos(t * Mathf.PI * .5f) + c + b;

					break;
					//SINE_OUT
				case EaseType.SineOut:
					lrp = c * Mathf.Sin(t * Mathf.PI * .5f) + b;

					break;
					//SINE_IN_OUT
				case EaseType.SineInOut:
					lrp = -c / 2 * (Mathf.Cos(Mathf.PI * t) - 1) + b;

					break;
					//QUAD_IN   accelerating from zero velocity   12
				case EaseType.QuadIn:
					lrp = c * t * t + b;

					break;
					//QUAD_OUT  decelerating to zero velocity
				case EaseType.QuadOut:
					lrp = -c * t * (t - 2) + b;

					break;
					//QUAD_IN_OUT   acceleraing until halfway, then deceleration
				case EaseType.QuadInOut:
					t *= 2f;
					if (t < 1f) {
						lrp = c * .5f * t * t + b;
					} else {
						t -= 1f;
						lrp = -c * .5f * (t * (t - 2f) - 1) + b;
					}

					break;
				//CUBIC_IN
				case EaseType.CubicIn:
					lrp = c * t * t * t + b;

					break;
				//CUBIC_OUT
				case EaseType.CubicOut:
					t -= 1f;
					lrp = c * (t * t * t + 1) + b;

					break;
				//CUBIC_IN_OUT
				case EaseType.CubicInOut:
					t *= 2f;
					if (t < 1f) {
						lrp = c * .5f * t * t * t + b;
					} else {
						t -= 2f;
						lrp = c * .5f * (t * t * t + 2f) + b;
					}

					break;
				//QUINT_IN
				case EaseType.QuartIn:
					lrp = c * t * t * t * t + b;

					break;
					//QUINT_OUT
				case EaseType.QuartOut:
					t -= 1f;
					lrp = c * (1 - t * t * t * t) + b;

					break;
					//QUINT_IN_OUT
				case EaseType.QuartInOut:
					t *= 2f;
					if (t < 1f) {
						lrp = c * .5f * t * t * t * t + b;
					} else {
						t -= 2f;
						lrp = -c * .5f * (t * t * t * t - 2f) + b;
					}

					break;
				//QUINT_IN
				case EaseType.QuintIn:
					lrp = c * t * t * t * t * t + b;

					break;
					//QUINT_OUT
				case EaseType.QuintOut:
					t -= 1f;
					lrp = c * (t * t * t * t * t + 1f) + b;

					break;
					//QUINT_IN_OUT
				case EaseType.QuintInOut:
					t *= 2f;
					if (t < 1f) {
						lrp = c * .5f * t * t * t * t * t + b;
					} else {
						t -= 2f;
						lrp = c * .5f * (t * t * t * t * t + 2f) + b;
					}

					break;

				case EaseType.ElasticIn:
					if (t <= 0) {
						lrp = b;
					} else if (t >= 1) {
						lrp = c;
					} else {
						lrp = c * (-Mathf.Pow(2, 10 * t - 10) * Mathf.Sin((t * 10 - 10.75f) * (2 * Mathf.PI) / 3)) + b;
					}
					break;
				case EaseType.ElasticOut:
					if (t <= 0) {
						lrp = b;
					} else if (t >= 1) {
						lrp = c;
					} else {
						lrp = c * (Mathf.Pow(2, -10 * t) * Mathf.Sin((t * 10 - 0.75f) * (2 * Mathf.PI) / 3) + 1) + b;
					}
					break;
				case EaseType.ElasticInOut:
					if (t <= 0) {
						lrp = b;
					} else if (t >= 1) {
						lrp = c;
					} else if (t < 0.5f) {
						lrp = c * (-(Mathf.Pow(2, 20 * t - 10) * Mathf.Sin((20 * t - 11.125f) * (2 * Mathf.PI) / 4.5f)) * 0.5f) + b;
					} else {
						lrp = c * (Mathf.Pow(2, -20 * t + 10) * Mathf.Sin((20 * t - 11.125f) * (2 * Mathf.PI) / 4.5f) * 0.5f + 1) + b;

					}
					break;
				case EaseType.OvershootIn: {
					const float c1 = 1.70158f;
					const float c3 = c1 + 1;

					lrp = c * (c3 * t * t * t - c1 * t * t) + b;
					break;
				}
				case EaseType.OvershootOut: {
					const float c1 = 1.70158f;
					const float c3 = c1 + 1;

					lrp = c * (1 + c3 * Mathf.Pow(t - 1, 3) + c1 * Mathf.Pow(t - 1, 2)) + b;
					break;
				}
				case EaseType.OvershootInOut: {
					const float c1 = 1.70158f;
					const float c2 = c1 * 1.525f;

					lrp = t < 0.5
						? c * (Mathf.Pow(2 * t, 2) * ((c2 + 1) * 2 * t - c2) * 0.5f) + b
						: c * ((Mathf.Pow(2 * t - 2, 2) * ((c2 + 1) * (t * 2 - 2) + c2) + 2) * 0.5f) + b;
					break;
				}
			}

			return lrp;
		}

		/// <summary>
		/// Change the layer of a Transform
		/// </summary>
		public static void SetLayer(Transform root, int layer, bool recursive = true) {
			//set cam layer for root and optional for its children
			root.gameObject.layer = layer;

			if (recursive) {
				foreach (Transform child in root) {
					SetLayer(child, layer);
				}
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public static Rect CalcUVRectangle(int orgW, int orgH, AdustUV adjust = AdustUV.Auto, float targW = 1, float targH = 1) {
			float w = 1;
			float h = 1;
			float x = 0;
			float y = 0;

			//calc factors for each side
			float fW = targW / orgW;
			float fH = targH / orgH;
			bool scaleW = false;
			bool scaleH = false;

			switch (adjust) {
				//DEFINE if width or height should be adjusted
				case AdustUV.TargetSize when fW > fH:
					scaleH = true;
					break;
				case AdustUV.TargetSize:
					scaleW = true;
					break;
				case AdustUV.Height:
					scaleH = true;
					break;
				case AdustUV.Width:
					scaleW = true;
					break;
			}

			//SCALE TO TARGET SIZE
			if (scaleH) {
				h = targH / (orgH * fW);
				h = h < 1 ? h : 1 / h;
				y = (1f - h) * .5f;
			}
			if (scaleW) {
				w = targW / (orgW * fH);
				x = (1f - w) * .5f;
			}

			return new Rect(x, y, w, h);

		}

		public static (Vector3 pos, Vector2 size) GetRect(Vector3 position, Vector3 boundingBox, Quaternion rotation, Camera projectionCam) {
			Vector3[] corners = {
				position + rotation * new Vector3(-boundingBox.x * 0.5f, -boundingBox.y * 0.5f, -boundingBox.z * 0.5f),
				position + rotation * new Vector3(boundingBox.x * 0.5f, -boundingBox.y * 0.5f, -boundingBox.z * 0.5f),
				position + rotation * new Vector3(-boundingBox.x * 0.5f, boundingBox.y * 0.5f, -boundingBox.z * 0.5f),
				position + rotation * new Vector3(boundingBox.x * 0.5f, boundingBox.y * 0.5f, -boundingBox.z * 0.5f),
				position + rotation * new Vector3(-boundingBox.x * 0.5f, -boundingBox.y * 0.5f, boundingBox.z * 0.5f),
				position + rotation * new Vector3(boundingBox.x * 0.5f, -boundingBox.y * 0.5f, boundingBox.z * 0.5f),
				position + rotation * new Vector3(-boundingBox.x * 0.5f, boundingBox.y * 0.5f, boundingBox.z * 0.5f),
				position + rotation * new Vector3(boundingBox.x * 0.5f, boundingBox.y * 0.5f, boundingBox.z * 0.5f),
			};

			float minX = float.PositiveInfinity;
			float minY = float.PositiveInfinity;
			float maxX = float.NegativeInfinity;
			float maxY = float.NegativeInfinity;

			float d = projectionCam.WorldToScreenPoint(position).z;

			foreach (Vector3 corner in corners) {
				Vector3 projectedVector = projectionCam.WorldToScreenPoint(corner);

				minX = Mathf.Min(projectedVector.x, minX);
				minY = Mathf.Min(projectedVector.y, minY);
				maxX = Mathf.Max(projectedVector.x, maxX);
				maxY = Mathf.Max(projectedVector.y, maxY);
			}

			Vector3 min = projectionCam.ScreenToWorldPoint(new Vector3(minX, minY, d));
			Vector3 max = projectionCam.ScreenToWorldPoint(new Vector3(maxX, maxY, d));

			Vector3 size = projectionCam.transform.InverseTransformVector(max - min);

			return ((min + max) * 0.5f, size.XY());
		}

		/// <summary>
		/// Convert length units CM, INCH, DPI, PX
		/// </summary>
		/// <param name="from">src unit</param>
		/// <param name="to">target unit</param>
		/// <param name="value">src unit value</param>
		/// <param name="dpi">Only relevant for PX conversions. if -1, use internal simulation DPI-.</param>
		/// <returns></returns>
		public static float ConvertUnit(UnitLength from, UnitLength to, float value, float dpi = -1) {
			float result = -1;
			dpi = dpi <= 0 ? internalDPI : dpi;        //use internal dpi if parameter isn't set
			if (from == UnitLength.Cm && to == UnitLength.Inch) {
				result = value * CM_TO_INCH;

			} else if (from == UnitLength.Inch && to == UnitLength.Cm) {
				result = value * INCH_TO_CM;

			} else if (from == UnitLength.Cm && to == UnitLength.Px) {
				result = ConvertUnit(UnitLength.Cm, UnitLength.Inch, value) * dpi;

			} else if (from == UnitLength.Inch && to == UnitLength.Px) {
				result = value * dpi;

			} else if (from == UnitLength.Px && to == UnitLength.Cm) {
				result = ConvertUnit(UnitLength.Inch, UnitLength.Cm, value / dpi);

			} else if (from == UnitLength.Px && to == UnitLength.Inch) {
				result = value / dpi;

			}

			return result;
		}

		/// <summary>
		/// Get root path to simulation exe
		/// </summary>
		/// <returns></returns>
		public static string GetRootPath() {
#if UNITY_ANDROID && !UNITY_EDITOR
			// ANDROID: !use .NET 2.0!
			// internal writeaccess: Application.persistentDataPath ---> /data/data/de.Usaneers.ProjectName/files/
			// extenral writeaccess: Application.persistenDataPath  ---> /storage/emulated/0/Android/data/de.Usaneers.ProjectName/files
			// BuildSettings: Write Permission = External!
			// store files via explorer in a folder called 'bundelidentifier' that lies in the root document directory: 
			// e.g.:   Tablet > /de.usaneers.MightyVL/Config/Config.xml
#if TARGET_PLAYSTORE
			return Application.persistentDataPath;
#elif UNITY_5_6_OR_NEWER && EVERYSIGHT
			return "/storage/emulated/0/Other/" + Application.identifier;
#elif UNITY_5_6_OR_NEWER
			return "/storage/emulated/0/" + Application.identifier;
#else
			return Application.persistentDataPath;
#endif

#elif UNITY_IOS && !UNITY_EDITOR
			// iOS !use .NET 2.0! no subnet
			// persistentDataPath: Data is stored in itunes/App Freigabe ...
			return Application.persistentDataPath;

#elif UNITY_STANDALONE_OSX && UNITY_EDITOR
			// OSX Editor: path to project folder 
			return Directory.GetParent(Application.dataPath).FullName;

#elif UNITY_STANDALONE_OSX
			// OSX Standalone: path to .app foleder
			return Directory.GetParent(Application.dataPath).Parent.FullName;

#elif UWP && !UNITY_EDITOR
			return Application.persistentDataPath;
#else
			// WINDOWS: path to .exe folder
			return Directory.GetParent(Application.dataPath).FullName;
#endif
		}

		/// <summary>
		/// Substitue all \ with /
		/// Delete first and leading characters:  /, newline, space
		/// </summary>
		public static string TrimPathString(string path, bool trim = true) {
			string result = path.Replace("\\", "/");
			if (trim) result = result.Trim('/', ' ', '\n');
			return result;
		}

		/// <summary>
		/// Get the absolute path from a local path 
		/// </summary>
		/// <param name="localPath">Path relative to the RootPath (<see cref="GetRootPath"/>)</param>
		public static string GetAbsolutePath(string localPath) {
			string path = TrimPathString(localPath);
			if (IOUtilities.IsFullPath(path)) return path;
			return string.Join(System.IO.Path.DirectorySeparatorChar.ToString(), GetRootPath(), path);
		}

		public static string GetAbsolutePath(params string[] localPathSegments)
			=> GetAbsolutePath(string.Join(System.IO.Path.DirectorySeparatorChar.ToString(), localPathSegments.Select((s, i) => TrimPathString(s, i != 0)).Where(s => !string.IsNullOrEmpty(s)).ToArray()));

		/// <summary>
		/// Create path string form various segments. Automatically adds separator chars
		/// </summary>
		public static string CreateAssembledPath(params string[] pathSegments)
			=> string.Join(System.IO.Path.DirectorySeparatorChar.ToString(), pathSegments.Select((s, i) => TrimPathString(s, i != 0)).Where(s => !string.IsNullOrEmpty(s)).ToArray());

		/// <summary>
		/// Count how many subfolder levels are in a path
		/// </summary>
		public static int GetPathLevels(string path) {
			if (string.IsNullOrWhiteSpace(path)) return 0;
			return path.Split('/', '\\').Count(s => !string.IsNullOrWhiteSpace(s));
		}

		private const string RESERVED_CHARACTERS = "!*'();:@&=+-$,/?%#[] ";

		/// <summary>
		/// Replace illegal characters from an URL string with their percent code
		/// </summary>
		public static string PercentCode(string value) {
			if (string.IsNullOrEmpty(value))
				return string.Empty;

			var sb = new StringBuilder();

			foreach (char c in value) {
				if (RESERVED_CHARACTERS.IndexOf(c) == -1)
					sb.Append(c);
				else
					sb.AppendFormat("%{0:X2}", (int)c);
			}
			return sb.ToString();
		}

#if UNITY_EDITOR
		/// <summary>
		/// Try to find the directory the .git folder is located in
		/// </summary>
		/// <returns>Success</returns>
		public static bool TryGetRepositoryRoot(out string path) {
			DirectoryInfo folder = new DirectoryInfo(GetRootPath());

			while (!Directory.Exists(CreateAssembledPath(folder.FullName, ".git"))) {
				if (folder.Parent == null) {
					path = string.Empty;
					return false;
				}
				folder = folder.Parent;
			}

			path = folder.FullName;
			return true;
		}
#endif

		/// <summary>
		/// Determines if a path points to an image
		/// </summary>
		public static bool IsImage(string path) {
			string ext = System.IO.Path.GetExtension(path);
			switch (ext.ToLowerInvariant()) {
				case ".png":
				case ".jpg":
				case ".jpeg":
				case ".tga":
				case ".tiff":
				case ".gif":
				case ".psd":
					return true;
				default:
					return false;
			}
		}

		/// <summary>
		/// Determines if a path points to a video
		/// </summary>
		public static bool IsVideo(string path) {
			string ext = System.IO.Path.GetExtension(path);

			switch (ext.ToLowerInvariant()) {
				case ".dv":
				case ".mp4":
				case ".mpg":
				case ".webm":
				case ".mpeg":
				case ".m4v":
				case ".ogv":
				case ".vp8":
				case ".wmv":
				case ".mov":
				case ".avi":
				case ".asf":
					return true;
				default:
					return false;
			}
		}

		/// <summary>
		/// Create Texture from pixel array
		/// </summary>
		public static Texture2D CreateTexture(int width, int height, Color[] pixelData) {
			Texture2D tex = new Texture2D(width, height);
			tex.SetPixels(pixelData);
			return tex;
		}

		/// <summary>
		/// Create Texture from pixel array
		/// </summary>
		public static Texture2D CreateTexture(int width, int height, Color32[] pixelData) {
			Texture2D tex = new Texture2D(width, height);
			tex.SetPixels32(pixelData);
			return tex;
		}

		/// <summary>
		/// Zip compression
		/// </summary>
		public static byte[] Compress(byte[] data) {
			using (MemoryStream compressedStream = new MemoryStream())
			using (GZipStream zipStream = new GZipStream(compressedStream, CompressionMode.Compress)) {
				zipStream.Write(data, 0, data.Length);
				zipStream.Close();
				return compressedStream.ToArray();
			}
		}

		/// <summary>
		/// Zip decompression
		/// </summary>
		public static byte[] Decompress(byte[] data) {
			try {
				using (MemoryStream compressedStream = new MemoryStream(data))
				using (GZipStream zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
				using (MemoryStream resultStream = new MemoryStream()) {
					zipStream.CopyTo(resultStream);
					return resultStream.ToArray();
				}
			} catch (IOException e) {
				Debug.LogError($"Could not decompress data: {e.Message}", Color.red);
				return null;
			}
		}

		/// <summary>
		/// Zip decompression with preallocated destination array
		/// </summary>
		public static bool DecompressNoAlloc(byte[] data, byte[] result) {
			if (result == null || result.Length == 0) return false;
			try {
				using (MemoryStream compressedStream = new MemoryStream(data))
				using (GZipStream zipStream = new GZipStream(compressedStream, CompressionMode.Decompress)) {
					zipStream.Read(result, 0, result.Length);
				}
			} catch (IOException e) {
				Debug.LogError($"Could not decompress data: {e.Message}", Color.red);
				return false;
			}

			return true;
		}
	}
}
