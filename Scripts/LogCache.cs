using System.Linq;
using UnityEngine;

namespace Utilities {
	public static class LogCache {
		public readonly struct LogEntry {
			public readonly string message;
			public readonly string trace;
			public readonly LogType type;

			public LogEntry(string message, string trace, LogType type) {
				this.message = message;
				this.trace = trace;
				this.type = type;
			}
		}

		private static CircularBuffer<LogEntry> _log;

		private static bool _initialized;

		public static void Init(int history) {
			_log = new CircularBuffer<LogEntry>(history);
			Application.logMessageReceived += HandleLogMessage;
			_initialized = true;
		}

		private static void HandleLogMessage(string message, string stackTrace, LogType type) {
			_log.Push(new LogEntry(message, stackTrace, type));
		}

		public static LogEntry[] log => _initialized ? _log.ToArray() : new LogEntry[0];
	}
}