﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {

	[ExecuteInEditMode]
	public class Path : MonoBehaviour {

		private List<PathVertex> _vertices = new List<PathVertex>();
		private Spline _spline;
		private PathVertex _startOut;
		private PathVertex _endOut;

		public Color gizmoColor = Color.cyan;
		public Color endControllerColor = Color.red;

		[Range(0.01f, 100)]
		public float gizmoScale = 0.25f;

		[SerializeField]
		private bool _closed = default;

		[SerializeField]
		private bool _reverse = default;

		[Range(0, 25)]
		[SerializeField]
		[Tooltip("Unless set to '0', the Smoothness value only affects the visual display of the path in the editor. The internal path's curve is unaffected")]
		private int _smoothness = 10;

		[SerializeField]
		[Tooltip("Activate this, if the path is meant to change during runtime")]
		private bool _dynamic = default;

		[SerializeField] private bool _remap = default;
		[SerializeField] private AnimationCurve _remapCurve = AnimationCurve.Linear(0, 0, 1, 1);

		public bool dynamic => _dynamic;

		public float length => _spline?.length ?? 0;

		public List<PathVertex> vertices => _vertices;

		public bool drawGizmos = true;

		public event Action updated;

		public Vector3 PositionAlongPath(float pathPercentage) {
			if (_spline == null) {
				if (_vertices.Count < 2) {
					return _vertices.Count > 0 ? _vertices[0].transform.position : Vector3.zero;
				}
				CreateSpline();
			}

			if (_remap) pathPercentage = _remapCurve.Evaluate(pathPercentage);

			return _spline.GetPoint(pathPercentage);
		}

		public void AddVertex(PathVertex vert) {
			if (_vertices == null) _vertices = new List<PathVertex>();
			switch (vert.vType) {
				case PathVertex.VertexType.Interpolant:
					if (!_vertices.Contains(vert)) _vertices.Add(vert);
					SortVertices();
					break;
				case PathVertex.VertexType.StartController:
					if (_vertices.Contains(vert)) _vertices.Remove(vert);
					if (_endOut == vert) _endOut = null;
					_startOut = vert;
					break;
				case PathVertex.VertexType.EndController:
					if (_vertices.Contains(vert)) _vertices.Remove(vert);
					if (_startOut == vert) _startOut = null;
					_endOut = vert;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			if (_vertices.Count > 1) {
				if (_spline == null) CreateSpline();
				else UpdatePath();
			}
		}

		public void RemoveVertex(PathVertex vert) {
			if (_vertices.Contains(vert)) {
				_vertices.Remove(vert);
				_vertices = _vertices.OrderBy(v => v.name).ToList();

				if (_vertices.Count < 2) {
					ClearSpline();
					_spline = null;
				}
			} else if (vert.vType == PathVertex.VertexType.StartController && _startOut == vert) {
				_startOut = null;
			} else if (vert.vType == PathVertex.VertexType.EndController && _endOut == vert) {
				_endOut = null;
			}
		}

		private void SortVertices() {
			if (_vertices == null || _vertices.Count < 2) return;
			_vertices = _reverse ?
				_vertices.Where(v => v.vType == PathVertex.VertexType.Interpolant).OrderByDescending(v => v.name).ToList()
				: _vertices.Where(v => v.vType == PathVertex.VertexType.Interpolant).OrderBy(v => v.name).ToList();
		}

		private void CreateSpline() {
			if (_spline != null) ClearSpline();
			Vector3? startOut = null;
			if (_startOut != null) startOut = _startOut.transform.position;

			Vector3? endOut = null;
			if (_endOut != null) endOut = _endOut.transform.position;

			if (_reverse) {
				_spline = Spline.Create(_vertices.Select(v => v.transform.position).ToArray(), null, startOut: endOut, endOut: startOut, closed: _closed, smooth: _smoothness > 0);
			} else {
				_spline = Spline.Create(_vertices.Select(v => v.transform.position).ToArray(), null, startOut: startOut, endOut: endOut, closed: _closed, smooth: _smoothness > 0);
			}
		}

		public void UpdatePath() {
			if (_vertices.Count < 2) return;
			if (_spline != null) {
				Vector3? startOut = null;
				if (_startOut != null) startOut = _startOut.transform.position;

				Vector3? endOut = null;
				if (_endOut != null) endOut = _endOut.transform.position;

				if (_reverse) {
					_spline.Update(_vertices.Select(v => v.transform.position).ToArray(), startOut: endOut, endOut: startOut);
				} else {
					_spline.Update(_vertices.Select(v => v.transform.position).ToArray(), startOut: startOut, endOut: endOut);
				}

				updated?.Invoke();
			}
		}

		private void ResetPath() {
			ClearSpline();
			SortVertices();
			CreateSpline();
		}

		private void ClearSpline() {
			_spline?.Clear();
			_spline = null;
		}

		private void OnValidate() {
			if (_spline == null) return;
			ResetPath();
		}

		private void Reset() {
			PathVertex[] tmp = GetComponentsInChildren<PathVertex>();
			_vertices = tmp.Where(v => v.vType == PathVertex.VertexType.Interpolant).OrderBy(v => v.name).ToList();
			_startOut = tmp.FirstOrDefault(v => v.vType == PathVertex.VertexType.StartController);
			_endOut = tmp.FirstOrDefault(v => v.vType == PathVertex.VertexType.EndController);
			ResetPath();
		}

		private void OnDrawGizmos() {

			if (!drawGizmos || _vertices.Count < 2) return;
			Gizmos.color = gizmoColor;

			_spline.DrawGizmo(_smoothness);
		}
	}
}
