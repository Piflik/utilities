﻿using UnityEngine;
using UnityEditor;

namespace Utilities.Editor {
	[CustomPropertyDrawer(typeof(IntRange), true)]
	public class RangedIntDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			label = EditorGUI.BeginProperty(position, label, property);
			position = EditorGUI.PrefixLabel(position, label);

			SerializedProperty minProp = property.FindPropertyRelative("min");
			SerializedProperty maxProp = property.FindPropertyRelative("max");

			float minValue = minProp.intValue;
			float maxValue = maxProp.intValue;

			float rangeMin = 0;
			float rangeMax = 1;

			MinMaxRangeAttribute[] ranges = (MinMaxRangeAttribute[])fieldInfo.GetCustomAttributes(typeof(MinMaxRangeAttribute), true);
			if (ranges.Length > 0) {
				rangeMin = ranges[0].min;
				rangeMax = ranges[0].max;
			}

			const float rangeBoundsLabelWidth = 30f;

			Rect rangeBoundsLabel1Rect = new Rect(position) { width = rangeBoundsLabelWidth };
			GUI.Label(rangeBoundsLabel1Rect, new GUIContent(((int)minValue).ToString()));
			position.xMin += rangeBoundsLabelWidth;

			Rect rangeBoundsLabel2Rect = new Rect(position);
			rangeBoundsLabel2Rect.xMin = rangeBoundsLabel2Rect.xMax - rangeBoundsLabelWidth;
			GUI.Label(rangeBoundsLabel2Rect, new GUIContent(((int)maxValue).ToString()));
			position.xMax -= rangeBoundsLabelWidth;

			EditorGUI.BeginChangeCheck();
			EditorGUI.MinMaxSlider(position, ref minValue, ref maxValue, rangeMin, rangeMax);
			if (EditorGUI.EndChangeCheck()) {
				minProp.intValue = (int)minValue;
				maxProp.intValue = (int)maxValue;
			}

			EditorGUI.EndProperty();
		}
	} 
}
