﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Utilities.Editor {
	[CustomPropertyDrawer(typeof(SerializableDictionaryBase), useForChildren: true)]
	public class SerializableDictionaryPropertyDrawer : PropertyDrawer {
		private const string KEYS_FIELD_NAME = "_keys";
		private const string VALUES_FIELD_NAME = "_values";

		protected const float INDENT_WIDTH = 15f;

		private static readonly GUIContent _iconPlus = IconContent("Toolbar Plus", "Add entry");
		private static readonly GUIContent _iconMinus = IconContent("Toolbar Minus", "Remove entry");
		private static readonly GUIContent _warningIconConflict = IconContent("console.warnicon.sml", "Conflicting key, this entry will be lost");
		private static readonly GUIContent _warningIconOther = IconContent("console.infoicon.sml", "Conflicting key");
		private static readonly GUIContent _warningIconNull = IconContent("console.warnicon.sml", "Null key, this entry will be lost");
		private static readonly GUIStyle _buttonStyle = GUIStyle.none;
		private static readonly GUIContent _tempContent = new GUIContent();

		private static readonly Dictionary<PropertyIdentity, ConflictState> _conflictStateDict = new Dictionary<PropertyIdentity, ConflictState>();

		private static readonly Dictionary<SerializedPropertyType, PropertyInfo> _serializedPropertyValueAccessorsDict;

		static SerializableDictionaryPropertyDrawer() {
			Dictionary<SerializedPropertyType, string> serializedPropertyValueAccessorsNameDict = new Dictionary<SerializedPropertyType, string> {
				{SerializedPropertyType.Integer, "intValue"},
				{SerializedPropertyType.Boolean, "boolValue"},
				{SerializedPropertyType.Float, "floatValue"},
				{SerializedPropertyType.String, "stringValue"},
				{SerializedPropertyType.Color, "colorValue"},
				{SerializedPropertyType.ObjectReference, "objectReferenceValue"},
				{SerializedPropertyType.LayerMask, "intValue"},
				{SerializedPropertyType.Enum, "intValue"},
				{SerializedPropertyType.Vector2, "vector2Value"},
				{SerializedPropertyType.Vector3, "vector3Value"},
				{SerializedPropertyType.Vector4, "vector4Value"},
				{SerializedPropertyType.Rect, "rectValue"},
				{SerializedPropertyType.ArraySize, "intValue"},
				{SerializedPropertyType.Character, "intValue"},
				{SerializedPropertyType.AnimationCurve, "animationCurveValue"},
				{SerializedPropertyType.Bounds, "boundsValue"},
				{SerializedPropertyType.Quaternion, "quaternionValue"}
			};
			Type serializedPropertyType = typeof(SerializedProperty);

			_serializedPropertyValueAccessorsDict = new Dictionary<SerializedPropertyType, PropertyInfo>();
			BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;

			foreach ((SerializedPropertyType key, string value) in serializedPropertyValueAccessorsNameDict) {
				PropertyInfo propertyInfo = serializedPropertyType.GetProperty(value, flags);
				_serializedPropertyValueAccessorsDict.Add(key, propertyInfo);
			}
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			label = EditorGUI.BeginProperty(position, label, property);

			Action buttonAction = Action.None;
			int buttonActionIndex = 0;

			SerializedProperty keyArrayProperty = property.FindPropertyRelative(KEYS_FIELD_NAME);
			SerializedProperty valueArrayProperty = property.FindPropertyRelative(VALUES_FIELD_NAME);

			ConflictState conflictState = GetConflictState(property);

			if (conflictState.conflictIndex != -1) {
				keyArrayProperty.InsertArrayElementAtIndex(conflictState.conflictIndex);
				SerializedProperty keyProperty = keyArrayProperty.GetArrayElementAtIndex(conflictState.conflictIndex);
				SetPropertyValue(keyProperty, conflictState.conflictKey);
				keyProperty.isExpanded = conflictState.conflictKeyPropertyExpanded;

				valueArrayProperty.InsertArrayElementAtIndex(conflictState.conflictIndex);
				SerializedProperty valueProperty = valueArrayProperty.GetArrayElementAtIndex(conflictState.conflictIndex);
				SetPropertyValue(valueProperty, conflictState.conflictValue);
				valueProperty.isExpanded = conflictState.conflictValuePropertyExpanded;
			}

			float buttonWidth = _buttonStyle.CalcSize(_iconPlus).x;

			Rect labelPosition = position;
			labelPosition.height = EditorGUIUtility.singleLineHeight;
			if (property.isExpanded)
				labelPosition.xMax -= _buttonStyle.CalcSize(_iconPlus).x;

			EditorGUI.PropertyField(labelPosition, property, label, false);
			// property.isExpanded = EditorGUI.Foldout(labelPosition, property.isExpanded, label);
			if (property.isExpanded) {
				Rect buttonPosition = position;
				buttonPosition.xMin = buttonPosition.xMax - buttonWidth;
				buttonPosition.height = EditorGUIUtility.singleLineHeight;
				EditorGUI.BeginDisabledGroup(conflictState.conflictIndex != -1);
				if (GUI.Button(buttonPosition, _iconPlus, _buttonStyle)) {
					buttonAction = Action.Add;
					buttonActionIndex = keyArrayProperty.arraySize;
				}

				EditorGUI.EndDisabledGroup();

				EditorGUI.indentLevel++;
				Rect linePosition = position;
				linePosition.y += EditorGUIUtility.singleLineHeight;
				linePosition.xMax -= buttonWidth;

				foreach (EnumerationEntry entry in EnumerateEntries(keyArrayProperty, valueArrayProperty)) {
					SerializedProperty keyProperty = entry.keyProperty;
					SerializedProperty valueProperty = entry.valueProperty;
					int i = entry.index;

					float lineHeight = DrawKeyValueLine(keyProperty, valueProperty, linePosition, i);

					buttonPosition = linePosition;
					buttonPosition.x = linePosition.xMax;
					buttonPosition.height = EditorGUIUtility.singleLineHeight;
					if (GUI.Button(buttonPosition, _iconMinus, _buttonStyle)) {
						buttonAction = Action.Remove;
						buttonActionIndex = i;
					}

					if (i == conflictState.conflictIndex && conflictState.conflictOtherIndex == -1) {
						Rect iconPosition = linePosition;
						iconPosition.size = _buttonStyle.CalcSize(_warningIconNull);
						GUI.Label(iconPosition, _warningIconNull);
					} else if (i == conflictState.conflictIndex) {
						Rect iconPosition = linePosition;
						iconPosition.size = _buttonStyle.CalcSize(_warningIconConflict);
						GUI.Label(iconPosition, _warningIconConflict);
					} else if (i == conflictState.conflictOtherIndex) {
						Rect iconPosition = linePosition;
						iconPosition.size = _buttonStyle.CalcSize(_warningIconOther);
						GUI.Label(iconPosition, _warningIconOther);
					}

					linePosition.y += lineHeight;
				}

				EditorGUI.indentLevel--;
			}

			if (buttonAction == Action.Add) {
				keyArrayProperty.InsertArrayElementAtIndex(buttonActionIndex);
				valueArrayProperty.InsertArrayElementAtIndex(buttonActionIndex);
			} else if (buttonAction == Action.Remove) {
				DeleteArrayElementAtIndex(keyArrayProperty, buttonActionIndex);
				DeleteArrayElementAtIndex(valueArrayProperty, buttonActionIndex);
			}

			conflictState.conflictKey = null;
			conflictState.conflictValue = null;
			conflictState.conflictIndex = -1;
			conflictState.conflictOtherIndex = -1;
			conflictState.conflictLineHeight = 0f;
			conflictState.conflictKeyPropertyExpanded = false;
			conflictState.conflictValuePropertyExpanded = false;

			foreach (EnumerationEntry entry1 in EnumerateEntries(keyArrayProperty, valueArrayProperty)) {
				SerializedProperty keyProperty1 = entry1.keyProperty;
				int i = entry1.index;
				object keyProperty1Value = GetPropertyValue(keyProperty1);

				if (keyProperty1Value == null) {
					SerializedProperty valueProperty1 = entry1.valueProperty;
					SaveProperty(keyProperty1, valueProperty1, i, -1, conflictState);
					DeleteArrayElementAtIndex(valueArrayProperty, i);
					DeleteArrayElementAtIndex(keyArrayProperty, i);

					break;
				}

				foreach (EnumerationEntry entry2 in EnumerateEntries(keyArrayProperty, valueArrayProperty, i + 1)) {
					SerializedProperty keyProperty2 = entry2.keyProperty;
					int j = entry2.index;
					object keyProperty2Value = GetPropertyValue(keyProperty2);

					if (ComparePropertyValues(keyProperty1Value, keyProperty2Value)) {
						SerializedProperty valueProperty2 = entry2.valueProperty;
						SaveProperty(keyProperty2, valueProperty2, j, i, conflictState);
						DeleteArrayElementAtIndex(keyArrayProperty, j);
						DeleteArrayElementAtIndex(valueArrayProperty, j);

						goto breakLoops;
					}
				}
			}

			breakLoops:

			EditorGUI.EndProperty();
		}

		private static float DrawKeyValueLine(SerializedProperty keyProperty, SerializedProperty valueProperty, Rect linePosition, int index) {
			bool keyCanBeExpanded = CanPropertyBeExpanded(keyProperty);
			bool valueCanBeExpanded = CanPropertyBeExpanded(valueProperty);

			if (!keyCanBeExpanded && valueCanBeExpanded)
				return DrawKeyValueLineExpand(keyProperty, valueProperty, linePosition);
			string keyLabel = keyCanBeExpanded ? $"Key {index}" : "";
			string valueLabel = valueCanBeExpanded ? $"Value {index}" : "";
			return DrawKeyValueLineSimple(keyProperty, valueProperty, keyLabel, valueLabel, linePosition);
		}

		private static float DrawKeyValueLineSimple(SerializedProperty keyProperty, SerializedProperty valueProperty, string keyLabel, string valueLabel, Rect linePosition) {
			float labelWidth = EditorGUIUtility.labelWidth;
			float labelWidthRelative = labelWidth / linePosition.width;

			float keyPropertyHeight = EditorGUI.GetPropertyHeight(keyProperty);
			Rect keyPosition = linePosition;
			keyPosition.height = keyPropertyHeight;
			keyPosition.width = labelWidth - INDENT_WIDTH;
			EditorGUIUtility.labelWidth = keyPosition.width * labelWidthRelative;
			EditorGUI.PropertyField(keyPosition, keyProperty, TempContent(keyLabel), true);

			float valuePropertyHeight = EditorGUI.GetPropertyHeight(valueProperty);
			Rect valuePosition = linePosition;
			valuePosition.height = valuePropertyHeight;
			valuePosition.xMin += labelWidth;
			EditorGUIUtility.labelWidth = valuePosition.width * labelWidthRelative;
			EditorGUI.indentLevel--;
			EditorGUI.PropertyField(valuePosition, valueProperty, TempContent(valueLabel), true);
			EditorGUI.indentLevel++;

			EditorGUIUtility.labelWidth = labelWidth;

			return Mathf.Max(keyPropertyHeight, valuePropertyHeight);
		}

		private static float DrawKeyValueLineExpand(SerializedProperty keyProperty, SerializedProperty valueProperty, Rect linePosition) {
			float labelWidth = EditorGUIUtility.labelWidth;

			float keyPropertyHeight = EditorGUI.GetPropertyHeight(keyProperty);
			Rect keyPosition = linePosition;
			keyPosition.height = keyPropertyHeight;
			keyPosition.width = labelWidth - INDENT_WIDTH;
			EditorGUI.PropertyField(keyPosition, keyProperty, GUIContent.none, true);

			float valuePropertyHeight = EditorGUI.GetPropertyHeight(valueProperty);
			Rect valuePosition = linePosition;
			valuePosition.height = valuePropertyHeight;
			EditorGUI.PropertyField(valuePosition, valueProperty, GUIContent.none, true);

			EditorGUIUtility.labelWidth = labelWidth;

			return Mathf.Max(keyPropertyHeight, valuePropertyHeight);
		}

		private static bool CanPropertyBeExpanded(SerializedProperty property) {
			switch (property.propertyType) {
				case SerializedPropertyType.Generic:
				case SerializedPropertyType.Vector4:
				case SerializedPropertyType.Quaternion:
					return true;
				default:
					return false;
			}
		}

		private static void SaveProperty(SerializedProperty keyProperty, SerializedProperty valueProperty, int index, int otherIndex, ConflictState conflictState) {
			conflictState.conflictKey = GetPropertyValue(keyProperty);
			conflictState.conflictValue = GetPropertyValue(valueProperty);
			float keyPropertyHeight = EditorGUI.GetPropertyHeight(keyProperty);
			float valuePropertyHeight = EditorGUI.GetPropertyHeight(valueProperty);
			float lineHeight = Mathf.Max(keyPropertyHeight, valuePropertyHeight);
			conflictState.conflictLineHeight = lineHeight;
			conflictState.conflictIndex = index;
			conflictState.conflictOtherIndex = otherIndex;
			conflictState.conflictKeyPropertyExpanded = keyProperty.isExpanded;
			conflictState.conflictValuePropertyExpanded = valueProperty.isExpanded;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
			float propertyHeight = EditorGUIUtility.singleLineHeight;

			if (property.isExpanded) {
				SerializedProperty keysProperty = property.FindPropertyRelative(KEYS_FIELD_NAME);
				SerializedProperty valuesProperty = property.FindPropertyRelative(VALUES_FIELD_NAME);

				foreach (EnumerationEntry entry in EnumerateEntries(keysProperty, valuesProperty)) {
					SerializedProperty keyProperty = entry.keyProperty;
					SerializedProperty valueProperty = entry.valueProperty;
					float keyPropertyHeight = EditorGUI.GetPropertyHeight(keyProperty);
					float valuePropertyHeight = EditorGUI.GetPropertyHeight(valueProperty);
					float lineHeight = Mathf.Max(keyPropertyHeight, valuePropertyHeight);
					propertyHeight += lineHeight;
				}

				ConflictState conflictState = GetConflictState(property);

				if (conflictState.conflictIndex != -1)
					propertyHeight += conflictState.conflictLineHeight;
			}

			return propertyHeight;
		}

		private static ConflictState GetConflictState(SerializedProperty property) {
			PropertyIdentity propId = new PropertyIdentity(property);
			if (!_conflictStateDict.TryGetValue(propId, out ConflictState conflictState)) {
				conflictState = new ConflictState();
				_conflictStateDict.Add(propId, conflictState);
			}

			return conflictState;
		}

		private static GUIContent IconContent(string name, string tooltip) {
			GUIContent builtinIcon = EditorGUIUtility.IconContent(name);
			return new GUIContent(builtinIcon.image, tooltip);
		}

		private static GUIContent TempContent(string text) {
			_tempContent.text = text;
			return _tempContent;
		}

		private static void DeleteArrayElementAtIndex(SerializedProperty arrayProperty, int index) {
			SerializedProperty property = arrayProperty.GetArrayElementAtIndex(index);
			// if(arrayProperty.arrayElementType.StartsWith("PPtr<$"))
			if (property.propertyType == SerializedPropertyType.ObjectReference)
				property.objectReferenceValue = null;

			arrayProperty.DeleteArrayElementAtIndex(index);
		}

		public static object GetPropertyValue(SerializedProperty p) {
			if (_serializedPropertyValueAccessorsDict.TryGetValue(p.propertyType, out PropertyInfo propertyInfo))
				return propertyInfo.GetValue(p, null);
			if (p.isArray)
				return GetPropertyValueArray(p);
			return GetPropertyValueGeneric(p);
		}

		private static void SetPropertyValue(SerializedProperty p, object v) {
			if (_serializedPropertyValueAccessorsDict.TryGetValue(p.propertyType, out PropertyInfo propertyInfo))
				propertyInfo.SetValue(p, v, null);
			else {
				if (p.isArray)
					SetPropertyValueArray(p, v);
				else
					SetPropertyValueGeneric(p, v);
			}
		}

		private static object GetPropertyValueArray(SerializedProperty property) {
			object[] array = new object[property.arraySize];
			for (int i = 0; i < property.arraySize; i++) {
				SerializedProperty item = property.GetArrayElementAtIndex(i);
				array[i] = GetPropertyValue(item);
			}

			return array;
		}

		private static object GetPropertyValueGeneric(SerializedProperty property) {
			Dictionary<string, object> dict = new Dictionary<string, object>();
			SerializedProperty iterator = property.Copy();
			if (iterator.Next(true)) {
				SerializedProperty end = property.GetEndProperty();
				do {
					string name = iterator.name;
					object value = GetPropertyValue(iterator);
					dict.Add(name, value);
				} while (iterator.Next(false) && iterator.propertyPath != end.propertyPath);
			}

			return dict;
		}

		private static void SetPropertyValueArray(SerializedProperty property, object v) {
			object[] array = (object[]) v;
			property.arraySize = array.Length;
			for (int i = 0; i < property.arraySize; i++) {
				SerializedProperty item = property.GetArrayElementAtIndex(i);
				SetPropertyValue(item, array[i]);
			}
		}

		private static void SetPropertyValueGeneric(SerializedProperty property, object v) {
			Dictionary<string, object> dict = (Dictionary<string, object>) v;
			SerializedProperty iterator = property.Copy();
			if (iterator.Next(true)) {
				SerializedProperty end = property.GetEndProperty();
				do {
					string name = iterator.name;
					SetPropertyValue(iterator, dict[name]);
				} while (iterator.Next(false) && iterator.propertyPath != end.propertyPath);
			}
		}

		private static bool ComparePropertyValues(object value1, object value2) {
			if (value1 is Dictionary<string, object> dict1 && value2 is Dictionary<string, object> dict2) {
				return CompareDictionaries(dict1, dict2);
			}

			return Equals(value1, value2);
		}

		private static bool CompareDictionaries(Dictionary<string, object> dict1, Dictionary<string, object> dict2) {
			if (dict1.Count != dict2.Count)
				return false;

			foreach ((string key1, object value1) in dict1) {

				if (!dict2.TryGetValue(key1, out object value2))
					return false;

				if (!ComparePropertyValues(value1, value2))
					return false;
			}

			return true;
		}

		private static IEnumerable<EnumerationEntry> EnumerateEntries(SerializedProperty keyArrayProperty, SerializedProperty valueArrayProperty, int startIndex = 0) {
			if (keyArrayProperty.arraySize > startIndex) {
				int index = startIndex;
				SerializedProperty keyProperty = keyArrayProperty.GetArrayElementAtIndex(startIndex);
				SerializedProperty valueProperty = valueArrayProperty.GetArrayElementAtIndex(startIndex);
				SerializedProperty endProperty = keyArrayProperty.GetEndProperty();

				do {
					yield return new EnumerationEntry(keyProperty, valueProperty, index);
					index++;
				} while (keyProperty.Next(false) && valueProperty.Next(false) && !SerializedProperty.EqualContents(keyProperty, endProperty));
			}
		}

		private class ConflictState {
			public int conflictIndex = -1;
			public object conflictKey;
			public bool conflictKeyPropertyExpanded;
			public float conflictLineHeight;
			public int conflictOtherIndex = -1;
			public object conflictValue;
			public bool conflictValuePropertyExpanded;
		}

		private struct PropertyIdentity : IEquatable<PropertyIdentity> {
			private readonly Object _instance;
			private readonly string _propertyPath;

			public PropertyIdentity(SerializedProperty property) {
				_instance = property.serializedObject.targetObject;
				_propertyPath = property.propertyPath;
			}

			public bool Equals(PropertyIdentity other) {
				return Equals(_instance, other._instance) && string.Equals(_propertyPath, other._propertyPath);
			}

			public override bool Equals(object obj) {
				return obj is PropertyIdentity other && Equals(other);
			}

			public override int GetHashCode() {
				unchecked {
					return ((_instance != null ? _instance.GetHashCode() : 0) * 397) ^ (_propertyPath != null ? _propertyPath.GetHashCode() : 0);
				}
			}
		}

		private enum Action {
			None,
			Add,
			Remove
		}

		private struct EnumerationEntry {
			public readonly SerializedProperty keyProperty;
			public readonly SerializedProperty valueProperty;
			public readonly int index;

			public EnumerationEntry(SerializedProperty keyProperty, SerializedProperty valueProperty, int index) {
				this.keyProperty = keyProperty;
				this.valueProperty = valueProperty;
				this.index = index;
			}
		}
	}
}
#endif