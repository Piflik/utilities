﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Utilities.Editor {
	public static class EditorExtension {
		public static int DrawBitMaskField(Rect aPosition, int aMask, Type aType, GUIContent aLabel) {
			string[] itemNames = Enum.GetNames(aType);
			if (!(Enum.GetValues(aType) is int[] itemValues)) return aMask;

			int val = aMask;
			int maskVal = 0;
			for (int i = 0; i < itemValues.Length; i++) {
				if (itemValues[i] != 0) {
					if ((val & itemValues[i]) == itemValues[i])
						maskVal |= 1 << i;
				} else if (val == 0)
					maskVal |= 1 << i;
			}
			int newMaskVal = EditorGUI.MaskField(aPosition, aLabel, maskVal, itemNames);
			int changes = maskVal ^ newMaskVal;

			for (int i = 0; i < itemValues.Length; i++) {
				if ((changes & (1 << i)) != 0) {          // has this list item changed?
					if ((newMaskVal & (1 << i)) != 0) {   // has it been set?
						if (itemValues[i] == 0) {         // special case: if "0" is set, just set the val to 0
							val = 0;
							break;
						}

						val |= itemValues[i];
					} else {                                 // it has been reset
						val &= ~itemValues[i];
					}
				}
			}
			return val;
		}
	}

	[CustomPropertyDrawer(typeof(EnumFlagsAttribute)), Obsolete]
	public class EnumFlagsAttributeDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label) {
			if (!(attribute is EnumFlagsAttribute typeAttr)) return;
			// Add the actual int value behind the field name
			label.text = $"{label.text}({prop.intValue})";

			EditorGUI.BeginChangeCheck();
			int newValue = EditorExtension.DrawBitMaskField(position, prop.intValue, typeAttr.propType, label);
			if (EditorGUI.EndChangeCheck()) {
				prop.intValue = newValue;
			}
		}
	}

	[CustomPropertyDrawer(typeof(EnumFlagSingleAttribute))]
	public class EnumFlagSingleAttributeDrawer : PropertyDrawer {
		public override void OnGUI(Rect position,
			SerializedProperty property, GUIContent label) {
			EnumFlagSingleAttribute singleEnumFlagSelectAttribute = (EnumFlagSingleAttribute)attribute;
			List<GUIContent> displayTexts = new List<GUIContent>();
			List<int> enumValues = new List<int>();
			foreach (object displayText in
				Enum.GetValues(singleEnumFlagSelectAttribute.propType)) {
				displayTexts.Add(new GUIContent(displayText.ToString()));
				enumValues.Add((int)displayText);
			}

			property.intValue = EditorGUI.IntPopup(position, label, property.intValue,
				displayTexts.ToArray(), enumValues.ToArray());
		}
	}


	[CustomPropertyDrawer(typeof(LayerAttribute))]
	internal class LayerAttributeEditor : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			property.intValue = EditorGUI.LayerField(position, label, property.intValue);
		}
	}

	[CustomPropertyDrawer(typeof(ReadOnlyInInspectorAttribute))]
	internal class ReadOnlyInInspectorAttributeEditor : PropertyDrawer {
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
			return EditorGUI.GetPropertyHeight(property, label, true);
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			GUI.enabled = false;
			EditorGUI.PropertyField(position, property, label, true);
			GUI.enabled = true;
		}
	}

	[CustomPropertyDrawer(typeof(TagSelectorAttribute))]
	public class TagSelectorPropertyDrawer : PropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			if (property.propertyType == SerializedPropertyType.String) {
				EditorGUI.BeginProperty(position, label, property);

				if (attribute is TagSelectorAttribute tagAttribute && tagAttribute.useDefaultTagFieldDrawer) {
					property.stringValue = EditorGUI.TagField(position, label, property.stringValue);
				} else {
					//generate the taglist + custom tags
					List<string> tagList = new List<string> { "<NoTag>" };
					tagList.AddRange(UnityEditorInternal.InternalEditorUtility.tags);
					string propertyString = property.stringValue;
					int index = -1;
					if (string.IsNullOrWhiteSpace(propertyString)) {
						//The tag is empty
						index = 0; //first index is the special <notag> entry
					} else {
						//check if there is an entry that matches the entry and get the index
						//we skip index 0 as that is a special custom case
						for (int i = 1; i < tagList.Count; i++) {
							if (tagList[i] == propertyString) {
								index = i;
								break;
							}
						}
					}

					//Draw the popup box with the current selected index
					index = EditorGUI.Popup(position, label.text, index, tagList.ToArray());

					//Adjust the actual string value of the property based on the selection
					if (index == 0) {
						property.stringValue = "";
					} else if (index >= 1) {
						property.stringValue = tagList[index];
					} else {
						property.stringValue = "";
					}
				}

				EditorGUI.EndProperty();
			} else {
				EditorGUI.PropertyField(position, property, label);
			}
		}
	}

	[CustomPropertyDrawer(typeof(InterfaceTypeAttribute))]
	public class InterfaceTypeDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			InterfaceTypeAttribute att = attribute as InterfaceTypeAttribute;

			if (property.propertyType != SerializedPropertyType.ObjectReference) {
				EditorGUI.LabelField(position, label.text, "InterfaceType Attribute can only be used with MonoBehaviour Components!");
				return;
			}

			// Pick a specific component
			MonoBehaviour oldComp = property.objectReferenceValue as MonoBehaviour;

			GameObject temp = null;
			string oldName = "";

			if (Event.current.type == EventType.Repaint) {
				if (oldComp == null) {
					temp = new GameObject("None [" + att.type.Name + "]");
					oldComp = temp.AddComponent<MonoInterface>();
				} else {
					oldName = oldComp.name;
					oldComp.name = oldName + " [" + att.type.Name + "]";
				}
			}

			MonoBehaviour comp = EditorGUI.ObjectField(position, label, oldComp, typeof(MonoBehaviour), true) as MonoBehaviour;

			if (Event.current.type == EventType.Repaint) {
				if (temp != null)
					UnityEngine.Object.DestroyImmediate(temp);
				else
					oldComp.name = oldName;
			}

			// Make sure something changed.
			if (oldComp == comp) return;

			// If a component is assigned, make sure it is the interface we are looking for.
			if (comp != null) {
				// Make sure component is of the right interface
				if (comp.GetType() != att.type)
					// Component failed. Check game object.
					comp = comp.gameObject.GetComponent(att.type) as MonoBehaviour;

				// Item failed test. Do not override old component
				if (comp == null) return;
			}

			property.objectReferenceValue = comp;
			property.serializedObject.ApplyModifiedProperties();
		}

		private class MonoInterface : MonoBehaviour { }
	}

	[CustomPropertyDrawer(typeof(IndentAttribute))]
	public class IndentAttributeDrawer : DecoratorDrawer {
		public override void OnGUI(Rect position) {
			if (!(attribute is IndentAttribute indentAttr)) return;
			EditorGUI.indentLevel += indentAttr.indentation;
		}

		public override float GetHeight() {
			return 0;
		}
	}
	
	//Original version of the ConditionalHideAttribute created by Brecht Lecluyse (www.brechtos.com)
	[CustomPropertyDrawer(typeof(ConditionalHideAttribute))]
	public class ConditionalHidePropertyDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

			ConditionalHideAttribute condHAtt = (ConditionalHideAttribute)attribute;
			bool enabled = GetConditionalHideAttributeResult(condHAtt, property);

			bool wasEnabled = GUI.enabled;
			GUI.enabled = enabled;
			if (!condHAtt.hideInactive || enabled) {
				EditorGUI.PropertyField(position, property, label, true);
			}

			GUI.enabled = wasEnabled;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
			ConditionalHideAttribute condHAtt = (ConditionalHideAttribute)attribute;
			bool enabled = GetConditionalHideAttributeResult(condHAtt, property);

			if (!condHAtt.hideInactive || enabled) {
				return EditorGUI.GetPropertyHeight(property, label);
			}

			return -EditorGUIUtility.standardVerticalSpacing;
		}

		private bool GetConditionalHideAttributeResult(ConditionalHideAttribute condHAtt, SerializedProperty property) {
			bool enabled = true;

			//Handle primary property
			SerializedProperty sourcePropertyValue = null;
			//Get the full relative property path of the sourcefield so we can have nested hiding.Use old method when dealing with arrays

			foreach ((string fieldName, bool invert) in condHAtt.conditionalSourceField) {

				if (!property.isArray) {
					string propertyPath = property.propertyPath; //returns the property path of the property we want to apply the attribute to
					string conditionPath = propertyPath.Replace(property.name, fieldName); //changes the path to the conditionalsource property path
					sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);

					//if the find failed->fall back to the old system
					if (sourcePropertyValue == null) {
						//original implementation (doens't work with nested serializedObjects)
						sourcePropertyValue = property.serializedObject.FindProperty(fieldName);
					}
				} else {
					//original implementation (doens't work with nested serializedObjects)
					sourcePropertyValue = property.serializedObject.FindProperty(fieldName);
				}

				if (sourcePropertyValue != null) {
					enabled &= CheckPropertyType(sourcePropertyValue) ^ invert;
				}
			}

			return enabled;
		}

		private bool CheckPropertyType(SerializedProperty sourcePropertyValue) {
			//Note: add others for custom handling if desired
			switch (sourcePropertyValue.propertyType) {
				case SerializedPropertyType.Boolean:
					return sourcePropertyValue.boolValue;
				case SerializedPropertyType.String:
					return !string.IsNullOrWhiteSpace(sourcePropertyValue.stringValue);
				case SerializedPropertyType.ObjectReference:
					return sourcePropertyValue.objectReferenceValue != null;
				default:
					Utilities.Debug.LogError("Data type of the property used for conditional hiding [" + sourcePropertyValue.propertyType + "] is currently not supported");
					return true;
			}
		}
	}
}


