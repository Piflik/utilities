﻿using System;
using UnityEditor;
using UnityEditor.Build;
#if UNITY_2018_1_OR_NEWER
using UnityEditor.Build.Reporting;
#endif
// ReSharper disable UnusedMember.Global

namespace Utilities.Editor {
#if UNITY_2018_1_OR_NEWER
	public class CompanyNamePreprocessor : IPreprocessBuildWithReport {
#else
	public class CompanyNamePreprocessor : IPreprocessBuild {
#endif
		public const string COMPANY_NAME = "Piflik";
		public const string DEFAULT_COMPANY_NAME = "DefaultCompany";

#if UNITY_2018_1_OR_NEWER
		public void OnPreprocessBuild(BuildReport report) {
#else
		public void OnPreprocessBuild(BuildTarget target, string path) {
#endif
			if (PlayerSettings.companyName.Equals(DEFAULT_COMPANY_NAME, StringComparison.OrdinalIgnoreCase)) {
				PlayerSettings.companyName = COMPANY_NAME;
			}

			PlayerSettings.SplashScreen.show = false;
		}

		public int callbackOrder => 0;
	} 
}
