﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Utilities.Editor {
	public class BatchRename : ScriptableWizard {

		/// <summary>
		/// Base name
		/// </summary>
		[SerializeField]
		private string _baseName = "MyObject_";

		[SerializeField]
		private string _suffix = string.Empty;

		/// <summary>
		/// Start count
		/// </summary>
		[SerializeField]
		private int _startNumber = default;

		/// <summary>
		/// Increment
		/// </summary>
		[SerializeField]
		private int _increment = 1;

		[SerializeField]
		private string _numberFormat = "0";

		[MenuItem("Edit/Batch Rename...")]
		private static void CreateWizard() {
			DisplayWizard("Batch Rename", typeof(BatchRename), "Rename");
		}

		/// <summary>
		/// Called when the window first appears
		/// </summary>
		private void OnEnable() {
			UpdateSelectionHelper();
		}

		/// <summary>
		/// Function called when selection changes in scene
		/// </summary>
		private void OnSelectionChange() {
			UpdateSelectionHelper();
		}

		/// <summary>
		/// Update selection counter
		/// </summary>
		private void UpdateSelectionHelper() {

			helpString = "";

			if (Selection.objects != null)
				helpString = $"Number of objects selected: {Selection.objects.Length}";
		}


		/// <summary>
		/// Rename
		/// </summary>
		private void OnWizardCreate() {

			// If selection is empty, then exit
			if (Selection.objects == null)
				return;

			// Current Increment
			int postFix = _startNumber;

			List<GameObject> mySelection = new List<GameObject>(Selection.gameObjects);
			mySelection.Sort((go1, go2) => go1.transform.GetSiblingIndex().CompareTo(go2.transform.GetSiblingIndex()));

			foreach (GameObject o in mySelection) {
				o.name = $"{_baseName}{(string.IsNullOrWhiteSpace(_numberFormat) ? string.Empty : postFix.ToString(_numberFormat))}{_suffix}";
				postFix += _increment;
			}
		}
	} 
}