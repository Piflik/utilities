﻿#if UNITY_EDITOR
#if UNITY_2018_1_OR_NEWER
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace Utilities.Editor {
	public class AutoCopyFolderPostBuild : IPostprocessBuildWithReport {

		public int callbackOrder => 10;

		public void OnPostprocessBuild(BuildReport report) {

			PostBuildConfig pbc = PostBuildConfig.instance;

			string outputFolder = System.IO.Path.GetDirectoryName(report.summary.outputPath);
			if (pbc.config.copy) CopyFolder(pbc.config.name, outputFolder, pbc.config.overwrite);
			if (pbc.database.copy) CopyFolder(pbc.database.name, outputFolder, pbc.database.overwrite);

			foreach (PostBuildConfig.CopyData folder in pbc.additionalFolders) {
				if (folder.copy) {
					CopyFolder(folder.name, outputFolder, folder.overwrite);
				}
			}

			foreach (PostBuildConfig.CopyData file in pbc.additionalFiles) {
				if (file.copy) {
					CopyFile(file.name, outputFolder, file.overwrite);
				}
			}
		}

		private void CopyFolder(string folderName, string outputFolder, IOUtilities.OverwriteMode overwriteMode) {
			string srcPath = System.IO.Path.Combine(Utils.GetRootPath(), folderName);
			string dstPath = System.IO.Path.Combine(outputFolder, folderName);

			IOUtilities.CopyDirectory(srcPath, dstPath, overwriteMode);
		}

		private void CopyFile(string fileName, string outputFolder, IOUtilities.OverwriteMode overwriteMode) {
			string srcPath = System.IO.Path.Combine(Utils.GetRootPath(), fileName);
			string dstPath = System.IO.Path.Combine(outputFolder, fileName);

			IOUtilities.CopyFile(srcPath, dstPath, overwriteMode);
		}
	}
}
#endif
#endif
