﻿using UnityEditor;

namespace Utilities.Editor {
	[CustomEditor(typeof(VersionContainer))]
	public class VersionContainerInspector : UnityEditor.Editor {
		SerializedProperty _versionOverride;

		void OnEnable() {
			VersionContainer vc = (VersionContainer)target;
			_versionOverride = serializedObject.FindProperty(nameof(vc.versionOverride));
		}

		public override void OnInspectorGUI() {
			VersionContainer vc = (VersionContainer)target;

			EditorGUILayout.LabelField("Version Standalone:", vc.versionStandalone);
			EditorGUILayout.LabelField("Version Android:", vc.versionAndroid);
			EditorGUILayout.LabelField("Version iOS:", vc.versionIOS);

			EditorGUILayout.PropertyField(_versionOverride);
			serializedObject.ApplyModifiedProperties();
		}
	} 
}
