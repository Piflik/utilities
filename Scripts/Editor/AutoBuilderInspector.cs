﻿using UnityEditor;
using UnityEngine;

namespace Utilities.Editor {
	[CustomEditor(typeof(AutoBuilder))]
	public class AutoBuilderInspector : UnityEditor.Editor {
		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			AutoBuilder ab = (AutoBuilder)target;

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Build", GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.6f))) {
				ab.Build(develop: false);
				GUIUtility.ExitGUI();
			}
			if (GUILayout.Button("Build Develop")) {
				ab.Build(develop: true);
				GUIUtility.ExitGUI();
			}
			GUILayout.EndHorizontal();
		}
	}
}