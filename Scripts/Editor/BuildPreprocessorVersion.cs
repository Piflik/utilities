﻿using UnityEditor;
using UnityEditor.Build;
#if UNITY_2018_1_OR_NEWER
using UnityEditor.Build.Reporting;
#endif
// ReSharper disable UnusedMember.Global

namespace Utilities.Editor {
#if UNITY_2018_1_OR_NEWER
	public class AutoVersionBuildPreprocessor : IPreprocessBuildWithReport {
#else
	public class AutoVersionBuildPreprocessor : IPreprocessBuild {
#endif

#if UNITY_2018_1_OR_NEWER
		public void OnPreprocessBuild(BuildReport report) {
			BuildTarget target = report.summary.platform;
#else
		public void OnPreprocessBuild(BuildTarget target, string path) {
#endif
			VersionContainer vc = VersionContainer.instance;

			if (vc == null || AutoBuilder.active) return;

			vc.UpdateVersion(target);
		}

		public int callbackOrder => 0;
	} 
}
