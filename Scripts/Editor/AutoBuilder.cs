﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Utilities.Editor {
	[CreateAssetMenu(fileName = "AutoBuilder", menuName = "AutoBuilder", order = 1)]
	public class AutoBuilder : ScriptableObject {

		[Serializable]
		struct Target {
			public BuildTarget buildTarget;
			public string folderName;
			public string platformDefineSymbols;

			public static implicit operator Target(BuildTarget bTarget) => new Target { buildTarget = bTarget };

			public override string ToString() {
				return string.IsNullOrWhiteSpace(folderName) ? buildTarget.ToString() : folderName;
			}
		}

		[Serializable]
		struct Variant {
			public string appName;
			public Texture2D appIcon;
			public string compilerDefineSymbol;
		}

		[SerializeField]
		[Tooltip("If empty, default path is \"Root-Path/Builds\"")]
		private string _folder = default;

		[SerializeField]
		[Tooltip("If empty, uses folder for non-develop build")]
		private string _devFolder = default;

		[SerializeField]
		[Tooltip("Override Version-Object")]
		private string _version = default;

		[SerializeField]
		private string _globalDefineSymbols = default;

		[SerializeField]
		private Variant[] _variants = new Variant[0];

		[SerializeField]
		private Target[] _buildTargets = new Target[0];

		public static bool active;

		public void Build(bool develop) {

			active = true;

			Target[] buildTargets = _buildTargets.Length > 0 ? _buildTargets : new[] { (Target) EditorUserBuildSettings.activeBuildTarget };
			Variant[] variants = _variants.Length > 0 ? _variants : new[] { default(Variant) };

			BuildTarget currentTarget = EditorUserBuildSettings.activeBuildTarget;

			string folder = develop && !string.IsNullOrWhiteSpace(_devFolder) ? _devFolder : _folder;
			if (string.IsNullOrEmpty(folder)) {
				folder = Utils.CreateAssembledPath(Utils.GetRootPath(), "Builds");
			}

			int currentStep = 0;
			int maxSteps = variants.Length * buildTargets.Length;

			if (!string.IsNullOrEmpty(_version)) {
				PlayerSettings.bundleVersion = _version;
			}

			foreach (Target target in buildTargets) {
				if (string.IsNullOrEmpty(_version)) {
					VersionContainer.instance.UpdateVersion(target.buildTarget);
				}

				BuildPlayerOptions bpo = develop ? new BuildPlayerOptions { target = target.buildTarget, options = BuildOptions.Development} : new BuildPlayerOptions { target = target.buildTarget };

				foreach (Variant variant in variants) {
					EditorUtility.DisplayProgressBar("Building", $"{target}:{variant.compilerDefineSymbol}", (float)currentStep++ / maxSteps);

					if (!variant.appName.IsNullOrWhiteSpace()) {
						PlayerSettings.productName = variant.appName;
					}

					string folderName = variant.appName.IsNullOrWhiteSpace() ? variant.compilerDefineSymbol.IsNullOrWhiteSpace() ? string.Empty : $"_{variant.compilerDefineSymbol.Replace(';', '_')}" : variant.appName;
					bpo.locationPathName = Utils.CreateAssembledPath(folder, string.Format(string.IsNullOrEmpty(folderName) ? "{0}" : "{0}/{1}", target.ToString(), folderName), $"{PlayerSettings.productName}.{GetExtension(target.buildTarget)}");

					if (variant.appIcon != null) {
						Texture2D[] iconSet = Enumerable.Repeat(variant.appIcon, 8).ToArray();
						PlayerSettings.SetIconsForTargetGroup(BuildPipeline.GetBuildTargetGroup(target.buildTarget), iconSet, IconKind.Any);
					}

					PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildPipeline.GetBuildTargetGroup(target.buildTarget), $"{_globalDefineSymbols};{target.platformDefineSymbols};{variant.compilerDefineSymbol}");
					BuildPipeline.BuildPlayer(bpo);
				}

				PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildPipeline.GetBuildTargetGroup(target.buildTarget), _globalDefineSymbols);
			}

			EditorUtility.DisplayProgressBar("Building", "Finalizing", 1);
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildPipeline.GetBuildTargetGroup(currentTarget), currentTarget);

			EditorUtility.ClearProgressBar();

			active = false;
		}

		private static string GetExtension(BuildTarget target) {
			switch (target) {
				case BuildTarget.StandaloneOSX:
					return "app";
				case BuildTarget.StandaloneWindows:
				case BuildTarget.StandaloneWindows64:
					return "exe";
				case BuildTarget.iOS:
					return "ipa";
				case BuildTarget.Android:
					return "apk";
				default:
					return string.Empty;
			}
		}
	}
}
