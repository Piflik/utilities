﻿using System;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Utilities {
	public class PostBuildConfig : ScriptableObject {

		[Serializable]
		public struct CopyData {
			public string name;
			public bool copy;
			public IOUtilities.OverwriteMode overwrite;
		}

		private const string CONFIG_PATH = "Assets/BuildConfig.asset";

#if UNITY_EDITOR
		public static PostBuildConfig instance {
			get {
				PostBuildConfig pbc = Resources.LoadAll<PostBuildConfig>("").FirstOrDefault();
				if (pbc == null) {
					pbc = AssetDatabase.LoadAssetAtPath<PostBuildConfig>(CONFIG_PATH);
				}

				if (pbc == null) {
					pbc = CreateInstance<PostBuildConfig>();
					AssetDatabase.CreateAsset(pbc, CONFIG_PATH);
					AssetDatabase.SaveAssets();
				}

				return pbc;
			}
		}
#endif

		[SerializeField]
		private CopyData _config = new CopyData { name = "Config", copy = true, overwrite = IOUtilities.OverwriteMode.Never };

		[SerializeField] private CopyData _database = new CopyData { name = "Databases", copy = false, overwrite = IOUtilities.OverwriteMode.Never };

		[SerializeField] private CopyData[] _additionalFolders = Array.Empty<CopyData>();

		[SerializeField] private CopyData[] _additionalFiles = Array.Empty<CopyData>();


		public CopyData config => _config;
		public CopyData database => _database;
		public CopyData[] additionalFolders => _additionalFolders;
		public CopyData[] additionalFiles => _additionalFiles;
	}
}