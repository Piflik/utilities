﻿using System;

namespace MathExpressionEngine {
	public abstract class Node {
		public abstract bool TryEvaluate(IContext context, out double result);
	}

	public sealed class NumberNode : Node {
		private readonly double _val;

		public NumberNode(double val) {
			_val = val;
		}

		public override bool TryEvaluate(IContext context, out double result) {
			result = _val;
			return true;
		}
	}

	public sealed class BinaryNode : Node {
		private readonly Node _leftOperand, _rightOperand;
		private readonly Func<double, double, double> _operation;

		public BinaryNode(Node leftOperand, Node rightOperand, Func<double, double, double> operation) {
			_leftOperand = leftOperand;
			_rightOperand = rightOperand;
			_operation = operation;
		}

		public override bool TryEvaluate(IContext context, out double result) {
			if (!_leftOperand.TryEvaluate(context, out double l) || !_rightOperand.TryEvaluate(context, out double r)) {
				result = double.NaN;
				return false;
			}
			result = _operation(l, r);
			return true;
		}
	}

	public sealed class UnaryNode : Node {
		private readonly Node _rightOperand;
		private readonly Func<double, double> _operation;

		public UnaryNode(Node rightOperand, Func<double, double> operation) {
			_rightOperand = rightOperand;
			_operation = operation;
		}

		public override bool TryEvaluate(IContext context, out double result) {
			if (!_rightOperand.TryEvaluate(context, out result)) {
				return false;
			}
			result = _operation(result);
			return true;
		}
	}

	public sealed class VariableNode : Node {
		private readonly string _name;

		public VariableNode(string name) {
			_name = name;
		}

		public override bool TryEvaluate(IContext context, out double result) {
			return context.TryResolveVariable(_name, out result);
		}
	}

	public sealed class FunctionNode : Node {
		private readonly string _name;
		private readonly Node[] _arguments;

		public FunctionNode(string name, Node[] arguments) {
			_name = name;
			_arguments = arguments;
		}

		public override bool TryEvaluate(IContext context, out double result) {
			double[] args = new double[_arguments.Length];
			for (int i = 0; i < _arguments.Length; i++) {
				if (!_arguments[i].TryEvaluate(context, out double d)) {
					result = double.NaN;
					return false;
				}
				args[i] = d;
			}
			return context.TryCallFunction(_name, args, out result);
		}
	}
}
