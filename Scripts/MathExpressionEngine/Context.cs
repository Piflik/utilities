﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
// ReSharper disable UnusedMember.Global

namespace MathExpressionEngine {
	public interface IContext {
		bool TryResolveVariable(string name, out double result);
		bool TryCallFunction(string name, double[] arguments, out double result);
	}

	public sealed class ReflectionContext : IContext {
		private readonly object _targetObject;

		public ReflectionContext(object targetObject) {
			_targetObject = targetObject;
		}

		public bool TryResolveVariable(string name, out double result) {
			PropertyInfo info = _targetObject.GetType().GetProperty(name);
			if (info == null) {
				Utilities.Debug.LogError($"Could not resolve variable {name}");
				result = double.NaN;
				return false;
			}
			result = (double)info.GetValue(_targetObject);
			return true;
		}

		public bool TryCallFunction(string name, double[] arguments, out double result) {
			MethodInfo info = _targetObject.GetType().GetMethod(name);
			if (info == null) {
				Utilities.Debug.LogError($"Could not resolve method {name}");
				result = double.NaN;
				return false;
			}

			try {
				result = (double)info.Invoke(_targetObject, arguments.Select(x => (object)x).ToArray());
			} catch (Exception e) {
				Utilities.Debug.LogError($"Could not call method {name}.\n{e.Message}");
				result = double.NaN;
				return false;
			}
			return true;
		}
	}

	public sealed class DefaultContext : IContext {
		private const double RAD_2_DEG = 180 / Math.PI;
		private const double DEG_2_RAD = Math.PI / 180;

		private readonly Dictionary<string, double> _variables = new Dictionary<string, double>();

		private static DefaultContext _instance;
		public static DefaultContext instance => _instance ?? (_instance = new DefaultContext());

		private DefaultContext() { }

		public void DefineVariable(string identifier, double value) {
			_variables[identifier] = value;
		}

		public void ClearVariables() {
			_variables.Clear();
		}

		public bool TryResolveVariable(string name, out double result) {
			switch (name.ToLower()) {
				case "pi":
					result = Math.PI;
					return true;
				case "e":
					result = Math.E;
					return true;
				case "rad2deg":
					result = RAD_2_DEG;
					return true;
				case "deg2rad":
					result = DEG_2_RAD;
					return true;
			}

			if (!_variables.TryGetValue(name, out result)) {
				Utilities.Debug.LogError($"Could not resolve variable {name}");
				result = double.NaN;
				return false;
			}

			return true;
		}

		public bool TryCallFunction(string name, double[] arguments, out double result) {
			int args = arguments.Length;
			int needed = -1;
			bool atLeast = false;
			switch (name.ToLower()) {
				case "sin":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Sin(arguments[0]);
					return true;
				case "asin":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Asin(arguments[0]);
					return true;
				case "cos":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Cos(arguments[0]);
					return true;
				case "acos":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Acos(arguments[0]);
					return true;
				case "tan":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Tan(arguments[0]);
					return true;
				case "atan":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Atan(arguments[0]);
					return true;
				case "atan2":
					if (args != 2) {
						needed = 2;
						break;
					}
					result = Math.Atan2(arguments[0], arguments[1]);
					return true;
				case "abs":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Abs(arguments[0]);
					return true;
				case "exp":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Exp(arguments[0]);
					return true;
				case "pow":
					if (args != 2) {
						needed = 2;
						break;
					}
					result = Math.Pow(arguments[0], arguments[1]);
					return true;
				case "log":
					switch (args) {
						case 1:
							result = Math.Log(arguments[0]);
							return true;
						case 2:
							result = Math.Log(arguments[0], arguments[1]);
							return true;
						default:
							needed = 1;
							atLeast = true;
							break;
					}
					break;
				case "lg":
				case "log10":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Log10(arguments[0]);
					return true;
				case "ln":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Log(arguments[0]);
					return true;
				case "ld":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Log(arguments[0], 2);
					return true;
				case "round":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Round(arguments[0]);
					return true;
				case "ceil":
				case "ceiling":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Ceiling(arguments[0]);
					return true;
				case "floor":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Floor(arguments[0]);
					return true;
				case "min":
					if (args < 2) {
						needed = 2;
						atLeast = true;
						break;
					}
					result = arguments[0];
					for (int i = 1; i < args; i++) {
						result = Math.Min(result, arguments[i]);
					}
					return true;
				case "max":
					if (args < 2) {
						needed = 2;
						atLeast = true;
						break;
					}
					result = arguments[0];
					for (int i = 1; i < args; i++) {
						result = Math.Max(result, arguments[i]);
					}
					return true;
				case "clamp":
					if (args < 3) {
						needed = 3;
						break;
					}

					result = arguments[0].Between(arguments[1], arguments[2]);
					
					return true;
				case "sign":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Sign(arguments[0]);
					return true;
				case "sqrt":
					if (args != 1) {
						needed = 1;
						break;
					}
					result = Math.Sqrt(arguments[0]);
					return true;
				default:
					Utilities.Debug.LogError($"Could not resolve method {name}");
					break;
			}

			if (needed != -1) {
				LogArgumentsError(name, needed, args, atLeast);
			}

			result = double.NaN;
			return false;
		}

		private static void LogArgumentsError(string functionName, int needed, int provided, bool atLeast) {
			Utilities.Debug.LogError($"Function {functionName} needs {(atLeast ? "at least " : string.Empty)}{needed} argument{(needed != 1 ? "s" : string.Empty)}, but received {provided}.");
		}
	}
}
