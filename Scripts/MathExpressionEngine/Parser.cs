﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MathExpressionEngine {
	public static class MathExpressionParser {
		public static double Evaluate(string expression) {
			return Evaluate(expression, DefaultContext.instance);
		}

		public static double Evaluate(string expression, IContext context) {
			return Evaluate(new Tokenizer(new StringReader(expression)), context);
		}

		public static double Evaluate(ITokenizer tokenizer, IContext context) {
			if (!TryEvaluate(tokenizer, context, out double result))
				throw new InvalidDataException("Could not evaluate string.");
			return result;
		}

		public static bool TryEvaluate(string expression, out double result) {
			return TryEvaluate(expression, DefaultContext.instance, out result);
		}

		public static bool TryEvaluate(string expression, IContext context, out double result) {
			return TryEvaluate(new Tokenizer(new StringReader(expression)), context, out result);
		}

		public static bool TryEvaluate(ITokenizer tokenizer, IContext context, out double result) {
			if (!TryParse(tokenizer, out Node n)) {
				Utilities.Debug.LogError("Could not parse string.");
				result = double.NaN;
				return false;
			}

			return n.TryEvaluate(context, out result);
		}

		public static bool TryParse(string expression, out Node node) {
			Tokenizer tokenizer = new Tokenizer(new StringReader(expression));
			return TryParse(tokenizer, out node) && tokenizer.token == Token.EOF;
		}

		private static bool TryParse(ITokenizer tokenizer, out Node node) {
			return TryParseAddSubstract(tokenizer, out node) && tokenizer.token == Token.EOF;
		}

		private static bool TryParseAddSubstract(ITokenizer tokenizer, out Node node) {
			if (!TryParseMultiplyDivide(tokenizer, out node)) {
				return false;
			}

			while (true) {
				Func<double, double, double> operation = null;
				switch (tokenizer.token) {
					case Token.Add:
						operation = (a, b) => a + b;
						break;
					case Token.Substract:
						operation = (a, b) => a - b;
						break;
				}

				if (operation == null) {
					return true;
				}

				tokenizer.ReadNextToken();
				if (!TryParseMultiplyDivide(tokenizer, out Node right)) {
					return false;
				}
				node = new BinaryNode(node, right, operation);
			}
		}

		private static bool TryParseMultiplyDivide(ITokenizer tokenizer, out Node node) {
			if (!TryParseUnary(tokenizer, out node)) {
				return false;
			}

			while (true) {
				Func<double, double, double> operation = null;
				switch (tokenizer.token) {
					case Token.Multiply:
						operation = (a, b) => a * b;
						break;
					case Token.Divide:
						operation = (a, b) => a / b;
						break;
					case Token.Modulo:
						operation = (a, b) => a % b;
						break;
				}

				if (operation == null) {
					return true;
				}

				tokenizer.ReadNextToken();
				if (!TryParseUnary(tokenizer, out Node right)) {
					return false;
				}
				node = new BinaryNode(node, right, operation);
			}
		}

		private static bool TryParseUnary(ITokenizer tokenizer, out Node node) {
			switch (tokenizer.token) {
				case Token.Add:
					tokenizer.ReadNextToken();
					return TryParseUnary(tokenizer, out node);
				case Token.Substract:
					tokenizer.ReadNextToken();
					if (!TryParseUnary(tokenizer, out node)) {
						return false;
					}
					node = new UnaryNode(node, val => -val);
					return true;
			}

			return TryParseLeaf(tokenizer, out node);
		}

		private static bool TryParseLeaf(ITokenizer tokenizer, out Node node) {
			switch (tokenizer.token) {
				case Token.Number:
					node = new NumberNode(tokenizer.number);
					tokenizer.ReadNextToken();
					return true;
				case Token.OpeningParenthesis:
					tokenizer.ReadNextToken();
					if (!TryParseAddSubstract(tokenizer, out node)) {
						return false;
					}
					if (tokenizer.token != Token.ClosingParenthesis) {
						Utilities.Debug.LogError("Missing a closing parenthesis");
						return false;
					}
					tokenizer.ReadNextToken();
					return true;
				case Token.Identifier:
					string name = tokenizer.identifier;
					tokenizer.ReadNextToken();
					if (tokenizer.token != Token.OpeningParenthesis) {
						node = new VariableNode(name);
						return true;
					}

					tokenizer.ReadNextToken();
					List<Node> arguments = new List<Node>();
					while (true) {
						if (!TryParseAddSubstract(tokenizer, out Node arg)) {
							node = null;
							return false;
						}
						arguments.Add(arg);
						if (tokenizer.token == Token.Comma) {
							tokenizer.ReadNextToken();
							continue;
						}
						break;
					}

					if (tokenizer.token != Token.ClosingParenthesis) {
						Utilities.Debug.LogError("Missing a closing parenthesis");
						node = null;
						return false;
					}

					tokenizer.ReadNextToken();
					node = new FunctionNode(name, arguments.ToArray());
					return true;
			}

			Utilities.Debug.LogError($"Cannot parse token {tokenizer.token} to a number expression");
			node = null;
			return false;
		}
	}
}
