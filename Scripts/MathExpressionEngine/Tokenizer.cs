﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace MathExpressionEngine {
	public enum Token {
		Undefined,
		EOF,
		Number,
		Add,
		Substract,
		Multiply,
		Divide,
		Modulo,
		OpeningParenthesis,
		ClosingParenthesis,
		Identifier,
		Comma,
	}

	public interface ITokenizer {
		Token token { get; }
		double number { get; }
		string identifier { get; }

		void ReadNextToken();
	}

	public sealed class Tokenizer : ITokenizer {
		public Tokenizer(TextReader reader) {
			_reader = reader ?? throw new ArgumentNullException();
			_currentChar = ReadChar();
			ReadNextToken();
			if (token == Token.Undefined) {
				throw new InvalidDataException($"Could not parse character {_currentChar} into a valid token.");
			}
		}

		private readonly TextReader _reader;
		private char _currentChar;

		public Token token { get; private set; }
		public double number { get; private set; }
		public string identifier { get; private set; }

		public void ReadNextToken() {
			token = ReadToken();
		}

		private char ReadChar() {
			int character = _reader.Read();
			return character < 0 ? '\0' : (char)character;
		}

		private Token ReadToken() {
			while (char.IsWhiteSpace(_currentChar)) {
				_currentChar = ReadChar();
			}

			switch (_currentChar) {
				case '\0':
					return Token.EOF;
				case '+':
					_currentChar = ReadChar();
					return Token.Add;
				case '-':
					_currentChar = ReadChar();
					return Token.Substract;
				case '*':
					_currentChar = ReadChar();
					return Token.Multiply;
				case '/':
					_currentChar = ReadChar();
					return Token.Divide;
				case '%':
					_currentChar = ReadChar();
					return Token.Modulo;
				case '(':
					_currentChar = ReadChar();
					return Token.OpeningParenthesis;
				case ')':
					_currentChar = ReadChar();
					return Token.ClosingParenthesis;
				case ',':
					_currentChar = ReadChar();
					return Token.Comma;
			}

			if (char.IsDigit(_currentChar) || _currentChar == '.') {
				StringBuilder sb = new StringBuilder();
				bool foundPoint = false;
				while (char.IsDigit(_currentChar) || !foundPoint && _currentChar == '.') {
					sb.Append(_currentChar);
					foundPoint = _currentChar == '.';
					_currentChar = ReadChar();
				}

				number = double.Parse(sb.ToString(), CultureInfo.InvariantCulture);
				return Token.Number;
			}

			if (char.IsLetter(_currentChar) || _currentChar == '_') {
				StringBuilder sb = new StringBuilder();
				while (char.IsLetterOrDigit(_currentChar)) {
					sb.Append(_currentChar);
					_currentChar = ReadChar();
				}

				identifier = sb.ToString();
				return Token.Identifier;
			}

			return Token.Undefined;
		}
	}
}
