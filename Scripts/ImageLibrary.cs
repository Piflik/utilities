﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using System.Collections;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public class ImageLibrary : ScriptableObject, IEnumerable<KeyValuePair<string, Sprite>> {
		private static readonly Dictionary<string, ImageLibrary> _instances = new Dictionary<string, ImageLibrary>(StringComparer.OrdinalIgnoreCase);

		private static ImageLibrary _dLib;

		private static ImageLibrary _defaultLib {
			get {
				if (_dLib == null) _dLib = CreateInstance(typeof(ImageLibrary)) as ImageLibrary;
				return _dLib;
			}
		}

		private Dictionary<string, Sprite> _lib = new Dictionary<string, Sprite>(StringComparer.OrdinalIgnoreCase);
		private string _path;

		public int count => _lib.Count;

		public static ImageLibrary GetInstance(string path, bool init = true) {
			if (!_instances.TryGetValue(path, out ImageLibrary lib)) {
				lib = CreateInstance(typeof(ImageLibrary)) as ImageLibrary;

				if (init && lib != null) {
					lib.Init(path);
				}

				_instances[path] = lib;
			}

			return lib;
		}

		public Sprite this[string path] {
			get {
				if (!string.IsNullOrEmpty(path) && _lib.ContainsKey(path)) return _lib[path];
				return null;
			}
			set => _lib[path] = value;
		}

		public void Init(string path) {
			Sprite[] sprites = Resources.LoadAll<Sprite>(path);
			_path = path;
			_lib = sprites.DistinctBy(s => s.name.ToLower()).ToDictionary(s => s.name, s => s, StringComparer.OrdinalIgnoreCase);
		}

		public Sprite GetSprite(string key, string fallback = null) {
			if (!string.IsNullOrEmpty(key)
				&& _lib.TryGetValue(key, out Sprite s)
				|| !string.IsNullOrEmpty(fallback)
				&& _lib.TryGetValue(fallback, out s)) return s;

			return null;
		}

		public void SetSprite(string key, Sprite sprite) {
			_lib[key] = sprite;
		}

		public string[] GetKeys() {
			return _lib.Keys.ToArray();
		}

		public static Sprite GetSingleSprite(string path) {
			return string.IsNullOrEmpty(path) ? null : GetInstance(path).FirstOrDefault().Value;
		}

		public static void SetSpriteID(string id, Sprite sprite) {
			_defaultLib.SetSprite(id, sprite);
		}

		public static bool TryGetSprite(string id, out Sprite sprite) {
			return _defaultLib._lib.TryGetValue(id, out sprite);
		}

		public IEnumerator<KeyValuePair<string, Sprite>> GetEnumerator() {
			foreach (KeyValuePair<string, Sprite> pair in _lib) {
				yield return pair;
			}
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}

		public void Clear() {
			_lib.Clear();
			_instances.Remove(_path);
		}

		public static void Clear(string path) {
			if (_instances.TryGetValue(path, out ImageLibrary lib)) {
				lib.Clear();
			}
		}

		public static void ClearDefaultLibrary() {
			if (_dLib != null) {
				_dLib._lib.Clear();
			}
		}

		public static void ClearAll() {
			foreach (ImageLibrary lib in _instances.Values) {
				lib.Clear();
			}

			_instances.Clear();
		}
	}
}
