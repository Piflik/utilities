﻿using UnityEngine;

namespace Utilities {
	public class MouseLook : MonoBehaviour {

		[SerializeField]
		float _verticalAngleMin = -85;

		[SerializeField]
		float _verticalAngleMax = 85;

		[SerializeField]
		float _mouseSensitivity = 15;

		float _rotationX;
		float _rotationY;
		Quaternion _rotation;

		// Use this for initialization
		void Start() {
			//_rotation = transform.localRotation;
			_rotation = Quaternion.identity;
		}

		// Update is called once per frame
		void Update() {

			_rotationX += Input.GetAxis("Mouse X") * _mouseSensitivity;
			_rotationY += Input.GetAxis("Mouse Y") * _mouseSensitivity;

			_rotationX = ClampAngle(_rotationX, -360f, 360f);
			_rotationY = ClampAngle(_rotationY, _verticalAngleMin, _verticalAngleMax);

			Quaternion xQuat = Quaternion.AngleAxis(_rotationX, Vector3.up);
			Quaternion yQuat = Quaternion.AngleAxis(_rotationY, -Vector3.right);

			transform.localRotation = _rotation * xQuat * yQuat;
		}

		public static float ClampAngle(float angle, float min, float max) {
			if (angle < -360f)
				angle += 360f;
			if (angle > 360f)
				angle -= 360f;
			return Mathf.Clamp(angle, min, max);
		}

		public void SetRotation(Quaternion q) {
			Vector3 euler = q.eulerAngles;

			_rotationX = euler.y;
			_rotationY = 0;

			Quaternion xQuat = Quaternion.AngleAxis(_rotationX, Vector3.up);
			Quaternion yQuat = Quaternion.AngleAxis(_rotationY, -Vector3.right);

			transform.localRotation = _rotation * xQuat * yQuat;
		}
	}
}