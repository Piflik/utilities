﻿using System;
using UnityEngine;

namespace Utilities {
	public static class CatmullRom {
		public static Vector3 Interpolate(Vector3 start, Vector3 end, Vector3 ctrlStart, Vector3 ctrlEnd, float t) {

			return start + 0.5f * t * (end - ctrlStart + t * (2 * ctrlStart - 5 * start + 4 * end - ctrlEnd + t * (3 * (start - end) + ctrlEnd - ctrlStart)));
		}

		public static Vector2 Interpolate(Vector2 start, Vector2 end, Vector2 ctrlStart, Vector2 ctrlEnd, float t) {

			return start + 0.5f * t * (end - ctrlStart + t * (2 * ctrlStart - 5 * start + 4 * end - ctrlEnd + t * (3 * (start - end) + ctrlEnd - ctrlStart)));
		}

		public static float Length(Vector3 start, Vector3 end, Vector3 outStart, Vector3 outEnd, int steps) {
			float length = 0;

			Vector3 fst = start;
			for (int i = 1; i <= steps; ++i) {
				Vector3 snd = Interpolate(start, end, outStart, outEnd, (float)i / steps);
				length += (snd - fst).magnitude;

				fst = snd;
			}

			return length;
		}

		public static float Length(Vector2 start, Vector2 end, Vector2 outStart, Vector2 outEnd, int steps) {
			float length = 0;

			Vector2 fst = start;
			for (int i = 1; i <= steps; ++i) {
				Vector2 snd = Interpolate(start, end, outStart, outEnd, (float)i / steps);
				length += (snd - fst).magnitude;

				fst = snd;
			}

			return length;
		}
	}

	public static class Bezier {

		public enum Type {
			Linear,
			Quadratic,
			Cubic,
		}

		public static Vector3 Interpolate(float t, Type interpolationType = Type.Cubic, params Vector3[] points) {
			switch (interpolationType) {
				case Type.Linear when points.Length >= 2:
					return (1 - t) * points[0] + t * points[1];
				case Type.Quadratic when points.Length >= 3:
					return (1 - t) * (1 - t) * points[0] + 2 * (1 - t) * t * points[1] + t * t * points[2];
				case Type.Cubic when points.Length >= 4:
					return (1 - t) * (1 - t) * (1 - t) * points[0] + 3 * (1 - t) * (1 - t) * t * points[1] + 3 * (1 - t) * t * t * points[2] + t * t * t * points[3];
				default:
					throw new ArgumentException($"Not enough points for interpolation type {interpolationType}");
			}
		}

		public static Vector2 Interpolate(float t, Type interpolationType = Type.Cubic, params Vector2[] points) {
			switch (interpolationType) {
				case Type.Linear when points.Length >= 2:
					return (1 - t) * points[0] + t * points[1];
				case Type.Quadratic when points.Length >= 3:
					return (1 - t) * (1 - t) * points[0] + 2 * (1 - t) * t * points[1] + t * t * points[2];
				case Type.Cubic when points.Length >= 4:
					return (1 - t) * (1 - t) * (1 - t) * points[0] + 3 * (1 - t) * (1 - t) * t * points[1] + 3 * (1 - t) * t * t * points[2] + t * t * t * points[3];
				default:
					throw new ArgumentException($"Not enough points for interpolation type {interpolationType}");
			}
		}

		public static float Length(int steps, Type interpolationType = Type.Cubic, params Vector3[] points) {
			float length = 0;

			if (points.Length > 0) {
				Vector3 fst = points[0];
				for (int i = 1; i <= steps; ++i) {
					Vector3 snd = Interpolate((float)i / steps, interpolationType, points);
					length += (snd - fst).magnitude;

					fst = snd;
				}
			}

			return length;
		}

		public static float Length(int steps, Type interpolationType = Type.Cubic, params Vector2[] points) {
			float length = 0;

			if (points.Length > 0) {
				Vector2 fst = points[0];
				for (int i = 1; i <= steps; ++i) {
					Vector2 snd = Interpolate((float)i / steps, interpolationType, points);
					length += (snd - fst).magnitude;

					fst = snd;
				}
			}

			return length;
		}
	}
}
