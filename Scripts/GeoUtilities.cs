﻿using System;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static partial class Geo {
		public const double RADIUS_EARTH = 6378137;
		private const double RAD_TO_DEGREE = 180 / Math.PI;

		/// <summary>
		/// Converts Latitude and Longitude to Mercator; adapted from http://cholla.mmto.org/gtopo/reference/utm/convert.html
		/// </summary>
		public static void LatLongToMercat(double lat, double lng, out double mercX, out double mercY, out int zoneNumber, out char zoneLetter) {

			//Make sure the longitude is between -180.00 .. 179.9
			double longTemp = lng + 180 - (int)((lng + 180) / 360) * 360 - 180;

			double latRad = lat * Mathf.Deg2Rad;
			double longRad = longTemp * Mathf.Deg2Rad;

			zoneLetter = MercatLetterDesignator(lat);
			zoneNumber = (int)((longTemp + 180) / 6) + 1;

			if (lat >= 56.0 && lat < 64.0 && longTemp >= 3.0 && longTemp < 12.0)
				zoneNumber = 32;

			// Special zones for Svalbard
			if (lat >= 72.0 && lat < 84.0) {
				if (longTemp >= 0.0 && longTemp < 9.0) zoneNumber = 31;
				else if (longTemp >= 9.0 && longTemp < 21.0) zoneNumber = 33;
				else if (longTemp >= 21.0 && longTemp < 33.0) zoneNumber = 35;
				else if (longTemp >= 33.0 && longTemp < 42.0) zoneNumber = 37;
			}

			LatLongToMercatInternal(latRad, longRad, zoneNumber, zoneLetter, out mercX, out mercY);
		}

		public static void LatLongToMercatReproject(double lat, double lng, int zoneNumber, char zoneLetter, out double mercX, out double mercY) {
			double latRad = lat * Mathf.Deg2Rad;
			double longRad = (lng + 180 - (int)((lng + 180) / 360) * 360 - 180) * Mathf.Deg2Rad;

			LatLongToMercatInternal(latRad, longRad, zoneNumber, zoneLetter, out mercX, out mercY);
		}

		private static void LatLongToMercatInternal(double latRad, double longRad, int zoneNumber, char zoneLetter, out double mercX, out double mercY) {
			const double radius = 6378137;
			const double eccSquared = 0.00669438;
			const double k0 = 0.9996;

			double longOrigin = (zoneNumber - 1) * 6 - 180 + 3;
			double longOriginRad = longOrigin * Mathf.Deg2Rad;

			const double eccPrimeSquared = eccSquared / (1 - eccSquared);

			double n = radius / Math.Sqrt(1 - eccSquared * Math.Sin(latRad) * Math.Sin(latRad));
			double t = Math.Tan(latRad) * Math.Tan(latRad);
			double c = eccPrimeSquared * Math.Cos(latRad) * Math.Cos(latRad);
			double a = Math.Cos(latRad) * (longRad - longOriginRad);

			double m = radius * ((1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5 * eccSquared * eccSquared * eccSquared / 256) * latRad
								- (3 * eccSquared / 8 + 3 * eccSquared * eccSquared / 32 + 45 * eccSquared * eccSquared * eccSquared / 1024) * Math.Sin(2 * latRad)
								+ (15 * eccSquared * eccSquared / 256 + 45 * eccSquared * eccSquared * eccSquared / 1024) * Math.Sin(4 * latRad)
								- 35 * eccSquared * eccSquared * eccSquared / 3072 * Math.Sin(6 * latRad));

			mercX = k0 * n * (a + (1 - t + c) * a * a * a / 6 + (5 - 18 * t + t * t + 72 * c - 58 * eccPrimeSquared) * a * a * a * a * a / 120) + 500000.0;
			mercY = k0 * (m + n * Math.Tan(latRad) * (a * a / 2 + (5 - t + 9 * c + 4 * c * c) * a * a * a * a / 24 + (61 - 58 * t + t * t + 600 * c - 330 * eccPrimeSquared) * a * a * a * a * a * a / 720));
			if (zoneLetter - 'N' < 0) {
				mercY += 10000000.0; //10000000 meter offset for southern hemisphere
			}
		}

		public static void MercatToLatLong(double mercX, double mercY, int zoneNumber, char zoneLetter, out double lat, out double lng) {

			const double radius = 6378137;
			const double eccSquared = 0.00669438;
			const double k0 = 0.9996;

			double e1 = (1 - Math.Sqrt(1 - eccSquared)) / (1 + Math.Sqrt(1 - eccSquared));

			double x = mercX - 500000.0;
			double y = mercY;

			if (zoneLetter - 'N' < 0) {
				y -= 10000000.0;//remove 10,000,000 meter offset used for southern hemisphere
			}

			double longOrigin = (zoneNumber - 1) * 6 - 180 + 3;

			double eccPrimeSquared = eccSquared / (1 - eccSquared);

			double m = y / k0;
			double mu = m / (radius * (1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5 * eccSquared * eccSquared * eccSquared / 256));

			double phi1Rad = mu + (3 * e1 / 2 - 27 * e1 * e1 * e1 / 32) * Math.Sin(2 * mu)
								+ (21 * e1 * e1 / 16 - 55 * e1 * e1 * e1 * e1 / 32) * Math.Sin(4 * mu)
								+ 151 * e1 * e1 * e1 / 96 * Math.Sin(6 * mu);

			double n = radius / Math.Sqrt(1 - eccSquared * Math.Sin(phi1Rad) * Math.Sin(phi1Rad));
			double t = Math.Tan(phi1Rad) * Math.Tan(phi1Rad);
			double c = eccPrimeSquared * Math.Cos(phi1Rad) * Math.Cos(phi1Rad);
			double r = radius * (1 - eccSquared) / Math.Pow(1 - eccSquared * Math.Sin(phi1Rad) * Math.Sin(phi1Rad), 1.5);
			double d = x / (n * k0);

			lat = phi1Rad - n * Math.Tan(phi1Rad) / r * (d * d / 2 - (5 + 3 * t + 10 * c - 4 * c * c - 9 * eccPrimeSquared) * d * d * d * d / 24
														+ (61 + 90 * t + 298 * c + 45 * t * t - 252 * eccPrimeSquared - 3 * c * c) * d * d * d * d * d * d / 720);
			lat = lat * Mathf.Rad2Deg;

			lng = (d - (1 + 2 * t + c) * d * d * d / 6 + (5 - 2 * c + 28 * t - 3 * c * c + 8 * eccPrimeSquared + 24 * t * t)
							* d * d * d * d * d / 120) / Math.Cos(phi1Rad);
			lng = longOrigin + lng * Mathf.Rad2Deg;
		}

		static char MercatLetterDesignator(double lat) {

			// 'Z' is an error flag, the Latitude is outside the UTM limits
			if (lat > 84 || lat < -80) return 'Z';

			if (lat >= 72) return 'X';
			if (lat >= 64) return 'W';
			if (lat >= 56) return 'V';
			if (lat >= 48) return 'U';
			if (lat >= 40) return 'T';
			if (lat >= 32) return 'S';
			if (lat >= 24) return 'R';
			if (lat >= 16) return 'Q';
			if (lat >= 8) return 'P';
			if (lat >= 0) return 'N';
			if (lat >= -8) return 'M';
			if (lat >= -16) return 'L';
			if (lat >= -24) return 'K';
			if (lat >= -32) return 'J';
			if (lat >= -40) return 'H';
			if (lat >= -48) return 'G';
			if (lat >= -56) return 'F';
			if (lat >= -64) return 'E';
			if (lat >= -72) return 'D';
			return 'C';
		}

		public class Converter {
			private readonly double _x0, _y0;
			private readonly int _zoneNumber;
			private readonly char _zoneLetter;

			public Converter(double lat0, double long0) {
				LatLongToMercat(lat0, long0, out _x0, out _y0, out _zoneNumber, out _zoneLetter);
			}

			public void ToGeo(double x, double y, out double lat, out double lng) {
				x += _x0;
				y += _y0;
				MercatToLatLong(x, y, _zoneNumber, _zoneLetter, out lat, out lng);
			}

			public void ToMercat(double lat, double lng, out double x, out double y) {
				LatLongToMercat(lat, lng, out x, out y, out _, out _);
				x -= _x0;
				y -= _y0;
			}
		}

		[Obsolete("Doesn't seem to work correctly")]
		public class PositionConverter {
			private readonly double _latOrigin;
			private readonly double _longOrigin;
			private readonly UnityEngine.Quaternion _rot;
			private readonly double _lonOffsetDivisor;

			public static PositionConverter Create(double latitudeOrigin, double longitudeOrigin, float rotationOffset) {
				PositionConverter converter = new PositionConverter(latitudeOrigin, longitudeOrigin, rotationOffset);
				return converter._lonOffsetDivisor.Approximates(0, 0.001) ? null : converter;
			}

			private PositionConverter(double latitudeOrigin, double longitudeOrigin, float rotationOffset) {
				_latOrigin = latitudeOrigin;
				_longOrigin = longitudeOrigin;
				_rot = UnityEngine.Quaternion.Euler(0, -rotationOffset, 0);
				_lonOffsetDivisor = RADIUS_EARTH * Math.Cos(Math.PI * _latOrigin / 180);
			}

			public void Convert(float x, float z, out double latitude, out double longitude) {
				Vector3 pos = new Vector3(x, 0, z);
				Convert(pos, out latitude, out longitude);
			}

			public void Convert(Vector3 pos, out double latitude, out double longitude) {
				pos = _rot * pos;
				double dLat = pos.x / RADIUS_EARTH * RAD_TO_DEGREE;
				double dLong = pos.z / _lonOffsetDivisor * RAD_TO_DEGREE;
				latitude = _latOrigin + dLat;
				longitude = _longOrigin + dLong;
			}
		}
	}
}
