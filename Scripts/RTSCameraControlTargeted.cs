﻿using System.Collections;
using UnityEngine;

namespace Utilities {
	public class RTSCameraControlTargeted : MonoBehaviour {
		private Camera _cam;
		private Camera _camera { get { return _cam ?? (_cam = GetComponentInChildren<Camera>()); } }

		private static RTSCameraControlTargeted _instance;
		public static RTSCameraControlTargeted instance { get { return _instance ?? (_instance = Camera.main.gameObject.AddComponent<RTSCameraControlTargeted>()); } }

		[SerializeField]
		[Range(0, 1)]
		private float _panSpeed = 1;
		private const float PAN_SPEED_MULTIPLIER = 25;
		private const float PAN_SEEP_MULTIPLIER_MOUSE = 5;

		[SerializeField]
		[Range(0, 1)]
		private float _rotateSpeed = 1;
		private const float ROTATE_SPEED_MULTIPLIER = 100;
		private const float ROTATE_SPEED_MULTIPLIER_MOUSE = 250;
		private const float MIN_ROTATION = 0.5f;
		private const float MAX_ROTATION = 85;

		[SerializeField]
		[Range(0, 1)]
		private float _zoomSpeed = 1;
		private const float ZOOM_SPEED_MULTIPLIER = 1000;
		private const float MIN_ZOOM = -25;
		private const float MAX_ZOOM = -0.5f;

		private Vector3 _desiredPosition;
		private Vector3 _desiredRotation;

		void Awake() {
			if (_instance != null && _instance != this) {
				Destroy(gameObject);
			} else {
				_instance = this;
				_camPos = _camera.transform.localPosition;
				_desiredPosition = transform.position;
				_desiredRotation = transform.rotation.eulerAngles;
			}
		}

		void Update() {

			Move(Input.GetAxis("Horizontal") * transform.right + Input.GetAxis("Vertical") * Vector3.Cross(transform.right, Vector3.up), _panSpeed * Time.deltaTime * PAN_SPEED_MULTIPLIER);
			Zoom(Input.GetAxis("Zoom") * _zoomSpeed * Time.deltaTime * ZOOM_SPEED_MULTIPLIER);
			Rotate(0, Input.GetAxisRaw("Rotate") * Time.deltaTime * _rotateSpeed * ROTATE_SPEED_MULTIPLIER);

			//TODO fix mouse Pan/Rotate to always keep cursor over same point in space
			if (Input.GetButton("Pan")) {
				if (Input.GetButton("Modify")) {
					Rotate(Input.GetAxis("Mouse Y") * Time.deltaTime * _rotateSpeed * ROTATE_SPEED_MULTIPLIER_MOUSE, -Input.GetAxis("Mouse X") * Time.deltaTime * _rotateSpeed * ROTATE_SPEED_MULTIPLIER_MOUSE);
				} else {
					Move(Input.GetAxis("Mouse X") * transform.right + Input.GetAxis("Mouse Y") * Vector3.Cross(transform.right, Vector3.up), Time.deltaTime * PAN_SEEP_MULTIPLIER_MOUSE * (-_camPos.z));
				}
			}

			ApplyTransformation();
		}

		private void Move(Vector3 dir, float speed) {
			_desiredPosition += speed * dir;
		}

		private Vector3 _camPos;
		private void Zoom(float amount) {
			_camPos.z = Mathf.Clamp(_camPos.z + amount, MIN_ZOOM, MAX_ZOOM);
		}

		private void Rotate(float x, float y) {

			_desiredRotation.x = Mathf.Clamp(_desiredRotation.x + x, MIN_ROTATION, MAX_ROTATION);
			_desiredRotation.y += y;
		}

		private void ApplyTransformation() {
			transform.position = _desiredPosition;
			transform.rotation = Quaternion.Euler(_desiredRotation);

			_camera.transform.localPosition = _camPos + _camDisplacement;
		}

		private bool _shaking = false;
		public void Shake(float strength, float duration) {
			if (_shaking) return;
			StartCoroutine(CamShake(strength, duration));
		}

		private Vector3 _camDisplacement = Vector3.zero;
		private IEnumerator CamShake(float strength, float duration) {
			_shaking = true;
			float timer = 0;

			while (timer < duration) {

				float displacement = FloatUtilities.Gaussian(timer, strength, 0, duration * 0.3f);
				_camDisplacement = displacement * Random.onUnitSphere;

				timer += Time.deltaTime;
				yield return null;
			}

			_camDisplacement = Vector3.zero;
			_shaking = false;
		}

		public void MoveToPosition(Vector3 position, float time) {
			StartCoroutine(CamMove(position, time));
		}

		private IEnumerator CamMove(Vector3 position, float time) {
			Vector4 oldPos = transform.position;
			float timer = 0;

			while (timer < time) {
				timer += Time.deltaTime;

				_desiredPosition = Vector3.Slerp(oldPos, position, timer / time);

				yield return null;
			}
		}
	}
}
