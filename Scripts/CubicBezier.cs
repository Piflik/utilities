﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities {
	public static class CubicBezier {
		[Serializable]
		public struct Point2 {
			public Vector2 point;
			public Vector2 incomingControlPoint;
			public Vector2 outgoingControlPoint;

			public static implicit operator Point3(Point2 p) => new Point3 {
				point = p.point,
				incomingControlPoint = p.incomingControlPoint,
				outgoingControlPoint = p.outgoingControlPoint,
			};
		}

		[Serializable]
		public struct Point3 {
			public Vector3 point;
			public Vector3 incomingControlPoint;
			public Vector3 outgoingControlPoint;

			public static implicit operator Point2(Point3 p) => new Point2 {
				point = p.point,
				incomingControlPoint = p.incomingControlPoint,
				outgoingControlPoint = p.outgoingControlPoint,
			};
		}

		public static Vector2 Interpolate(float t, Point2 p0, Point2 p1) => Bezier.Interpolate(t, Bezier.Type.Cubic, p0.point, p0.outgoingControlPoint, p1.incomingControlPoint, p1.point);

		public static Vector3 Interpolate(float t, Point3 p0, Point3 p1) => Bezier.Interpolate(t, Bezier.Type.Cubic, p0.point, p0.outgoingControlPoint, p1.incomingControlPoint, p1.point);

		public static Vector2[] GetPath(float stepSize, bool looping, params Point2[] points) {
			if (points == null || points.Length < 2)
				throw new ArgumentException($"Not enough points for interpolation type {Bezier.Type.Cubic}");
			List<Vector2> list = new List<Vector2>();
			int length = points.Length;
			for (int i = 0; i < length; ++i) {
				Point2 p0;
				Point2 p1;
				if (i != length - 1) {
					p0 = points[i];
					p1 = points[i + 1];
				} else if (looping) {
					p0 = points[i];
					p1 = points[0];
				} else {
					break;
				}

				float t = 0;
				while (t < 1) {
					list.Add(Interpolate(t, p0, p1));
					t += stepSize;
				}
				list.Add(Interpolate(1, p0, p1));
			}
			return list.ToArray();
		}


		public static Vector3[] GetPath(float stepSize, bool looping, params Point3[] points) {
			if (points == null || points.Length < 2)
				throw new ArgumentException($"Not enough points for interpolation type {Bezier.Type.Cubic}");
			List<Vector3> list = new List<Vector3>();
			int length = points.Length;
			for (int i = 0; i < length; ++i) {
				Point3 p0;
				Point3 p1;
				if (i != length - 1) {
					p0 = points[i];
					p1 = points[i + 1];
				} else if (looping) {
					p0 = points[i];
					p1 = points[0];
				} else {
					break;
				}

				float t = 0;
				while (t < 1) {
					list.Add(Interpolate(t, p0, p1));
					t += stepSize;
				}
				list.Add(Interpolate(1, p0, p1));
			}
			return list.ToArray();
		}
	}
}