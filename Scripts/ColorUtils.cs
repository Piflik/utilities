﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static partial class ColorUtilities {

		/// <summary>
		/// Calculates the brighter of two colors
		/// </summary>
		public static Color MaxColor(Color a, Color b) {
			return a.Intensity() > b.Intensity() ? a : b;
		}

		/// <summary>
		/// Calculates the brightest of a set of colors
		/// </summary>
		public static Color MaxColor(params Color[] colors) {
			return colors.OrderByDescending(c => c.Intensity()).FirstOrDefault();
		}

		/// <summary>
		/// Converts a hex string into a color
		/// </summary>
		/// <param name="hex">hex string; can be prefixed by "0x", "0X" or "#"</param>
		/// <param name="defaultColor">color to return if parsing fails</param>
		/// <param name="autoExpand">if true, expands input string to a 6 char hex representation. <see cref="ExpandHex"/></param>
		/// <returns></returns>
		public static Color HexToColor(string hex, Color? defaultColor = null, bool autoExpand = false) {
			TryParseHex(hex, out Color col, defaultColor: defaultColor, autoExpand: autoExpand);
			return col;
		}

		/// <summary>
		/// Parse a RGB hex string into a color
		/// </summary>
		/// <param name="hex">hex string; can be prefixed by "0x", "0X" or "#"</param>
		/// <param name="color"></param>
		/// <param name="defaultColor"></param>
		/// <param name="autoExpand">if true, expands input string to a 6 char hex representation. <see cref="ExpandHex"/></param>
		/// <returns>success</returns>
		public static bool TryParseHex(string hex, out Color color, Color? defaultColor = null, bool autoExpand = false) {
			color = defaultColor ?? Color.black;
			if (string.IsNullOrEmpty(hex))
				return false;

			hex = hex.Replace("0x", ""); //in case the string is formatted 0xFFFFFF
			hex = hex.Replace("0X", ""); //in case the string is formatted 0XFFFFFF
			hex = hex.Replace("#", ""); //in case the string is formatted #FFFFFF
			hex = Regex.Replace(hex, "[^0-9ABCDEFabcdef]", string.Empty);

			//not enough chars? return. minimum: RRGGBB -> 6
			if (hex.Length < 6) {
				if (autoExpand && hex.Length > 0) {
					hex = ExpandHex(hex);
				} else {
					return false;
				}
			}

			byte a = 255;
			if (!byte.TryParse(hex.Substring(0, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out byte r)
				|| !byte.TryParse(hex.Substring(2, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out byte g)
				|| !byte.TryParse(hex.Substring(4, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out byte b)

				//Only use alpha if the string has enough characters
				|| hex.Length >= 8 && !byte.TryParse(hex.Substring(6, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out a)) {
				return false;
			}

			color = new Color32(r, g, b, a);
			return true;
		}

		/// <summary>
		/// Parse a HSV hex string into a color
		/// </summary>
		/// <param name="hex">hex string; can be prefixed by "0x", "0X" or "#"</param>
		/// <param name="color"></param>
		/// <param name="defaultColor"></param>
		/// <param name="autoExpand">if true, expands input string to a 6 char hex representation. <see cref="ExpandHex"/></param>
		/// <returns>success</returns>
		public static bool TryParseHexHSV(string hex, out Color color, Color? defaultColor = null, bool autoExpand = false) {
			color = defaultColor ?? Color.black;
			if (string.IsNullOrEmpty(hex))
				return false;

			hex = hex.Replace("0x", ""); //in case the string is formatted 0xFFFFFF
			hex = hex.Replace("0X", ""); //in case the string is formatted 0XFFFFFF
			hex = hex.Replace("#", ""); //in case the string is formatted #FFFFFF
			hex = Regex.Replace(hex, "[^0-9ABCDEFabcdef]", string.Empty);

			//not enough chars? return. minimum: RRGGBB -> 6
			if (hex.Length < 6) {
				if (autoExpand && hex.Length > 0) {
					hex = ExpandHex(hex);
				} else {
					return false;
				}
			}

			byte a = 255;
			if (!byte.TryParse(hex.Substring(0, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out byte h)
				|| !byte.TryParse(hex.Substring(2, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out byte s)
				|| !byte.TryParse(hex.Substring(4, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out byte v)

				//Only use alpha if the string has enough characters
				|| hex.Length >= 8 && !byte.TryParse(hex.Substring(6, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out a)) {
				return false;
			}

			color = Color.HSVToRGB(h / 255f, s / 255f, v / 255f);
			color.a = a / 255f;
			return true;
		}

		/// <summary>
		/// <para>Expand a string into a valid 6 char hex representation, if possible (8 char with alpha channel). Strings that are 6 or 8 chars long will be returned unmodified.
		/// The input string will not be sanity checked, so invalid chars will </para>
		/// <para>"a" -> "aaaaaa"</para>
		/// <para>"aa" -> "aaaaaa"</para>
		/// <para>"rgb" -> "rrggbb"</para>
		/// <para>"rgba" -> "rrggbbaa"</para>
		/// <para>"rrggb" -> "rrggbb"</para>
		/// <para>"rrggbba" -> "rrggbbaa"</para>
		/// <para>"rrggbbaaxxxx..." -> "rrggbbaa"</para>
		/// </summary>
		public static string ExpandHex(string hexInput) {
			switch (hexInput.Length) {
				case 1:
					hexInput = new string(hexInput[0], 6);
					break;
				case 2:
					hexInput = string.Concat(Enumerable.Repeat(hexInput, 3).ToArray());
					break;
				case 3:
				case 4:
					hexInput = new string(hexInput.SelectMany(c => new[] { c, c }).ToArray());
					break;
				case 5:
					hexInput = $"{hexInput}{hexInput[4]}";
					break;
				case 6:
				case 8:
					break;
				case 7:
					hexInput = $"{hexInput}{hexInput[6]}";
					break;
				default:
					hexInput = hexInput.Substring(0, 8);
					break;
			}

			return hexInput;
		}

		/// <summary>
		/// Hex string from color with prefix; no alpha channel
		/// </summary>
		public static string ColorToHex(Color col, string pre = "#") {
			Color32 col32 = col;
			return $"{pre}{col32.r:X2}{col32.g:X2}{col32.b:X2}";
		}

		/// <summary>
		/// Hex string from color with prefix with alpha channel
		/// </summary>
		public static string ColorToHexAlpha(Color col, string pre = "#") {
			Color32 col32 = col;
			return $"{pre}{col32.r:X2}{col32.g:X2}{col32.b:X2}{col32.a:X2}";
		}

		/// <summary>
		/// Parses three numbers, comma separated into a color. Additional chars will be ignored. <para />No mixing between int and float <para />WARNING: 1 is interpreted as int, use 1.0 if you want it interpreted as float
		/// </summary>
		/// <param name="colorString">String to parse</param>
		/// <param name="color">UnityEngine.Color output</param>
		/// <param name="defaultColor"></param>
		/// <param name="separator">Char used to separate Vector components</param>
		/// <returns>success</returns>
		public static bool TryParseRGB(string colorString, out Color color, Color defaultColor = default, char separator = ',') {
			color = defaultColor;

			string[] parts = Regex.Replace(colorString, $"[^0-9.{separator}]", string.Empty).Split(separator).Where(s => !string.IsNullOrEmpty(s)).ToArray();

			if (parts.Length < 3) return false;

			byte aInt = 255;
			if (byte.TryParse(parts[0], out byte rInt) && byte.TryParse(parts[1], out byte gInt) && byte.TryParse(parts[2], out byte bInt) && (parts.Length < 4 || byte.TryParse(parts[3], out aInt))) {

				color = new Color32(rInt, gInt, bInt, aInt);
				return true;
			}

			float a = 1;
			if (float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out float r) && float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out float g) && float.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out float b) && (parts.Length < 4 || float.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out a))) {
				color = new Color(r, g, b, a);
				return true;
			}

			return false;
		}

		/// <summary>
		/// Parses three numbers, comma separated into a color. Additional chars will be ignored. <para />No mixing between int and float <para />WARNING: 1 is interpreted as int, use 1.0 if you want it interpreted as float
		/// </summary>
		/// <param name="colorString">String to parse</param>
		/// <param name="color">UnityEngine.Color output</param>
		/// <param name="defaultColor"></param>
		/// <param name="separator">Char used to separate Vector components</param>
		/// <returns>success</returns>
		public static bool TryParseHSV(string colorString, out Color color, Color defaultColor = default, char separator = ',') {
			color = defaultColor;
			string[] parts = Regex.Replace(colorString, $"[^0-9.{separator}]", string.Empty).Split(separator).Where(part => !string.IsNullOrEmpty(part)).ToArray();

			if (parts.Length < 3) return false;

			byte aInt = 255;
			if (byte.TryParse(parts[0], out byte hInt) && byte.TryParse(parts[1], out byte sInt) && byte.TryParse(parts[2], out byte vInt) && (parts.Length < 4 || byte.TryParse(parts[3], out aInt))) {

				color = Color.HSVToRGB(hInt / 255f, sInt / 255f, vInt / 255f);
				color.a = aInt / 255f;
				return true;
			}

			float a = 1;
			if (float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out float h) && float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out float s) && float.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out float v) && (parts.Length < 4 || float.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out a))) {
				color = Color.HSVToRGB(h, s, v);
				color.a = a;
				return true;
			}

			return false;
		}

		private static readonly Dictionary<string, Color> _colorNames = new Dictionary<string, Color> {
				{"white", Color.white},
				{"black", Color.black},
				{"red", Color.red},
				{"green", Color.green},
				{"blue", Color.blue},
				{"yellow", Color.yellow},
				{"cyan", Color.cyan},
				{"magenta", Color.magenta},
				{"gray", Color.gray},
				{"grey", Color.grey},
				{"clear", Color.clear},
			};

		/// <summary>
		/// Basic colors by name string (white, black, red, green, blue, yellow, cyan, magenta, gray, grey, clear are available by default, other colors can be registered <see cref="RegisterColorName"/>)
		/// </summary>
		public static bool ColorByName(string colorName, out Color color, Color defaultColor = default) {
			bool success = _colorNames.TryGetValue(colorName.ToLower(), out color);
			if (!success) color = defaultColor;
			return success;
		}

		/// <summary>
		/// Register colors for ColorByName function
		/// </summary>
		public static void RegisterColorName(string name, Color color) {
			_colorNames[name] = color;
		}

		/// <summary>
		/// Convert HSV vector(0..360, 0..100, 0.100) to Unity RGB color
		/// </summary>
		public static Color HSVToRGB(int h, int s, int v) {
			return Color.HSVToRGB(h / 360f, s * 0.01f, v * 0.01f);
		}

		/// <summary>
		/// Convert RGB Unity color to HSV Vector(0..360, 0..100, 0..100)
		/// </summary>
		public static Vector3 RGBToHSV(Color color) {
			Color.RGBToHSV(color, out float h, out float s, out float v);

			int hue = Mathf.RoundToInt(h * 360);
			int sat = Mathf.RoundToInt(s * 100);
			int val = Mathf.RoundToInt(v * 100);

			return new Vector3(hue, sat, val);
		}

		/// <summary>
		/// RGB string from Color ("RRRGGGBBB")
		/// </summary>
		public static string ColorToRGBString255(Color col) {
			return CreateColorString(Mathf.RoundToInt(col.r * 255f), Mathf.RoundToInt(col.g * 255f), Mathf.RoundToInt(col.b * 255f));
		}

		/// <summary>
		/// HSV string from Color ("HHHSSSVVV")
		/// </summary>
		public static string ColorToHSVString(Color color) {
			Vector3 hsv = RGBToHSV(color);
			int hue = (int)hsv.x;
			int sat = (int)hsv.y;
			int val = (int)hsv.z;

			return CreateColorString(hue, sat, val);
		}

		/// <summary>
		/// HSV string from H, S and V ("HHHSSSVVV")
		/// </summary>
		public static string CreateColorString(int hue, int sat, int val) {
			return $"{hue:D3}{sat:D3}{val:D3}";
		}


		/// <summary>
		/// Adjust a color's brightness by hsv conversion, changing value, rgb conversion
		/// </summary>
		/// <param name="color">UnityEngine.Color</param>
		/// <param name="val">0..100</param>
		public static Color AdjustColorBrightness(Color color, int val) {
			//convert color to hsv, adjust value, reconvert to rgb
			Vector3 hsv = RGBToHSV(color);
			Color rgb = HSVToRGB((int)hsv.x, (int)hsv.y, val);

			return rgb;
		}


		/// <summary>
		/// Adjust a color's saturation by hsv conversion, changing value, rgb conversion
		/// </summary>
		public static Color AdjustColorSaturation(Color color, int sat) {
			//convert color to hsv, adjust saturation, reconvert to rgb
			Vector3 hsv = RGBToHSV(color);
			Color rgb = HSVToRGB((int)hsv.x, sat, (int)hsv.z);

			return rgb;
		}

		public static Color Min(Color a, Color b) {
			return new Color(
				Mathf.Min(a.r, b.r),
				Mathf.Min(a.g, b.g),
				Mathf.Min(a.b, b.b),
				Mathf.Min(a.a, b.a));
		}

		public static Color Max(Color a, Color b) {
			return new Color(
				Mathf.Max(a.r, b.r),
				Mathf.Max(a.g, b.g),
				Mathf.Max(a.b, b.b),
				Mathf.Max(a.a, b.a));
		}

		public static Color Saturate(Color color) {
			color.r = color.r.Between(0, 1);
			color.g = color.g.Between(0, 1);
			color.b = color.b.Between(0, 1);
			color.a = color.a.Between(0, 1);

			return color;
		}

		public static Color Abs(Color color) {
			color.r = Mathf.Abs(color.r);
			color.g = Mathf.Abs(color.g);
			color.b = Mathf.Abs(color.b);
			color.a = Mathf.Abs(color.a);

			return color;
		}

		public static Color Sqrt(Color color) {
			color.r = Mathf.Sqrt(color.r);
			color.g = Mathf.Sqrt(color.g);
			color.b = Mathf.Sqrt(color.b);
			color.a = Mathf.Sqrt(color.a);

			return color;
		}
	}
}
