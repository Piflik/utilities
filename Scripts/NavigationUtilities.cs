﻿using UnityEngine;
using UnityEngine.AI;

namespace Utilities {
	public static class NavigationUtilities {

		/// <summary>
		/// Calculate the distance to a position along the fastest path
		/// </summary>
		/// <param name="agent"></param>
		/// <param name="target"></param>
		/// <returns></returns>
		public static float DistanceTo(this NavMeshAgent agent, Vector3 target) {
			NavMeshPath path = agent.path;

			agent.SetDestination(target);
			float distance = agent.remainingDistance;

			agent.path = path;

			return distance;
		}

		/// <summary>
		/// Calculates the length of a path
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static float Length(this NavMeshPath path) {
			if (path.corners.Length < 2) return 0;
			float length = 0;
			for (int i = 1; i < path.corners.Length; ++i) {
				length += Vector3.Distance(path.corners[i], path.corners[i - 1]);
			}

			return length;
		}

		//TODO fix for infinite loop...
		private const float SMALLEST_STEP_TRESHOLD = 0.01f;
		/// <summary>
		/// Calculates the cost of a path
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static float Cost(this NavMeshPath path) {
			if (path.corners.Length < 2) return 0;

			float cost = 0;
			NavMeshHit hit;
			NavMesh.SamplePosition(path.corners[0], out hit, 0.1f, NavMesh.AllAreas);
			Vector3 rayStart = path.corners[0];
			int mask = hit.mask;
			int index = IndexFromMask(mask);

			for (int i = 1; i < path.corners.Length; ++i) {

				while (true) {
					NavMesh.Raycast(rayStart, path.corners[i], out hit, mask);

					cost += NavMesh.GetAreaCost(index) * hit.distance;

					if (hit.mask != 0) mask = hit.mask;

					index = IndexFromMask(mask);
					rayStart = hit.position;

					if (hit.mask == 0) {
						rayStart += (path.corners[i] - rayStart).normalized * SMALLEST_STEP_TRESHOLD;
					}

					if (!hit.hit) break;
				}
			}

			return cost;
		}

		/// <summary>
		/// Calculates the terrain-cost at the agent's current position
		/// </summary>
		/// <param name="agent"></param>
		/// <returns></returns>
		public static float CostAtPosition(this NavMeshAgent agent) {
			NavMeshHit hit;
			agent.SamplePathPosition(NavMesh.AllAreas, 0.0f, out hit);

			return NavMesh.GetAreaCost(IndexFromMask(hit.mask));
		}

		///// <summary>
		///// returns the index of the most significant bit of a bitmask
		///// </summary>
		///// <param name="mask"></param>
		///// <returns></returns>
		//public static int IndexFromMask(int mask) {
		//	int i = 0;

		//	while((mask >>= 1) > 0) {
		//		i++;
		//	}

		//	return i;
		//}

		/// <summary>
		/// returns the index of the least significant bit of a bitmask; -1 if mask is 0
		/// </summary>
		/// <param name="mask"></param>
		/// <returns></returns>
		public static int IndexFromMask(int mask) {
			for (int i = 0; i < 32; ++i) {
				if ((1 << i & mask) != 0) {
					return i;
				}
			}
			return -1;
		}
	}
}