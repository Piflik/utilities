﻿using System;
using System.Collections.Generic;
using UnityEngine;
// ReSharper disable UnusedMember.Global

public static class VectorExtensions {

	/// <summary>
	/// Clamp the vector within the rectangle defined by min and max
	/// </summary>
	public static Vector2 Clamp(this Vector2 vec, Vector2 min, Vector2 max) {
		return new Vector2(
			Mathf.Clamp(vec.x, min.x, max.x),
			Mathf.Clamp(vec.y, min.y, max.y)
		);
	}

	/// <summary>
	/// Clamp the vector within the cuboid defined by min and max
	/// </summary>
	public static Vector3 Clamp(this Vector3 vec, Vector3 min, Vector3 max) {
		return new Vector3(
			Mathf.Clamp(vec.x, min.x, max.x),
			Mathf.Clamp(vec.y, min.y, max.y),
			Mathf.Clamp(vec.z, min.z, max.z)
		);
	}

	/// <summary>
	/// Clamp the vector within the hyper-cuboid defined by min and max
	/// </summary>
	public static Vector4 Clamp(this Vector4 vec, Vector4 min, Vector4 max) {
		return new Vector4(
			Mathf.Clamp(vec.x, min.x, max.x),
			Mathf.Clamp(vec.y, min.y, max.y),
			Mathf.Clamp(vec.z, min.z, max.z),
			Mathf.Clamp(vec.w, min.w, max.w)
		);
	}

	/// <summary>
	/// Copy this vector and optionally override components or add to them
	/// </summary>
	public static Vector2 Copy(this Vector2 vec, float x = float.NaN, float y = float.NaN, float dx = float.NaN, float dy = float.NaN) {
		if (float.IsNaN(x))
			x = vec.x;
		if (float.IsNaN(y))
			y = vec.y;
		if (!float.IsNaN(dx))
			x += dx;
		if (!float.IsNaN(dy))
			y += dy;
		return new Vector2(x, y);
	}

	/// <summary>
	/// Copy this vector and optionally override components or add to them
	/// </summary>
	public static Vector3 Copy(this Vector3 vec, float x = float.NaN, float y = float.NaN, float z = float.NaN, float dx = float.NaN, float dy = float.NaN, float dz = float.NaN) {
		if (float.IsNaN(x))
			x = vec.x;
		if (float.IsNaN(y))
			y = vec.y;
		if (float.IsNaN(z))
			z = vec.z;
		if (!float.IsNaN(dx))
			x += dx;
		if (!float.IsNaN(dy))
			y += dy;
		if (!float.IsNaN(dz))
			z += dz;
		return new Vector3(x, y, z);
	}

	/// <summary>
	/// Copy this vector and optionally override components or add to them
	/// </summary>
	public static Vector4 Copy(this Vector4 vec, float x = float.NaN, float y = float.NaN, float z = float.NaN, float w = float.NaN, float dx = float.NaN, float dy = float.NaN, float dz = float.NaN, float dw = float.NaN) {
		if (float.IsNaN(x))
			x = vec.x;
		if (float.IsNaN(y))
			y = vec.y;
		if (float.IsNaN(z))
			z = vec.z;
		if (float.IsNaN(w))
			w = vec.w;
		if (!float.IsNaN(dx))
			x += dx;
		if (!float.IsNaN(dy))
			y += dy;
		if (!float.IsNaN(dz))
			z += dz;
		if (!float.IsNaN(dw))
			w += dw;
		return new Vector4(x, y, z, w);
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this Vector2 v1, Vector2 v2, float precision) {
		return v1.x.Approximates(v2.x, precision) && v1.y.Approximates(v2.y, precision);
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this Vector3 v1, Vector3 v2, float precision) {
		return v1.x.Approximates(v2.x, precision) && v1.y.Approximates(v2.y, precision) && v1.z.Approximates(v2.z, precision);
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this Vector4 v1, Vector4 v2, float precision) {
		return v1.x.Approximates(v2.x, precision) && v1.y.Approximates(v2.y, precision) && v1.z.Approximates(v2.z, precision) && v1.w.Approximates(v2.w, precision);
	}

	/// <summary>
	/// Multiplies every component of this Vector by the same component of b
	/// </summary>
	public static Vector2 ScaleVector(this Vector2 a, Vector2 b) {
		return new Vector2(a.x * b.x, a.y * b.y);
	}

	/// <summary>
	/// Multiplies every component of this Vector by component
	/// </summary>
	public static Vector2 ScaleVector(this Vector2 a, float x, float y) {
		return new Vector2(a.x * x, a.y * y);
	}

	/// <summary>
	/// Multiplies every component of this Vector by the same component of b
	/// </summary>
	public static Vector3 ScaleVector(this Vector3 a, Vector3 b) {
		return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
	}

	/// <summary>
	/// Multiplies every component of this Vector by component
	/// </summary>
	public static Vector3 ScaleVector(this Vector3 a, float x, float y, float z) {
		return new Vector3(a.x * x, a.y * y, a.z * z);
	}

	/// <summary>
	/// Multiplies every component of this Vector by the same component of b
	/// </summary>
	public static Vector4 ScaleVector(this Vector4 a, Vector4 b) {
		return new Vector4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
	}

	/// <summary>
	/// Multiplies every component of this Vector by component
	/// </summary>
	public static Vector4 ScaleVector(this Vector4 a, float x, float y, float z, float w) {
		return new Vector4(a.x * x, a.y * y, a.z * z, a.w * w);
	}

	/// <summary>
	/// Divides every component of this Vector by the same component of b
	/// </summary>
	public static Vector2 InverseScaleVector(this Vector2 a, Vector2 b) {
		return new Vector2(a.x / b.x, a.y / b.y);
	}

	/// <summary>
	/// Divides every component of this Vector by component
	/// </summary>
	public static Vector2 InverseScaleVector(this Vector2 a, float x, float y) {
		return new Vector2(a.x / x, a.y / y);
	}

	/// <summary>
	/// Divides every component of this Vector by the same component of b
	/// </summary>
	public static Vector3 InverseScaleVector(this Vector3 a, Vector3 b) {
		return new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
	}

	/// <summary>
	/// Divides every component of this Vector by component
	/// </summary>
	public static Vector3 InverseScaleVector(this Vector3 a, float x, float y, float z) {
		return new Vector3(a.x / x, a.y / y, a.z / z);
	}

	/// <summary>
	/// Divides every component of this Vector by the same component of b
	/// </summary>
	public static Vector4 InverseScaleVector(this Vector4 a, Vector4 b) {
		return new Vector4(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w);
	}

	/// <summary>
	/// Divides every component of this Vector by component
	/// </summary>
	public static Vector4 InverseScaleVector(this Vector4 a, float x, float y, float z, float w) {
		return new Vector4(a.x / x, a.y / y, a.z / z, a.w / w);
	}


	public static Vector2 ComplexMul(this Vector2 a, Vector2 b) {
		return new Vector2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
	}

	public static Vector2 ComplexDiv(this Vector2 a, Vector2 b) {
		Vector2 c;
		float r, den;
		if (Mathf.Abs(b.x) >= Mathf.Abs(b.y)) {
			r = b.y / b.x;
			den = b.x + r * b.y;
			c.x = (a.x + r * a.y) / den;
			c.y = (a.y - r * a.x) / den;
		} else {
			r = b.x / b.y;
			den = b.y + r * b.x;
			c.x = (a.x * r + a.y) / den;
			c.y = (a.y * r - a.x) / den;
		}
		return c;
	}


	public static Vector2 X(this Vector2 vec, float x) {
		vec.x = x;
		return vec;
	}

	public static Vector2 Y(this Vector2 vec, float y) {
		vec.y = y;
		return vec;
	}

	public static Vector3 X(this Vector3 vec, float x) {
		vec.x = x;
		return vec;
	}

	public static Vector3 Y(this Vector3 vec, float y) {
		vec.y = y;
		return vec;
	}

	public static Vector3 Z(this Vector3 vec, float z) {
		vec.z = z;
		return vec;
	}

	public static Vector4 X(this Vector4 vec, float x) {
		vec.x = x;
		return vec;
	}

	public static Vector4 Y(this Vector4 vec, float y) {
		vec.y = y;
		return vec;
	}

	public static Vector4 Z(this Vector4 vec, float z) {
		vec.z = z;
		return vec;
	}

	public static Vector4 W(this Vector4 vec, float w) {
		vec.w = w;
		return vec;
	}

	/// <summary>
	/// Test if this Vector lies within given bounds
	/// </summary>
	public static bool WithinBounds(this Vector3 vec, Bounds b) {
		return vec.x >= b.min.x && vec.x <= b.max.x
			&& vec.y >= b.min.y && vec.y <= b.max.y
			&& vec.z >= b.min.z && vec.z <= b.max.z;
	}

	/// <summary>
	/// Test if this Vector lies within given bounds
	/// </summary>
	public static bool WithinBounds(this Vector2 vec, Bounds b) {
		return vec.x >= b.min.x && vec.x <= b.max.x
			&& vec.y >= b.min.y && vec.y <= b.max.y;
	}

	/// <summary>
	/// Test if this Vector lies within given rectangle when projected onto the XY-plane
	/// </summary>
	public static bool WithinRect(this Vector3 vec, Rect r) {
		return vec.x >= r.xMin && vec.x <= r.xMax
			&& vec.y >= r.yMin && vec.y <= r.yMax;
	}

	/// <summary>
	/// Test if this Vector lies within given rectangle
	/// </summary>
	public static bool WithinRect(this Vector2 vec, Rect r) {
		return vec.x >= r.xMin && vec.x <= r.xMax
			&& vec.y >= r.yMin && vec.y <= r.yMax;
	}

	/// <summary>
	/// Test if this Vector lies within given rectangle
	/// </summary>
	public static bool WithinRect(this Vector2 vec, Vector2 min, Vector2 max) {
		return vec.x >= min.x && vec.x <= max.x
			&& vec.y >= min.y && vec.y <= max.y;
	}

	/// <summary>
	/// Test if this Vector lies within given rectangle when projected onto the XY-plane
	/// </summary>
	public static bool WithinRect(this Vector3 vec, Vector2 min, Vector2 max) {
		return vec.x >= min.x && vec.x <= max.x
			&& vec.y >= min.y && vec.y <= max.y;
	}

	/// <summary>
	/// Test if this Vector lies within given volume
	/// </summary>
	public static bool WithinVolume(this Vector3 vec, Vector3 min, Vector3 max) {
		return vec.x >= min.x && vec.x <= max.x
			&& vec.y >= min.y && vec.y <= max.y
			&& vec.z >= min.z && vec.z <= max.z;
	}

	/// <summary>
	/// Test if this Vector lies within a Quad specified by its corners (CW or CCW)
	/// </summary>
	public static bool WithinQuad(this Vector3 vec, Vector3 a, Vector3 b, Vector3 c, Vector3 d) {
		return vec.WithinTriangle(a, b, d) || vec.WithinTriangle(b, c, d);
	}

	/// <summary>
	/// Test if this Vector lies within a Triangle specified by its corners
	/// </summary>
	public static bool WithinTriangle(this Vector3 vec, Vector3 a, Vector3 b, Vector3 c) {
		float area = Vector3.Cross(b - a, c - a).magnitude;
		float a1 = Vector3.Cross(vec - a, c - a).magnitude;
		float a2 = Vector3.Cross(vec - b, a - b).magnitude;
		float a3 = Vector3.Cross(vec - c, b - c).magnitude;

		return area.Approximates(a1 + a2 + a3, 0.001f * area);
	}
	/// <summary>
	/// Returns the absolute of this Vector
	/// </summary>
	public static Vector2 Abs(this Vector2 vec) {
		return new Vector2(Mathf.Abs(vec.x), Mathf.Abs(vec.y));
	}

	/// <summary>
	/// Returns the absolute of this Vector
	/// </summary>
	public static Vector3 Abs(this Vector3 vec) {
		return new Vector3(Mathf.Abs(vec.x), Mathf.Abs(vec.y), Mathf.Abs(vec.z));
	}

	/// <summary>
	/// Returns the absolute of this Vector
	/// </summary>
	public static Vector4 Abs(this Vector4 vec) {
		return new Vector4(Mathf.Abs(vec.x), Mathf.Abs(vec.y), Mathf.Abs(vec.z), Mathf.Abs(vec.w));
	}

	/// <summary>
	/// Calculate the Average of a set of Vectors
	/// </summary>
	public static Vector2 Average(this IEnumerable<Vector2> source) {
		if (source == null)
			throw new InvalidOperationException("Cannot calculate average, because the object to calculate on is null.");
		Vector2 avrg = Vector2.zero;
		long count = 0;
		foreach (Vector2 v in source) {
			avrg += v;
			checked { count++; }
		}
		if (count > 0L)
			return avrg / count;
		throw new InvalidOperationException("Cannot calculate average of IEnumerable with length 0.");
	}

	public static Vector2 Average<T>(this IEnumerable<T> source, Func<T, Vector2> selector) {
		if (source == null)
			throw new InvalidOperationException("Cannot calculate average, because the object to calculate on is null.");
		Vector2 avrg = Vector3.zero;
		long count = 0;
		foreach (T t in source) {
			avrg += selector(t);
			checked { count++; }
		}
		if (count > 0L)
			return avrg / count;
		throw new InvalidOperationException("Cannot calculate average of IEnumerable with length 0.");
	}

	/// <summary>
	/// Calculate the Average of a set of Vectors
	/// </summary>
	public static Vector3 Average(this IEnumerable<Vector3> source) {
		if (source == null)
			throw new InvalidOperationException("Cannot calculate average, because the object to calculate on is null.");
		Vector3 avrg = Vector3.zero;
		long count = 0;
		foreach (Vector3 v in source) {
			avrg += v;
			checked { count++; }
		}
		if (count > 0L)
			return avrg / count;
		throw new InvalidOperationException("Cannot calculate average of IEnumerable with length 0.");
	}

	public static Vector3 Average<T>(this IEnumerable<T> source, Func<T, Vector3> selector) {
		if(source == null)
			throw new InvalidOperationException("Cannot calculate average, because the object to calculate on is null.");
		Vector3 avrg = Vector3.zero;
		long count = 0;
		foreach (T t in source) {
			avrg += selector(t);
			checked { count++; }
		}
		if (count > 0L)
			return avrg / count;
		throw new InvalidOperationException("Cannot calculate average of IEnumerable with length 0.");
	}

	/// <summary>
	/// Returns the angle in radians between this Vector and the horizontal plane.
	/// </summary>
	/// <param name="vector"></param>
	/// <returns></returns>
	public static float Theta(this Vector3 vector) {
		float y = vector.y;
		float x = Mathf.Sqrt(vector.x * vector.x + vector.z * vector.z);

		return Mathf.Atan2(y, x);
	}

	/// <summary>
	/// Round to the smalles integer vector greater or equal to this
	/// </summary>
	public static Vector2 Ceil(this Vector2 source) {
		return new Vector2(Mathf.Ceil(source.x), Mathf.Ceil(source.y));
	}

	/// <summary>
	/// Round to the smalles integer vector greater or equal to this
	/// </summary>
	public static Vector2Int CeilToInt(this Vector2 source) {
		return new Vector2Int(Mathf.CeilToInt(source.x), Mathf.CeilToInt(source.y));
	}

	/// <summary>
	/// Round to the largest integer vector smalle or equal to this
	/// </summary>
	public static Vector2 Floor(this Vector2 source) {
		return new Vector2(Mathf.Floor(source.x), Mathf.Floor(source.y));
	}

	/// <summary>
	/// Round to the largest integer vector smalle or equal to this
	/// </summary>
	public static Vector2Int FloorToInt(this Vector2 source) {
		return new Vector2Int(Mathf.FloorToInt(source.x), Mathf.FloorToInt(source.y));
	}

	/// <summary>
	/// Round to the nearest integer vector
	/// </summary>
	public static Vector2 Round(this Vector2 source) {
		return new Vector2(Mathf.Round(source.x), Mathf.Round(source.y));
	}

	/// <summary>
	/// Round to the nearest integer vector
	/// </summary>
	public static Vector2Int RoundToInt(this Vector2 source) {
		return new Vector2Int(Mathf.RoundToInt(source.x), Mathf.RoundToInt(source.y));
	}

	/// <summary>
	/// Round to the smalles integer vector greater or equal to this
	/// </summary>
	public static Vector3 Ceil(this Vector3 source) {
		return new Vector3(Mathf.Ceil(source.x), Mathf.Ceil(source.y), Mathf.Ceil(source.z));
	}

	/// <summary>
	/// Round to the smalles integer vector greater or equal to this
	/// </summary>
	public static Vector3Int CeilToInt(this Vector3 source) {
		return new Vector3Int(Mathf.CeilToInt(source.x), Mathf.CeilToInt(source.y), Mathf.CeilToInt(source.z));
	}

	/// <summary>
	/// Round to the largest integer vector smalle or equal to this
	/// </summary>
	public static Vector3 Floor(this Vector3 source) {
		return new Vector3(Mathf.Floor(source.x), Mathf.Floor(source.y), Mathf.Floor(source.z));
	}

	/// <summary>
	/// Round to the largest integer vector smalle or equal to this
	/// </summary>
	public static Vector3Int FloorToInt(this Vector3 source) {
		return new Vector3Int(Mathf.FloorToInt(source.x), Mathf.FloorToInt(source.y), Mathf.FloorToInt(source.z));
	}

	/// <summary>
	/// Round to the nearest integer vector
	/// </summary>
	public static Vector3 Round(this Vector3 source) {
		return new Vector3(Mathf.Round(source.x), Mathf.Round(source.y), Mathf.Round(source.z));
	}

	/// <summary>
	/// Round to the nearest integer vector
	/// </summary>
	public static Vector3Int RoundToInt(this Vector3 source) {
		return new Vector3Int(Mathf.RoundToInt(source.x), Mathf.RoundToInt(source.y), Mathf.RoundToInt(source.z));
	}

	/// <summary>
	/// Round to the smalles integer vector greater or equal to this
	/// </summary>
	public static Vector4 Ceil(this Vector4 source) {
		return new Vector4(Mathf.Ceil(source.x), Mathf.Ceil(source.y), Mathf.Ceil(source.z), Mathf.Ceil(source.w));
	}

	/// <summary>
	/// Round to the largest integer vector smalle or equal to this
	/// </summary>
	public static Vector4 Floor(this Vector4 source) {
		return new Vector4(Mathf.Floor(source.x), Mathf.Floor(source.y), Mathf.Floor(source.z), Mathf.Floor(source.w));
	}

	/// <summary>
	/// Round to the nearest integer vector
	/// </summary>
	public static Vector4 Round(this Vector4 source) {
		return new Vector4(Mathf.Round(source.x), Mathf.Round(source.y), Mathf.Round(source.z), Mathf.Round(source.w));
	}

	#region Float -> Int

	/// <summary>
	/// Convert to integer Vector by casting each component
	/// </summary>
	public static Vector2Int ToVector2Int(this Vector2 vector2) {
		return new Vector2Int(
			(int)vector2.x,
			(int)vector2.y
		);
	}

	/// <summary>
	/// Convert to integer Vector by casting each component
	/// </summary>
	public static Vector2Int ToVector2Int(this Vector3 vector3) {
		return new Vector2Int(
			(int)vector3.x,
			(int)vector3.y
		);
	}

	/// <summary>
	/// Convert to integer Vector by casting each component
	/// </summary>
	public static Vector3Int ToVector3Int(this Vector2 vector2) {
		return new Vector3Int(
			(int)vector2.x,
			(int)vector2.y,
			0
		);
	}

	/// <summary>
	/// Convert to integer Vector by casting each component
	/// </summary>
	public static Vector3Int ToVector3Int(this Vector3 vector3) {
		return new Vector3Int(
			(int)vector3.x,
			(int)vector3.y,
			(int)vector3.z
		);
	}

	#endregion

	#region Int Vectors

	/// <summary>
	/// Multiplies every component of this Vector by the same component of b
	/// </summary>
	public static Vector2Int ScaleVector(this Vector2Int a, Vector2Int b) {
		return new Vector2Int(a.x * b.x, a.y * b.y);
	}

	/// <summary>
	/// Multiplies every component of this Vector by the same component of b
	/// </summary>
	public static Vector3Int ScaleVector(this Vector3Int a, Vector3Int b) {
		return new Vector3Int(a.x * b.x, a.y * b.y, a.z * b.z);
	}

	/// <summary>
	/// Divides every component of this Vector by the same component of b
	/// </summary>
	public static Vector2Int InverseScaleVector(this Vector2Int a, Vector2Int b) {
		return new Vector2Int(a.x / b.x, a.y / b.y);
	}

	/// <summary>
	/// Divides every component of this Vector by the same component of b
	/// </summary>
	public static Vector3Int InverseScaleVector(this Vector3Int a, Vector3Int b) {
		return new Vector3Int(a.x / b.x, a.y / b.y, a.z / b.z);
	}

	/// <summary>
	/// Reflect this Vector at a normal
	/// </summary>
	public static Vector2 Reflect(this Vector2Int inDirection, Vector2 inNormal) {
		return -2f * Vector2.Dot(inNormal, inDirection) * inNormal + inDirection;
	}

	/// <summary>
	/// Reflect this Vector at a normal
	/// </summary>
	public static Vector3 Reflect(Vector3Int inDirection, Vector3 inNormal) {
		return -2f * Vector3.Dot(inNormal, inDirection) * inNormal + inDirection;
	}

	/// <summary>
	/// Returns this vector with a magnitude of 1
	/// </summary>
	public static Vector2 Normalized(this Vector2Int vec) {
		Vector2 v = vec;
		v.Normalize();
		return v;
	}

	/// <summary>
	/// Returns this vector with a magnitude of 1
	/// </summary>
	public static Vector3 Normalized(this Vector3Int vec) {
		Vector3 v = vec;
		v.Normalize();
		return v;
	}

	/// <summary>
	/// Returns this vector with a magnitude that is smaller or equal to <paramref name="maxLength"/>, while keeping the components integer values
	/// </summary>
	public static Vector2Int ClampMagnitude(Vector2Int vector, float maxLength) {
		if (vector.sqrMagnitude > maxLength * maxLength) {
			return (vector.Normalized() * maxLength).FloorToInt();
		}
		return vector;
	}
	/// <summary>
	/// Returns this vector with a magnitude that is smaller or equal to <paramref name="maxLength"/>, while keeping the components integer values
	/// </summary>
	public static Vector3Int ClampMagnitude(Vector3Int vector, float maxLength) {
		if (vector.sqrMagnitude > maxLength * maxLength) {
			return (vector.Normalized() * maxLength).FloorToInt();
		}
		return vector;
	}

	/// <summary>
	/// Add an integer to each component
	/// </summary>
	public static Vector2Int ShiftAll(this Vector2Int vec, int amount) {
		vec.x += amount;
		vec.y += amount;

		return vec;
	}

	/// <summary>
	/// Add a float to each component
	/// </summary>
	public static Vector2 ShiftAll(this Vector2Int vec, float amount) {
		return new Vector2(vec.x + amount, vec.y + amount);
	}

	/// <summary>
	/// Add an integer to each component
	/// </summary>
	public static Vector3Int ShiftAll(this Vector3Int vec, int amount) {
		vec.x += amount;
		vec.y += amount;
		vec.z += amount;

		return vec;
	}

	/// <summary>
	/// Add a float to each component
	/// </summary>
	public static Vector3 ShiftAll(Vector3Int vec, float amount) {
		return new Vector3(vec.x + amount, vec.y + amount, vec.z + amount);
	}

	/// <summary>
	/// Multiply this vector by an int
	/// </summary>
	public static Vector2Int Mul(this Vector2Int vec, int a) {
		vec.x *= a;
		vec.y *= a;

		return vec;
	}

	/// <summary>
	/// Multiply this vector by a float
	/// </summary>
	public static Vector2 Mul(this Vector2Int vec, float a) {
		Vector2 v = vec;

		v.x *= a;
		v.y *= a;

		return v;
	}

	/// <summary>
	/// Multiply this vector by an int
	/// </summary>
	public static Vector3Int Mul(this Vector3Int vec, int a) {
		vec.x *= a;
		vec.y *= a;
		vec.z *= a;

		return vec;
	}

	/// <summary>
	/// Multiply this vector by a float
	/// </summary>
	public static Vector3 Mul(this Vector3Int vec, float a) {
		Vector3 v = vec;

		v.x *= a;
		v.y *= a;
		v.z *= a;

		return v;
	}

	/// <summary>
	/// Divide this vector by an int
	/// </summary>
	public static Vector2Int Div(this Vector2Int vec, int a) {
		vec.x /= a;
		vec.y /= a;

		return vec;
	}

	/// <summary>
	/// Divide this vector by a float
	/// </summary>
	public static Vector2 Div(this Vector2Int vec, float a) {
		Vector2 v = vec;

		v.x /= a;
		v.y /= a;

		return v;
	}

	/// <summary>
	/// Divide this vector by an int
	/// </summary>
	public static Vector3Int Div(this Vector3Int vec, int a) {
		vec.x /= a;
		vec.y /= a;
		vec.z /= a;

		return vec;
	}

	/// <summary>
	/// Divide this vector by a float
	/// </summary>
	public static Vector3 Div(this Vector3Int vec, float a) {
		Vector3 v = vec;

		v.x /= a;
		v.y /= a;
		v.z /= a;

		return v;
	}

	public static Vector2Int X(this Vector2Int vec, int x) {
		vec.x = x;
		return vec;
	}

	public static Vector2Int Y(this Vector2Int vec, int y) {
		vec.y = y;
		return vec;
	}

	public static Vector3Int X(this Vector3Int vec, int x) {
		vec.x = x;
		return vec;
	}

	public static Vector3Int Y(this Vector3Int vec, int y) {
		vec.y = y;
		return vec;
	}

	public static Vector3Int Z(this Vector3Int vec, int z) {
		vec.z = z;
		return vec;
	}

	public static float Dot(this Vector3 a, Vector3 b) => Vector3.Dot(a, b);
	public static float Dot(this Vector3Int a, Vector3Int b) => Vector3.Dot(a, b);
	public static Vector3 Cross(this Vector3 a, Vector3 b) => Vector3.Cross(a, b);
	public static Vector3Int Cross(this Vector3Int a, Vector3Int b) => RoundToInt(Vector3.Cross(a, b));

	#endregion

	#region Occupied space

	public static float Area(this Vector2 vec) {
		return vec.x * vec.y;
	}

	public static float Volume(this Vector3 vec) {
		return vec.x * vec.y * vec.z;
	}

	public static float HyperVolume(this Vector4 vec) {
		return vec.x * vec.y * vec.z * vec.w;
	}

	public static int Area(this Vector2Int vec) {
		return vec.x * vec.y;
	}

	public static int Volume(this Vector3Int vec) {
		return vec.x * vec.y * vec.z;
	}

	#endregion

	public static float MinDim(this Vector2 vec) {
		return Mathf.Min(vec.x, vec.y);
	}

	public static float MaxDim(this Vector2 vec) {
		return Mathf.Max(vec.x, vec.y);
	}

	public static float MinDim(this Vector3 vec) {
		return Mathf.Min(vec.x, vec.y, vec.z);
	}

	public static float MaxDim(this Vector3 vec) {
		return Mathf.Max(vec.x, vec.y, vec.z);
	}

	public static float MinDim(this Vector4 vec) {
		return Mathf.Min(vec.x, vec.y, vec.z, vec.w);
	}

	public static float MaxDim(this Vector4 vec) {
		return Mathf.Max(vec.x, vec.y, vec.z, vec.w);
	}

	public static float MinDim(this Vector2Int vec) {
		return Mathf.Min(vec.x, vec.y);
	}

	public static float MaxDim(this Vector2Int vec) {
		return Mathf.Max(vec.x, vec.y);
	}

	public static float MinDim(this Vector3Int vec) {
		return Mathf.Min(vec.x, vec.y, vec.z);
	}

	public static float MaxDim(this Vector3Int vec) {
		return Mathf.Max(vec.x, vec.y, vec.z);
	}

	public static void Deconstruct(this Vector2 vec, out float x, out float y) {
		x = vec.x;
		y = vec.y;
	}

	public static void Deconstruct(this Vector3 vec, out float x, out float y, out float z) {
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}

	public static void Deconstruct(this Vector4 vec, out float x, out float y, out float z, out float w) {
		x = vec.x;
		y = vec.y;
		z = vec.z;
		w = vec.w;
	}

	public static void Deconstruct(this Vector2Int vec, out int x, out int y) {
		x = vec.x;
		y = vec.y;
	}

	public static void Deconstruct(this Vector3Int vec, out int x, out int y, out int z) {
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}

	#region Vector2 -> Vector2

	// ReSharper disable InconsistentNaming
	public static Vector2 YX(this Vector2 vec) {
		return new Vector2(vec.y, vec.x);
	}
	public static Vector2Int YX(this Vector2Int vec) {
		return new Vector2Int(vec.y, vec.x);
	}

	#endregion

	#region Vector3 -> Vector2

	public static Vector2 XY(this Vector3 vec) {
		return new Vector2(vec.x, vec.y);
	}

	public static Vector2 XZ(this Vector3 vec) {
		return new Vector2(vec.x, vec.z);
	}

	public static Vector2 YX(this Vector3 vec) {
		return new Vector2(vec.y, vec.x);
	}

	public static Vector2 YZ(this Vector3 vec) {
		return new Vector2(vec.y, vec.z);
	}

	public static Vector2 ZX(this Vector3 vec) {
		return new Vector2(vec.z, vec.x);
	}

	public static Vector2 ZY(this Vector3 vec) {
		return new Vector2(vec.z, vec.y);
	}

	public static Vector2Int XY(this Vector3Int vec) {
		return new Vector2Int(vec.x, vec.y);
	}

	public static Vector2Int XZ(this Vector3Int vec) {
		return new Vector2Int(vec.x, vec.z);
	}

	public static Vector2Int YX(this Vector3Int vec) {
		return new Vector2Int(vec.y, vec.x);
	}

	public static Vector2Int YZ(this Vector3Int vec) {
		return new Vector2Int(vec.y, vec.z);
	}

	public static Vector2Int ZX(this Vector3Int vec) {
		return new Vector2Int(vec.z, vec.x);
	}

	public static Vector2Int ZY(this Vector3Int vec) {
		return new Vector2Int(vec.z, vec.y);
	}

	#endregion

	#region Vector3 -> Vector3

	public static Vector3 XZY(this Vector3 vec) {
		return new Vector3(vec.x, vec.z, vec.y);
	}

	public static Vector3 YXZ(this Vector3 vec) {
		return new Vector3(vec.y, vec.x, vec.z);
	}

	public static Vector3 YZX(this Vector3 vec) {
		return new Vector3(vec.y, vec.z, vec.x);
	}

	public static Vector3 ZXY(this Vector3 vec) {
		return new Vector3(vec.z, vec.x, vec.y);
	}

	public static Vector3 ZYX(this Vector3 vec) {
		return new Vector3(vec.z, vec.y, vec.x);
	}

	public static Vector3Int XZY(this Vector3Int vec) {
		return new Vector3Int(vec.x, vec.z, vec.y);
	}

	public static Vector3Int YXZ(this Vector3Int vec) {
		return new Vector3Int(vec.y, vec.x, vec.z);
	}

	public static Vector3Int YZX(this Vector3Int vec) {
		return new Vector3Int(vec.y, vec.z, vec.x);
	}

	public static Vector3Int ZXY(this Vector3Int vec) {
		return new Vector3Int(vec.z, vec.x, vec.y);
	}

	public static Vector3Int ZYX(this Vector3Int vec) {
		return new Vector3Int(vec.z, vec.y, vec.x);
	}

	#endregion

	#region Vector4 -> Vector2

	public static Vector2 XY(this Vector4 vec) {
		return new Vector2(vec.x, vec.y);
	}

	public static Vector2 XZ(this Vector4 vec) {
		return new Vector2(vec.x, vec.z);
	}

	public static Vector2 XW(this Vector4 vec) {
		return new Vector2(vec.x, vec.w);
	}

	public static Vector2 YX(this Vector4 vec) {
		return new Vector2(vec.y, vec.x);
	}

	public static Vector2 YZ(this Vector4 vec) {
		return new Vector2(vec.y, vec.z);
	}

	public static Vector2 YW(this Vector4 vec) {
		return new Vector2(vec.y, vec.w);
	}

	public static Vector2 ZX(this Vector4 vec) {
		return new Vector2(vec.z, vec.x);
	}

	public static Vector2 ZY(this Vector4 vec) {
		return new Vector2(vec.z, vec.y);
	}

	public static Vector2 ZW(this Vector4 vec) {
		return new Vector2(vec.z, vec.w);
	}

	public static Vector2 WX(this Vector4 vec) {
		return new Vector2(vec.w, vec.x);
	}

	public static Vector2 WY(this Vector4 vec) {
		return new Vector2(vec.w, vec.y);
	}

	public static Vector2 WZ(this Vector4 vec) {
		return new Vector2(vec.w, vec.z);
	}

	#endregion

	#region Vector4 -> Vector3

	public static Vector3 XYZ(this Vector4 vec) {
		return new Vector3(vec.x, vec.y, vec.z);
	}

	public static Vector3 XYW(this Vector4 vec) {
		return new Vector3(vec.x, vec.y, vec.w);
	}

	public static Vector3 XZY(this Vector4 vec) {
		return new Vector3(vec.x, vec.z, vec.y);
	}

	public static Vector3 XZW(this Vector4 vec) {
		return new Vector3(vec.x, vec.z, vec.w);
	}

	public static Vector3 XWY(this Vector4 vec) {
		return new Vector3(vec.x, vec.w, vec.y);
	}

	public static Vector3 XWZ(this Vector4 vec) {
		return new Vector3(vec.x, vec.w, vec.z);
	}

	public static Vector3 YXZ(this Vector4 vec) {
		return new Vector3(vec.y, vec.x, vec.z);
	}

	public static Vector3 YXW(this Vector4 vec) {
		return new Vector3(vec.y, vec.x, vec.w);
	}

	public static Vector3 YZX(this Vector4 vec) {
		return new Vector3(vec.y, vec.z, vec.x);
	}

	public static Vector3 YZW(this Vector4 vec) {
		return new Vector3(vec.y, vec.z, vec.w);
	}

	public static Vector3 YWX(this Vector4 vec) {
		return new Vector3(vec.y, vec.w, vec.x);
	}

	public static Vector3 YWZ(this Vector4 vec) {
		return new Vector3(vec.y, vec.w, vec.z);
	}

	public static Vector3 ZXY(this Vector4 vec) {
		return new Vector3(vec.z, vec.x, vec.y);
	}

	public static Vector3 ZXW(this Vector4 vec) {
		return new Vector3(vec.z, vec.x, vec.w);
	}

	public static Vector3 ZYX(this Vector4 vec) {
		return new Vector3(vec.z, vec.y, vec.x);
	}

	public static Vector3 ZYW(this Vector4 vec) {
		return new Vector3(vec.z, vec.y, vec.w);
	}

	public static Vector3 ZWX(this Vector4 vec) {
		return new Vector3(vec.z, vec.w, vec.x);
	}

	public static Vector3 ZWY(this Vector4 vec) {
		return new Vector3(vec.z, vec.w, vec.y);
	}

	public static Vector3 WXY(this Vector4 vec) {
		return new Vector3(vec.w, vec.x, vec.y);
	}

	public static Vector3 WXZ(this Vector4 vec) {
		return new Vector3(vec.w, vec.x, vec.z);
	}

	public static Vector3 WYX(this Vector4 vec) {
		return new Vector3(vec.w, vec.y, vec.x);
	}

	public static Vector3 WYZ(this Vector4 vec) {
		return new Vector3(vec.w, vec.y, vec.z);
	}

	public static Vector3 WZX(this Vector4 vec) {
		return new Vector3(vec.w, vec.z, vec.x);
	}

	public static Vector3 WZY(this Vector4 vec) {
		return new Vector3(vec.w, vec.z, vec.y);
	}

	#endregion

	#region Vector4 -> Vector4

	public static Vector4 XYWZ(this Vector4 vec) {
		return new Vector4(vec.x, vec.y, vec.w, vec.z);
	}

	public static Vector4 XZYW(this Vector4 vec) {
		return new Vector4(vec.x, vec.z, vec.y, vec.w);
	}

	public static Vector4 XZWY(this Vector4 vec) {
		return new Vector4(vec.x, vec.z, vec.w, vec.y);
	}

	public static Vector4 XWYZ(this Vector4 vec) {
		return new Vector4(vec.x, vec.w, vec.y, vec.z);
	}

	public static Vector4 XWZY(this Vector4 vec) {
		return new Vector4(vec.x, vec.w, vec.z, vec.y);
	}

	public static Vector4 YXZW(this Vector4 vec) {
		return new Vector4(vec.y, vec.x, vec.z, vec.w);
	}

	public static Vector4 YXWZ(this Vector4 vec) {
		return new Vector4(vec.y, vec.x, vec.w, vec.z);
	}

	public static Vector4 YZXW(this Vector4 vec) {
		return new Vector4(vec.y, vec.z, vec.x, vec.w);
	}

	public static Vector4 YZWX(this Vector4 vec) {
		return new Vector4(vec.y, vec.z, vec.w, vec.x);
	}

	public static Vector4 YWXZ(this Vector4 vec) {
		return new Vector4(vec.y, vec.w, vec.x, vec.z);
	}

	public static Vector4 YWZX(this Vector4 vec) {
		return new Vector4(vec.y, vec.w, vec.z, vec.x);
	}

	public static Vector4 ZXYW(this Vector4 vec) {
		return new Vector4(vec.z, vec.x, vec.y, vec.w);
	}

	public static Vector4 ZXWY(this Vector4 vec) {
		return new Vector4(vec.z, vec.x, vec.w, vec.y);
	}

	public static Vector4 ZYXW(this Vector4 vec) {
		return new Vector4(vec.z, vec.y, vec.x, vec.w);
	}

	public static Vector4 ZYWX(this Vector4 vec) {
		return new Vector4(vec.z, vec.y, vec.w, vec.x);
	}

	public static Vector4 ZWXY(this Vector4 vec) {
		return new Vector4(vec.z, vec.w, vec.x, vec.y);
	}

	public static Vector4 ZWYX(this Vector4 vec) {
		return new Vector4(vec.z, vec.w, vec.y, vec.x);
	}

	public static Vector4 WXYZ(this Vector4 vec) {
		return new Vector4(vec.w, vec.x, vec.y, vec.z);
	}

	public static Vector4 WXZY(this Vector4 vec) {
		return new Vector4(vec.w, vec.x, vec.z, vec.y);
	}

	public static Vector4 WYXZ(this Vector4 vec) {
		return new Vector4(vec.w, vec.y, vec.x, vec.z);
	}

	public static Vector4 WYZX(this Vector4 vec) {
		return new Vector4(vec.w, vec.y, vec.z, vec.x);
	}

	public static Vector4 WZXY(this Vector4 vec) {
		return new Vector4(vec.w, vec.z, vec.x, vec.y);
	}

	public static Vector4 WZYX(this Vector4 vec) {
		return new Vector4(vec.w, vec.z, vec.y, vec.x);
	}

	// ReSharper restore InconsistentNaming
	#endregion

	public static string ConstructorCode(this Vector2 vec) => $"new Vector2({vec.x:0.###}f, {vec.y:0.###}f)";
	public static string ConstructorCode(this Vector3 vec) => $"new Vector3({vec.x:0.###}f, {vec.y:0.###}f, {vec.z:0.###}f)";
	public static string ConstructorCode(this Vector4 vec) => $"new Vector4({vec.x:0.###}f, {vec.y:0.###}f, {vec.z:0.###}f, {vec.w:0.###}f)";
}
