﻿using UnityEngine;
using System.Collections.Generic;
// ReSharper disable UnusedMember.Global

public static class LayerMaskExtensions {
	/// <summary>
	/// Create layermask from a set of layer names
	/// </summary>
	public static LayerMask Create(params string[] layerNames) {
		LayerMask ret = 0;
		foreach (string name in layerNames) {
			ret |= 1 << LayerMask.NameToLayer(name);
		}
		return ret;
	}

	/// <summary>
	/// Create a layermask from a set of layer numbers
	/// </summary>
	public static LayerMask Create(params int[] layerNumbers) {
		LayerMask ret = 0;
		foreach (int layer in layerNumbers) {
			ret |= 1 << layer;
		}
		return ret;
	}

	/// <summary>
	/// Test if layer with a given number is included in layermask;
	/// </summary>
	public static bool Contains(this LayerMask mask, int layerNumber) {
		return mask == (mask | (1 << layerNumber));
	}

	/// <summary>
	/// Test if layer with a given name is included in layermask
	/// </summary>
	/// <param name="mask"></param>
	/// <param name="layerName"></param>
	/// <returns></returns>
	public static bool Contains(this LayerMask mask, string layerName) {
		return mask == (mask | (1 << LayerMask.NameToLayer(layerName)));
	}

	/// <summary>
	/// Inverse layermask
	/// </summary>
	public static LayerMask Inverse(this LayerMask original) {
		return ~original;
	}

	/// <summary>
	/// Add layers to existing layrermask
	/// </summary>
	/// <returns></returns>
	public static LayerMask AddToMask(this LayerMask original, params string[] layerNames) {
		return original | Create(layerNames);
	}

	/// <summary>
	/// Add layers to existing layermask
	/// </summary>
	public static LayerMask RemoveFromMask(this LayerMask original, params string[] layerNames) {
		LayerMask invertedOriginal = ~original;
		return ~(invertedOriginal | Create(layerNames));
	}

	/// <summary>
	/// Get names of layers included in this layermask
	/// </summary>
	public static string[] MaskToNames(this LayerMask original) {
		List<string> output = new List<string>();

		for (int i = 0; i < 32; ++i) {
			int shifted = 1 << i;
			if ((original & shifted) == shifted) {
				string layerName = LayerMask.LayerToName(i);
				if (!string.IsNullOrEmpty(layerName)) {
					output.Add(layerName);
				}
			}
		}
		return output.ToArray();
	}

	/// <summary>
	/// Get numbers of layers included in this layermask
	/// </summary>
	public static int[] MaskToNumbers(this LayerMask original) {
		List<int> output = new List<int>();

		for (int i = 0; i < 32; ++i) {
			int shifted = 1 << i;
			if ((original & shifted) == shifted) {
				output.Add(i);
			}
		}
		return output.ToArray();
	}

	/// <summary>
	/// Print names of layers included in this layermask, separated with ','
	/// </summary>
	public static string MaskToString(this LayerMask original) {
		return MaskToString(original, ", ");
	}


	/// <summary>
	/// Print names of layers included in this layermask, separated with <paramref name="delimiter"/>
	/// </summary>
	public static string MaskToString(this LayerMask original, string delimiter) {
		return string.Join(delimiter, MaskToNames(original));
	}
}