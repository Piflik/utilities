using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
// ReSharper disable UnusedMember.Global

public static class XmlExtensions {

	//************************************************
	//************ GET NODE BEGIN ********************
	//************************************************
	/// <summary>
	/// Get a child node by path
	/// </summary>
	public static XmlNode GetNode(this XmlNode parentNode, string nodePath, bool caseSensitive = true) {
		string query = nodePath;

		// if not case sensitive, check each path node for lower case match (xpath translation)
		if (!caseSensitive) {
			query = "";
			string[] path = nodePath.Split('/');
			foreach (string p in path) {
				if (!string.IsNullOrEmpty(p)) {
					query += $"/*[translate(local-name(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='{p.ToLower()}']";
				}
			}
			if (!string.IsNullOrEmpty(query)) query = query.Remove(0, 1);        // remove first /
		}
		return parentNode != null && !string.IsNullOrEmpty(query) ? parentNode.SelectSingleNode(query) : null;
	}

	/// <summary>
	/// Get a child node by path or create it
	/// </summary>
	/// <returns>True if the node has already existed</returns>
	public static bool ForceGetNode(this XmlNode parentNode, string nodePath, out XmlNode node, bool caseSensitive = true) {
		if (parentNode.TryGetNode(nodePath, out node, caseSensitive))
			return true;
		string[] nodeTree = nodePath.Split('/');
		foreach (string nodeName in nodeTree) {
			if (string.IsNullOrEmpty(nodeName))
				continue;
			if (parentNode.TryGetNode(nodeName, out XmlNode localNode, caseSensitive)) {
				parentNode = localNode;
			} else {
				XmlDocument doc = parentNode.OwnerDocument;
				if (doc != null) parentNode = parentNode.AppendChild(doc.CreateNode(XmlNodeType.Element, nodeName, string.Empty));
			}
		}

		node = parentNode;
		return false;
	}

	/// <summary>
	/// Try to get a child node by path
	/// </summary>
	/// <returns>Success</returns>
	public static bool TryGetNode(this XmlNode parentNode, string nodePath, out XmlNode node, bool caseSensitive = true) {
		node = GetNode(parentNode, nodePath, caseSensitive: caseSensitive);
		return node != null;
	}

	/// <summary>
	/// Get multiple child nodes by path
	/// </summary>
	public static XmlNodeList GetNodes(this XmlNode parentNode, string nodePath, bool caseSensitive = true) {

		string query = nodePath;

		// if not case sensitive, check each path node for lower case match (xpath translation)
		if (!caseSensitive) {
			query = "";
			string[] path = nodePath.Split('/');
			foreach (string p in path) {
				if (!string.IsNullOrEmpty(p)) {
					query += $"/*[translate(local-name(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='{p.ToLower()}']";
				}
			}
			if (!string.IsNullOrEmpty(query)) query = query.Remove(0, 1);        // remove first /
		}
		return parentNode != null && !string.IsNullOrEmpty(query) ? parentNode.SelectNodes(query) : null;
	}

	/// <summary>
	/// Try to get multiple child nodes by path
	/// </summary>
	/// <returns>Success</returns>
	public static bool TryGetNodes(this XmlNode parentNode, string nodePath, out XmlNodeList nodeList, bool caseSensitive = true) {
		nodeList = GetNodes(parentNode, nodePath, caseSensitive: caseSensitive);
		return nodeList != null && nodeList.Count > 0;
	}
	//************************************************
	//************ GET NODE END **********************
	//************************************************


	//************************************************
	//************ NODE VALUE BEGIN ******************
	//************************************************

	//************************************************
	//************ STRING ****************************
	//************************************************
	/// <summary>
	/// Get the value of a child node
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode parentNode, string nodeName, out string value, string defaultValue = default, bool ignoreEmptyString = false, bool cs = true) {
		return TryGetNodeValue(GetNode(parentNode, nodeName, caseSensitive: cs), out value, defaultValue, ignoreEmptyString);
	}

	/// <summary>
	/// Get the value of this node
	/// </summary>
	/// <param name="node">This Node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode node, out string value, string defaultValue = default, bool ignoreEmptyString = false) {
		if (node != null) {
			value = node.InnerText;
			if (!ignoreEmptyString || !string.IsNullOrEmpty(value)) {
				return true;
			}
		}
		value = defaultValue;
		return false;
	}

	/// <summary>
	/// Get the value of a child node or create it with the default value
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <returns>True, if the Node exists and has a value</returns>
	public static bool ForceGetNodeValue(this XmlNode parentNode, string nodeName, out string value, string defaultValue = default, bool ignoreEmptyString = false, bool cs = true) {
		return ForceGetNode(parentNode, nodeName, out XmlNode node, cs) & node.ForceGetNodeValue(out value, defaultValue, ignoreEmptyString);
	}

	/// <summary>
	/// Get the value of a node or set it to the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <returns>True, if the value exists</returns>
	public static bool ForceGetNodeValue(this XmlNode node, out string value, string defaultValue = default, bool ignoreEmptyString = false) {
		value = defaultValue;
		if (node != null && TryGetNodeValue(node, out value, defaultValue, ignoreEmptyString)) {
			return true;
		}

		node.Value = defaultValue;
		return false;
	}

	//************************************************
	//************ COLOR *****************************
	//************************************************
	/// <summary>
	/// Get the value of a child node
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode parentNode, string nodeName, out Color value, Color defaultValue = default, bool ignoreEmptyString = true, bool cs = true, char separator = ',') {
		return TryGetNodeValue(GetNode(parentNode, nodeName, caseSensitive: cs), out value, defaultValue, ignoreEmptyString, separator);
	}

	/// <summary>
	/// Get the value of this node
	/// </summary>
	/// <param name="node">This Node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode node, out Color value, Color defaultValue = default, bool ignoreEmptyString = true, char separator = ',') {
		value = defaultValue;
		if (node != null) {
			if (ignoreEmptyString && string.IsNullOrEmpty(node.InnerText.Trim())) {
				return false;
			}

			return Utilities.Parsing.TryParse(node.InnerText, out value, defaultValue, separator: separator);
		}
		value = defaultValue;
		return false;
	}

	/// <summary>
	/// Get the value of a child node or create it with the default value
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <returns>True, if the Node exists and has a value</returns>
	public static bool ForceGetNodeValue(this XmlNode parentNode, string nodeName, out Color value, Color defaultValue = default, bool ignoreEmptyString = false, bool cs = true, char separator = ',') {
		return ForceGetNode(parentNode, nodeName, out XmlNode node, cs) & node.ForceGetNodeValue(out value, defaultValue, ignoreEmptyString, separator);
	}

	/// <summary>
	/// Get the value of a node or set it to the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <returns>True, if the value exists</returns>
	public static bool ForceGetNodeValue(this XmlNode node, out Color value, Color defaultValue = default, bool ignoreEmptyString = false, char separator = ',') {
		value = defaultValue;
		if (node != null && TryGetNodeValue(node, out value, defaultValue, ignoreEmptyString, separator)) {
			return true;
		}

		node.Value = defaultValue.ToString();
		return false;
	}

	//************************************************
	//************ BOOL ******************************
	//************************************************
	/// <summary>
	/// Get the value of a child node
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode parentNode, string nodeName, out bool value, bool defaultValue = default, bool ignoreEmptyString = true, bool cs = true) {
		return TryGetNodeValue(GetNode(parentNode, nodeName, caseSensitive: cs), out value, defaultValue, ignoreEmptyString);
	}

	/// <summary>
	/// Get the value of this node
	/// </summary>
	/// <param name="node">This Node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode node, out bool value, bool defaultValue = default, bool ignoreEmptyString = true) {
		value = defaultValue;
		if (node != null) {
			if (ignoreEmptyString && string.IsNullOrEmpty(node.InnerText.Trim())) {
				return false;
			}

			value = Utilities.Parsing.StrToBool(node.InnerText);
			return true;
		}
		value = defaultValue;
		return false;
	}

	/// <summary>
	/// Get the value of a child node or create it with the default value
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <returns>True, if the Node exists and has a value</returns>
	public static bool ForceGetNodeValue(this XmlNode parentNode, string nodeName, out bool value, bool defaultValue = default, bool ignoreEmptyString = false, bool cs = true) {
		return ForceGetNode(parentNode, nodeName, out XmlNode node, cs) & node.ForceGetNodeValue(out value, defaultValue, ignoreEmptyString);
	}

	/// <summary>
	/// Get the value of a node or set it to the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <returns>True, if the value exists</returns>
	public static bool ForceGetNodeValue(this XmlNode node, out bool value, bool defaultValue = default, bool ignoreEmptyString = false) {
		value = defaultValue;
		if (node != null && TryGetNodeValue(node, out value, defaultValue, ignoreEmptyString)) {
			return true;
		}

		node.Value = defaultValue.ToString();
		return false;
	}

	//************************************************
	//************ NUMBERS AND ENUMS *****************
	//************************************************
	/// <summary>
	/// Get the value of a child node
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue<T>(this XmlNode parentNode, string nodeName, out T value, T defaultValue = default, bool ignoreEmptyString = true, bool cs = true) where T : struct, IConvertible {
		return TryGetNodeValue(GetNode(parentNode, nodeName, caseSensitive: cs), out value, defaultValue, ignoreEmptyString);
	}

	/// <summary>
	/// Get the value of this node
	/// </summary>
	/// <param name="node">This Node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue<T>(this XmlNode node, out T value, T defaultValue = default, bool ignoreEmptyString = true) where T : struct, IConvertible {
		value = defaultValue;

		if (node == null) return false;
		string str = node.InnerText;
		if (ignoreEmptyString && string.IsNullOrWhiteSpace(str)) return false;

		if (typeof(T).IsEnum) {

			str = Regex.Replace(str, @"\s+", "");

			if (Utilities.Parsing.TryParse(str, out T t, true)) {
				value = t;
				return true;
			}

			return false;
		}

		value = str.Convert(defaultValue);
		return true;
	}

	/// <summary>
	/// Get the value of a child node or create it with the default value
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <returns>True, if the Node exists and has a value</returns>
	public static bool ForceGetNodeValue<T>(this XmlNode parentNode, string nodeName, out T value, T defaultValue = default, bool ignoreEmptyString = false, bool cs = true) where T : struct, IConvertible {
		return ForceGetNode(parentNode, nodeName, out XmlNode node, cs) & node.ForceGetNodeValue(out value, defaultValue, ignoreEmptyString);
	}

	/// <summary>
	/// Get the value of a node or set it to the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <returns>True, if the value exists</returns>
	public static bool ForceGetNodeValue<T>(this XmlNode node, out T value, T defaultValue = default, bool ignoreEmptyString = false) where T : struct, IConvertible {
		value = defaultValue;
		if (node != null && TryGetNodeValue(node, out value, defaultValue, ignoreEmptyString)) {
			return true;
		}

		node.Value = defaultValue.ToString();
		return false;
	}

	//************************************************
	//************ VECTOR2 ***************************
	//************************************************
	/// <summary>
	/// Get the value of a child node
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode parentNode, string nodeName, out Vector2 value, Vector2 defaultValue = default, bool ignoreEmptyString = true, bool cs = true, char separator = ',') {
		return TryGetNodeValue(GetNode(parentNode, nodeName, caseSensitive: cs), out value, defaultValue, ignoreEmptyString, separator);
	}

	/// <summary>
	/// Get the value of this node
	/// </summary>
	/// <param name="node">This Node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode node, out Vector2 value, Vector2 defaultValue = default, bool ignoreEmptyString = true, char separator = ',') {
		value = defaultValue;
		if (node != null) {
			if (ignoreEmptyString && string.IsNullOrEmpty(node.InnerText.Trim())) {
				return false;
			}

			if (Utilities.Parsing.TryParse(node.InnerText, out value, separator: separator)) {
				return true;
			}
		}
		value = defaultValue;
		return false;
	}

	/// <summary>
	/// Get the value of a child node or create it with the default value
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if the Node exists and has a value</returns>
	public static bool ForceGetNodeValue(this XmlNode parentNode, string nodeName, out Vector2 value, Vector2 defaultValue = default, bool ignoreEmptyString = false, bool cs = true, char separator = ',') {
		return ForceGetNode(parentNode, nodeName, out XmlNode node, cs) & node.ForceGetNodeValue(out value, defaultValue, ignoreEmptyString, separator);
	}

	/// <summary>
	/// Get the value of a node or set it to the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if the value exists</returns>
	public static bool ForceGetNodeValue(this XmlNode node, out Vector2 value, Vector2 defaultValue = default, bool ignoreEmptyString = false, char separator = ',') {
		value = defaultValue;
		if (node != null && TryGetNodeValue(node, out value, defaultValue, ignoreEmptyString, separator)) {
			return true;
		}

		node.Value = defaultValue.ToString();
		return false;
	}

	//************************************************
	//************ VECTOR3 ***************************
	//************************************************
	/// <summary>
	/// Get the value of a child node
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode parentNode, string nodeName, out Vector3 value, Vector3 defaultValue = default, bool ignoreEmptyString = true, bool cs = true, char separator = ',') {
		return TryGetNodeValue(GetNode(parentNode, nodeName, caseSensitive: cs), out value, defaultValue, ignoreEmptyString, separator);
	}

	/// <summary>
	/// Get the value of this node
	/// </summary>
	/// <param name="node">This Node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode node, out Vector3 value, Vector3 defaultValue = default, bool ignoreEmptyString = true, char separator = ',') {
		value = defaultValue;
		if (node != null) {
			if (ignoreEmptyString && string.IsNullOrEmpty(node.InnerText.Trim())) {
				return false;
			}

			if (Utilities.Parsing.TryParse(node.InnerText, out value, separator:separator)) {
				return true;
			}
		}
		value = defaultValue;
		return false;
	}

	/// <summary>
	/// Get the value of a child node or create it with the default value
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if the Node exists and has a value</returns>
	public static bool ForceGetNodeValue(this XmlNode parentNode, string nodeName, out Vector3 value, Vector3 defaultValue = default, bool ignoreEmptyString = false, bool cs = true, char separator = ',') {
		return ForceGetNode(parentNode, nodeName, out XmlNode node, cs) & node.ForceGetNodeValue(out value, defaultValue, ignoreEmptyString, separator);
	}

	/// <summary>
	/// Get the value of a node or set it to the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if the value exists</returns>
	public static bool ForceGetNodeValue(this XmlNode node, out Vector3 value, Vector3 defaultValue = default, bool ignoreEmptyString = false, char separator = ',') {
		value = defaultValue;
		if (node != null && TryGetNodeValue(node, out value, defaultValue, ignoreEmptyString, separator)) {
			return true;
		}

		node.Value = defaultValue.ToString();
		return false;
	}

	//************************************************
	//************ VECTOR4 ***************************
	//************************************************
	/// <summary>
	/// Get the value of a child node
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode parentNode, string nodeName, out Vector4 value, Vector4 defaultValue = default, bool ignoreEmptyString = true, bool cs = true, char separator = ',') {
		return TryGetNodeValue(GetNode(parentNode, nodeName, caseSensitive: cs), out value, defaultValue, ignoreEmptyString, separator);
	}

	/// <summary>
	/// Get the value of this node
	/// </summary>
	/// <param name="node">This Node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetNodeValue(this XmlNode node, out Vector4 value, Vector4 defaultValue = default, bool ignoreEmptyString = true, char separator = ',') {
		value = defaultValue;
		if (node != null) {
			if (ignoreEmptyString && string.IsNullOrEmpty(node.InnerText.Trim())) {
				return false;
			}

			if (Utilities.Parsing.TryParse(node.InnerText, out value, separator:separator)) {
				return true;
			}
		}
		value = defaultValue;
		return false;
	}

	/// <summary>
	/// Get the value of a child node or create it with the default value
	/// </summary>
	/// <param name="parentNode">This node</param>
	/// <param name="nodeName">Path to target node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the node is not found</param>
	/// <param name="ignoreEmptyString">If true, an empty node will also cause the method to fail and use the default value</param>
	/// <param name="cs"><paramref name="nodeName"/> is case sensitive</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if the Node exists and has a value</returns>
	public static bool ForceGetNodeValue(this XmlNode parentNode, string nodeName, out Vector4 value, Vector4 defaultValue = default, bool ignoreEmptyString = false, bool cs = true, char separator = ',') {
		return ForceGetNode(parentNode, nodeName, out XmlNode node, cs) & node.ForceGetNodeValue(out value, defaultValue, ignoreEmptyString, separator);
	}

	/// <summary>
	/// Get the value of a node or set it to the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="value">Output</param>
	/// <param name="defaultValue">Value to use if the method fails</param>
	/// <param name="ignoreEmptyString">If true, an empty node will cause the method to fail and use the default value</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if the value exists</returns>
	public static bool ForceGetNodeValue(this XmlNode node, out Vector4 value, Vector4 defaultValue = default, bool ignoreEmptyString = false, char separator = ',') {
		value = defaultValue;
		if (node != null && TryGetNodeValue(node, out value, defaultValue, ignoreEmptyString, separator)) {
			return true;
		}

		node.Value = defaultValue.ToString();
		return false;
	}
	//************************************************
	//************ NODE VALUE END ********************
	//************************************************


	//************************************************
	//************ NODE ATTRIBUTES BEGIN *************
	//************************************************
	private static bool IsElementNode(XmlNode node) {
		return node != null && node.NodeType == XmlNodeType.Element;
	}

	private static void SetAttribute<T>(this XmlNode node, string attr, T t) {
		if (!IsElementNode(node))
			return;
		if (node.Attributes?[attr] != null) {
			node.Attributes[attr].Value = t.ToString();
		} else {
			XmlDocument doc = node.OwnerDocument ?? node as XmlDocument;
			if (doc != null) {
				XmlAttribute attribute = doc.CreateAttribute(attr);
				attribute.Value = t.ToString();
				node.Attributes?.SetNamedItem(attribute);
			}
		}
	}

	/// <summary>
	/// Set an attribute value
	/// Only here for backwards compatability.
	/// </summary>
	public static void SetAttribute(this XmlNode node, XmlDocument xmlDoc, string attr, string value) {
		if (node != null) {
			if (node.Attributes?[attr] != null) {
				node.Attributes[attr].Value = value;
			} else {
				XmlAttribute a = xmlDoc.CreateAttribute(attr);
				a.Value = value;
				node.Attributes?.SetNamedItem(a);
			}
		}
	}

	//************************************************
	//************ STRING ****************************
	//************************************************
	/// <summary>
	/// Get an attribute of this node
	/// </summary>
	public static string GetAttribute(this XmlNode node, string attr, string defaultValue = "") {
		if (node?.Attributes?[attr] != null) return node.Attributes[attr].Value;
		return defaultValue;
	}

	/// <summary>
	/// Try to get an attribute of this node
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="result">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <returns>Success</returns>
	public static bool TryGetAttribute(this XmlNode node, string attr, out string result, string defaultResult = default, bool ignoreEmptyStr = false) {
		if (node?.Attributes?[attr] != null) {
			result = node.Attributes[attr].Value;
			if (!ignoreEmptyStr || !string.IsNullOrWhiteSpace(result)) return true;
		}
		result = defaultResult;
		return false;
	}

	/// <summary>
	/// Try to get an attribute of this node or add it with the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="value">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <returns>True, if attribute has been found</returns>
	public static bool ForceGetAttribute(this XmlNode node, string attr, out string value, string defaultResult = default, bool ignoreEmptyStr = false) {
		value = defaultResult;
		if (!IsElementNode(node)) {
			return false;
		}

		if (TryGetAttribute(node, attr, out value, defaultResult, ignoreEmptyStr)) {
			return true;
		}

		SetAttribute(node, attr, defaultResult);
		return false;
	}

	//************************************************
	//************ COLOR *****************************
	//************************************************
	/// <summary>
	/// Try to get an attribute of this node
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="result">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetAttribute(this XmlNode node, string attr, out Color result, Color defaultResult = default, bool ignoreEmptyStr = false, char separator = ',') {
		result = defaultResult;
		if (node?.Attributes?[attr] != null) {
			string str = node.Attributes[attr].Value;
			if (ignoreEmptyStr && string.IsNullOrWhiteSpace(str)) {
				return false;
			}

			return Utilities.Parsing.TryParse(str, out result, defaultResult, separator:separator);
		}
		return false;
	}

	/// <summary>
	/// Try to get an attribute of this node or add it with the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="value">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if attribute has been found</returns>
	public static bool ForceGetAttribute(this XmlNode node, string attr, out Color value, Color defaultResult = default, bool ignoreEmptyStr = false, char separator = ',') {
		value = defaultResult;
		if (!IsElementNode(node)) {
			return false;
		}

		if (TryGetAttribute(node, attr, out value, defaultResult, ignoreEmptyStr, separator)) {
			return true;
		}

		SetAttribute(node, attr, defaultResult);
		return false;
	}

	//************************************************
	//************ BOOL ******************************
	//************************************************
	/// <summary>
	/// Try to get an attribute of this node
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="result">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <returns>Success</returns>
	public static bool TryGetAttribute(this XmlNode node, string attr, out bool result, bool defaultResult = false, bool ignoreEmptyStr = false) {
		result = defaultResult;
		if (node?.Attributes?[attr] != null) {
			string str = node.Attributes[attr].Value;
			if (ignoreEmptyStr && string.IsNullOrWhiteSpace(str)) {
				return false;
			}

			result = Utilities.Parsing.StrToBool(str);
			return true;
		}

		return false;
	}

	/// <summary>
	/// Try to get an attribute of this node or add it with the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="value">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <returns>True, if attribute has been found</returns>
	public static bool ForceGetAttribute(this XmlNode node, string attr, out bool value, bool defaultResult = default, bool ignoreEmptyStr = false) {
		value = defaultResult;
		if (!IsElementNode(node)) {
			return false;
		}

		if (TryGetAttribute(node, attr, out value, defaultResult, ignoreEmptyStr)) {
			return true;
		}

		SetAttribute(node, attr, defaultResult);
		return false;
	}

	//************************************************
	//************ NUMBERS AND ENUMS *****************
	//************************************************
	/// <summary>
	/// Try to get an attribute of this node
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="result">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <returns>Success</returns>
	public static bool TryGetAttribute<T>(this XmlNode node, string attr, out T result, T defaultResult = default, bool ignoreEmptyStr = false) where T : struct, IConvertible {
		result = defaultResult;
		if (node?.Attributes?[attr] == null) {
			return false;
		}

		string str = node.Attributes[attr].Value;
		if (ignoreEmptyStr && string.IsNullOrWhiteSpace(str)) return false;

		if (typeof(T).IsEnum) {

			str = Regex.Replace(str, @"\s+", "");

			if (Utilities.Parsing.TryParse(str, out T t, ignoreCase: true)) {
				result = t;
				return true;
			}
			return false;
		}

		result = str.Convert(defaultResult);
		return true;
	}

	/// <summary>
	/// Try to get an attribute of this node or add it with the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="value">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <returns>True, if attribute has been found</returns>
	public static bool ForceGetAttribute<T>(this XmlNode node, string attr, out T value, T defaultResult = default, bool ignoreEmptyStr = false) where T : struct, IConvertible {
		value = defaultResult;
		if (!IsElementNode(node)) {
			return false;
		}

		if (TryGetAttribute(node, attr, out value, defaultResult, ignoreEmptyStr)) {
			return true;
		}

		SetAttribute(node, attr, defaultResult);
		return false;
	}

	//************************************************
	//************ VECTOR2 ***************************
	//************************************************
	/// <summary>
	/// Try to get an attribute of this node
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="result">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetAttribute(this XmlNode node, string attr, out Vector2 result, Vector2 defaultResult = default, bool ignoreEmptyStr = false, char separator = ',') {
		result = defaultResult;
		return TryGetAttribute(node, attr, out string strResult, ignoreEmptyStr: ignoreEmptyStr) && Utilities.Parsing.TryParse(strResult, out result, defaultResult, separator);
	}

	/// <summary>
	/// Try to get an attribute of this node or add it with the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="value">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if attribute has been found</returns>
	public static bool ForceGetAttribute(this XmlNode node, string attr, out Vector2 value, Vector2 defaultResult = default, bool ignoreEmptyStr = false, char separator = ',') {
		value = defaultResult;
		if (!IsElementNode(node)) {
			return false;
		}

		if (TryGetAttribute(node, attr, out value, defaultResult, ignoreEmptyStr, separator)) {
			return true;
		}

		SetAttribute(node, attr, defaultResult);
		return false;
	}

	//************************************************
	//************ VECTOR3 ***************************
	//************************************************
	/// <summary>
	/// Try to get an attribute of this node
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="result">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	public static bool TryGetAttribute(this XmlNode node, string attr, out Vector3 result, Vector3 defaultResult = default, bool ignoreEmptyStr = false, char separator = ',') {
		result = defaultResult;
		return TryGetAttribute(node, attr, out string strResult, ignoreEmptyStr: ignoreEmptyStr) && Utilities.Parsing.TryParse(strResult, out result, defaultResult, separator);
	}

	/// <summary>
	/// Try to get an attribute of this node or add it with the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="value">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if attribute has been found</returns>
	public static bool ForceGetAttribute(this XmlNode node, string attr, out Vector3 value, Vector3 defaultResult = default, bool ignoreEmptyStr = false, char separator = ',') {
		value = defaultResult;
		if (!IsElementNode(node)) {
			return false;
		}

		if (TryGetAttribute(node, attr, out value, defaultResult, ignoreEmptyStr, separator)) {
			return true;
		}

		SetAttribute(node, attr, defaultResult);
		return false;
	}

	//************************************************
	//************ VECTOR4 ***************************
	/// <summary>
	/// Try to get an attribute of this node
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="result">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>Success</returns>
	//************************************************
	public static bool TryGetAttribute(this XmlNode node, string attr, out Vector4 result, Vector4 defaultResult = default, bool ignoreEmptyStr = false, char separator = ',') {
		result = defaultResult;
		return TryGetAttribute(node, attr, out string strResult, ignoreEmptyStr: ignoreEmptyStr) && Utilities.Parsing.TryParse(strResult, out result, defaultResult, separator);
	}

	/// <summary>
	/// Try to get an attribute of this node or add it with the default value
	/// </summary>
	/// <param name="node">This node</param>
	/// <param name="attr">Attribute name</param>
	/// <param name="value">Output</param>
	/// <param name="ignoreEmptyStr">If true, empty attribute will count as not found</param>
	/// <param name="defaultResult">Default result to use if attribute is not found</param>
	/// <param name="separator">Char used to separate Vector components</param>
	/// <returns>True, if attribute has been found</returns>
	public static bool ForceGetAttribute(this XmlNode node, string attr, out Vector4 value, Vector4 defaultResult = default, bool ignoreEmptyStr = false, char separator = ',') {
		value = defaultResult;
		if (!IsElementNode(node)) {
			return false;
		}

		if (TryGetAttribute(node, attr, out value, defaultResult, ignoreEmptyStr, separator)) {
			return true;
		}

		SetAttribute(node, attr, defaultResult);
		return false;
	}
	//************************************************
	//************ NODE ATTRIBUTES END ***************
	//************************************************

	/// <summary>Serialises an object of type T in to an xml string</summary>
	/// <typeparam name="T">Any class type</typeparam>
	/// <param name="objectToSerialise">Object to serialise</param>
	/// <returns>A string that represents Xml, empty oterwise</returns>
	public static string XmlSerialize<T>(this T objectToSerialise) where T : class {
		XmlSerializer serialiser = new XmlSerializer(typeof(T));
		string xml;
		using (MemoryStream memStream = new MemoryStream()) {
			using (XmlTextWriter xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8)) {
				serialiser.Serialize(xmlWriter, objectToSerialise);
				xml = Encoding.UTF8.GetString(memStream.GetBuffer());
			}
		}

		// ascii 60 = '<' and ascii 62 = '>'
		xml = xml.Substring(xml.IndexOf(Convert.ToChar(60)));
		xml = xml.Substring(0, xml.LastIndexOf(Convert.ToChar(62)) + 1);
		return xml;
	}

	/// <summary>Deserialises an xml string in to an object of Type T</summary>
	/// <typeparam name="T">Any class type</typeparam>
	/// <param name="xml">Xml as string to deserialise from</param>
	/// <returns>A new object of type T is successful, null if failed</returns>
	public static T XmlDeserialize<T>(this string xml) where T : class {
		XmlSerializer serialiser = new XmlSerializer(typeof(T));
		T newObject;

		using (StringReader stringReader = new StringReader(xml)) {
			using (XmlTextReader xmlReader = new XmlTextReader(stringReader)) {
				try {
					newObject = serialiser.Deserialize(xmlReader) as T;
				} catch (InvalidOperationException) {   // String passed is not Xml, return null
					return null;
				}

			}
		}

		return newObject;
	}
}
