﻿#if USE_TMP
using TMPro;
#endif
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MathExpressionEngine;
using UnityEngine;

// ReSharper disable UnusedMember.Global

public static partial class StringExtensions {

	public static string Longest(this IEnumerable<string> strings) {
		if (strings == null) return null;
		string longest = null;

		foreach (string newString in strings) {
			if (longest == null || newString != null && newString.Length > longest.Length) {
				longest = newString;
			}
		}

		return longest;
	}

	public static string Shortest(this IEnumerable<string> strings) {
		if (strings == null) return null;
		string shortest = null;
		long currentCount = long.MaxValue;

		foreach (string newString in strings) {
			if (shortest == null || newString != null && newString.Length < currentCount) {
				shortest = newString;
				currentCount = newString.Length;
			}
		}

		return shortest;
	}

	public static string StripRichText(this string s) {
		s = Regex.Replace(s, "<(/?b|/?i|/material|material=[0-9]+|/color|color=#[0-9a-f]+)>", string.Empty);
		return s;
	}

	/// <summary>
	/// Replace illegal characters from an URL string with their percent code
	/// </summary>
	public static string PercentCode(this string s) => Utilities.Utils.PercentCode(s);

	/// <summary>
	/// Calculate the width of a char for a given font and fontsize
	/// </summary>
	public static int Width(this char c, Font font, float fontsize, FontStyle fontstyle = FontStyle.Normal) {

		font.RequestCharactersInTexture(c.ToString(), Mathf.CeilToInt(fontsize), fontstyle);

		font.GetCharacterInfo(c, out CharacterInfo cInfo, Mathf.CeilToInt(fontsize));
		return cInfo.advance;
	}

	/// <summary>
	/// Calculate the width of a string for a given font and fontsize
	/// </summary>
	public static int Width(this string s, Font font, float fontsize, FontStyle fontstyle = FontStyle.Normal, bool ignoreRichText = false) {
		if (string.IsNullOrEmpty(s))
			return 0;
		if (ignoreRichText) s = s.StripRichText();

		int w = 0;
		font.RequestCharactersInTexture(s, Mathf.CeilToInt(fontsize), fontstyle);

		foreach (char c in s) {
			font.GetCharacterInfo(c, out CharacterInfo cInfo, Mathf.CeilToInt(fontsize));
			w += cInfo.advance;
		}

		return w;
	}

	/// <summary>
	/// Determine if a string fits into a given width with a given font and fontsize
	/// </summary>
	public static bool FitsWidth(this string s, float width, Font font, float fontsize, FontStyle fontstyle = FontStyle.Normal, bool ignoreRichText = false) {
		if (string.IsNullOrEmpty(s))
			return true;
		if (ignoreRichText) s = s.StripRichText();

		int w = 0;
		font.RequestCharactersInTexture(s, Mathf.CeilToInt(fontsize), fontstyle);

		foreach (char c in s) {
			font.GetCharacterInfo(c, out CharacterInfo cInfo, Mathf.CeilToInt(fontsize));

			w += cInfo.advance;

			if (w > width)
				return false;
		}

		return true;
	}

#if USE_TMP

	/// <summary>
	/// Calculate the width of a char for a given font and fontsize
	/// </summary>
	public static int Width(this char c, TMP_FontAsset fontAsset, float fontSize, FontStyles style = FontStyles.Normal) {
		// Compute scale of the target point size relative to the sampling point size of the font asset.
		float pointSizeScale = fontSize / (fontAsset.faceInfo.pointSize * fontAsset.faceInfo.scale);
		float emScale = fontSize * 0.01f;

		float styleSpacingAdjustment = (style & FontStyles.Bold) == FontStyles.Bold ? fontAsset.boldSpacing : 0;
		float normalSpacingAdjustment = fontAsset.normalSpacingOffset;

		// Make sure the given unicode exists in the font asset.
		if (fontAsset.characterLookupTable.TryGetValue(c, out TMP_Character character))
			return Mathf.RoundToInt(character.glyph.metrics.horizontalAdvance * pointSizeScale + (styleSpacingAdjustment + normalSpacingAdjustment) * emScale);

		return 0;
	}

	/// <summary>
	/// Calculate the width of a string for a given font and fontsize
	/// </summary>
	public static int Width(this string s, TMP_FontAsset fontAsset, float fontSize, FontStyles style = FontStyles.Normal, bool ignoreRichText = false) {
		if (string.IsNullOrEmpty(s))
			return 0;
		if (ignoreRichText) s = s.StripRichText();

		// Compute scale of the target point size relative to the sampling point size of the font asset.
		float pointSizeScale = fontSize / (fontAsset.faceInfo.pointSize * fontAsset.faceInfo.scale);
		float emScale = fontSize * 0.01f;

		float styleSpacingAdjustment = (style & FontStyles.Bold) == FontStyles.Bold ? fontAsset.boldSpacing : 0;
		float normalSpacingAdjustment = fontAsset.normalSpacingOffset;

		float w = 0;

		for (int i = 0; i < s.Length; i++) {
			char c = s[i];
			// Make sure the given unicode exists in the font asset.
			if (fontAsset.characterLookupTable.TryGetValue(c, out TMP_Character character))
				w += character.glyph.metrics.horizontalAdvance * pointSizeScale + (styleSpacingAdjustment + normalSpacingAdjustment) * emScale;
		}

		return Mathf.RoundToInt(w);
	}

	/// <summary>
	/// Determine if a string fits into a given width with a given font and fontsize
	/// </summary>
	public static bool FitsWidth(this string s, float width, TMP_FontAsset fontAsset, float fontSize, FontStyles style = FontStyles.Normal, bool ignoreRichText = false) {
		if (string.IsNullOrEmpty(s))
			return true;
		if (ignoreRichText) s = s.StripRichText();

		// Compute scale of the target point size relative to the sampling point size of the font asset.
		float pointSizeScale = fontSize / (fontAsset.faceInfo.pointSize * fontAsset.faceInfo.scale);
		float emScale = fontSize * 0.01f;

		float styleSpacingAdjustment = (style & FontStyles.Bold) == FontStyles.Bold ? fontAsset.boldSpacing : 0;
		float normalSpacingAdjustment = fontAsset.normalSpacingOffset;

		float w = 0;

		for (int i = 0; i < s.Length; i++) {
			char c = s[i];
			// Make sure the given unicode exists in the font asset.
			if (fontAsset.characterLookupTable.TryGetValue(c, out TMP_Character character)) {
				w += character.glyph.metrics.horizontalAdvance * pointSizeScale + (styleSpacingAdjustment + normalSpacingAdjustment) * emScale;
				if (w > width) return false;
			}
		}

		return true;
	}

#endif

	/// <summary>
	/// Test if a string is a substring of this
	/// </summary>
	public static bool Contains(this string source, string value, bool caseSensitive = true) {
		return source?.IndexOf(value, caseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase) >= 0;
	}

	/// <summary>
	/// Remove all whitespace from this string
	/// </summary>
	public static string RemoveWhitespace(this string str) {

		int len = str.Length;
		char[] src = str.ToCharArray();
		int dstIdx = 0;

		for (int i = 0; i < len; i++) {
			char ch = src[i];

			switch (ch) {
				case '\u0020':
				case '\u00A0':
				case '\u1680':
				case '\u2000':
				case '\u2001':
				case '\u2002':
				case '\u2003':
				case '\u2004':
				case '\u2005':
				case '\u2006':
				case '\u2007':
				case '\u2008':
				case '\u2009':
				case '\u200A':
				case '\u202F':
				case '\u205F':
				case '\u3000':
				case '\u2028':
				case '\u2029':
				case '\u0009':
				case '\u000A':
				case '\u000B':
				case '\u000C':
				case '\u000D':
				case '\u0085':
					continue;

				default:
					src[dstIdx++] = ch;
					break;
			}
		}
		return new string(src, 0, dstIdx);
	}

	/// <summary>
	/// Evaluate this string as a mathematical expression
	/// </summary>
	public static double Evaluate(this string s) {
		return MathExpressionParser.Evaluate(s);
	}

	/// <summary>
	/// Try to evaluate this string as a mathematical expression
	/// </summary>
	public static bool TryEvaluate(this string s, out double result, bool nanIsValid = true) {
		if (MathExpressionParser.TryEvaluate(s, out result)) {
			return nanIsValid || !double.IsNaN(result);
		}
		return false;
	}

	/// <summary>
	/// Shortcut for <see cref="string.IsNullOrEmpty"/>
	/// </summary>
	public static bool IsNullOrEmpty(this string s) => string.IsNullOrEmpty(s);
	/// <summary>
	/// Shortcut for <see cref="string.IsNullOrWhiteSpace"/>
	/// </summary>
	public static bool IsNullOrWhiteSpace(this string s) => string.IsNullOrWhiteSpace(s);

	public static bool IsWhiteSpace(this string value) {
		for (int index = 0; index < value.Length; ++index) {
			if (!char.IsWhiteSpace(value[index]))
				return false;
		}
		return true;
	}

	/// <summary>
	/// Returns a copy of the string with the first letter being uppercase.
	/// </summary>
	/// <param name="s">The string</param>
	/// <returns>A Copy of the string with the first letter being uppercase.</returns>
	public static string FirstLetterToUpper(this string s) {
		if (string.IsNullOrEmpty(s))
			return s;
		return s.Length == 1 ? s.ToUpper(CultureInfo.InvariantCulture) : $"{s.First().ToString().ToUpper(CultureInfo.InvariantCulture)}{s.Substring(1)}";
	}

	/// <summary>
	/// Returns a new string in which all occurrences of a specified string in the current instance are replaced with another 
	/// specified string according the type of search to use for the specified string.
	/// </summary>
	/// <param name="str">The string performing the replace method.</param>
	/// <param name="oldValue">The string to be replaced.</param>
	/// <param name="newValue">The string replace all occurrences of <paramref name="oldValue"/>. 
	/// If value is equal to <c>null</c>, than all occurrences of <paramref name="oldValue"/> will be removed from the <paramref name="str"/>.</param>
	/// <param name="comparisonType">One of the enumeration values that specifies the rules for the search.</param>
	/// <returns>A string that is equivalent to the current string except that all instances of <paramref name="oldValue"/> are replaced with <paramref name="newValue"/>. 
	/// If <paramref name="oldValue"/> is not found in the current instance, the method returns the current instance unchanged.</returns>
	public static string Replace(this string str, string oldValue, string newValue, StringComparison comparisonType) {
		if (str == null) throw new ArgumentNullException(nameof(str));
		if (str.Length == 0) return str;
		if (oldValue == null) throw new ArgumentNullException(nameof(oldValue));
		if (oldValue.Length == 0) throw new ArgumentException("String cannot be of zero length.");

		if (oldValue.Equals(newValue)) return str;

		StringBuilder resultStringBuilder = new StringBuilder(str.Length);

		bool isReplacementNullOrEmpty = string.IsNullOrEmpty(newValue);

		// Replace all values.
		const int valueNotFound = -1;
		int foundAt;
		int startSearchFromIndex = 0;
		while ((foundAt = str.IndexOf(oldValue, startSearchFromIndex, comparisonType)) != valueNotFound) {

			int charsUntilReplacment = foundAt - startSearchFromIndex;
			bool isNothingToAppend = charsUntilReplacment == 0;
			if (!isNothingToAppend) {
				resultStringBuilder.Append(str, startSearchFromIndex, charsUntilReplacment);
			}

			// Process the replacement.
			if (!isReplacementNullOrEmpty) {
				resultStringBuilder.Append(newValue);
			}

			// Prepare start index for the next search.
			startSearchFromIndex = foundAt + oldValue.Length;
			if (startSearchFromIndex == str.Length) {
				return resultStringBuilder.ToString();
			}
		}

		// Append the last part to the result.
		int charsUntilStringEnd = str.Length - startSearchFromIndex;
		resultStringBuilder.Append(str, startSearchFromIndex, charsUntilStringEnd);

		return resultStringBuilder.ToString();
	}

	/// <summary>
	/// Removes all occurances of given char(s)
	/// </summary>
	public static string RemoveChars(this string str, params char[] c) => string.Join(string.Empty, str.Split(c));

	public static bool TryMatchRegex(this string str, string pattern, out Match match) {
		match = Regex.Match(str, pattern);
		return match.Success;
	}

	public static bool TryMatchRegex(this string str, string pattern, out MatchCollection matches) {
		matches = Regex.Matches(str, pattern);
		return matches.Count > 0;
	}

	public static bool IsValidFormat<T>(this string s) where T : IFormattable {
		try {
			string _ = default(T)?.ToString(s, null);
		} catch (Exception) {
			return false;
		}

		return true;
	}

	public static string SHA256(this string s) {
		using (System.Security.Cryptography.SHA256 sha = System.Security.Cryptography.SHA256.Create()) {
			byte[] hash = (sha.ComputeHash(Encoding.UTF8.GetBytes(s)));
			// Convert byte array to a string   
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < hash.Length; i++) {
				builder.Append(hash[i].ToString("x2"));
			}
			return builder.ToString();
		}
	}

	public static string MD5(this string s) {
		using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create()) {
			byte[] hash = (md5.ComputeHash(Encoding.UTF8.GetBytes(s)));
			// Convert byte array to a string   
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < hash.Length; i++) {
				builder.Append(hash[i].ToString("x2"));
			}
			return builder.ToString();
		}
	}

	private static readonly Regex _cjkCharRegex = new Regex(@"\p{IsCJKUnifiedIdeographs}");
	public static bool IsChinese(this char c) {
		return _cjkCharRegex.IsMatch(c.ToString());
	}

}
