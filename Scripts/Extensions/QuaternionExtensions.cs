﻿using UnityEngine;
// ReSharper disable UnusedMember.Global

public static class QuaternionExtensions {
	public static Quaternion Pow(this Quaternion input, float power) {
		float inputMagnitude = input.Magnitude();
		Vector3 nHat = new Vector3(input.x, input.y, input.z).normalized;
		Quaternion vectorBit = new Quaternion(nHat.x, nHat.y, nHat.z, 0)
			.Scale(power * Mathf.Acos(input.w / inputMagnitude))
			.Exp();
		return vectorBit.Scale(Mathf.Pow(inputMagnitude, power));
	}

	public static Quaternion Exp(this Quaternion input) {
		float inputA = input.w;
		Vector3 inputV = new Vector3(input.x, input.y, input.z);
		float outputA = Mathf.Exp(inputA) * Mathf.Cos(inputV.magnitude);
		Vector3 outputV = Mathf.Exp(inputA) * Mathf.Sin(inputV.magnitude) * inputV.normalized;
		return new Quaternion(outputV.x, outputV.y, outputV.z, outputA);
	}

	public static Quaternion Scale(this Quaternion input, float scalar) {
		return new Quaternion(input.x * scalar, input.y * scalar, input.z * scalar, input.w * scalar);
	}

	/// <summary>
	/// Calculate the length of this quaternion
	/// </summary>
	public static float Magnitude(this Quaternion input) {
		return Mathf.Sqrt(input.x * input.x + input.y * input.y + input.z * input.z + input.w * input.w);
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this Quaternion q1, Quaternion q2, float precision) {
		return q1.x.Approximates(q2.x, precision) && q1.y.Approximates(q2.y, precision) && q1.z.Approximates(q2.z, precision) && q1.w.Approximates(q2.w, precision)
			|| q1.x.Approximates(-q2.x, precision) && q1.y.Approximates(-q2.y, precision) && q1.z.Approximates(-q2.z, precision) && q1.w.Approximates(-q2.w, precision);

	}

	/// <summary>
	/// Calculate Euler angles for this quaternion and given order of rotations
	/// </summary>
	public static Vector3 EulerAngles(this Quaternion q, RotSeq rotSeq) {
		switch (rotSeq) {
			case RotSeq.zyx:
				return ThreeAxisRot(2 * (q.x * q.y + q.w * q.z),
					q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z,
					-2 * (q.x * q.z - q.w * q.y),
					2 * (q.y * q.z + q.w * q.x),
					q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z);


			case RotSeq.zyz:
				return TwoAxisRot(2 * (q.y * q.z - q.w * q.x),
					2 * (q.x * q.z + q.w * q.y),
					q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z,
					2 * (q.y * q.z + q.w * q.x),
					-2 * (q.x * q.z - q.w * q.y));


			case RotSeq.zxy:
				return ThreeAxisRot(-2 * (q.x * q.y - q.w * q.z),
					q.w * q.w - q.x * q.x + q.y * q.y - q.z * q.z,
					2 * (q.y * q.z + q.w * q.x),
					-2 * (q.x * q.z - q.w * q.y),
					q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z);


			case RotSeq.zxz:
				return TwoAxisRot(2 * (q.x * q.z + q.w * q.y),
					-2 * (q.y * q.z - q.w * q.x),
					q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z,
					2 * (q.x * q.z - q.w * q.y),
					2 * (q.y * q.z + q.w * q.x));


			case RotSeq.yxz:
				return ThreeAxisRot(2 * (q.x * q.z + q.w * q.y),
					q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z,
					-2 * (q.y * q.z - q.w * q.x),
					2 * (q.x * q.y + q.w * q.z),
					q.w * q.w - q.x * q.x + q.y * q.y - q.z * q.z);

			case RotSeq.yxy:
				return TwoAxisRot(2 * (q.x * q.y - q.w * q.z),
					2 * (q.y * q.z + q.w * q.x),
					q.w * q.w - q.x * q.x + q.y * q.y - q.z * q.z,
					2 * (q.x * q.y + q.w * q.z),
					-2 * (q.y * q.z - q.w * q.x));


			case RotSeq.yzx:
				return ThreeAxisRot(-2 * (q.x * q.z - q.w * q.y),
					q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z,
					2 * (q.x * q.y + q.w * q.z),
					-2 * (q.y * q.z - q.w * q.x),
					q.w * q.w - q.x * q.x + q.y * q.y - q.z * q.z);


			case RotSeq.yzy:
				return TwoAxisRot(2 * (q.y * q.z + q.w * q.x),
					-2 * (q.x * q.y - q.w * q.z),
					q.w * q.w - q.x * q.x + q.y * q.y - q.z * q.z,
					2 * (q.y * q.z - q.w * q.x),
					2 * (q.x * q.y + q.w * q.z));


			case RotSeq.xyz:
				return ThreeAxisRot(-2 * (q.y * q.z - q.w * q.x),
					q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z,
					2 * (q.x * q.z + q.w * q.y),
					-2 * (q.x * q.y - q.w * q.z),
					q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z);


			case RotSeq.xyx:
				return TwoAxisRot(2 * (q.x * q.y + q.w * q.z),
					-2 * (q.x * q.z - q.w * q.y),
					q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z,
					2 * (q.x * q.y - q.w * q.z),
					2 * (q.x * q.z + q.w * q.y));


			case RotSeq.xzy:
				return ThreeAxisRot(2 * (q.y * q.z + q.w * q.x),
					q.w * q.w - q.x * q.x + q.y * q.y - q.z * q.z,
					-2 * (q.x * q.y - q.w * q.z),
					2 * (q.x * q.z + q.w * q.y),
					q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z);


			case RotSeq.xzx:
				return TwoAxisRot(2 * (q.x * q.z - q.w * q.y),
					2 * (q.x * q.y + q.w * q.z),
					q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z,
					2 * (q.x * q.z + q.w * q.y),
					-2 * (q.x * q.y - q.w * q.z));

			default:
				Utilities.Debug.LogError("No good sequence");
				return Vector3.zero;

		}
	}

	// ReSharper disable InconsistentNaming
	public enum RotSeq {
		zyx,
		zyz,
		zxy,
		zxz,
		yxz,
		yxy,
		yzx,
		yzy,
		xyz,
		xyx,
		xzy,
		xzx
	};
	// ReSharper restore InconsistentNaming

	private static Vector3 TwoAxisRot(float r11, float r12, float r21, float r31, float r32) {
		return new Vector3 {
			x = Mathf.Atan2(r11, r12) * Mathf.Rad2Deg,
			y = Mathf.Acos(r21) * Mathf.Rad2Deg,
			z = Mathf.Atan2(r31, r32) * Mathf.Rad2Deg
		};
	}

	private static Vector3 ThreeAxisRot(float r11, float r12, float r21, float r31, float r32) {
		return new Vector3 {
			x = Mathf.Atan2(r31, r32) * Mathf.Rad2Deg,
			y = Mathf.Asin(r21) * Mathf.Rad2Deg,
			z = Mathf.Atan2(r11, r12) * Mathf.Rad2Deg
		};
	}

	public static void Deconstruct(this Quaternion quat, out float x, out float y, out float z, out float w) {
		x = quat.x;
		y = quat.y;
		z = quat.z;
		w = quat.w;
	}

	public static void Set(this Quaternion quat, float x, float y, float z, float w) {
		quat.x = x;
		quat.y = y;
		quat.z = z;
		quat.w = w;
	}
	
	public static Quaternion X(this Quaternion vec, float x) {
		vec.x = x;
		return vec;
	}

	public static Quaternion Y(this Quaternion vec, float y) {
		vec.y = y;
		return vec;
	}

	public static Quaternion Z(this Quaternion vec, float z) {
		vec.z = z;
		return vec;
	}

	public static Quaternion W(this Quaternion vec, float w) {
		vec.w = w;
		return vec;
	}

	public static string ConstructorCode(this Quaternion quat) => $"new Vector4({quat.x:0.###}f, {quat.y:0.###}f, {quat.z:0.###}f, {quat.w:0.###}f)";
}
