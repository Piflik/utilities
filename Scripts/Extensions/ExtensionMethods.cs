﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Reflection;
using Utilities;
using UnityEngine;
using UnityEngine.UI;
using Component = UnityEngine.Component;
using Object = UnityEngine.Object;
// ReSharper disable UnusedMember.Global

public static partial class ExtensionMethods {

	public static T AddComponent<T>(this Component c) where T : Component {
		return c.gameObject.AddComponent<T>();
	}

	/// <summary>
	/// Gets an existing Component or adds it
	/// </summary>
	public static T GetOrAddComponent<T>(this GameObject go) where T : Component {
		T t = go.GetComponent<T>();
		return t != null ? t : go.AddComponent<T>();
	}

	/// <summary>
	/// Gets an existing Component or adds it
	/// </summary>
	public static T GetOrAddComponent<T>(this Component component) where T : Component {
		T com = component.GetComponent<T>();
		return com != null ? com : component.AddComponent<T>();
	}

	/// <summary>
	/// Try to get a Component
	/// </summary>
	/// <returns>Success</returns>
	public static bool TryGetComponent<T>(this Component component, out T outComponent) where T : Component {
		return component.gameObject.TryGetComponent(out outComponent);
	}

	/// <summary>
	/// Try to get a Component
	/// </summary>
	/// <returns>Success</returns>
	public static bool TryGetComponent<T>(this GameObject gameObject, out T outComponent) where T : Component {
		outComponent = gameObject.GetComponent<T>();
		return outComponent != null;
	}

	public static bool TryGetComponentInParent<T>(this GameObject gameObject, out T outComponent) where T : Component {
		outComponent = gameObject.GetComponentInParent<T>();
		return outComponent != null;
	}

	public static bool TryGetComponentInParent<T>(this Component component, out T outComponent) where T : Component {
		outComponent = component.GetComponentInParent<T>();
		return outComponent != null;
	}

	public static bool TryGetComponentsInParent<T>(this GameObject gameObject, out T[] outComponents, bool includeInactive = false) where T : Component {
		outComponents = gameObject.GetComponentsInParent<T>(includeInactive);
		return outComponents != null && outComponents.Length > 0;
	}

	public static bool TryGetComponentsInParent<T>(this Component component, out T[] outComponents, bool includeInactive = false) where T : Component {
		outComponents = component.GetComponentsInParent<T>(includeInactive);
		return outComponents != null && outComponents.Length > 0;
	}

	public static bool TryGetComponentInChildren<T>(this GameObject gameObject, out T outComponent, bool includeInactive = false) where T : Component {
		outComponent = gameObject.GetComponentInChildren<T>(includeInactive);
		return outComponent != null;
	}

	public static bool TryGetComponentInChildren<T>(this Component component, out T outComponent, bool includeInactive = false) where T : Component {
		outComponent = component.GetComponentInChildren<T>(includeInactive);
		return outComponent != null;
	}

	public static bool TryGetComponentsInChildren<T>(this GameObject gameObject, out T[] outComponents, bool includeInactive = false) where T : Component {
		outComponents = gameObject.GetComponentsInChildren<T>(includeInactive);
		return outComponents != null && outComponents.Length > 0;
	}

	public static bool TryGetComponentsInChildren<T>(this Component component, out T[] outComponents, bool includeInactive = false) where T : Component {
		outComponents = component.GetComponentsInChildren<T>(includeInactive);
		return outComponents != null && outComponents.Length > 0;
	}

	/// <summary>
	/// Calculate the intensity of a color
	/// </summary>
	public static float Intensity(this Color c) {
		return c.r * 0.3f + c.g * 0.59f + c.b * 0.11f;
	}

	/// <summary>
	/// Get the Next Enum.
	/// Useful for cycling through enum states
	/// </summary>
	public static T NextEnum<T>(this T src) where T : struct {
		if (!typeof(T).IsEnum)
			throw new ArgumentException($"Arguments {typeof(T).FullName} is not an Enum");

		T[] arr = (T[])Enum.GetValues(src.GetType());
		int j = Array.IndexOf(arr, src) + 1;
		return arr.Length == j ? arr[0] : arr[j];
	}

	/// <summary>
	/// Get the Previous Enum.
	/// Useful for cycling through enum states
	/// </summary>
	public static T PreviousEnum<T>(this T src) where T : struct {
		if (!typeof(T).IsEnum)
			throw new ArgumentException($"Arguments {typeof(T).FullName} is not an Enum");

		T[] arr = (T[])Enum.GetValues(src.GetType());
		int j = Array.IndexOf(arr, src) - 1;
		return j == -1 ? arr[arr.Length - 1] : arr[j];
	}

	public static string GetDescription(this Enum value) {
		Type type = value.GetType();
		string name = Enum.GetName(type, value);
		if (name != null) {
			FieldInfo field = type.GetField(name);
			if (field != null && Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attr) {
				return attr.Description;
			}
		}

		return name;
	}

	/// <summary>
	/// Returns true if two Transforms share a hierarchy
	/// </summary>
	public static bool InHierarchy(this Transform transA, Transform transB) {
		return transA.root == transB.root;
	}

	/// <summary>
	/// Determine if a Transform is descendant of another Transform
	/// </summary>
	public static bool InSubtreeOf(this Transform transA, Transform transB) {
		if (transB == null)
			return false;
		while (transA != null) {
			if (ReferenceEquals(transA, transB))
				return true;
			transA = transA.parent;
		}

		return false;
	}

	//Breadth-first search
	[Obsolete("Since Unity 5.4, transforms are stored in Depth-First order in memory, so DFS-search is more efficient")]
	public static Transform FindBroad(this Transform aParent, string aName) {
		Transform result = aParent.Find(aName);
		if (result != null)
			return result;
		foreach (Transform child in aParent) {
			result = child.FindBroad(aName);
			if (result != null)
				return result;
		}

		return null;
	}

	//Depth-first search
	public static Transform FindDeep(this Transform aParent, string aName) {
		foreach (Transform child in aParent) {
			if (child.name == aName)
				return child;
			Transform result = child.FindDeep(aName);
			if (result != null)
				return result;
		}

		return null;
	}

	/// <summary>
	/// Find a parent of a Transform by name
	/// </summary>
	public static Transform FindParent(this Transform trans, string name) {
		while (trans.name != name) {
			if (trans.parent == null)
				return null;

			trans = trans.parent;
		}

		return trans;
	}

	public static void SortChildren<TKey>(this Transform transform, Func<Transform, TKey> indexSelector) {
		List<Transform> children = transform.Cast<Transform>().OrderBy(indexSelector).ToList();
		for (int i = 0; i < children.Count; i++) {
			children[i].SetSiblingIndex(i);
		}
	}

	/// <summary>
	/// Set position and Rotation simultaneously to the Values of another Transform
	/// </summary>
	public static void SetPositionAndRotation(this Transform dst, Transform src) {
		dst.SetPositionAndRotation(src.position, src.rotation);
	}

	public static void Set(this Transform dst, Transform src, bool local = false) {
		dst.Set(src.position, src.rotation, src.localScale, local);
	}

	public static void Set(this Transform dst, Vector3 position, Quaternion rotation, Vector3 scale, bool local = false) {
		if (local) {
			dst.localPosition = position;
			dst.localRotation = rotation;
		} else {
			dst.SetPositionAndRotation(position, rotation);
		}

		dst.localScale = scale;
	}

	/// <summary>
	/// Calculate the depth within the hierarchy
	/// </summary>
	public static int Depth(this Transform t) {
		int d = 0;

		while (t.parent != null) {
			t = t.parent;
			d++;
		}

		return d;
	}

	/// <summary>
	/// Get the Canvas on the closest ancestor of a transform (including the transform itself)
	/// </summary>
	public static Canvas GetCanvas(this Component comp) {
		if (comp == null) return null;

		Transform t = comp.transform;
		Canvas c = t.GetComponent<Canvas>();

		while (c == null) {
			if (t.parent == null) return null;

			t = t.parent;
			c = t.GetComponent<Canvas>();
		}

		return c;
	}

	/// <summary>
	/// Set size of canvas; no effect if canvas is not in World Space
	/// pixelsPerUnit less than 0 are ignored
	/// </summary>
	public static void SetResolution(this Canvas canvas, int width, int height, float referencePixelsPerUnit = -1, float dynamicPixelsPerUnit = -1) {
		if (canvas.renderMode != RenderMode.WorldSpace) return;

		if (canvas.TryGetComponent(out RectTransform rTrans)) {
			rTrans.sizeDelta = new Vector2(width, height);
		}

		if (canvas.TryGetComponent(out CanvasScaler cScaler)) {
			if (referencePixelsPerUnit > 0) cScaler.referencePixelsPerUnit = referencePixelsPerUnit;
			if (dynamicPixelsPerUnit > 0) cScaler.dynamicPixelsPerUnit = dynamicPixelsPerUnit;
		}
	}

	/// <summary>
	/// Find the lowest common ancestor
	/// </summary>
	public static Transform LCA(this Transform me, Transform other) {
		int dMe = me.Depth();
		int dOther = other.Depth();

		int diff = dMe - dOther;

		if (diff < 0) {
			for (int i = 0; i < -diff; ++i) {
				other = other.parent;
			}
		} else if (diff > 0) {
			for (int i = 0; i < diff; ++i) {
				me = me.parent;
			}
		}

		while (me != other) {
			me = me.parent;
			other = other.parent;
		}

		return me;
	}

	/// <summary>
	/// Set the layer of the GameObject owning this component and its children, if recursive
	/// </summary>
	public static void SetLayer(this Component c, int layer, bool recursive = true) {
		SetLayer(c.gameObject, layer, recursive);
	}

	/// <summary>
	/// Set the layer of this GameObject and its children, if recursive
	/// </summary>
	public static void SetLayer(this GameObject go, int layer, bool recursive = true) {
		go.layer = layer;

		if (recursive) {
			foreach (Transform t in go.transform) {
				t.gameObject.SetLayer(layer);
			}
		}
	}

	/// <summary>
	/// Parent the transform of this component to the transform of another component
	/// </summary>
	public static void SetParent(this Component c, Component parent, bool worldPositionStays = true, bool inheritLayer = false, bool recursivelyForChildren = false) {
		SetParent(c.transform, parent.transform, worldPositionStays, inheritLayer, recursivelyForChildren);
	}

	/// <summary>
	/// Parent the transform of this component to another transform
	/// </summary>
	public static void SetParent(this Component c, Transform parent, bool worldPositionStays = true, bool inheritLayer = false, bool recursivelyForChildren = false) {
		SetParent(c.transform, parent, worldPositionStays, inheritLayer, recursivelyForChildren);
	}

	/// <summary>
	/// Parent the transform of this GameObject to another transform
	/// </summary>
	public static void SetParent(this GameObject go, Transform parent, bool worldPositionStays = true, bool inheritLayer = false, bool recursivelyForChildren = false) {
		SetParent(go.transform, parent, worldPositionStays, inheritLayer, recursivelyForChildren);
	}

	/// <summary>
	/// Parent this transform to another transform
	/// </summary>
	public static void SetParent(this Transform transform, Transform parent, bool worldPositionStays, bool inheritLayer, bool recursivelyForChildren = false) {
		if (transform == parent) return;
		transform.SetParent(parent, worldPositionStays);
		if (inheritLayer) {
			transform.SetLayer(transform.parent.gameObject.layer, recursivelyForChildren);
		}
	}

	/// <summary>
	/// Destroys all children of this Component's GameObject
	/// </summary>
	public static void DestroyAllChildren(this Component c) {
		c.transform.DestroyAllChildren();
	}

	/// <summary>
	/// Destroys all children of this GameObject
	/// </summary>
	public static void DestroyAllChildren(this GameObject go) {
		go.transform.DestroyAllChildren();
	}

	/// <summary>
	/// Destroys all children of this transform
	/// </summary>
	public static void DestroyAllChildren(this Transform transform) {
		foreach (Transform t in transform) {
			Object.Destroy(t.gameObject);
		}
	}

	/// <summary>
	/// Read a Field via Reflection; CAUTION: Does not work with Properties
	/// </summary>
	/// <returns>success</returns>
	public static bool GetField(this object obj, string fieldName, out object value, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance) {
		FieldInfo field = obj.GetType().GetField(fieldName, bindingFlags);

		if (field == null) {
			value = null;
			return false;
		}

		value = field.GetValue(obj);
		return true;
	}

	/// <summary>
	/// Read a Field via Reflection; CAUTION: Does not work with Properties
	/// </summary>
	/// <returns>success</returns>
	public static bool GetField<T>(this object obj, string fieldName, out T value, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance) {
		if (GetField(obj, fieldName, out object objValue, bindingFlags) && objValue is T val) {
			value = val;
			return true;
		}

		value = default;
		return false;
	}

	/// <summary>
	/// Write a Field via Reflection; CAUTION: Does not work with Properties
	/// </summary>
	/// <returns>success</returns>
	public static bool SetField(this object obj, string fieldName, object value, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance) {
		FieldInfo prop = obj.GetType().GetField(fieldName, bindingFlags);
		if (prop == null)
			return false;
		prop.SetValue(obj, value);
		return true;
	}



	/// <summary>
	/// Read a Property via Reflection; CAUTION: Does not work with Fields
	/// </summary>
	/// <returns>success</returns>
	public static bool GetProperty(this object obj, string propertyName, out object value, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance) {
		PropertyInfo prop = obj.GetType().GetProperty(propertyName, bindingFlags);

		if (prop == null) {
			value = null;
			return false;
		}

		value = prop.GetValue(obj, null);
		return true;
	}

	/// <summary>
	/// Read a Property via Reflection; CAUTION: Does not work with Fields
	/// </summary>
	/// <returns>success</returns>
	public static bool GetProperty<T>(this object obj, string propertyName, out T value, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance) {
		if (GetProperty(obj, propertyName, out object objValue, bindingFlags) && objValue is T val) {
			value = val;
			return true;
		}

		value = default;
		return false;
	}

	/// <summary>
	/// Write a Property via Reflection; CAUTION: Does not work with Fields
	/// </summary>
	/// <returns>success</returns>
	public static bool SetProperty(this object obj, string propertyName, object value, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance) {
		PropertyInfo prop = obj.GetType().GetProperty(propertyName, bindingFlags);
		if (prop == null)
			return false;
		prop.SetValue(obj, value, null);
		return true;
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this float f1, float f2) {
		return Mathf.Approximately(f1, f2);
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this float f1, float f2, float precision) {
		return Mathf.Abs(f1 - f2) <= precision;
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this double d1, double d2) {
		return Math.Abs(d1 - d2) <= Math.Max(1E-15 * Math.Max(Math.Abs(d1), Math.Abs(d2)), double.Epsilon * 8f);
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this double d1, double d2, double precision) {
		return Math.Abs(d1 - d2) <= precision;
	}

	/// <summary>
	/// Approximate equality (to avoid false negatives due to float inaccuracies)
	/// </summary>
	public static bool Approximates(this Color c1, Color c2, float precision) {
		return c1.r.Approximates(c2.r, precision) && c1.g.Approximates(c2.g, precision) && c1.b.Approximates(c2.b, precision) && c1.a.Approximates(c2.a, precision);
	}

	/// <summary>
	/// Linearly interpolate a value from one range into another; Values outside of the source range will be clamped to min and max
	/// </summary>
	public static float Map(this float value, float valueMin, float valueMax, float resultMin, float resultMax) {
		if (valueMin.Approximates(valueMax)) return resultMin;
		return Mathf.Lerp(resultMin, resultMax, Mathf.InverseLerp(valueMin, valueMax, value));
	}

	/// <summary>
	/// Linearly interpolate a value from one range into another; Values outside of the source range will be clamped to min and max
	/// </summary>
	public static float Map(this float value, FloatRange valueRange, FloatRange resultRange) {
		if (valueRange.size.Approximates(0)) return resultRange.min;
		return resultRange.Lerp(valueRange.RelativeValue(value), true);
	}

	/// <summary>
	/// Linearly interpolate a value from one range into another; Values outside of the source range will be extrapolated
	/// </summary>
	public static float MapUnclamped(this float value, float valueMin, float valueMax, float resultMin, float resultMax) {
		if (valueMin.Approximates(valueMax)) return resultMin;
		return Mathf.LerpUnclamped(resultMin, resultMax, (value - valueMin) / (valueMax - valueMin));
	}

	/// <summary>
	/// Linearly interpolate a value from one range into another; Values outside of the source range will be extrapolated
	/// </summary>
	public static float MapUnclamped(this float value, FloatRange valueRange, FloatRange resultRange) {
		if (valueRange.size.Approximates(0)) return resultRange.min;
		return resultRange.Lerp(valueRange.RelativeValue(value), false);
	}

	/// <summary>
	/// Get the Broadcast Address for a Subnet
	/// </summary>
	public static IPAddress GetBroadcastAddress(this IPAddress address, IPAddress subnetMask) {
		byte[] ipAdressBytes = address.GetAddressBytes();
		byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

		if (ipAdressBytes.Length != subnetMaskBytes.Length)
			throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

		byte[] broadcastAddress = new byte[ipAdressBytes.Length];
		for (int i = 0; i < broadcastAddress.Length; i++) {
			broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 0xff));
		}

		return new IPAddress(broadcastAddress);
	}

	/// <summary>
	/// Get the Network Addres for a Subnet
	/// </summary>
	public static IPAddress GetNetworkAddress(this IPAddress address, IPAddress subnetMask) {
		byte[] ipAdressBytes = address.GetAddressBytes();
		byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

		if (ipAdressBytes.Length != subnetMaskBytes.Length)
			throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

		byte[] networkAddress = new byte[ipAdressBytes.Length];
		for (int i = 0; i < networkAddress.Length; i++) {
			networkAddress[i] = (byte)(ipAdressBytes[i] & subnetMaskBytes[i]);
		}

		return new IPAddress(networkAddress);
	}

	/// <summary>
	/// Determine if two IP Addresses are in the same Subnet
	/// </summary>
	public static bool IsInSameSubnet(this IPAddress address2, IPAddress address, IPAddress subnetMask) {
		IPAddress network1 = address.GetNetworkAddress(subnetMask);
		IPAddress network2 = address2.GetNetworkAddress(subnetMask);

		return network1.Equals(network2);
	}

	/// <summary>
	/// Get the string slice between the two indexes.
	/// Inclusive for start index, exclusive for end index.
	/// </summary>
	public static string Slice(this string source, int start, int end) {
		if (end < 0) {
			// Keep this for negative end support
			end = source.Length + end;
		}

		int len = end - start; // Calculate length
		return source.Substring(start, len); // Return Substring of length
	}

	/// <summary>
	/// Get the array slice between the two indexes.
	/// ... Inclusive for start index, exclusive for end index.
	/// </summary>
	public static T[] Slice<T>(this T[] source, int start, int end) {
		// Handles negative ends.
		if (end < 0) {
			end = source.Length + end;
		}

		int len = end - start;

		// Return new array.
		T[] res = new T[len];
		Array.Copy(source, start, res, 0, len);
		return res;
	}

	public static bool TryGet<T>(this IEnumerable<T> source, Func<T, bool> predicate, out T output) {
		output = source.FirstOrDefault(predicate);
		return output != null;
	}

	public static bool TryGetIndex<T>(this T[] source, T obj, out int index) {
		index = Array.IndexOf(source, obj);
		return index >= 0;
	}

	public static int GetIndex<T>(this T[] source, Func<T, bool> predicate) {
		for (int i = 0; i < source.Length; ++i) {
			if (predicate(source[i])) {
				return i;
			}
		}

		return -1;
	}

	public static bool TryGetIndex<T>(this T[] source, Func<T, bool> predicate, out int index) {
		index = source.GetIndex(predicate);
		return index >= 0;
	}

	public static bool TryGetIndex<T>(this List<T> source, T obj, out int index) {
		index = source.IndexOf(obj);
		return index >= 0;
	}

	public static int GetIndex<T>(this List<T> source, Func<T, bool> predicate) {
		for (int i = 0; i < source.Count; ++i) {
			if (predicate(source[i])) {
				return i;
			}
		}

		return -1;
	}

	public static bool TryGetIndex<T>(this List<T> source, Func<T, bool> predicate, out int index) {
		index = source.GetIndex(predicate);
		return index >= 0;
	}

	public static T Random<T>(this T[] array) {
		return array[UnityEngine.Random.Range(0, array.Length)];
	}

	public static T Random<T>(this List<T> list) {
		return list[UnityEngine.Random.Range(0, list.Count)];
	}

	public static T[] Shuffle<T>(this T[] array, bool inPlace = true, System.Random rng = null) {
		if (rng == null) rng = new System.Random();

		T[] output;
		if (inPlace) {
			output = array;
		} else {
			output = new T[array.Length];
			Array.Copy(array, output, array.Length);
		}

		int n = output.Length;
		while (n > 1) {
			int k = rng.Next(n--);
			T temp = output[n];
			output[n] = output[k];
			output[k] = temp;
		}

		return output;
	}

	/// <summary>
	/// Append the content of a Queue to this Queue
	/// </summary>
	public static void Append<T>(this Queue<T> dst, Queue<T> src) {
		foreach (T obj in src) {
			dst.Enqueue(obj);
		}
	}

	/// <summary>
	/// Remove the first occurance of an object
	/// Warning: O(n)
	/// </summary>
	/// <returns>True if an object has been removed</returns>
	public static bool Remove<T>(this Queue<T> queue, T obj) where T : IEquatable<T> {
		bool removed = false;
		int c = queue.Count;
		for (int i = 0; i < c; ++i) {
			T o = queue.Dequeue();
			if (!removed && Equals(obj, o)) {
				removed = true;
				continue;
			}

			queue.Enqueue(o);
		}

		return removed;
	}

	/// <summary>
	/// Remove all occurances of an object
	/// Warning: O(n)
	/// </summary>
	/// <returns>True if an object has been removed</returns>
	public static bool RemoveAll<T>(this Queue<T> queue, T obj) where T : IEquatable<T> {
		bool removed = false;
		int c = queue.Count;
		for (int i = 0; i < c; ++i) {
			T o = queue.Dequeue();
			if (Equals(o, obj)) {
				removed = true;
				continue;
			}

			queue.Enqueue(o);
		}

		return removed;
	}

	/// <summary>
	/// Remove the first occurance of an object
	/// Warning: O(2n)
	/// </summary>
	/// <returns>True if an object has been removed</returns>
	public static bool Remove<T>(this Stack<T> stack, T obj) where T : IEquatable<T> {
		bool removed = false;
		Stack<T> tempStack = new Stack<T>();
		int c = stack.Count;
		for (int i = 0; i < c; ++i) {
			T o = stack.Pop();
			if (Equals(o, obj)) {
				removed = true;
				break;
			}

			tempStack.Push(o);
		}

		while (!tempStack.IsEmpty()) {
			stack.Push(tempStack.Pop());
		}

		return removed;
	}

	/// <summary>
	/// Remove all occurances of an object
	/// Warning: O(2n)
	/// </summary>
	/// <returns>True if an object has been removed</returns>
	public static bool RemoveAll<T>(this Stack<T> stack, T obj) where T : IEquatable<T> {
		bool removed = false;
		Stack<T> tempStack = new Stack<T>();
		int c = stack.Count;
		for (int i = 0; i < c; ++i) {
			T o = stack.Pop();
			if (Equals(o, obj)) {
				removed = true;
				continue;
			}

			tempStack.Push(o);
		}

		while (!tempStack.IsEmpty()) {
			stack.Push(tempStack.Pop());
		}

		return removed;
	}

	public static void Add<T>(this HashSet<T> src, IEnumerable<T> objs) {
		foreach (T obj in objs) {
			src.Add(obj);
		}
	}

	public static void Remove<T>(this HashSet<T> src, IEnumerable<T> objs) {
		foreach (T obj in objs) {
			src.Remove(obj);
		}
	}

	/// <summary>
	/// Try to dequeue an element; returns false if empty
	/// </summary>
	public static bool TryDequeue<T>(this Queue<T> queue, out T element) {
		if (queue.Count < 1) {
			element = default;
			return false;
		}

		element = queue.Dequeue();
		return true;
	}

	/// <summary>
	/// Try to pop an element; returns false if empty
	/// </summary>
	public static bool TryPop<T>(this Stack<T> stack, out T element) {
		if (stack.Count < 1) {
			element = default;
			return false;
		}

		element = stack.Pop();
		return true;
	}

	/// <summary>
	/// Adjust the Camera Settings to provide a given resolution at a given FoV 
	/// </summary>
	public static void AdjustFoV(this Camera cam, float fieldOfView, int resolutionWidth, int resolutionHeight, float clFarOff = 100f, float clNearOff = 400f) {
		float distance = CameraUtilities.CalcCamDistance(fieldOfView, resolutionWidth, resolutionHeight);

		Vector3 newPos = cam.transform.localPosition;
		newPos.z = (int)-distance;
		cam.transform.localPosition = newPos;

		cam.fieldOfView = fieldOfView;
		cam.farClipPlane = distance + clFarOff;
		cam.nearClipPlane = Mathf.Max(distance - clNearOff, 0.01f);
	}

	/// <summary>
	/// Test if an object is visible to this camera
	/// </summary>
	public static bool CanSee(this Camera cam, Renderer obj) {
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);
		return GeometryUtility.TestPlanesAABB(planes, obj.bounds);
	}

	/// <summary>
	/// Test if a point in world space is visible to this camera
	/// </summary>
	public static bool CanSee(this Camera cam, Vector3 point) {
		point = cam.WorldToViewportPoint(point, Camera.MonoOrStereoscopicEye.Mono);
		return point.z >= cam.nearClipPlane && point.z <= cam.farClipPlane
											&& point.x >= 0 && point.x <= 1
											&& point.y >= 0 && point.y <= 1;
	}

	/// <summary>
	/// Test if a number is a power of 2
	/// </summary>
	public static bool IsPowerOfTwo(this int i) {
		return i != 0 && (i & (i - 1)) == 0;
	}

	/// <summary>
	/// Set Start and End Color   
	/// </summary>
	public static void SetColor(this LineRenderer renderer, Color color) {
		renderer.startColor = renderer.endColor = color;
	}

	/// <summary>
	/// Test if this object is part of a list of objects
	/// </summary>
	public static bool IsIn<T>(this T source, params T[] list) {
		if (null == source) throw new ArgumentNullException(nameof(source));
		return list.Contains(source);
	}

	/// <summary>
	/// Test if an object is between an lower and upper bound
	/// </summary>
	public static bool IsInRange<T>(this T value, T lower, T upper, bool includeLower = true, bool includeUpper = true) where T : IComparable<T> {
		return (includeLower && value.CompareTo(lower) >= 0 || value.CompareTo(lower) > 0) &&
				(includeUpper && value.CompareTo(upper) <= 0 || value.CompareTo(upper) < 0);
	}

	/// <summary>
	/// Convert to type T
	/// </summary>
	public static T Convert<T>(this IConvertible obj) {
		return (T)System.Convert.ChangeType(obj, typeof(T));
	}

	/// <summary>
	/// Convert to type T; returns default value if conversion fails
	/// </summary>
	public static T Convert<T>(this IConvertible obj, T defaultValue) {
		try {
			return Convert<T>(obj);
		} catch (Exception e) {
			if (e is InvalidCastException || e is FormatException || e is OverflowException || e is ArgumentNullException) {
				return defaultValue;
			}

			throw;
		}
	}

	/// <summary>
	/// Try to convert to type T
	/// </summary>
	/// <returns>Success</returns>
	public static bool TryConvert<T>(this IConvertible obj, out T convertedObject, T defaultValue = default) {
		try {
			convertedObject = Convert<T>(obj);
			return true;
		} catch (Exception e) {
			if (e is InvalidCastException || e is FormatException || e is OverflowException || e is ArgumentNullException) {
				convertedObject = defaultValue;
				return false;
			}

			throw;
		}
	}

	/// <summary>
	/// Try to get the key to a value stored in this dictionary
	/// When a value is present multiple times, it returns one of the respective keys, which one is undefined
	/// </summary>
	public static bool TryGetKey<TKey, TValue>(this Dictionary<TKey, TValue> dict, TValue value, out TKey key) where TValue : IEquatable<TValue> {
		if (dict != null) {
			foreach ((TKey key1, TValue value1) in dict) {
				if (value1.Equals(value)) {
					key = key1;
					return true;
				}
			}
		}

		key = default;
		return false;
	}

	public static bool TryGetValueAny<TKey, TValue>(this Dictionary<TKey, TValue> dict, out TValue value, params TKey[] keys) {
		foreach (TKey key in keys) {
			if (dict.TryGetValue(key, out value)) return true;
		}

		value = default;
		return false;
	}

	public static bool TryGetValuesAll<TKey, TValue>(this Dictionary<TKey, TValue> dict, out TValue[] values, params TKey[] keys) {
		values = new TValue[keys.Length];
		for (int i = 0; i < keys.Length; ++i) {
			if (!dict.TryGetValue(keys[i], out values[i])) return false;
		}

		return true;
	}

	public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue defaultValue = default)
		=> dict.TryGetValue(key, out TValue value) ? value : defaultValue;

	//TODO remove when/if this is added in C#8.0
	public static bool TryAdd<TKey, TVal>(this Dictionary<TKey, TVal> dict, TKey key, TVal value) {
		if (dict.ContainsKey(key)) return false;
		dict.Add(key, value);
		return true;
	}

	public static bool ContainsKeyAny<TKey, TVal>(this Dictionary<TKey, TVal> dict, params TKey[] keys) {
		foreach (TKey key in keys) {
			if (dict.ContainsKey(key)) return true;
		}

		return false;
	}

	public static bool ContainsKeyAll<TKey, TVal>(this Dictionary<TKey, TVal> dict, params TKey[] keys) {
		foreach (TKey key in keys) {
			if (!dict.ContainsKey(key)) return false;
		}

		return true;
	}

	private static readonly byte[] _pngHeader = { 137, 80, 78, 71, 13, 10, 26, 10 };
	private static readonly byte[] _jpgHeader = { 255, 216, 255, 224, 0, 16, 74, 70, 73, 70 };

	/// <summary>
	/// Test if this byte array starts with a png-header
	/// </summary>
	public static bool IsPNG(this byte[] data) {
		return data.StartsWith(_pngHeader);
	}

	/// <summary>
	/// Test if this byte array starts with a jpg-header
	/// </summary>
	/// <param name="data"></param>
	/// <returns></returns>
	public static bool IsJPG(this byte[] data) {
		return data.StartsWith(_jpgHeader);
	}

	/// <summary>
	/// Test if this array starts with another array
	/// </summary>
	public static bool StartsWith<T>(this T[] data, params T[] src) where T : IEquatable<T> {
		if (data.Length < src.Length) return false;
		bool equal = true;
		for (int i = 0; i < src.Length; ++i) {
			equal &= data[i].Equals(src[i]);
		}

		return equal;
	}

	/// <summary>
	/// Deconstruct <see cref="KeyValuePair{TKey,TValue}"/> into separate key and value
	/// </summary>
	public static void Deconstruct<T1, T2>(this KeyValuePair<T1, T2> kvp, out T1 key, out T2 value) {
		key = kvp.Key;
		value = kvp.Value;
	}

	public static Texture2D GetTexture(this Gradient gradient, int width, int height, TextureFormat textureFormat = TextureFormat.ARGB32, bool linear = true, bool mipChain = false, TextureWrapMode wrapMode = TextureWrapMode.Clamp) {
		if (width < 1 || height < 1) return new Texture2D(1, 1);

		Texture2D tex = new Texture2D(width, height, textureFormat, mipChain, linear) { wrapMode = wrapMode };
		Color32[] colors = new Color32[width * height];

		float sampleStep = 1f / width;

		for (int x = 0; x < width; ++x) {
			Color32 c = gradient.Evaluate(x * sampleStep);
			for (int y = 0; y < height; ++y) {
				colors[y * width + x] = c;
			}
		}

		tex.SetPixels32(colors);
		tex.Apply(mipChain);

		return tex;
	}

	public static Texture2D ToTexture2D(this Texture texture, bool copy = false) {
		switch (texture) {
			case null:
				return null;
			case Texture2D tex2D: {
				if (copy) {
					Texture2D outTexture = new Texture2D(tex2D.width, tex2D.height, tex2D.format, tex2D.mipmapCount > 1);
					outTexture.LoadRawTextureData(tex2D.GetRawTextureData());
					outTexture.Apply();
					return outTexture;
				}

				return tex2D;
			}
			case RenderTexture rTex: {
				RenderTexture current = RenderTexture.active;
				RenderTexture.active = rTex;
				Texture2D tex2D = new Texture2D(rTex.width, rTex.height);
				tex2D.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
				RenderTexture.active = current;
				tex2D.Apply();
				return tex2D;
			}
			default:
				return Texture2D.CreateExternalTexture(texture.width, texture.height, TextureFormat.RGB24, false, false, texture.GetNativeTexturePtr());
		}
	}

	public static float DistanceToPoint(this Ray ray, Vector3 point) {
		return Vector3.Dot(point - ray.origin, ray.direction);
	}

	//ref: http://geomalgorithms.com/a07-_distance.html
	public static float DistanceToLine(this Ray ray, Vector3 lineStart, Vector3 lineEnd) {
		Vector3 rayDir = ray.direction;
		Vector3 lineDir = lineEnd - lineStart;
		Vector3 startDistance = ray.origin - lineStart;

		float lineLengthSqr = lineDir.sqrMagnitude;

		float b = Vector3.Dot(rayDir, lineDir);
		float d = Vector3.Dot(rayDir, startDistance);
		float e = Vector3.Dot(lineDir, startDistance);

		float D = lineLengthSqr - b * b;

		if (D.Approximates(0, 0.0001f)) {
			return Mathf.Min((ray.origin - lineStart).magnitude, (ray.origin - lineEnd).magnitude);
		}

		return (b * e - lineLengthSqr * d) / D;
	}

	public static float DistanceToPlane(this Ray ray, Vector3 position, Vector3 normal) {
		float d = Vector3.Dot(position, -normal);
		float t = -(d + Vector3.Dot(ray.origin, normal)) / Vector3.Dot(ray.direction, normal);
		return t;
	}

	/// <summary>
	/// Reposition a Parent Transform so that a child Transforms position aligns with a target position and rotation; can be limited to specific axes
	/// </summary>
	public static void AlignChildToTarget(this Transform parent, Transform child, Vector3 targetPosition, Quaternion targetRotation, Axis positionAxes = Axis.XYZ, Axis rotationAxes = Axis.XYZ) {

		Vector3 childOffset = parent.InverseTransformVector(child.position - parent.position);

		Vector3 parentRot = parent.rotation.eulerAngles;
		Vector3 childRot = child.rotation.eulerAngles;
		Vector3 rotOffset = childRot - parentRot;

		Vector3 targetEuler = targetRotation.eulerAngles - rotOffset;

		if ((rotationAxes & Axis.X) == 0) targetEuler.x = parentRot.x;
		if ((rotationAxes & Axis.Y) == 0) targetEuler.y = parentRot.y;
		if ((rotationAxes & Axis.Z) == 0) targetEuler.z = parentRot.z;

		Quaternion targetRot = Quaternion.Euler(targetEuler);

		Vector3 targetPos = targetPosition - targetRot * childOffset;

		if ((positionAxes & Axis.X) == 0) targetPos.x = parent.position.x;
		if ((positionAxes & Axis.Y) == 0) targetPos.y = parent.position.y;
		if ((positionAxes & Axis.Z) == 0) targetPos.z = parent.position.z;

		parent.SetPositionAndRotation(targetPos, targetRot);
	}

	public static Rect Flip(this Rect rect, bool flipHorizontal, bool flipVertical) {
		if (flipHorizontal) {
			rect.x = 1 - rect.x;
			rect.width = -rect.width;
		}

		if (flipVertical) {
			rect.y = 1 - rect.y;
			rect.height = -rect.height;
		}

		return rect;
	}

	public static void SetHorizontalFOV(this Camera cam, float horizontalFOV, float aspectRatio = -1) {
		if (aspectRatio < 0) {
			aspectRatio = cam.aspect;
		}
		cam.fieldOfView = 2 * Mathf.Rad2Deg * Mathf.Atan(1 / aspectRatio * Mathf.Tan(horizontalFOV * Mathf.Deg2Rad * 0.5f));
	}

	public static float GetHorizontalFOV(this Camera cam, float aspectRatio = -1) {
		if (aspectRatio < 0) {
			aspectRatio = cam.aspect;
		}
		return 2 * Mathf.Rad2Deg * Mathf.Atan(aspectRatio * Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad * 0.5f));
	}

	/// <summary>
	/// Execute an action on an object and return the object
	/// </summary>
	public static T Execute<T>(this T obj, Action<T> action) {
		action?.Invoke(obj);
		return obj;
	}

	/// <summary>
	/// Execute an function on an object and return its return value
	/// </summary>
	public static U Execute<T, U>(this T obj, Func<T, U> func) => func.Invoke(obj);
}
