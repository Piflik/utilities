﻿// ReSharper disable UnusedMember.Global
using System;
using UnityEngine;

public static class NumberExtensions {

	[Flags]
	public enum BorderInclusion {
		None = 0,
		Lower = 1,
		Upper = 2,
		Both = 3,
	}

	public static T AtLeast<T>(this T value, T minimum) where T : IComparable<T> {
		return value.CompareTo(minimum) < 0 ? minimum : value;
	}
	
	public static T AtMost<T>(this T value, T maximum) where T : IComparable<T> {
		return value.CompareTo(maximum) > 0 ? maximum : value;
	}
	
	public static T Between<T>(this T value, T minimum, T maximum) where T : IComparable<T> {
		if (value.CompareTo(minimum) < 0) return minimum; 
		if (value.CompareTo(maximum) > 0) return maximum;

		return value;
	}

	public static bool IsAtLeast<T>(this T value, T minimum) where T : IComparable<T> {
		return value.CompareTo(minimum) >= 0;
	}
	
	public static bool IsGreaterThan<T>(this T value, T minimum) where T : IComparable<T> {
		return value.CompareTo(minimum) > 0;
	}

	public static bool IsAtMost<T>(this T value, T maximum) where T : IComparable<T> {
		return value.CompareTo(maximum) <= 0;
	}
	
	public static bool IsLessThan<T>(this T value, T maximum) where T : IComparable<T> {
		return value.CompareTo(maximum) < 0;
	}

	public static bool IsBetween<T>(this T value, T minimum, T maximum, BorderInclusion borderInclusion = BorderInclusion.Both) where T : IComparable<T> {

		if (value.CompareTo(minimum) < 0 || (borderInclusion & BorderInclusion.Lower) == 0 && value.CompareTo(minimum) <= 0) return false;
		if (value.CompareTo(maximum) > 0 || (borderInclusion & BorderInclusion.Upper) == 0 && value.CompareTo(maximum) >= 0) return false;

		return true;
	}

	public static float Round(this float f) => Mathf.Round(f);
	public static int RoundToInt(this float f) => Mathf.RoundToInt(f);
	public static float Ceil(this float f) => Mathf.Ceil(f);
	public static int CeilToInt(this float f) => Mathf.CeilToInt(f);
	public static float Floor(this float f) => Mathf.Floor(f);
	public static int FloorToInt(this float f) => Mathf.FloorToInt(f);
	public static float Abs(this float f) => Mathf.Abs(f);
	public static int Abs(this int i) => Mathf.Abs(i);
	public static float Sqrt(this float f) => Mathf.Sqrt(f);
	public static float Sin(this float f) => Mathf.Sin(f);
	public static float Cos(this float f) => Mathf.Cos(f);
	public static float Tan(this float f) => Mathf.Tan(f);
	public static float Asin(this float f) => Mathf.Asin(f);
	public static float Acos(this float f) => Mathf.Acos(f);
	public static float Atan(this float f) => Mathf.Atan(f);
	public static float ClosestPowerOfTwo(this int i) => Mathf.ClosestPowerOfTwo(i);
	public static float NextPowerOfTwo(this int i) => Mathf.NextPowerOfTwo(i);
	public static float Exp(this float f) => Mathf.Exp(f);
	public static float Log(this float f) => Mathf.Log(f);
	public static float Log10(this float f) => Mathf.Log10(f);
	public static float Pow(this float f, float p) => Mathf.Pow(f, p);
	public static float Sign(this float f) => Mathf.Sign(f);
}
