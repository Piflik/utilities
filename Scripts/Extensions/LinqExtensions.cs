﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
// ReSharper disable UnusedMember.Global

public static class LinqExtensions {

	/// <summary>
	/// Overload for Distinct that uses a Func instead of a Comparer
	/// </summary>
	public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property) {
		Utilities.GeneralPropertyComparer<T, TKey> comparer = new Utilities.GeneralPropertyComparer<T, TKey>(property);
		return items.Distinct(comparer);
	}

	/// <summary>
	/// Overload for Union that uses a Func instead of a Comparer
	/// </summary>
	public static IEnumerable<T> UnionBy<T, TKey>(this IEnumerable<T> first, IEnumerable<T> second, Func<T, TKey> property) {
		Utilities.GeneralPropertyComparer<T, TKey> comparer = new Utilities.GeneralPropertyComparer<T, TKey>(property);
		return first.Union(second, comparer);
	}

	/// <summary>
	/// Partition a collection into two collections according to a predicate
	/// </summary>
	/// <param name="source">Source collection</param>
	/// <param name="predicate">Predicate to distinguish between the two sets</param>
	/// <param name="selector">Function to apply to the two sets</param>
	public static TResult Partition<T, TResult>(this IEnumerable<T> source, Func<T, bool> predicate, Func<IEnumerable<T>, IEnumerable<T>, TResult> selector) {
		if (source == null) throw new ArgumentNullException(nameof(source));
		if (predicate == null) throw new ArgumentNullException(nameof(predicate));
		if (selector == null) throw new ArgumentNullException(nameof(selector));

		IEnumerable<T> fs = null;
		IEnumerable<T> ts = null;

		foreach (IGrouping<bool, T> g in source.GroupBy(predicate)) {
			if (g.Key) {
				Debug.Assert(ts == null);
				ts = g;
			} else {
				Debug.Assert(fs == null);
				fs = g;
			}
		}

		return selector(fs ?? Enumerable.Empty<T>(), ts ?? Enumerable.Empty<T>());
	}

	/// <summary>
	/// Inverse of Where
	/// </summary>
	public static IEnumerable<T> WhereNot<T>(this IEnumerable<T> source, Func<T, bool> predicate) {
		if (source == null) throw new ArgumentNullException(nameof(source));
		if (predicate == null) throw new ArgumentNullException(nameof(predicate));
		return source.Where(e => !predicate(e));
	}

	/// <summary>
	/// Append an item to a collection
	/// </summary>
	public static IEnumerable<T> Append<T>(this IEnumerable<T> source, T item) {
		if (null == source) throw new ArgumentNullException(nameof(source));
		foreach (T t in source) yield return t;
		yield return item;
	}

	/// <summary>
	/// Prepend an item to a collection
	/// </summary>
	public static IEnumerable<T> Prepend<T>(this IEnumerable<T> source, T item) {
		if (null == source) throw new ArgumentNullException(nameof(source));
		yield return item;
		foreach (T t in source) yield return t;
	}


	/// <summary>
	/// Exclude all occurances of an item from collection
	/// </summary>
	public static IEnumerable<T> Except<T>(this IEnumerable<T> source, T item) {
		if (null == source) throw new ArgumentNullException(nameof(source));
		foreach (T t in source) {
			if(t.Equals(item)) continue;
			yield return t;
		}
	}

	/// <summary>
	/// Test if collection contains no items
	/// </summary>
	public static bool IsEmpty<T>(this IEnumerable<T> collection) {
		if (null == collection) throw new ArgumentNullException(nameof(collection));
		return !collection.Any();
	}

	private static System.Random _random;

	/// <summary>
	/// Get a random element from collection (uniformly distributed)
	/// </summary>
	public static T RandomOrDefault<T>(this IEnumerable<T> source) {
		if (null == source) throw new ArgumentNullException(nameof(source));
		if (_random == null) {
			_random = new System.Random();
		}

		T current = default;
		int count = 0;
		foreach (T element in source) {
			count++;
			if (_random.Next(count) == 0) {
				current = element;
			}
		}
		
		return current;
	}

	/// <summary>
	/// Convert collection to a Queue
	/// </summary>
	public static Queue<T> ToQueue<T>(this IEnumerable<T> source) {
		if (null == source) throw new ArgumentNullException(nameof(source));
		Queue<T> queue = new Queue<T>();

		foreach (T obj in source) {
			queue.Enqueue(obj);
		}

		return queue;
	}

	/// <summary>
	/// Returns the first element of a sequence that satisfies a condition or a custom default value if no such element is found.
	/// </summary>
	/// <param name="predicate">A function to test each element for a condition.</param>
	/// <param name="defaultResult">The default result.</param>
	public static T FirstOrDefault<T>(this IEnumerable<T> source, Func<T, bool> predicate, T defaultResult) {
		if (source == null) throw new ArgumentNullException(nameof(source));
		if (predicate == null) throw new ArgumentNullException(nameof (predicate));
		foreach (T element in source) {
			if (predicate(element))
				return element;
		}
		return defaultResult;
	}

	/// <summary>
	/// Returns the last element of a sequence that satisfies a condition or a custom default value if no such element is found.
	/// </summary>
	/// <param name="predicate">A function to test each element for a condition.</param>
	/// <param name="defaultResult">The default result.</param>
	public static T LastOrDefault<T>(this IEnumerable<T> source, Func<T, bool> predicate, T defaultResult) {
		if (source == null) throw new ArgumentNullException(nameof(source));
		if (predicate == null) throw new ArgumentNullException(nameof (predicate));
		T result = defaultResult;
		foreach (T element in source) {
			if (predicate(element))
				result = element;
		}
		return result;
	}

	/// <summary>
	/// Select for recursive collections (DFS)
	/// </summary>
	/// <param name="childSelector"></param>
	/// <returns></returns>
	public static IEnumerable<TSource> RecursiveSelect<TSource>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TSource>> childSelector) {
		Stack<IEnumerator<TSource>> stack = new Stack<IEnumerator<TSource>>();
		IEnumerator<TSource> enumerator = source.GetEnumerator();

		try {
			while (true) {
				if (enumerator.MoveNext()) {
					TSource element = enumerator.Current;
					yield return element;

					stack.Push(enumerator);
					enumerator = childSelector(element).GetEnumerator();
				} else if (stack.Count > 0) {
					enumerator.Dispose();
					enumerator = stack.Pop();
				} else {
					yield break;
				}
			}
		} finally {
			enumerator.Dispose();

			while (stack.Count > 0) // Clean up in case of an exception.
			{
				enumerator = stack.Pop();
				enumerator.Dispose();
			}
		}
	}

	public static IEnumerable<TSource> Map<TSource>(this IEnumerable<TSource> source, Action<TSource> action) {
		if (source == null) throw new ArgumentNullException(nameof(source));
		if (action == null) throw new ArgumentNullException(nameof(action));

		foreach (TSource obj in source) {
			action.Invoke(obj);
			yield return obj;
		}
	}

	/// <summary>
	/// Determines whether any element of a sequence satisfies a condition and stores the first match in <paramref name="foundElement"/>.
	/// </summary>
	/// <typeparam name="TSource"></typeparam>
	/// <param name="predicate">A function to test each element for a condition.</param>
	/// <param name="foundElement">Stores the first match or the default value of <typeparam name="TSource"></typeparam></param>
	/// <returns>True if any matching element was found, otherwise false.</returns>
	public static bool Any<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate, out TSource foundElement) {
		if (source == null)
			throw new ArgumentNullException(nameof(source));
		if (predicate == null)
			throw new ArgumentNullException(nameof(predicate));
		foreach (TSource source1 in source) {
			if (predicate(source1)) {
				foundElement = source1;
				return true;
			}
		}
		foundElement = default;
		return false;
	}
}
