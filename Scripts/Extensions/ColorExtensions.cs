﻿using UnityEngine;
using Utilities;

public static partial class ColorExtensions {

	public static string ToHexString(this Color color, string prefix = "#") => ColorUtilities.ColorToHex(color, prefix);
	public static string ToHexStringAlpha(this Color color, string prefix = "#") => ColorUtilities.ColorToHexAlpha(color, prefix);
	public static string ToHSVString(this Color color) => ColorUtilities.ColorToHSVString(color);
	public static string ToRGBString(this Color color) => ColorUtilities.ColorToRGBString255(color);

	public static string ToHexString(this Color32 color, string prefix = "#") => ColorUtilities.ColorToHex(color, prefix);
	public static string ToHexStringAlpha(this Color32 color, string prefix = "#") => ColorUtilities.ColorToHexAlpha(color, prefix);
	public static string ToHSVString(this Color32 color) => ColorUtilities.ColorToHSVString(color);
	public static string ToRGBString(this Color32 color) => ColorUtilities.ColorToRGBString255(color);

	/// <summary>
	/// Copy this color and optionally override components
	/// </summary>
	public static Color Copy(this Color c, float r = float.NaN, float g = float.NaN, float b = float.NaN, float a = float.NaN) {
		if (float.IsNaN(r))
			r = c.r;
		if (float.IsNaN(g))
			g = c.g;
		if (float.IsNaN(b))
			b = c.b;
		if (float.IsNaN(a))
			a = c.a;
		return new Color(r, g, b, a);
	}

	/// <summary>
	/// Inverse color
	/// </summary>
	public static Color Inverse(this Color color) {
		return new Color(1 - color.r, 1 - color.g, 1 - color.b, color.a);
	}

	/// <summary>
	/// Inverse Color
	/// </summary>
	public static Color32 Inverse(this Color32 color) {
		return new Color32((byte)(255 - color.r), (byte)(255 - color.g), (byte)(255 - color.b), color.a);
	}

	public static void Deconstruct(this Color col, out float r, out float g, out float b, out float a) {
		r = col.r;
		g = col.g;
		b = col.b;
		a = col.a;
	}

	public static void Deconstruct(this Color32 col, out int r, out int g, out int b, out int a) {
		r = col.r;
		g = col.g;
		b = col.b;
		a = col.a;
	}

	public static string ConstructorCode(this Color col) => $"new Color({col.r:0.###}f, {col.g:0.###}f, {col.b:0.###}f, {col.a:0.###}f)";
	public static string ConstructorCode(this Color32 col) => $"new Color({col.r:0}f, {col.g:0}f, {col.b:0}f, {col.a:0}f)";

	public static Color R(this Color color, float r) {
		color.r = r;
		return color;
	}

	public static Color G(this Color color, float g) {
		color.g = g;
		return color;
	}

	public static Color B(this Color color, float b) {
		color.b = b;
		return color;
	}

	public static Color RGB(this Color color, float r, float g, float b) {
		color.r = r;
		color.g = g;
		color.b = b;
		return color;
	}

	public static Color RGB(this Color color, Color other) {
		color.r = other.r;
		color.g = other.g;
		color.b = other.b;
		return color;
	}

	public static Color Alpha(this Color color, float alpha) {
		color.a = alpha;
		return color;
	}

	public static Color H(this Color color, float h) {
		float a = color.a;
		Color.RGBToHSV(color, out _, out float s, out float v);
		color = Color.HSVToRGB(h, s, v);
		color.a = a;
		return color;
	}

	public static Color S(this Color color, float s) {
		float a = color.a;
		Color.RGBToHSV(color, out float h, out _, out float v);
		color = Color.HSVToRGB(h, s, v);
		color.a = a;
		return color;
	}

	public static Color V(this Color color, float v) {
		float a = color.a;
		Color.RGBToHSV(color, out float h, out float s, out _);
		color = Color.HSVToRGB(h, s, v);
		color.a = a;
		return color;
	}

	public static Color HSV(this Color color, float h, float s, float v) {
		float a = color.a;
		color = Color.HSVToRGB(h, s, v);
		color.a = a;
		return color;
	}

	public static Color DivideBy(this Color baseColor, Color blendColor) {
		return new Color(
			baseColor.r / blendColor.r,
			baseColor.g / blendColor.g,
			baseColor.b / blendColor.b,
			baseColor.a);
	}

	public static Color Darken(this Color baseColor, Color blendColor) {
		return ColorUtilities.Min(blendColor, baseColor).Alpha(baseColor.a);
	}

	public static Color Multiply(this Color baseColor, Color blendColor) {
		return (blendColor * baseColor).Alpha(baseColor.a);
	}

	public static Color ColorBurn(this Color baseColor, Color blendColor) {
		return blendColor == Color.black ? blendColor.Alpha(baseColor.a) : ColorUtilities.Saturate(Color.white - (Color.white - baseColor).DivideBy(blendColor)).Alpha(baseColor.a);
	}

	public static Color LinearBurn(this Color baseColor, Color blendColor) {
		return ColorUtilities.Saturate(baseColor + blendColor - Color.white).Alpha(baseColor.a);
	}

	public static Color DarkerColor(this Color baseColor, Color blendColor) {
		return (baseColor.r + baseColor.g + baseColor.b < blendColor.r + blendColor.g + blendColor.b) ? baseColor : blendColor.Alpha(baseColor.a);
	}

	public static Color Lighten(this Color baseColor, Color blendColor) {
		return ColorUtilities.Max(blendColor, baseColor).Alpha(baseColor.a);
	}

	public static Color Screen(this Color baseColor, Color blendColor) {
		return (Color.white - (Color.white - baseColor) * (Color.white - blendColor)).Alpha(baseColor.a);
	}

	public static Color ColorDodge(this Color baseColor, Color blendColor) {
		return blendColor == Color.white ? blendColor.Alpha(baseColor.a) : ColorUtilities.Saturate(baseColor.DivideBy(Color.white - blendColor)).Alpha(baseColor.a);
	}

	public static Color LinearDodge(this Color baseColor, Color blendColor) {
		return ColorUtilities.Saturate(baseColor + blendColor).Alpha(baseColor.a);
	}

	public static Color LighterColor(this Color baseColor, Color blendColor) {
		return (baseColor.r + baseColor.g + baseColor.b > blendColor.r + blendColor.g + blendColor.b) ? baseColor : blendColor.Alpha(baseColor.a);
	}

	public static Color Overlay(this Color baseColor, Color blendColor) {
		Color a = Multiply(baseColor, 2 * blendColor);
		Color b = Screen(baseColor, 2 * (blendColor - Color.grey));

		baseColor.r = baseColor.r < 0.5f ? a.r : b.r;
		baseColor.g = baseColor.g < 0.5f ? a.g : b.g;
		baseColor.b = baseColor.b < 0.5f ? a.b : b.b;

		return baseColor;
	}

	public static Color SoftLight(this Color baseColor, Color blendColor) {

		Color a = 2 * baseColor * blendColor + baseColor * baseColor * (Color.white - 2 * blendColor);
		Color b = ColorUtilities.Sqrt(baseColor) * (2 * blendColor - Color.white) + 2 * baseColor * (Color.white - blendColor);

		baseColor.r = baseColor.r < 0.5f ? a.r : b.r;
		baseColor.g = baseColor.g < 0.5f ? a.g : b.g;
		baseColor.b = baseColor.b < 0.5f ? a.b : b.b;

		return baseColor;
	}

	public static Color HardLight(this Color baseColor, Color blendColor) {
		return Overlay(blendColor, baseColor).Alpha(baseColor.a);
	}

	public static Color VividLight(this Color baseColor, Color blendColor) {

		Color a = ColorBurn(baseColor, 2 * blendColor);
		Color b = ColorDodge(baseColor, 2 * (blendColor - Color.grey));

		baseColor.r = baseColor.r < 0.5f ? a.r : b.r;
		baseColor.g = baseColor.g < 0.5f ? a.g : b.g;
		baseColor.b = baseColor.b < 0.5f ? a.b : b.b;

		return baseColor;
	}

	public static Color LinearLight(this Color baseColor, Color blendColor) {

		Color a = LinearBurn(baseColor, 2 * blendColor);
		Color b = LinearDodge(baseColor, 2 * (blendColor - Color.grey));

		baseColor.r = baseColor.r < 0.5f ? a.r : b.r;
		baseColor.g = baseColor.g < 0.5f ? a.g : b.g;
		baseColor.b = baseColor.b < 0.5f ? a.b : b.b;

		return baseColor;
	}

	public static Color PinLight(this Color baseColor, Color blendColor) {

		Color a = Darken(baseColor, 2 * blendColor);
		Color b = Lighten(baseColor, 2 * (blendColor - Color.grey));

		baseColor.r = baseColor.r < 0.5f ? a.r : b.r;
		baseColor.g = baseColor.g < 0.5f ? a.g : b.g;
		baseColor.b = baseColor.b < 0.5f ? a.b : b.b;

		return baseColor;
	}

	public static Color HardMix(this Color baseColor, Color blendColor) {
		Color c = VividLight(baseColor, blendColor);
		c.r = c.r < 0.5f ? 0 : 1;
		c.r = c.g < 0.5f ? 0 : 1;
		c.r = c.b < 0.5f ? 0 : 1;
		c.a = baseColor.a;
		return c;
	}

	public static Color Reflect(this Color baseColor, Color blendColor) {
		return blendColor == Color.white ? blendColor.Alpha(baseColor.a) : ColorUtilities.Min((baseColor * baseColor).DivideBy(Color.white - blendColor), Color.white).Alpha(baseColor.a);
	}

	public static Color Difference(this Color baseColor, Color blendColor) {
		return ColorUtilities.Abs(baseColor - blendColor).Alpha(baseColor.a);
	}

	public static Color Exclusion(this Color baseColor, Color blendColor) {
		return (baseColor + blendColor - 2 * baseColor * blendColor).Alpha(baseColor.a);
	}
}
