﻿using System;
using System.Collections;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public class RateTweener : IDisposable {
		private float _start;
		private float _end;
		private float _changeRate;

		public float delay;
		public Action<float> onUpdate;
		public Action onFinish;

		public float value { get; private set; }
		public bool isTweening { get; private set; }

		public float start {
			get => _start;
			set {
				_start = value;
				CalculateChangeRate();
			}
		}

		public float end {
			get => _end;
			set {
				_end = value;
				CalculateChangeRate();
			}
		}

		public float changeRate {
			get => _changeRate;
			set {
				_changeRate = value;
				CalculateChangeRate();
			}
		}

		private Coroutine _tweening;

		private RateTweener() { }

		public static RateTweener Create(float start, float end, float changeRate, float delay = 0, bool autoStart = true, Action<float> onUpdate = null, Action onFinish = null) {
			RateTweener t = new RateTweener {
				_start = start,
				value = start,
				_end = end,
				_changeRate = changeRate,
				delay = delay,
				onUpdate = onUpdate,
				onFinish = onFinish
			};

			t.CalculateChangeRate();

			if (autoStart) t._tweening = CoroutineRunner.Run(t.Tweening());

			return t;
		}

		private IEnumerator Tweening() {
			isTweening = true;
			yield return Waiter.Call(delay);

			while (!value.Approximates(end, 0.01f)) {

				float diff = Mathf.Abs(end - value);
				float delta = _changeRate * Time.deltaTime;
				if (diff < Mathf.Abs(delta)) {
					break;
				}

				value += delta;

				onUpdate?.Invoke(value);
				yield return null;
			}

			value = end;
			onUpdate?.Invoke(value);

			onFinish?.Invoke();
			isTweening = false;
		}

		public void Start(bool reset = true) {
			CoroutineRunner.Stop(_tweening);
			if (reset) {
				value = start;
			}

			_tweening = CoroutineRunner.Run(Tweening());
		}

		public void Pause() {
			CoroutineRunner.Stop(_tweening);
		}

		public void Dispose() {
			CoroutineRunner.Stop(_tweening);
		}

		private void CalculateChangeRate() {
			_changeRate = Mathf.Abs(_changeRate) * Mathf.Sign(end - start);
		}
	}
}
