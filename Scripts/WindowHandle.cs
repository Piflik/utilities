﻿using System;
#if UNITY_STANDALONE_WIN
using System.Runtime.InteropServices;
using System.Text;
#endif

namespace Utilities {
	public static class WindowHandle {
		public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

#if UNITY_STANDALONE_WIN
		private const string UNITY_WINDOW_CLASS_NAME = "UnityWndClass";

		public static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
		public static readonly IntPtr HWND_DEFAULT = new IntPtr(0);

		private static IntPtr _windowHandle = IntPtr.Zero;

		[DllImport("kernel32.dll")]
		private static extern uint GetCurrentThreadId();

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern int GetClassName(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool EnumThreadWindows(uint dwThreadId, EnumWindowsProc lpEnumFunc, IntPtr lParam);


		[DllImport("user32.dll", EntryPoint = "SetWindowPos")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool SetWindowPos(IntPtr hwnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

		[DllImport("user32.dll", EntryPoint = "FindWindow")]
		public static extern IntPtr FindWindow(string className, string windowName);

		public static bool SetWindowPos(IntPtr hwnd, int x, int y, int cx, int cy, bool alwaysOnTop = false) 
			=> SetWindowPos(hwnd, alwaysOnTop ? HWND_TOPMOST : HWND_DEFAULT, x, y, cx, cy, 0);

#endif

		public static int Get() {
#if UNITY_STANDALONE_WIN
			if (_windowHandle == IntPtr.Zero) {
				uint threadId = GetCurrentThreadId();
				EnumThreadWindows(threadId, (hWnd, lParam) => {
					StringBuilder classText = new StringBuilder(UNITY_WINDOW_CLASS_NAME.Length + 1);
					GetClassName(hWnd, classText, classText.Capacity);
					if (classText.ToString() == UNITY_WINDOW_CLASS_NAME) {
						_windowHandle = hWnd;
						return false;
					}

					return true;
				}, IntPtr.Zero);
			}

			return _windowHandle.ToInt32();
#else
			return 0;
#endif
		}
	}
}
