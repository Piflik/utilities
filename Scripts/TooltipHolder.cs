﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace Utilities {
	public class TooltipHolder : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

		[SerializeField]
		string _tooltipText;

		Tooltip _tooltip;

		float _tooltipDelay = 0.5f;

		IEnumerator Show(float waitTime) {
			if (_tooltip == null) {
				_tooltip = transform.GetCanvas().transform.Find("ToolTip").GetComponent<Tooltip>();
			}

			if (_tooltip == null) yield break;

			yield return new WaitForSeconds(waitTime);

			_tooltip.Show(_tooltipText, Input.mousePosition);
		}

		void Hide() {
			if (!_tooltip) return;
			_tooltip.Hide();
		}

		Coroutine _running;
		public void OnPointerEnter(PointerEventData eventData) {
			if (_running != null) StopCoroutine(_running);
			_running = StartCoroutine(Show(_tooltipDelay));
		}

		public void OnPointerExit(PointerEventData eventData) {
			if (_running != null) StopCoroutine(_running);
			Hide();
		}
	}
}