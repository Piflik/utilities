﻿using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static class Integration {
		public static void ExplicitEuler(ref float position, ref float speed, float acceleration, float timestep) {
			position += speed * timestep;
			speed += acceleration * timestep;
		}

		public static void ExplicitEuler(ref Vector2 position, ref Vector2 speed, Vector2 acceleration, float timestep) {
			position += speed * timestep;
			speed += acceleration * timestep;
		}

		public static void ExplicitEuler(ref Vector3 position, ref Vector3 speed, Vector3 acceleration, float timestep) {
			position += speed * timestep;
			speed += acceleration * timestep;
		}

		public static void SemiImplicitEuler(ref float position, ref float speed, float acceleration, float timestep) {
			speed += acceleration * timestep;
			position += speed * timestep;
		}

		public static void SemiImplicitEuler(ref Vector2 position, ref Vector2 speed, Vector2 acceleration, float timestep) {
			speed += acceleration * timestep;
			position += speed * timestep;
		}

		public static void SemiImplicitEuler(ref Vector3 position, ref Vector3 speed, Vector3 acceleration, float timestep) {
			speed += acceleration * timestep;
			position += speed * timestep;
		}

		public static void Midpoint(ref float position, ref float speed, float acceleration, float timestep) {
			float halfSpeed = speed + acceleration * timestep * 0.5f;
			position += halfSpeed * timestep;
			speed += acceleration * timestep;
		}

		public static void Midpoint(ref Vector2 position, ref Vector2 speed, Vector2 acceleration, float timestep) {
			Vector2 halfSpeed = speed + timestep * 0.5f * acceleration;
			position += halfSpeed * timestep;
			speed += acceleration * timestep;
		}

		public static void Midpoint(ref Vector3 position, ref Vector3 speed, Vector3 acceleration, float timestep) {
			Vector3 halfSpeed = speed + timestep * 0.5f * acceleration;
			position += halfSpeed * timestep;
			speed += acceleration * timestep;
		}

		public static void Leapfrog(ref float position, ref float speed, float acceleration, float timestep) {
			position += speed * timestep + 0.5f * acceleration * timestep * timestep;
			speed += acceleration * timestep;
		}

		public static void Leapfrog(ref Vector2 position, ref Vector2 speed, Vector2 acceleration, float timestep) {
			position += speed * timestep + 0.5f * timestep * timestep * acceleration;
			speed += acceleration * timestep;
		}

		public static void Leapfrog(ref Vector3 position, ref Vector3 speed, Vector3 acceleration, float timestep) {
			position += speed * timestep + 0.5f * timestep * timestep * acceleration;
			speed += acceleration * timestep;
		}
	}
}
