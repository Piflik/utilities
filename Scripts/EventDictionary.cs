﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Utilities {
	public enum DictitonaryChangeType {
		Add,
		Set,
		Remove,
		Cleared,
		Changed
	}

	public class EventDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IEquatable<EventDictionary<TKey, TValue>> where TValue : IComparable {
		private readonly Dictionary<TKey, TValue> _dictionary;

		public delegate void DictionaryChangedEventDelegate(EventDictionary<TKey, TValue> sender, DictitonaryChangeType type, KeyValuePair<TKey, TValue> newValue = default, TKey key = default);

		public event DictionaryChangedEventDelegate dictionaryChanged;

		public EventDictionary() {
			_dictionary = new Dictionary<TKey, TValue>();
		}

		public EventDictionary(Dictionary<TKey, TValue> initialVal) {
			_dictionary = initialVal;
		}

		public void Add(KeyValuePair<TKey, TValue> item) {
			_dictionary.Add(item.Key, item.Value);
			dictionaryChanged?.Invoke(this, DictitonaryChangeType.Add, item, item.Key);
		}

		public Dictionary<TKey, TValue> items {
			get => _dictionary;
			set {
				_dictionary.Clear();

				foreach (KeyValuePair<TKey, TValue> kv in value) {
					_dictionary[kv.Key] = kv.Value;
				}

				dictionaryChanged?.Invoke(this, DictitonaryChangeType.Set);
			}
		}

		public void Clear() {
			_dictionary.Clear();
			dictionaryChanged?.Invoke(this, DictitonaryChangeType.Cleared);
		}

		public bool Contains(KeyValuePair<TKey, TValue> item) => _dictionary.Contains(item);
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => _dictionary.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => ((IDictionary<TKey, TValue>)_dictionary).CopyTo(array, arrayIndex);
		public bool ContainsKey(TKey key) => _dictionary.ContainsKey(key);
		public bool TryGetValue(TKey key, out TValue value) => _dictionary.TryGetValue(key, out value);

		public bool Remove(KeyValuePair<TKey, TValue> item) {
			bool b = _dictionary.Remove(item.Key);
			dictionaryChanged?.Invoke(this, DictitonaryChangeType.Remove, item);
			return b;
		}

		public int Count => _dictionary.Count;
		public bool IsReadOnly => ((IDictionary<TKey, TValue>)_dictionary).IsReadOnly;

		public void Add(TKey key, TValue value) {
			_dictionary.Add(key, value);
			dictionaryChanged?.Invoke(this, DictitonaryChangeType.Add, new KeyValuePair<TKey, TValue>(key, value), key);
		}

		public bool Remove(TKey key) {
			KeyValuePair<TKey, TValue> item = new KeyValuePair<TKey, TValue>(key, _dictionary[key]);
			bool b = _dictionary.Remove(key);
			dictionaryChanged?.Invoke(this, DictitonaryChangeType.Remove, item, key);
			return b;
		}

		public TValue this[TKey key] {
			get => _dictionary[key];
			set {
				TValue val = _dictionary[key];
				_dictionary[key] = value;

				if (val.CompareTo(value) != 0) {
					dictionaryChanged?.Invoke(this, DictitonaryChangeType.Changed, new KeyValuePair<TKey, TValue>(key, value), key);
				}
			}
		}

		public ICollection<TKey> Keys => _dictionary.Keys;
		public ICollection<TValue> Values => _dictionary.Values;

		public bool Equals(EventDictionary<TKey, TValue> other) {
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(_dictionary, other._dictionary);
		}

		public override bool Equals(object obj) {
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((EventDictionary<TKey, TValue>)obj);
		}

		public override int GetHashCode() {
			return (_dictionary != null ? _dictionary.GetHashCode() : 0);
		}

		public static bool operator ==(EventDictionary<TKey, TValue> a, EventDictionary<TKey, TValue> b) => a != null && a.Equals(b) || b == null;
		public static bool operator !=(EventDictionary<TKey, TValue> a, EventDictionary<TKey, TValue> b) => a != null && !a.Equals(b) || b != null;
	}
}
