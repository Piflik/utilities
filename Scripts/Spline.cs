﻿using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
// ReSharper disable UnusedMember.Global


namespace Utilities {
	//TODO curve with only one segment and start/end control points
	public class Spline {
		private const int GIZMO_LAYER = 8;

		private Vector3[] _vertices { get; set; }
		private Vector3? _startOut;
		private Vector3? _endOut;

		public bool closed { get; private set; }
		public bool smooth { get; private set; }

		private float _thickness;

		private bool _isInitialized;

		private GameObject _visualizationObject;
		private LineRenderer _visualizationRenderer;
		private Material _visualizationMaterial;

		public struct Segment {
			public Vector3 start;
			public Vector3 end;

			public Vector3 a, b;

			public float length;
			public float startLength;
		};

		private Segment[] _segments;

		public float length { get; private set; }

		public bool visible {
			get => _visualizationRenderer != null && _visualizationRenderer.enabled;
			set {
				if (_visualizationRenderer != null) {
					_visualizationRenderer.enabled = value;
				}

				if (value) {
					SetupVisualization();
				}
			}
		}

		private Spline() { }

		public static Spline Create(Vector3[] points, Material material, Vector3? startOut = null, Vector3? endOut = null, float thickness = 0.1f, bool smooth = true, bool closed = true) {

			if (points.Length < 2) return null;

			Spline spline = new Spline {
				_vertices = points,
				_startOut = startOut,
				_endOut = endOut,
				smooth = smooth,
				closed = closed,
				_thickness = thickness,
				_visualizationMaterial = material,
				_visualizationObject = null
			};



			spline.Setup();

			return spline;
		}

		public void Update(Vector3[] points, Vector3? startOut = null, Vector3? endOut = null) {
			_vertices = points;
			_startOut = startOut;
			_endOut = endOut;

			Setup();
		}

		private void Setup() {
			if (_vertices.Length < 2) return;

			_segments = new Segment[closed ? _vertices.Length : _vertices.Length - 1];
			length = 0;

			if (!smooth || _vertices.Length == 2) {
				for (int i = 0; i < _vertices.Length - 1; ++i) {

					float l = (_vertices[i + 1] - _vertices[i]).magnitude;

					_segments[i] = new Segment {
						start = _vertices[i],
						end = _vertices[i + 1],
						length = l,
						startLength = length
					};

					length += l;
				}

				if (closed) {
					float l = (_vertices[0] - _vertices[_vertices.Length - 1]).magnitude;

					_segments[_segments.Length - 1] = new Segment {
						start = _vertices[_vertices.Length - 1],
						end = _vertices[0],
						length = l,
						startLength = length
					};

					length += l;
				}

			} else if (closed) {
				for (int i = 0; i < _vertices.Length; ++i) {
					Vector3 start = _vertices[i];
					Vector3 end = _vertices[(i + 1) % _vertices.Length];
					Vector3 controlPointA = _vertices[(i - 1 + _vertices.Length) % _vertices.Length];
					Vector3 controlPointB = _vertices[(i + 2) % _vertices.Length];

					float l = CatmullRom.Length(start, end, controlPointA, controlPointB, 10);

					_segments[i] = new Segment {
						start = start,
						end = end,
						length = l,
						startLength = length,
						a = controlPointA,
						b = controlPointB
					};

					length += l;
				}

			} else {
				Vector3 start = _vertices[0];
				Vector3 end = _vertices[1];
				Vector3 controlPointB = _vertices[2];
				Vector3 controlPointA = _startOut ?? 2 * start - end;

				float l = CatmullRom.Length(start, end, controlPointA, controlPointB, 10);

				_segments[0] = new Segment {
					start = start,
					end = end,
					length = l,
					startLength = 0,
					a = controlPointA,
					b = controlPointB
				};

				length += l;

				for (int i = 1; i < _vertices.Length - 2; ++i) {
					start = _vertices[i];
					end = _vertices[i + 1];
					controlPointA = _vertices[i - 1];
					controlPointB = _vertices[i + 2];

					l = CatmullRom.Length(start, end, controlPointA, controlPointB, 10);

					_segments[i] = new Segment {
						start = start,
						end = end,
						length = l,
						startLength = length,
						a = controlPointA,
						b = controlPointB
					};

					length += l;
				}

				start = _vertices[_vertices.Length - 2];
				end = _vertices[_vertices.Length - 1];
				controlPointA = _vertices[_vertices.Length - 3];
				controlPointB = _endOut ?? 2 * end - start;

				l = CatmullRom.Length(start, end, controlPointA, controlPointB, 10);

				_segments[_vertices.Length - 2] = new Segment {
					start = start,
					end = end,
					length = l,
					startLength = length,
					a = controlPointA,
					b = controlPointB
				};
				length += l;
			}

			_isInitialized = true;
		}

		private void SetupVisualization() {
			_visualizationObject = new GameObject("Spline Vertex") { layer = GIZMO_LAYER };

			_visualizationRenderer = _visualizationObject.AddComponent<LineRenderer>();
			_visualizationRenderer.material = _visualizationMaterial;
		}

		public void SetMaterial(Material material) {
			_visualizationMaterial = material;
			if (_visualizationRenderer != null) _visualizationRenderer.material = _visualizationMaterial;
		}

		public void SetColor(Color color) {
			_visualizationMaterial.color = color;
			if (_visualizationRenderer != null) _visualizationRenderer.material = _visualizationMaterial;

		}

		public void Draw(int pointsPerSegment = 10) {
			if (_visualizationObject == null) {
				SetupVisualization();
			}

			if (_vertices.Length < 2) {
				_visualizationRenderer.positionCount = 0;
				return;
			}

			List<Vector3> segmentPoints = GetCurvePoints(pointsPerSegment);

			_visualizationRenderer.positionCount = segmentPoints.Count;
			_visualizationRenderer.SetPositions(segmentPoints.ToArray());

			_visualizationRenderer.startWidth = _visualizationRenderer.endWidth = _thickness;
			visible = true;
		}

		public void DrawGizmo(int pointsPerSegment = 10) {
			if (_vertices.Length < 2) return;

			List<Vector3> segmentPoints = GetCurvePoints(pointsPerSegment);

			for (int i = 0; i < segmentPoints.Count - 1; ++i) {
				Gizmos.DrawLine(segmentPoints[i], segmentPoints[i + 1]);
			}

#if UNITY_EDITOR
			GUIStyle style = new GUIStyle();
			Color fadedColor = Color.Lerp(Gizmos.color, Color.grey, 0.75f);

			for (int i = 0; i < 101; ++i) {
				float l = 0.01f * i;

				Vector3 pos = GetPoint(l);

				if (Camera.current.WorldToScreenPoint(pos).z < 0) continue;

				style.normal.textColor = i % 10 == 0 ? Gizmos.color : fadedColor;

				Handles.Label(pos, l.ToString("0.##"), style);
			}
#endif
		}

		public void Hide() {
			visible = false;
		}

		public void Clear() {
			_vertices = null;
			Hide();
		}

		public List<Vector3> GetCurvePoints(int pointsPerSegment = 10) {
			if (_vertices.Length < 2) return null;

			if (!_isInitialized) Setup();

			List<Vector3> segmentPoints;

			if (!smooth || _vertices.Length == 2) {
				segmentPoints = _segments.Select(s => s.start).ToList();
			} else {
				segmentPoints = _segments.SelectMany(s => GetSegmentPoints(s, pointsPerSegment)).ToList();
			}

			segmentPoints.Add(_segments[_segments.Length - 1].end);

			return segmentPoints;
		}

		private static Vector3[] GetSegmentPoints(Segment segment, int pointsPerSegment = 10) {
			Vector3[] positions = new Vector3[pointsPerSegment];

			for (int i = 0; i < pointsPerSegment; ++i) {
				positions[i] = CatmullRom.Interpolate(segment.start, segment.end, segment.a, segment.b, (float)i / pointsPerSegment);
			}

			return positions;
		}

		public Vector3 GetPoint(float alongPath) {
			if (!_isInitialized) {
				Setup();
			}

			if (alongPath >= 1) {
				return closed ? _vertices[0] : _vertices[_vertices.Length - 1];
			}

			if (alongPath <= 0) {
				return _vertices[0];
			}

			if (_vertices.Length == 2) {
				return Vector3.Lerp(_vertices[0], _vertices[1], alongPath);
			}

			float alongLength = alongPath * length;

			int segIndex = 1;

			while (segIndex < _segments.Length && _segments[segIndex].startLength < alongLength) {
				segIndex++;
			}

			Segment seg = _segments[segIndex - 1];
			float localPercentage = (alongLength - seg.startLength) / seg.length;

			return !smooth ? Vector3.Lerp(seg.start, seg.end, localPercentage) : CatmullRom.Interpolate(seg.start, seg.end, seg.a, seg.b, localPercentage);
		}
	}
}
