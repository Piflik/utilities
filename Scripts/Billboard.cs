using UnityEngine;

namespace Utilities {
	[ExecuteInEditMode]
	public class Billboard : MonoBehaviour {

		enum Mode {
			LookAt,
			SameDirection
		}

		[SerializeField] private Mode _mode = Mode.LookAt;

		[SerializeField, EnumFlagSingle(typeof(Axis))]
		private Axis _lookDirection = Axis.Z;

		[SerializeField, EnumFlagSingle(typeof(Axis))]
		private Axis _upDirection = Axis.Y;

		[SerializeField] private bool _reverseLook;
		[SerializeField] private bool _reverseUp;


#pragma warning disable 414
		[SerializeField, ReadOnlyInInspector, TextArea(2, 20)]
		private string _note =
			@"This Component can only function on GameObjects that directly have a visible Renderer Component attached (UI doesn't count)! 
Children objects don't count either. 
To be visible, the Rendere needs to have a Mesh that is within the Camera's frustum. 
If it doesn't have a Mesh, it is considered visible, if the pivot is within the Camera's frustum.

CHILDREN OBJECTS DO NOT COUNT!

Possible Renderer components: 
MeshRenderer, SkinnedMeshRenderer, SpriteRenderer, LineRenderer, TrailRenderer, BillboardRenderer, ParticleSystemRenderer, SpriteMask, SpriteShapeRenderer, TilemapRenderer";
#pragma warning restore 414

		private void OnWillRenderObject() {
			Camera cam = Camera.current;

			switch (_mode) {
				case Mode.LookAt:
				default:
					transform.rotation = QuaternionUtilities.LookRotation((cam.transform.position - transform.position).normalized, Vector3.up, _lookDirection, _upDirection, !_reverseLook, _reverseUp);
					break;
				case Mode.SameDirection:
					transform.rotation = QuaternionUtilities.LookRotation(cam.transform.forward, Vector3.up, _lookDirection, _upDirection, _reverseLook, _reverseUp);
					break;
			}
		}
	}
}