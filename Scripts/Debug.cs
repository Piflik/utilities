﻿using System.Diagnostics;
using UnityEngine;

namespace Utilities {
	public static class Debug {
		public static string Format(string msg, DebugType type = DebugType.Default, string msgAdd = "") {
			string output = msg;
			switch (type) {
				case DebugType.Error:
					output = $"<color=#781E1E><b>Error</b> ---- </color>{msg}";
					break;
				case DebugType.Warning:
					output = $"<color=#858100><b>Warning</b> ---- </color>{msg}";
					break;
				case DebugType.Fail:
					output = $"<color=#858100><b>Failed</b> ---- </color>{msg}";
					break;
				case DebugType.Success:
					output = $"<color=#0D6B00><b>Success</b> ---- </color>{msg}";
					break;
				case DebugType.Header:
					output = $"<b>{msg}</b>";
					break;
				case DebugType.Info:
					output = $"<color=#1E5A78>{msg}</color>";
					break;
			}

			output += msgAdd;

			return output;
		}

		/// <summary>
		/// Automatic Debug format
		/// </summary>
		[Conditional("DEBUG"), Conditional("UNITY_EDITOR"), Conditional("WRITE_LOG")]
		public static void LogFormat(string msg, DebugType type = DebugType.Default, string msgAdd = "") {

			string output = Format(msg, type, msgAdd);

			switch (type) {
				case DebugType.Default:
				case DebugType.Configurator:
				case DebugType.Header:
				case DebugType.Fail:
				case DebugType.Success:
				case DebugType.Info:
					Log(output);
					break;
				case DebugType.Warning:
					LogWarning(output);
					break;
				case DebugType.Error:
					LogError(output);
					break;
			}
		}

		/// <summary>
		/// Colored Debug Log
		/// </summary>
		[Conditional("DEBUG"), Conditional("UNITY_EDITOR"), Conditional("WRITE_LOG")]
		public static void Log(object message, string color = null, bool showTimeStamp = false) {
			string messageStr = message != null ? message.ToString() : "null";

			if (string.IsNullOrWhiteSpace(color)) {
				if (showTimeStamp) {
					UnityEngine.Debug.LogFormat("[{1}] {0}", messageStr, Time.time);
				} else {
					UnityEngine.Debug.Log(messageStr);
				}
			} else {
				if (showTimeStamp) {
					UnityEngine.Debug.LogFormat("[{2}] <color=\"{0}\">{1}</color>", color, messageStr, Time.time);
				} else {
					UnityEngine.Debug.LogFormat("<color=\"{0}\">{1}</color>", color, messageStr);
				}
			}
		}

		/// <summary>
		/// Colored Debug Log
		/// </summary>
		[Conditional("DEBUG"), Conditional("UNITY_EDITOR"), Conditional("WRITE_LOG")]
		public static void Log(object message, UnityEngine.Color color, bool showTimeStamp = false) {
			Log(message, ColorUtilities.ColorToHex(color), showTimeStamp);
		}

		/// <summary>
		/// Colored Debug Log Warning
		/// </summary>
		[Conditional("DEBUG"), Conditional("UNITY_EDITOR"), Conditional("WRITE_LOG")]
		public static void LogWarning(object message, string color = null, bool showTimeStamp = false) {
			string messageStr = message != null ? message.ToString() : "null";

			if (string.IsNullOrWhiteSpace(color)) {
				if (showTimeStamp) {
					UnityEngine.Debug.LogWarningFormat("[{1}] {0}", messageStr, Time.time);
				} else {
					UnityEngine.Debug.LogWarning(messageStr);
				}
			} else {
				if (showTimeStamp) {
					UnityEngine.Debug.LogWarningFormat("[{2}] <color=\"{0}\">{1}</color>", color, messageStr, Time.time);
				} else {
					UnityEngine.Debug.LogWarningFormat("<color=\"{0}\">{1}</color>", color, messageStr);
				}
			}
		}

		/// <summary>
		/// Colored Debug Log Warning
		/// </summary>
		[Conditional("DEBUG"), Conditional("UNITY_EDITOR"), Conditional("WRITE_LOG")]
		public static void LogWarning(object message, UnityEngine.Color color, bool showTimeStamp = false) {
			LogWarning(message, ColorUtilities.ColorToHex(color), showTimeStamp);
		}

		/// <summary>
		/// Colored Debug Log Error
		/// </summary>
		public static void LogError(object message, string color = null, bool showTimeStamp = false) {
			string messageStr = message != null ? message.ToString() : "null";

			if (string.IsNullOrWhiteSpace(color)) {
				if (showTimeStamp) {
					UnityEngine.Debug.LogErrorFormat("[{1}] {0}", messageStr, Time.time);
				} else {
					UnityEngine.Debug.LogError(messageStr);
				}
			} else {
				if (showTimeStamp) {
					UnityEngine.Debug.LogErrorFormat("[{2}] <color=\"{0}\">{1}</color>", color, messageStr, Time.time);
				} else {
					UnityEngine.Debug.LogErrorFormat("<color=\"{0}\">{1}</color>", color, messageStr);
				}
			}
		}

		/// <summary>
		/// Colored Debug Log Error
		/// </summary>
		public static void LogError(object message, UnityEngine.Color color, bool showTimeStamp = false) {
			LogError(message, ColorUtilities.ColorToHex(color), showTimeStamp);
		}
	}
}
