﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities {
	public class QuaternionUtilities {

		/// <summary>
		/// Calculates a look Rotation
		/// </summary>
		/// <param name="dir">direction to look</param>
		/// <param name="up">up-direction</param>
		/// <param name="dirAxis">axis to look along</param>
		/// <param name="upAxis">axis to use for the up direction</param>
		/// <param name="reverseDir">reverse the look-direction?</param>
		/// <param name="reverseUp">reverse the up-direction?</param>
		/// <returns>Look Rotation as Quaternion</returns>
		public static Quaternion LookRotation(Vector3 dir, Vector3 up, Axis dirAxis = Axis.Z, Axis upAxis = Axis.Y, bool reverseDir = false, bool reverseUp = false) {

			if(upAxis == dirAxis) {
				upAxis = (Axis)(((int)upAxis + 1) % Enum.GetNames(typeof(Axis)).Length);
			}

			Vector3 vec1 = reverseDir ? -dir.normalized : dir.normalized;
			Vector3 vec2 = Vector3.Cross(reverseUp ? -up : up, vec1).normalized;
			Vector3 vec3 = Vector3.Cross(vec1, vec2);

			Matrix3 rotM = new Matrix3();

			switch(dirAxis) {
				case Axis.X:
					if(upAxis == Axis.Y) {
						rotM.SetColumns(vec1, vec3, -vec2);
					} else {
						rotM.SetColumns(vec1, vec2, vec3);
					}
					break;
				case Axis.Y:
					if(upAxis == Axis.X) {
						rotM.SetColumns(vec3, vec1, vec2);
					} else {
						rotM.SetColumns(-vec2, vec1, vec3);
					}
					break;
				default:
					if(upAxis == Axis.X) {
						rotM.SetColumns(vec3, -vec2, vec1);
					} else {
						rotM.SetColumns(vec2, vec3, vec1);
					}
					break;
			}

			return rotM.quaternion;
		}

		/// <summary>
		/// Calculates a Quaternion from Yaw, Pitch and Roll (angles in degrees)
		/// </summary>
		public static Quaternion YawPitchRoll(float yaw, float pitch, float roll) {
			return Quaternion.Euler(pitch, yaw, roll);
		}

		//TODO fix this shit...doesn't work correctly
		public static Quaternion AverageQuaternionCollection(IEnumerable<Quaternion> collection) {

			int addAmount = 0;

			Quaternion addedRotation = Quaternion.identity;

			//Loop through all the rotational values.
			foreach(Quaternion singleRotation in collection) {

				//Number of separate rotational values so far
				addAmount++;
				
				addedRotation.w += singleRotation.w;
				addedRotation.x += singleRotation.x;
				addedRotation.y += singleRotation.y;
				addedRotation.z += singleRotation.z;
			}

			float addDet = 1.0f / addAmount;
			float w = addedRotation.w * addDet;
			float x = addedRotation.x * addDet;
			float y = addedRotation.y * addDet;
			float z = addedRotation.z * addDet;


			float d = 1.0f / (w * w + x * x + y * y + z * z);
			w *= d;
			x *= d;
			y *= d;
			z *= d;

			return new Quaternion(x, y, z, w);
		}

		public static Quaternion GetClosestSnappedAngle(Vector3 pos, Vector3 target, Axis snappedAxis = Axis.Y) {
			Vector3 toTarget = (target - pos); 
			Vector3 up;

			float xAbs = Mathf.Abs(toTarget.x);
			float yAbs = Mathf.Abs(toTarget.y);
			float zAbs = Mathf.Abs(toTarget.z);

			switch (snappedAxis) {
				case Axis.X:
					up = Vector3.right;
					if (yAbs > zAbs) toTarget.z = 0;
					else toTarget.y = 0;
					break;
				case Axis.Z:
					up = Vector3.forward;
					if(xAbs > yAbs) toTarget.y = 0;
					else toTarget.x = 0;
					break;
				default:
					up = Vector3.up;
					if(xAbs > zAbs) toTarget.z = 0;
					else toTarget.x = 0;
					break;
			}

			return Quaternion.LookRotation(toTarget.normalized, up);
		}
	}
}