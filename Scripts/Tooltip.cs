﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Utilities {
	public class Tooltip : MonoBehaviour {

		RectTransform _imageTrans;
		Text _text;
		RectTransform _trans;
		Canvas _canvas;

		//TODO create automatically if not existing
		void Start() {
			_canvas = transform.GetCanvas();
			_trans = GetComponent<RectTransform>();
			_text = GetComponentInChildren<Text>();
			_imageTrans = GetComponentInChildren<Image>().GetComponent<RectTransform>();
			Hide();
		}

		public void Show(string text, Vector2 position) {
			StartCoroutine(ShowTooltip(text, position));
		}

		IEnumerator ShowTooltip(string text, Vector2 position) {
			_text.text = text;
			yield return null;
			_trans.anchoredPosition = new Vector2(Mathf.Min(position.x, _canvas.pixelRect.width - _imageTrans.rect.width), Mathf.Min(position.y, _canvas.pixelRect.height - _imageTrans.rect.height));
		}


		public void Hide(bool menuOpen = false) {
			_text.text = "";
			_trans.anchoredPosition = new Vector2(-100, -100);
		}
	}
}