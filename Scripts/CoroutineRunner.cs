﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static class CoroutineRunner {

		private static bool _preventInstantiation;

		private class Runner : MonoBehaviour { }

		private static Runner _i;

		private static Runner _instance {
			get {
				if (_i == null) {
					_i = new GameObject("Coroutine Runner").AddComponent<Runner>();
					_i.gameObject.hideFlags = HideFlags.HideInHierarchy;
					Object.DontDestroyOnLoad(_i.gameObject);
					Application.quitting += () => _preventInstantiation = true;
				}

				return _i;
			}
		}

		public static Coroutine Run(IEnumerator coroutine) {
			return _instance.StartCoroutine(coroutine);
		}

		public static Coroutine RunAllParallel(params IEnumerator[] coroutines) {
			return _instance.StartCoroutine(ParallelInvoke(coroutines));
		}

		public static Coroutine RunAllSequential(params IEnumerator[] coroutines) {
			return _instance.StartCoroutine(SequentialInvoke(coroutines));
		}

		public static Coroutine Then(this Coroutine previous, IEnumerator coroutine) {
			return _instance.StartCoroutine(SequentialInvoke(previous, coroutine));
		}

		public static Coroutine Then(this Coroutine previous, Action action) {
			return _instance.StartCoroutine(SequentialInvoke(previous, action));
		}

		public static Coroutine Wait(params Coroutine[] coroutines) {
			return _instance.StartCoroutine(ParallelWait(coroutines));
		}

		public static void RunBlocking(IEnumerator coroutine) {
			while (coroutine.MoveNext()) {
				if (coroutine.Current != null) {
					IEnumerator num;
					try {
						num = (IEnumerator)coroutine.Current;
					} catch (InvalidCastException) {
						if (coroutine.Current is WaitForSeconds)
							Debug.LogWarning("Skipped call to WaitForSeconds. Use WaitForSecondsRealtime instead.");
						continue; // Skip WaitForSeconds, WaitForEndOfFrame and WaitForFixedUpdate
					}

					RunBlocking(num);
				}
			}
		}

		public static void Stop(Coroutine coroutine) {
			if (_preventInstantiation) return;
			if (coroutine != null) _instance.StopCoroutine(coroutine);
		}

		public static void StopAll() {
			if (_preventInstantiation) return;
			_instance.StopAllCoroutines();
		}

		public static Coroutine RunAtFrameEnd(Action action) {
			if (_preventInstantiation) return null;
			return _instance.StartCoroutine(EndOfFrameInvoke(action));
		}

		public static Coroutine RunAtNextFixedUpdate(Action action) {
			if (_preventInstantiation) return null;
			return _instance.StartCoroutine(FixedUpdateInvoke(action));
		}

		public static Coroutine Delay(Action action, float? delay = null) {
			if (_preventInstantiation) return null;
			return _instance.StartCoroutine(DelayedInvoke(action, delay));
		}

		public static Coroutine DelayFrames(Action action, int? delay = null) {
			if (_preventInstantiation) return null;
			return _instance.StartCoroutine(DelayedInvokeFrames(action, delay));
		}

		public static Coroutine DelayWhile(Action action, Func<bool> predicate, float timeout = 0) {
			if (_preventInstantiation) return null;
			return _instance.StartCoroutine(DelayedInvokeWhile(action, predicate, timeout));
		}

		public static Coroutine DelayUntil(Action action, Func<bool> predicate, float timeout = 0) {
			if (_preventInstantiation) return null;
			return _instance.StartCoroutine(DelayedInvokeUntil(action, predicate, timeout));
		}

		public static Coroutine Repeat(Action action, float interval, int repetitions = -1, float delay = 0) {
			if (_preventInstantiation) return null;
			return _instance.StartCoroutine(RepeatedInvoke(action, interval, repetitions, delay));
		}

		public static Coroutine Cycle(float interval, params Action[] actions) {
			if (_preventInstantiation) return null;
			return Cycle(interval, -1, 0, actions);
		}

		public static Coroutine Cycle(float interval, int repetitions, params Action[] actions) {
			if (_preventInstantiation) return null;
			return Cycle(interval, repetitions, 0, actions);
		}

		public static Coroutine Cycle(float interval, float delay, params Action[] actions) {
			if (_preventInstantiation) return null;
			return Cycle(interval, -1, delay, actions);
		}

		public static Coroutine Cycle(float interval, int repetitions, float delay, params Action[] actions) {
			if (_preventInstantiation) return null;
			return actions.Length < 1 ? null : _instance.StartCoroutine(CycledInvoke(actions, interval, repetitions, delay));
		}

		private static IEnumerator EndOfFrameInvoke(Action action) {
			yield return new WaitForEndOfFrame();
			action?.Invoke();
		}

		private static IEnumerator FixedUpdateInvoke(Action action) {
			yield return new WaitForFixedUpdate();
			action?.Invoke();
		}

		private static IEnumerator DelayedInvoke(Action action, float? delay = null) {
			if (delay == null) yield return null;
			else if (delay > 0) yield return Waiter.Call(delay.Value);
			action?.Invoke();
		}

		private static IEnumerator DelayedInvokeFrames(Action action, int? delay = null) {
			if (delay == null) yield return null;
			else if (delay > 0) yield return new WaitForFrames(delay.Value);
			action?.Invoke();
		}

		private static IEnumerator DelayedInvokeWhile(Action action, Func<bool> predicate, float timeout = 0) {
			if (predicate()) {
				if (timeout > 0) {
					float endTime = Time.time + timeout;
					yield return new WaitWhile(() => predicate() && Time.time < endTime);
				} else {
					yield return new WaitWhile(predicate);
				}
			}
			action?.Invoke();
		}

		private static IEnumerator DelayedInvokeUntil(Action action, Func<bool> predicate, float timeout = 0) {
			if (!predicate()) {
				if (timeout > 0) {
					float endTime = Time.time + timeout;
					yield return new WaitUntil(() => predicate() || Time.time >= endTime);
				} else {
					yield return new WaitUntil(predicate);
				}
			}
			action?.Invoke();
		}

		private static IEnumerator RepeatedInvoke(Action action, float interval, int repetitions = -1, float delay = 0) {
			if (delay > 0) yield return Waiter.Call(delay);

			WaitForSeconds wait = !interval.Approximates(0) ? Waiter.Call(Mathf.Abs(interval)) : null;
			int nmbr = 0;

			for (; ; ) {
				action?.Invoke();
				nmbr++;
				if (repetitions > 0 && nmbr >= repetitions) yield break;
				yield return wait;
			}

		}

		private static IEnumerator CycledInvoke(Action[] actions, float interval, int repetitions = -1, float delay = 0) {
			if (delay > 0) yield return Waiter.Call(delay);

			WaitForSeconds wait = !interval.Approximates(0) ? Waiter.Call(Mathf.Abs(interval)) : null;

			int nmbr = 0;
			int actionIndex = 0;

			for (; ; ) {
				actions[actionIndex]?.Invoke();
				actionIndex++;
				if (actionIndex > actions.Length) {
					actionIndex = 0;
					nmbr++;
					if (repetitions > 0 && nmbr >= repetitions) yield break;
				}

				yield return wait;
			}
		}

		private static IEnumerator SequentialInvoke(Coroutine previous, IEnumerator coroutine) {
			yield return previous;
			yield return _instance.StartCoroutine(coroutine);
		}

		private static IEnumerator SequentialInvoke(Coroutine previous, Action action) {
			yield return previous;
			action?.Invoke();
		}

		private static IEnumerator SequentialInvoke(params IEnumerator[] routines) {
			for (int i = 0; i < routines.Length; ++i) {
				yield return _instance.StartCoroutine(routines[i]);
			}
		}

		private static IEnumerator ParallelInvoke(params IEnumerator[] routines) {

			Coroutine[] waitHandles = new Coroutine[routines.Length];

			for (int i = 0; i < routines.Length; ++i) {
				waitHandles[i] = _instance.StartCoroutine(routines[i]);
			}

			foreach (Coroutine handle in waitHandles) {
				yield return handle;
			}
		}

		private static IEnumerator ParallelWait(params Coroutine[] routines) {
			foreach (Coroutine coroutine in routines) {
				yield return coroutine;
			}
		}
	}
}
