﻿
using System;

namespace Utilities {
	public static class CommandLine {
		private static string[] _args;

		public static bool HasArgument(string name) {
			if (_args == null) _args = Environment.GetCommandLineArgs();

			for (int i = 0; i < _args.Length; ++i) {
				if (string.Equals(_args[i], name, StringComparison.OrdinalIgnoreCase)) return true;
			}

			return false;
		}

		public static bool TryGetArgumentValue(string name, out string value) {
			if (_args == null) _args = Environment.GetCommandLineArgs();

			for (int i = 0; i < _args.Length - 1; ++i) {
				if (string.Equals(_args[i], name, StringComparison.OrdinalIgnoreCase)) {
					value = _args[i + 1];
					return true;
				}
			}

			value = string.Empty;
			return false;
		}
	}
}