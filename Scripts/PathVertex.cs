﻿using UnityEngine;

namespace Utilities {

	[ExecuteInEditMode]
	public class PathVertex : MonoBehaviour {

		public enum VertexType {
			Interpolant = 0,
			StartController = 1,
			EndController = 2
		}

		public VertexType vType;
		private Path _path;

		private void InitPath() {
			_path = GetComponent<Path>();

			Transform parent = transform.parent;
			while (_path == null && parent != null) {
				_path = parent.GetComponent<Path>();
				parent = parent.parent;
			}

			if (_path != null) {
				_path.AddVertex(this);
			}
		}

		private void Reset() {
			if (_path != null) {
				_path.RemoveVertex(this);
			}
			InitPath();
		}

		private void OnValidate() {
			if (_path == null) {
				InitPath();
			}
			_path.RemoveVertex(this);
			_path.AddVertex(this);
		}

		private void OnDestroy() {
			if (_path != null) {
				_path.RemoveVertex(this);
			}
		}

		private void Start() {
			if (_path == null) {
				InitPath();
			}
			if (Application.isPlaying && !_path.dynamic) enabled = false;
		}

		private void Update() {
			if (Application.isPlaying && !_path.dynamic) return;

			if (transform.hasChanged) {
				_path.UpdatePath();
				transform.hasChanged = false;
			}
		}

		private void OnDrawGizmos() {
			if (_path == null || !_path.drawGizmos) return;

			Color color = vType == VertexType.Interpolant ? _path.gizmoColor : _path.endControllerColor;
			color.a = 0.75f;

			Gizmos.color = color;

			Gizmos.DrawSphere(transform.position, _path.gizmoScale);
		}
	}
}
