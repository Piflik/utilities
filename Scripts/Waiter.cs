﻿using UnityEngine;
using System.Collections.Generic;

namespace Utilities {
	public static class Waiter {
		private static readonly Dictionary<int, WaitForSeconds> _waiters = new Dictionary<int, WaitForSeconds>();

		public static WaitForSeconds Call(float seconds) {
			if (seconds <= 0) {
				return null;
			}

			int key = (int)(seconds * 1000);
			if (key == 0) {
				return null;
			}

			if (_waiters.TryGetValue(key, out WaitForSeconds waiter)) {
				return waiter;
			}

			waiter = new WaitForSeconds(seconds);
			_waiters[key] = waiter;
			return waiter;
		}
		
	}
}
