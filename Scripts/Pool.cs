using System;
using System.Collections.Generic;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public class Pool<T> {
		private readonly Stack<T> _pool = new Stack<T>();

		private readonly Func<T> _create;
		private readonly Action<T> _activate, _reset;
		private readonly bool _needsActivation, _needsReset;

		public Pool(int initialSize, Func<T> createMethod, Action<T> reset = null, Action<T> activate = null) {
			if (createMethod != null) {
				_create = createMethod;
			} else if (_hasDefaultConstructor) {
				_create = Activator.CreateInstance<T>;
			} else {
				_create = () => default;
			}

			_activate = activate;
			_needsActivation = _activate != null;

			_reset = reset;
			_needsReset = _reset != null;

			for (int i = 0; i < initialSize; i++) {
				T t = _create();
				Return(t);
			}
		}

		public T Get() {
			T obj = _pool.Count > 0 ? _pool.Pop() : _create();
			if (_needsActivation)
				_activate(obj);
			return obj;
		}

		public void Return(params T[] objs) {
			if (objs != null) {
				foreach (T obj in objs) {
					if (_needsReset) {
						_reset(obj);
					}

					_pool.Push(obj);
				}
			}
		}

		private static bool _hasDefaultConstructor {
			get {
				Type u = typeof(T);
				return u.IsValueType || u.GetConstructor(Type.EmptyTypes) != null;
			}
		}
	}
}
