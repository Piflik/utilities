﻿using UnityEngine;

namespace Utilities {
	/// <summary>
	/// Projection utils.
	/// </summary>
	public static class CameraUtilities {
		public static readonly Vector3 invalidPositionV3 = new Vector3(float.NaN, float.NaN, float.NaN);


		/// <summary>
		/// Projects a screen point to a plane
		/// </summary>
		/// <returns></returns>
		public static Vector3 CameraToPlaneProjection(Vector2 screenPos, UnityEngine.Camera camera, Plane projectionPlane) {
			Ray ray = camera.ScreenPointToRay(screenPos);
			projectionPlane.Raycast(ray, out float relativeIntersection);
			return ray.origin + ray.direction * relativeIntersection;
		}

		/// <summary>
		/// Screen to world point calculation that works also with non perspective projection!
		/// </summary>
		public static Vector3 ScreenToWorldProjection(Vector2 screenPos, UnityEngine.Camera raycastCam, Transform target, float raycastDistance = Mathf.Infinity) {
			if (raycastCam == null) raycastCam = UnityEngine.Camera.main;

			if (target != null) {
				Plane plane;
				//create projection plane from hitdistance if valid 
				//plane does not include rotation!
				//   _____
				//  |     |
				//  |_____|
				//
				if (raycastDistance < Mathf.Infinity) {
					plane = new Plane(target.forward, raycastDistance);


					//create plane that includes target rotation!
					//    3D            
					//     /|    
					//    / /
					//    |/
					//
				} else {
					plane = new Plane(target.forward, target.transform.position);


				}

				return CameraToPlaneProjection(screenPos, raycastCam, plane);
			}

			return invalidPositionV3;
		}

		/// <summary>
		/// Calculate local postion of a point on Transform t
		/// </summary>
		public static Vector3 WorldToLocalPos(Vector3 global, Transform t) {
			Vector3 local = new Vector3(0f, 0f, 0f);
			if (t != null && !float.IsNaN(global.x) && !float.IsNaN(global.y)) {
				local = t.InverseTransformPoint(global);
			}

			return local;
		}

		/// <summary>
		/// Calculates the distance needed to have a certain resolution at a given vertical FoV
		/// </summary>
		public static int CalcCamDistance(float fovAngle, int width, int height) {
			int d = (int)(height * .5f / Mathf.Tan(fovAngle * .5f * Mathf.Deg2Rad));

			return d;
		}

		/// <summary>
		/// Calculate the Camera Rect from the target resolution and the window resolution
		/// </summary>
		public static Rect CalcCamRectangle(Vector2 targetResolution, Vector2 curScreenSize) {
			int screenWidth = (int)curScreenSize.x;               //window resolution in px
			int screenHeight = (int)curScreenSize.y;

			//calc width and height factor
			float fW = screenWidth / targetResolution.x;
			float fH = screenHeight / targetResolution.y;

			float rectW;                               //0..1 >> 0..100% from screenWidth
			float rectH;

			//if width was more down scaled than height, adjust height
			if (fW < fH) {
				rectW = 1;
				rectH = fW * targetResolution.y / screenHeight;
				rectH = 1f - rectH < 0.01f ? 1f : rectH;

				//else adjust width
			} else {
				rectH = 1;
				rectW = fH * targetResolution.x / screenWidth;
				rectW = 1f - rectW < 0.01f ? 1f : rectW;
			}

			float rectX = (1f - rectW) * .5f;
			float rectY = (1f - rectH) * .5f;

			return new Rect(rectX, rectY, rectW, rectH);
		}
	}
}
