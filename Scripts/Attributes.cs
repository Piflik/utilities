﻿using System;
using System.Linq;
using UnityEngine;

namespace Utilities {
	[Obsolete("EnumFlags Attribute is obsolete. Unity draws Flags enums now by default")]
	public sealed class EnumFlagsAttribute : PropertyAttribute {
		public Type propType;

		public EnumFlagsAttribute(Type aType) {
			propType = aType;
		}
	}

	public sealed class EnumFlagSingleAttribute : PropertyAttribute {
		public Type propType;

		public EnumFlagSingleAttribute(Type aType) {
			propType = aType;
		}
	}

	/// <summary>
	/// Attribute to select a single layer.
	/// </summary>
	public sealed class LayerAttribute : PropertyAttribute { }

	/// <summary>
	/// Attribute to make a field readonly for inspector.
	/// </summary>
	public sealed class ReadOnlyInInspectorAttribute : PropertyAttribute { }

	/// <summary>
	/// Attribute for string properties to select a tag.
	/// </summary>
	public sealed class TagSelectorAttribute : PropertyAttribute {
		public bool useDefaultTagFieldDrawer = false;
	}

	/// <summary>
	/// Attribute for limiting a serialized field to a certain interface
	/// </summary>
	public sealed class InterfaceTypeAttribute : PropertyAttribute {
		public Type type;

		public InterfaceTypeAttribute(Type type) {
			this.type = type;
		}
	}

	public sealed class IndentAttribute : PropertyAttribute {
		public readonly int indentation;

		public IndentAttribute() {
			indentation = 1;
		}

		public IndentAttribute(int indentation) {
			this.indentation = indentation;
		}
	}

	//Original version of the ConditionalHideAttribute created by Brecht Lecluyse (www.brechtos.com)
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct)]
	public class ConditionalHideAttribute : PropertyAttribute {
		public readonly (string, bool)[] conditionalSourceField;
		public readonly bool hideInactive;

		public ConditionalHideAttribute(string conditionalSourceField, bool invert = false, bool hideInactive = true) {
			this.conditionalSourceField = new[] {(conditionalSourceField, invert)};
			this.hideInactive = hideInactive;
		}

		public ConditionalHideAttribute(string[] sourceFields, bool[] invert, bool hideInactive = true) {
			conditionalSourceField = sourceFields.Zip(invert, (s, b) => (s, b)).ToArray();
			this.hideInactive = hideInactive;
		}
	}
}