﻿using System;

namespace Utilities {
	public delegate void ValueChanged<T>(T newValue, T oldValue);
	public interface IObservable<T> {
		event ValueChanged<T> valueChanged;
	}

	public class ChangeNotifier<T> : IObservable<T>, IEquatable<ChangeNotifier<T>>, IComparable<ChangeNotifier<T>>, IEquatable<T>, IComparable<T> where T : struct, IComparable {

		private T _value;

		public event ValueChanged<T> valueChanged;

		public ChangeNotifier() { }

		public ChangeNotifier(T value) {
			_value = value;
			valueChanged = null;
		}

		public T value {
			get => _value;
			set => SetValue(value);
		}

		public void SetValue(T value, DispatchMode dispatchMode = DispatchMode.Dispatch) {
			if (Equals(value) && dispatchMode != DispatchMode.ForceDispatch) return;
			T oldVal = _value;
			_value = value;

			if (dispatchMode != DispatchMode.NoDispatch) {
				valueChanged?.Invoke(_value, oldVal);
			}
		}

		public void Subscribe(ValueChanged<T> action, bool initialize = true) {
			valueChanged += action;
			if(initialize) action?.Invoke(_value, _value);
		}

		public void Unsubscribe(ValueChanged<T> action) {
			valueChanged -= action;
		}

		public override int GetHashCode() {
			return _value.GetHashCode();
		}

		public bool Equals(T other) {
			return _value.Equals(other);
		}

		public int CompareTo(T other) {
			return _value.CompareTo(other);
		}

		public override bool Equals(object obj) {
			if (obj is null) return false;
			if (obj is T val) return _value.Equals(val);
			return obj is ChangeNotifier<T> notifier && Equals(notifier);
		}

		public bool Equals(ChangeNotifier<T> other) {
			return !(other is null) && _value.Equals(other._value);
		}

		public int CompareTo(ChangeNotifier<T> other) {
			if (other is null) return 1;
			return _value.CompareTo(other._value);
		}

		public static bool operator ==(ChangeNotifier<T> left, ChangeNotifier<T> right) {
			if (left is null) return right is null;
			return left.Equals(right);
		}

		public static bool operator !=(ChangeNotifier<T> left, ChangeNotifier<T> right) {
			if (left is null) return !(right is null);
			return !left.Equals(right);
		}

		public static bool operator <(ChangeNotifier<T> left, ChangeNotifier<T> right) {
			if (left is null || right is null) return false;
			return left.CompareTo(right) < 0;
		}

		public static bool operator >(ChangeNotifier<T> left, ChangeNotifier<T> right) {
			if (left is null || right is null) return false;
			return left.CompareTo(right) > 0;
		}

		public static bool operator <=(ChangeNotifier<T> left, ChangeNotifier<T> right) {
			if (left is null) return right is null;
			return left.CompareTo(right) <= 0;
		}

		public static bool operator >=(ChangeNotifier<T> left, ChangeNotifier<T> right) {
			if (left is null) return right is null;
			return left.CompareTo(right) >= 0;
		}

		//comparisson operators with values
		public static bool operator ==(ChangeNotifier<T> left, T right) {
			if (left is null) return false;
			return left.value.Equals(right);
		}

		public static bool operator !=(ChangeNotifier<T> left, T right) {
			if (left is null) return false;
			return !left.value.Equals(right);
		}

		public static bool operator <(ChangeNotifier<T> left, T right) {
			if (left is null) return false;
			return left.value.CompareTo(right) < 0;
		}

		public static bool operator >(ChangeNotifier<T> left, T right) {
			if (left is null) return false;
			return left.value.CompareTo(right) > 0;
		}

		public static bool operator <=(ChangeNotifier<T> left, T right) {
			if (left is null) return false;
			return left.value.CompareTo(right) <= 0;
		}

		public static bool operator >=(ChangeNotifier<T> left, T right) {
			if (left is null) return false;
			return left.value.CompareTo(right) >= 0;
		}

		public static bool operator ==(T left, ChangeNotifier<T> right) {
			if (right is null) return false;
			return left.Equals(right.value);
		}

		public static bool operator !=(T left, ChangeNotifier<T> right) {
			if (right is null) return false;
			return !left.Equals(right.value);
		}

		public static bool operator <(T left, ChangeNotifier<T> right) {
			if (right is null) return false;
			return left.CompareTo(right.value) < 0;
		}

		public static bool operator >(T left, ChangeNotifier<T> right) {
			if (right is null) return false;
			return left.CompareTo(right.value) > 0;
		}

		public static bool operator <=(T left, ChangeNotifier<T> right) {
			if (right is null) return false;
			return left.CompareTo(right.value) <= 0;
		}

		public static bool operator >=(T left, ChangeNotifier<T> right) {
			if (right is null) return false;
			return left.CompareTo(right.value) >= 0;
		}
	}
}
