﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static partial class Parsing {
		public static bool StrToBool(string str) {
			return str != null && (str.ToLower() == "true" || float.TryParse(str, NumberStyles.Float, CultureInfo.InvariantCulture, out float f) && !f.Approximates(0));
		}

		public static bool TryParse(string s, out bool b) {
			if (s == null) {
				b = false;
				return false;
			}

			s = s.ToLower();
			switch (s) {
				case "true":
				case "1":
					b = true;
					return true;
				case "false":
				case "0":
					b = false;
					return true;
				default:
					b = false;
					return false;
			}
		}

		public static bool TryParse(string s, out Vector2 v, Vector2 defaultResult = default, char separator = ',') {
			v = defaultResult;
			if (!TryParse(s, 2, out float[] components, separator))
				return false;
			v.Set(components[0], components[1]);
			return true;
		}

		public static bool TryParse(string s, out Vector3 v, Vector3 defaultResult = default, char separator = ',') {
			v = defaultResult;
			if (!TryParse(s, 3, out float[] components, separator))
				return false;
			v.Set(components[0], components[1], components[2]);
			return true;
		}

		public static bool TryParse(string s, out Vector4 v, Vector4 defaultResult = default, char separator = ',') {
			v = defaultResult;
			if (!TryParse(s, 4, out float[] components, separator))
				return false;
			v.Set(components[0], components[1], components[2], components[3]);
			return true;
		}

		public static bool TryParse(string s, out Vector2Int v, Vector2Int defaultResult = default, char separator = ',') {
			v = defaultResult;
			if (!TryParse(s, 2, out int[] components, separator))
				return false;
			v.Set(components[0], components[1]);
			return true;
		}

		public static bool TryParse(string s, out Vector3Int v, Vector3Int defaultResult = default, char separator = ',') {
			v = defaultResult;
			if (!TryParse(s, 3, out int[] components, separator))
				return false;
			v.Set(components[0], components[1], components[2]);
			return true;
		}

		public static bool TryParse(string s, out Quaternion q, Quaternion defaultResult = default, char separator = ',') {
			q = defaultResult;
			if (!TryParse(s, 4, out float[] components, separator))
				return false;
			q.Set(components[0], components[1], components[2], components[3]);
			return true;
		}

		public static bool TryParse(string s, int dimension, out float[] components, char separator = ',') {
			components = new float[dimension];
			if (string.IsNullOrWhiteSpace(s)) return false;
			string[] parts = s.Trim('(', ')').Split(separator);
			if (parts.Length != dimension)
				return false;
			for (int i = 0; i < dimension; i++) {
				if (!float.TryParse(parts[i], NumberStyles.Float, CultureInfo.InvariantCulture, out float val))
					return false;
				components[i] = val;
			}

			return true;
		}

		public static bool TryParse(string s, int dimension, out int[] components, char separator = ',') {
			components = new int[dimension];
			if (string.IsNullOrWhiteSpace(s)) return false;
			string[] parts = s.Trim('(', ')').Split(separator);
			if (parts.Length != dimension)
				return false;
			for (int i = 0; i < dimension; i++) {
				if (!int.TryParse(parts[i], NumberStyles.Integer, CultureInfo.InvariantCulture, out int val))
					return false;
				components[i] = val;
			}

			return true;
		}

		public static bool TryParse<T>(string value, out T outEnum, bool ignoreCase = false, bool ignoreUndefinedValue = false) where T : struct, IConvertible {
			if (!typeof(T).IsEnum || string.IsNullOrEmpty(value)) {
				outEnum = default;
				return false;
			}

#if NET_4_6
			return Enum.TryParse(value, ignoreCase, out outEnum) && (ignoreUndefinedValue || Enum.IsDefined(typeof(T), outEnum));
#else

				try {
					outEnum = (T)Enum.Parse(typeof(T), value, ignoreCase);
					return ignoreUndefinedValue || Enum.IsDefined(typeof(T), outEnum);
				} catch {
					outEnum = default(T);
					return false;
				}
#endif
		}

		public static T Parse<T>(string value, T defaultValue) where T : struct, IConvertible {
			if (!typeof(T).IsEnum || string.IsNullOrEmpty(value)) {
				return defaultValue;
			}

			try {
				return (T)Enum.Parse(typeof(T), value);
			} catch {
				return defaultValue;
			}
		}

		/// <summary>
		/// Convert string to a color by name, comma-separated values or Hex (first that applies in that order)
		/// </summary>
		public static bool TryParse(string colorString, out Color color, Color defaultColor = default, bool hsv = false, char separator = ',') {
			if (ColorUtilities.ColorByName(colorString, out color)) return true;

			if (hsv)
				return ColorUtilities.TryParseHSV(colorString, out color, separator: separator) || ColorUtilities.TryParseHexHSV(colorString, out color, defaultColor);

			return ColorUtilities.TryParseRGB(colorString, out color, separator: separator) || ColorUtilities.TryParseHex(colorString, out color, defaultColor);
		}

		public static bool TryMatchRegex(string input, string pattern, out Match match) {
			match = Regex.Match(input, pattern);
			return match.Success;
		}
		public static bool TryMatchRegex(string input, string pattern, out MatchCollection matches) {
			matches = Regex.Matches(input, pattern);
			return matches.Count > 0;
		}
	}
}

public static class ParsingExtensions {

	public static bool ToBool(this string str)
		=> Utilities.Parsing.StrToBool(str);

	public static int ToInt(this string str, int defaultValue = 0)
		=> int.TryParse(str, out int value) ? value : defaultValue;

	public static uint ToUInt(this string str, uint defaultValue = 0)
		=> uint.TryParse(str, out uint value) ? value : defaultValue;

	public static short ToShort(this string str, short defaultValue = 0)
		=> short.TryParse(str, out short value) ? value : defaultValue;

	public static ushort ToUShort(this string str, ushort defaultValue = 0)
		=> ushort.TryParse(str, out ushort value) ? value : defaultValue;

	public static byte ToByte(this string str, byte defaultValue = 0)
		=> byte.TryParse(str, out byte value) ? value : defaultValue;

	public static sbyte ToSByte(this string str, sbyte defaultValue = 0)
		=> sbyte.TryParse(str, out sbyte value) ? value : defaultValue;

	public static long ToLong(this string str, long defaultValue = 0)
		=> long.TryParse(str, out long value) ? value : defaultValue;

	public static ulong ToULong(this string str, ulong defaultValue = 0)
		=> ulong.TryParse(str, out ulong value) ? value : defaultValue;

	public static float ToFloat(this string str, float defaultValue = 0)
		=> float.TryParse(str, out float value) ? value : defaultValue;

	public static double ToDouble(this string str, double defaultValue = 0)
		=> double.TryParse(str, out double value) ? value : defaultValue;

	public static Vector2 ToVector2(this string s, Vector2 defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out Vector2 outVec, defaultResult, separator) ? outVec : defaultResult;

	public static Vector3 ToVector3(this string s, Vector3 defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out Vector3 outVec, defaultResult, separator) ? outVec : defaultResult;

	public static Vector4 ToVector4(this string s, Vector4 defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out Vector4 outVec, defaultResult, separator) ? outVec : defaultResult;

	public static Vector2Int ToVector2Int(this string s, Vector2Int defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out Vector2Int outVec, defaultResult, separator) ? outVec : defaultResult;

	public static Vector3Int ToVector3Int(this string s, Vector3Int defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out Vector3Int outVec, defaultResult, separator) ? outVec : defaultResult;

	public static Quaternion ToQuaternion(this string s, Quaternion defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out Quaternion outVec, defaultResult, separator) ? outVec : defaultResult;

	public static float[] ToVector(this string s, int dimension, char separator = ',')
		=> Utilities.Parsing.TryParse(s, dimension, out float[] components, separator) ? components : new float[dimension];

	public static T ToEnum<T>(this string str, T defaultValue = default, bool ignoreCase = false, bool ignoreUndefinedValue = false) where T : struct, IConvertible
		=> TryParse(str, out T outEnum, ignoreCase, ignoreUndefinedValue) ? outEnum : defaultValue;

	public static Color ToColor(this string str, Color defaultColor = default, bool hsv = false)
		=> TryParse(str, out Color outColor, defaultColor, hsv) ? outColor : defaultColor;

	public static bool TryParse(this string s, out bool b)
		=> Utilities.Parsing.TryParse(s, out b);

	public static bool TryParse(this string s, out int n)
		=> int.TryParse(s, out n);

	public static bool TryParse(this string s, out uint n)
		=> uint.TryParse(s, out n);

	public static bool TryParse(this string s, out short n)
		=> short.TryParse(s, out n);

	public static bool TryParse(this string s, out ushort n)
		=> ushort.TryParse(s, out n);

	public static bool TryParse(this string s, out byte n)
		=> byte.TryParse(s, out n);

	public static bool TryParse(this string s, out sbyte n)
		=> sbyte.TryParse(s, out n);

	public static bool TryParse(this string s, out long n)
		=> long.TryParse(s, out n);

	public static bool TryParse(this string s, out ulong n)
		=> ulong.TryParse(s, out n);

	public static bool TryParse(this string s, out float n)
		=> float.TryParse(s, out n);

	public static bool TryParse(this string s, out double n)
		=> double.TryParse(s, out n);

	public static bool TryParse(this string s, out Vector2 v, Vector2 defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out v, defaultResult, separator);

	public static bool TryParse(this string s, out Vector3 v, Vector3 defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out v, defaultResult, separator);

	public static bool TryParse(this string s, out Vector4 v, Vector4 defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out v, defaultResult, separator);

	public static bool TryParse(this string s, out Vector2Int v, Vector2Int defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out v, defaultResult, separator);

	public static bool TryParse(this string s, out Vector3Int v, Vector3Int defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out v, defaultResult, separator);

	public static bool TryParse(this string s, out Quaternion q, Quaternion defaultResult = default, char separator = ',')
		=> Utilities.Parsing.TryParse(s, out q, defaultResult, separator);

	public static bool TryParse(this string s, int dimension, out float[] components, char separator = ',')
		=> Utilities.Parsing.TryParse(s, dimension, out components, separator);

	public static bool TryParse<T>(this string value, out T outEnum, bool ignoreCase = false, bool ignoreUndefinedValue = false) where T : struct, IConvertible
		=> Utilities.Parsing.TryParse(value, out outEnum, ignoreCase, ignoreUndefinedValue);

	/// <summary>
	/// Convert string to a color by name, comma-separated values or Hex (first that applies in that order)
	/// </summary>
	public static bool TryParse(this string colorString, out Color color, Color defaultColor = default, bool hsv = false)
		=> Utilities.Parsing.TryParse(colorString, out color, defaultColor, hsv);

	public static bool TryMatchRegex(this string input, string pattern, out Match match)
		=> Utilities.Parsing.TryMatchRegex(input, pattern, out match);

	public static bool TryMatchRegex(this string input, string pattern, out MatchCollection matches)
		=> Utilities.Parsing.TryMatchRegex(input, pattern, out matches);
}
