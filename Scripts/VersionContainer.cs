﻿using System;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Utilities {
	public class VersionContainer : ScriptableObject {
		private const string VERSION_PATH = "Assets/Version.asset";

		public string versionStandalone = new Version(DateTime.Now.ToString("yy.M.d.0")).ToString();
		public string versionAndroid = new Version(DateTime.Now.ToString("yy.M.d.0")).ToString();
		public string versionIOS = new Version(DateTime.Now.ToString("yy.M.d")).ToString();

		public string versionOverride;

#if UNITY_EDITOR
		public void UpdateVersion(BuildTarget target) {

			if (!versionOverride.IsNullOrWhiteSpace()) {
				PlayerSettings.bundleVersion = versionOverride;
				return;
			}

			DateTime now = DateTime.Now;

			Version standalone = new Version(versionStandalone);
			Version android = new Version(versionAndroid);
			Version ios = new Version(versionIOS);

			Version vMax = Max(ios, android, standalone);
			Version vNow = new Version(now.Year % 100, now.Month, now.Day, 1);


			switch (target) {
				case BuildTarget.Android:

					if (vMax > android) {
						versionAndroid = Max(vNow, vMax).ToString();
					} else {
						if (android.Major == vNow.Major && android.Minor == vNow.Minor && android.Build == vNow.Build) {
							versionAndroid = new Version(android.Major, android.Minor, android.Build, android.Revision + 1).ToString();
						} else {
							versionAndroid = vNow.ToString();
						}
					}

					PlayerSettings.bundleVersion = versionAndroid;
					break;
				case BuildTarget.iOS:

					if (vMax > ios) {
						versionIOS = Max(vNow, vMax).ToString();
					} else {
						if (ios.Major == vNow.Major && ios.Minor == vNow.Minor && ios.Build == vNow.Build) {
							versionIOS = new Version(ios.Major, ios.Minor, ios.Build).ToString();
						} else {
							versionIOS = vNow.ToString();
						}
					}

					PlayerSettings.bundleVersion = versionIOS;
					break;
				default:

					if (vMax > standalone) {
						versionStandalone = Max(vNow, vMax).ToString();
					} else {
						if (standalone.Major == vNow.Major && standalone.Minor == vNow.Minor && standalone.Build == vNow.Build) {
							versionStandalone = new Version(standalone.Major, standalone.Minor, standalone.Build, standalone.Revision + 1).ToString();
						} else {
							versionStandalone = vNow.ToString();
						}
					}

					PlayerSettings.bundleVersion = versionStandalone;
					break;
			}

			EditorUtility.SetDirty(this);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}


		public static VersionContainer instance {
			get {
				VersionContainer vc = Resources.LoadAll<VersionContainer>("").FirstOrDefault();
				if (vc == null) {
					vc = AssetDatabase.LoadAssetAtPath<VersionContainer>(VERSION_PATH);
				}

				if (vc == null) {
					vc = CreateInstance<VersionContainer>();
					AssetDatabase.CreateAsset(vc, VERSION_PATH);
					AssetDatabase.SaveAssets();
				}

				return vc;
			}
		}

#endif

		private static Version Max(Version a, Version b, Version c) {
			return a > b && a > c ? a : b > c ? b : c;
		}

		private static Version Max(Version a, Version b) {
			return a > b ? a : b;
		}

	}
}