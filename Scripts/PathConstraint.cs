﻿using UnityEngine;

namespace Utilities {
	[DisallowMultipleComponent]
	public class PathConstraint : MonoBehaviour {

		public float speed;
		public bool loop;

		public Spline path;
		float _progress;

		void Update() {
			if (path == null) return;
			if (Input.GetButton("Pause")) return;

			Vector3 vertex;

			if (path.length <= 0.0001f) {
				vertex = path.GetPoint(0);
			} else {
				_progress += Time.deltaTime * speed / path.length;
				_progress = loop ? _progress % 1 : Mathf.Clamp(_progress, 0, 1);

				vertex = path.GetPoint(_progress);
			}

			transform.position = vertex;
		}
	}
}