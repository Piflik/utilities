﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

// ReSharper disable UnusedMember.Global

namespace Utilities {
	public class Tweener : IDisposable {
		public float duration;
		public float delay;
		public EaseType easeType = EaseType.Linear;
		public Action<float> onUpdate;
		public Action onFinish;
		public bool reverse;

		private AnimationCurve _curve;
		public AnimationCurve curve {
			get => _curve;
			set {
				_curve = value;

				if (_curve == null) {
					_evalFunc = Evaluate;
				} else {
					_evalFunc = EvaluateCurve;
				}
			}
		}

		public bool isTweening { get; private set; }

		private float _time;

		private Coroutine _tweening;

		private Tweener() { }
		private Func<float, float> _evalFunc;

		public static Tweener Create(float duration, float delay = 0, bool autoStart = true, bool reverse = false, float startAlpha = 0, EaseType easeType = EaseType.Linear, AnimationCurve curve = null, Action<float> onUpdate = null, Action onFinish = null) {
			Tweener t = new Tweener {
				duration = duration,
				delay = delay,
				reverse = reverse,
				easeType = easeType,
				onUpdate = onUpdate,
				onFinish = onFinish,
				curve = curve,
			};

			t._time = startAlpha.Between(0, 1) * duration;

			if (autoStart) t._tweening = CoroutineRunner.Run(t.Tweening());

			return t;
		}

		private IEnumerator Tweening() {
			isTweening = true;
			yield return Waiter.Call(delay);

			while (_time < duration) {
				_time += Time.deltaTime;

				float alpha = _evalFunc(reverse ? duration - _time : _time);

				onUpdate?.Invoke(alpha);
				yield return null;
			}

			onFinish?.Invoke();
			isTweening = false;
		}

		private float Evaluate(float time) => Utils.TweenByLerp(0, 1, duration, time, easeType);
		private float EvaluateCurve(float time) => _curve.Evaluate(Evaluate(time));

		public void Start(bool reset = true) {
			CoroutineRunner.Stop(_tweening);
			if (reset) _time = 0;
			_tweening = CoroutineRunner.Run(Tweening());
		}

		public void ToEnd() {
			Pause();
			_time = duration;
			onUpdate?.Invoke(1);
			onFinish?.Invoke();
		}

		public void Resume() => Start(reset: false);

		public void Pause() {
			CoroutineRunner.Stop(_tweening);
			isTweening = false;
		}

		public void Dispose() {
			CoroutineRunner.Stop(_tweening);
			isTweening = false;
		}

		public static Tweener TweenFromTo(float start, float end, float duration, float delay = 0, Action<float> onUpdate = null, Action onFinish = null, EaseType easeType = EaseType.Linear) {
			return Create(duration, delay,
				easeType: easeType,
				onUpdate: alpha => onUpdate(Mathf.Lerp(start, end, alpha)),
				onFinish: onFinish);
		}

		public static Tweener TweenFromTo(Vector4 start, Vector4 end, float duration, float delay = 0, Action<Vector4> onUpdate = null, Action onFinish = null, EaseType easeType = EaseType.Linear) {
			return Create(duration, delay,
				easeType: easeType,
				onUpdate: alpha => onUpdate(Vector4.Lerp(start, end, alpha)),
				onFinish: onFinish);
		}

		public static Tweener TweenFromTo(Vector3 start, Vector3 end, float duration, float delay = 0, Action<Vector3> onUpdate = null, Action onFinish = null, EaseType easeType = EaseType.Linear) {
			return Create(duration, delay,
				easeType: easeType,
				onUpdate: alpha => onUpdate(Vector3.Lerp(start, end, alpha)),
				onFinish: onFinish);
		}

		public static Tweener TweenFromTo(Vector2 start, Vector2 end, float duration, float delay = 0, Action<Vector2> onUpdate = null, Action onFinish = null, EaseType easeType = EaseType.Linear) {
			return Create(duration, delay,
				easeType: easeType,
				onUpdate: alpha => onUpdate(Vector2.Lerp(start, end, alpha)),
				onFinish: onFinish);
		}

		public static Tweener TweenFromTo(Quaternion start, Quaternion end, float duration, float delay = 0, Action<Quaternion> onUpdate = null, Action onFinish = null, EaseType easeType = EaseType.Linear) {
			return Create(duration, delay,
				easeType: easeType,
				onUpdate: alpha => onUpdate(Quaternion.Slerp(start, end, alpha)),
				onFinish: onFinish);
		}

		public static Tweener TweenFromTo(Color start, Color end, float duration, float delay = 0, Action<Color> onUpdate = null, Action onFinish = null, EaseType easeType = EaseType.Linear) {
			return Create(duration, delay,
				easeType: easeType,
				onUpdate: alpha => onUpdate(Color.Lerp(start, end, alpha)),
				onFinish: onFinish);
		}

		public static Tweener TweenFromTo(Color32 start, Color32 end, float duration, float delay = 0, Action<Color32> onUpdate = null, Action onFinish = null, EaseType easeType = EaseType.Linear) {
			return Create(duration, delay,
				easeType: easeType,
				onUpdate: alpha => onUpdate(Color32.Lerp(start, end, alpha)),
				onFinish: onFinish);
		}

		public static Tweener MoveTo(Transform transform, Vector3 targetLocalPosition, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector3> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(transform.localPosition, targetLocalPosition, duration, delay, pos => {
				transform.localPosition = pos;
				onUpdate?.Invoke(pos);
			}, () => {
				transform.localPosition = targetLocalPosition;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener RotateTo(Transform transform, Quaternion targetLocalRotation, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Quaternion> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(transform.localRotation, targetLocalRotation, duration, delay, rot => {
				transform.localRotation = rot;
				onUpdate?.Invoke(rot);
			}, () => {
				transform.localRotation = targetLocalRotation;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener MoveToWorld(Transform transform, Vector3 targetWorldPosition, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector3> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(transform.position, targetWorldPosition, duration, delay, pos => {
				transform.position = pos;
				onUpdate?.Invoke(pos);
			}, () => {
				transform.position = targetWorldPosition;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener RotateToWorld(Transform transform, Quaternion targetWorldRotation, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Quaternion> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(transform.rotation, targetWorldRotation, duration, delay, rot => {
				transform.rotation = rot;
				onUpdate?.Invoke(rot);
			}, () => {
				transform.rotation = targetWorldRotation;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener ScaleTo(Transform transform, Vector3 targetLocalScale, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector3> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(transform.localScale, targetLocalScale, duration, delay, scale => {
				transform.localScale = scale;
				onUpdate?.Invoke(scale);
			}, () => {
				transform.localScale = targetLocalScale;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener SizeTo(RectTransform transform, Vector2 targetSize, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector2> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(transform.sizeDelta, targetSize, duration, delay, size => {
				transform.sizeDelta = size;
				onUpdate?.Invoke(size);
			}, () => {
				transform.sizeDelta = targetSize;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener TransformTo(Transform transform, Transform targetTransform, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector3, Quaternion, Vector3> onUpdate = null, Action onComplete = null) {

			Vector3 startPos = transform.position;
			Quaternion startRot = transform.rotation;
			Vector3 startScale = transform.localScale;

			Vector3 targetPos = targetTransform.position;
			Quaternion targetRot = targetTransform.rotation;
			Vector3 targetScale = targetTransform.lossyScale;

			return Create(duration, delay,
				easeType: easeType,
				onUpdate: alpha => {

					Vector3 intermediatePosition = Vector3.Lerp(startPos, targetPos, alpha);
					Quaternion intermediateRotation = Quaternion.Slerp(startRot, targetRot, alpha);
					Vector3 intermediateScale = Vector3.Lerp(startScale, targetScale, alpha);

					transform.localScale = intermediateScale;
					transform.SetPositionAndRotation(intermediatePosition, intermediateRotation);

					onUpdate?.Invoke(intermediatePosition, intermediateRotation, intermediateScale);
				},
				onFinish: () => {
					transform.localScale = targetScale;
					transform.SetPositionAndRotation(targetPos, targetRot);
					onComplete?.Invoke();
				});
		}

		public static Tweener AnchorTo(RectTransform transform, Vector2 targetPosition, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector2> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(transform.anchoredPosition, targetPosition, duration, delay, pos => {
				transform.anchoredPosition = pos;
				onUpdate?.Invoke(pos);
			}, () => {
				transform.anchoredPosition = targetPosition;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener ColorTo(Material material, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(material.color, targetColor, duration, delay, color => {
				material.color = color;
				onUpdate?.Invoke(color);
			}, () => {
				material.color = targetColor;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener ColorTo(Material material, Color targetColor, string colorProperty, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(material.color, targetColor, duration, delay, color => {
				material.SetColor(colorProperty, color);
				onUpdate?.Invoke(color);
			}, () => {
				material.SetColor(colorProperty, targetColor);
				onComplete?.Invoke();
			}, easeType);

		public static Tweener ColorTo(MaskableGraphic graphic, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(graphic.color, targetColor, duration, delay, color => {
				graphic.color = color;
				onUpdate?.Invoke(color);
			}, () => {
				graphic.color = targetColor;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener ColorTo(SpriteRenderer spriteRenderer, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(spriteRenderer.color, targetColor, duration, delay, color => {
				spriteRenderer.color = color;
				onUpdate?.Invoke(color);
			}, () => {
				spriteRenderer.color = targetColor;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener RGBTo(Material material, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(material.color, targetColor.Alpha(material.color.a), duration, delay, color => {
				material.color = color;
				onUpdate?.Invoke(color);
			}, () => {
				material.color = targetColor.Alpha(material.color.a);
				onComplete?.Invoke();
			}, easeType);

		public static Tweener RGBTo(MaskableGraphic graphic, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(graphic.color, targetColor.Alpha(graphic.color.a), duration, delay, color => {
				graphic.color = color;
				onUpdate?.Invoke(color);
			}, () => {
				graphic.color = targetColor.Alpha(graphic.color.a);
				onComplete?.Invoke();
			}, easeType);

		public static Tweener RGBTo(SpriteRenderer spriteRenderer, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(spriteRenderer.color, targetColor.Alpha(spriteRenderer.color.a), duration, delay, color => {
				spriteRenderer.color = color;
				onUpdate?.Invoke(color);
			}, () => {
				spriteRenderer.color = targetColor.Alpha(spriteRenderer.color.a);
				onComplete?.Invoke();
			}, easeType);

		public static Tweener AlphaTo(Material material, float targetAlpha, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<float> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(material.color.a, targetAlpha, duration, delay, a => {
				material.color = material.color.Alpha(a);
				onUpdate?.Invoke(a);
			}, () => {
				material.color = material.color.Alpha(targetAlpha);
				onComplete?.Invoke();
			}, easeType);

		public static Tweener AlphaTo(MaskableGraphic graphic, float targetAlpha, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<float> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(graphic.color.a, targetAlpha, duration, delay, a => {
				graphic.color = graphic.color.Alpha(a);
				onUpdate?.Invoke(a);
			}, () => {
				graphic.color = graphic.color.Alpha(targetAlpha);
				onComplete?.Invoke();
			}, easeType);

		public static Tweener AlphaTo(CanvasGroup canvasGroup, float targetAlpha, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<float> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(canvasGroup.alpha, targetAlpha, duration, delay, a => {
				canvasGroup.alpha = a;
				onUpdate?.Invoke(a);
			}, () => {
				canvasGroup.alpha = targetAlpha;
				onComplete?.Invoke();
			}, easeType);

		public static Tweener AlphaTo(SpriteRenderer spriteRenderer, float targetAlpha, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<float> onUpdate = null, Action onComplete = null)
			=> TweenFromTo(spriteRenderer.color.a, targetAlpha, duration, delay, a => {
				spriteRenderer.color = spriteRenderer.color.Alpha(a);
				onUpdate?.Invoke(a);
			}, () => {
				spriteRenderer.color = spriteRenderer.color.Alpha(targetAlpha);
				onComplete?.Invoke();
			}, easeType);
	}

	public static class TweenExtensions {
		public static Tweener MoveTo(this Transform transform, Vector3 targetLocalPosition, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector3> onUpdate = null, Action onComplete = null)
			=> Tweener.MoveTo(transform, targetLocalPosition, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener RotateTo(this Transform transform, Quaternion targetLocalRotation, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Quaternion> onUpdate = null, Action onComplete = null)
			=> Tweener.RotateTo(transform, targetLocalRotation, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener MoveToWorld(this Transform transform, Vector3 targetWorldPosition, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector3> onUpdate = null, Action onComplete = null)
			=> Tweener.MoveToWorld(transform, targetWorldPosition, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener RotateToWorld(this Transform transform, Quaternion targetWorldRotation, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Quaternion> onUpdate = null, Action onComplete = null)
			=> Tweener.RotateToWorld(transform, targetWorldRotation, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener ScaleTo(this Transform transform, Vector3 targetLocalScale, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector3> onUpdate = null, Action onComplete = null)
			=> Tweener.ScaleTo(transform, targetLocalScale, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener SizeTo(this RectTransform transform, Vector2 targetSize, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector2> onUpdate = null, Action onComplete = null)
			=> Tweener.SizeTo(transform, targetSize, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener TransformTo(this Transform transform, Transform targetTransform, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector3, Quaternion, Vector3> onUpdate = null, Action onComplete = null)
			=> Tweener.TransformTo(transform, targetTransform, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener AnchorTo(this RectTransform transform, Vector2 targetPosition, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Vector2> onUpdate = null, Action onComplete = null)
			=> Tweener.AnchorTo(transform, targetPosition, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener ColorTo(this Material material, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> Tweener.ColorTo(material, targetColor, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener ColorTo(this Material material, Color targetColor, string colorProperty, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> Tweener.ColorTo(material, targetColor, colorProperty, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener ColorTo(this MaskableGraphic graphic, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> Tweener.ColorTo(graphic, targetColor, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener ColorTo(this SpriteRenderer spriteRenderer, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> Tweener.ColorTo(spriteRenderer, targetColor, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener RGBTo(this Material material, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> Tweener.RGBTo(material, targetColor, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener RGBTo(this MaskableGraphic graphic, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> Tweener.RGBTo(graphic, targetColor, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener RGBTo(this SpriteRenderer spriteRenderer, Color targetColor, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<Color> onUpdate = null, Action onComplete = null)
			=> Tweener.RGBTo(spriteRenderer, targetColor, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener AlphaTo(this Material material, float targetAlpha, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<float> onUpdate = null, Action onComplete = null)
			=> Tweener.AlphaTo(material, targetAlpha, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener AlphaTo(this MaskableGraphic graphic, float targetAlpha, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<float> onUpdate = null, Action onComplete = null)
			=> Tweener.AlphaTo(graphic, targetAlpha, duration, delay, easeType, onUpdate, onComplete);

		public static Tweener AlphaTo(this CanvasGroup canvasGroup, float targetAlpha, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<float> onUpdate = null, Action onComplete = null)
			=> Tweener.AlphaTo(canvasGroup, targetAlpha, duration, delay, easeType, onUpdate, onComplete);
		
		public static Tweener AlphaTo(this SpriteRenderer spriteRenderer, float targetAlpha, float duration, float delay = 0, EaseType easeType = EaseType.Linear, Action<float> onUpdate = null, Action onComplete = null)
			=> Tweener.AlphaTo(spriteRenderer, targetAlpha, duration, delay, easeType, onUpdate, onComplete);
	}
}
