﻿// Credit jack.sydorenko, firagon
// Sourced from - http://forum.unity3d.com/threads/new-ui-and-line-drawing.253772/
// Updated/Refactored from - http://forum.unity3d.com/threads/new-ui-and-line-drawing.253772/#post-2528050

using System.Collections.Generic;

namespace UnityEngine.UI.Extensions {
	[AddComponentMenu("UI/Extensions/Primitives/UILineRenderer")]
	[RequireComponent(typeof(RectTransform))]
	public class UILineRenderer : UIPrimitiveBase {
		private enum SegmentType {
			Start,
			Middle,
			End,
			Full,
		}

		public enum JoinType {
			Bevel,
			Miter
		}

		public enum BezierType {
			None,
			Quick,
			Basic,
			Improved,
			Catenary,
		}

		private const float MIN_MITER_JOIN = 15 * Mathf.Deg2Rad;

		// A bevel 'nice' join displaces the vertices of the line segment instead of simply rendering a
		// quad to connect the endpoints. This improves the look of textured and transparent lines, since
		// there is no overlapping.
		private const float MIN_BEVEL_NICE_JOIN = 30 * Mathf.Deg2Rad;

		private static Vector2 _uvTopLeft, _uvBottomLeft, _uvTopCenterLeft, _uvTopCenterRight, _uvBottomCenterLeft, _uvBottomCenterRight, _uvTopRight, _uvBottomRight;
		private static Vector2[] _startUvs, _middleUvs, _endUvs, _fullUvs;

		[SerializeField, Tooltip("Points to draw lines between\n Can be improved using the Resolution Option")]
		private Vector2[] _mPoints;

		[SerializeField, Tooltip("Thickness of the line")]
		private float _lineThickness = 2;
		[SerializeField, Tooltip("Use the relative bounds of the Rect Transform (0,0 -> 0,1) or screen space coordinates")]
		private bool _relativeSize;
		[SerializeField, Tooltip("Do the points identify a single line or split pairs of lines")]
		private bool _lineList;
		[SerializeField, Tooltip("Add end caps to each line\nMultiple caps when used with Line List")]
		private bool _lineCaps;

		public float lineThickness {
			get { return _lineThickness; }
			set { _lineThickness = value; SetAllDirty(); }
		}

		public bool relativeSize {
			get { return _relativeSize; }
			set { _relativeSize = value; SetAllDirty(); }
		}

		public bool lineList {
			get { return _lineList; }
			set { _lineList = value; SetAllDirty(); }
		}

		public bool lineCaps {
			get { return _lineCaps; }
			set { _lineCaps = value; SetAllDirty(); }
		}

		[Tooltip("The type of Join used between lines, Square/Mitre or Curved/Bevel")]
		public JoinType lineJoins = JoinType.Bevel;

		[HideInInspector]
		public bool drivenExternally;


		/// <summary>
		/// Points to be drawn in the line.
		/// </summary>
		public Vector2[] points {
			get => _mPoints;

			set {
				if (_mPoints == value)
					return;
				_mPoints = value;
				SetAllDirty();
			}
		}

		private void PopulateMesh(VertexHelper vh, Vector2[] pointsToDraw) {

			if (ImproveResolution != ResolutionMode.None) {
				pointsToDraw = IncreaseResolution(pointsToDraw);
			}

			// scale based on the size of the rect or use absolute, this is switchable
			float sizeX = !_relativeSize ? 1 : rectTransform.rect.width;
			float sizeY = !_relativeSize ? 1 : rectTransform.rect.height;
			float offsetX = -rectTransform.pivot.x * sizeX;
			float offsetY = -rectTransform.pivot.y * sizeY;

			// Generate the quads that make up the wide line
			List<UIVertex[]> segments = new List<UIVertex[]>();
			if (_lineList) {
				for (int i = 1; i < pointsToDraw.Length; i += 2) {
					Vector2 start = pointsToDraw[i - 1];
					Vector2 end = pointsToDraw[i];
					start = new Vector2(start.x * sizeX + offsetX, start.y * sizeY + offsetY);
					end = new Vector2(end.x * sizeX + offsetX, end.y * sizeY + offsetY);

					if (_lineCaps) {
						segments.Add(CreateLineCap(start, end, SegmentType.Start));
					}

					segments.Add(CreateLineSegment(start, end, SegmentType.Middle, segments.Count > 1 ? segments[segments.Count - 2] : null));

					if (_lineCaps) {
						segments.Add(CreateLineCap(start, end, SegmentType.End));
					}
				}
			} else {
				for (int i = 1; i < pointsToDraw.Length; i++) {
					Vector2 start = pointsToDraw[i - 1];
					Vector2 end = pointsToDraw[i];
					start = new Vector2(start.x * sizeX + offsetX, start.y * sizeY + offsetY);
					end = new Vector2(end.x * sizeX + offsetX, end.y * sizeY + offsetY);

					if (_lineCaps && i == 1) {
						segments.Add(CreateLineCap(start, end, SegmentType.Start));
					}

					segments.Add(CreateLineSegment(start, end, SegmentType.Middle));

					if (_lineCaps && i == pointsToDraw.Length - 1) {
						segments.Add(CreateLineCap(start, end, SegmentType.End));
					}
				}
			}

			// Add the line segments to the vertex helper, creating any joins as needed
			for (int i = 0; i < segments.Count; i++) {
				if (!_lineList && i < segments.Count - 1) {
					Vector3 vec1 = segments[i][1].position - segments[i][2].position;
					Vector3 vec2 = segments[i + 1][2].position - segments[i + 1][1].position;
					float angle = Vector2.Angle(vec1, vec2) * Mathf.Deg2Rad;

					// Positive sign means the line is turning in a 'clockwise' direction
					float sign = Mathf.Sign(Vector3.Cross(vec1.normalized, vec2.normalized).z);

					// Calculate the miter point
					float miterDistance = _lineThickness / (2 * Mathf.Tan(angle / 2));
					Vector3 miterPointA = segments[i][2].position - miterDistance * sign * vec1.normalized;
					Vector3 miterPointB = segments[i][3].position + miterDistance * sign * vec1.normalized;

					JoinType joinType = lineJoins;
					if (joinType == JoinType.Miter) {
						// Make sure we can make a miter join without too many artifacts.
						if (miterDistance < vec1.magnitude / 2 && miterDistance < vec2.magnitude / 2 && angle > MIN_MITER_JOIN) {
							segments[i][2].position = miterPointA;
							segments[i][3].position = miterPointB;
							segments[i + 1][0].position = miterPointB;
							segments[i + 1][1].position = miterPointA;
						} else {
							joinType = JoinType.Bevel;
						}
					}

					if (joinType == JoinType.Bevel) {
						if (miterDistance < vec1.magnitude / 2 && miterDistance < vec2.magnitude / 2 && angle > MIN_BEVEL_NICE_JOIN) {
							if (sign < 0) {
								segments[i][2].position = miterPointA;
								segments[i + 1][1].position = miterPointA;
							} else {
								segments[i][3].position = miterPointB;
								segments[i + 1][0].position = miterPointB;
							}
						}

						UIVertex[] join = new UIVertex[] { segments[i][2], segments[i][3], segments[i + 1][0], segments[i + 1][1] };
						vh.AddUIVertexQuad(join);
					}
				}

				vh.AddUIVertexQuad(segments[i]);
			}
			if (vh.currentVertCount > 64000) {
				Utilities.Debug.LogError("Max Verticies size is 64000, current mesh vertcies count is [" + vh.currentVertCount + "] - Cannot Draw");
				vh.Clear();
			}

		}

		protected override void OnPopulateMesh(VertexHelper vh) {
			if (_mPoints != null && _mPoints.Length > 0) {
				GeneratedUVs();
				vh.Clear();

				PopulateMesh(vh, _mPoints);
			}


		}

		private UIVertex[] CreateLineCap(Vector2 start, Vector2 end, SegmentType type) {
			if (type == SegmentType.Start) {
				Vector2 capStart = start - ((end - start).normalized * _lineThickness / 2);
				return CreateLineSegment(capStart, start, SegmentType.Start);
			} else if (type == SegmentType.End) {
				Vector2 capEnd = end + ((end - start).normalized * _lineThickness / 2);
				return CreateLineSegment(end, capEnd, SegmentType.End);
			}

			Utilities.Debug.LogError("Bad SegmentType passed in to CreateLineCap. Must be SegmentType.Start or SegmentType.End");
			return null;
		}

		private UIVertex[] CreateLineSegment(Vector2 start, Vector2 end, SegmentType type, UIVertex[] previousVert = null) {
			Vector2 offset = new Vector2((start.y - end.y), end.x - start.x).normalized * _lineThickness / 2;

			Vector2 v1;
			Vector2 v2;
			if (previousVert != null) {
				v1 = new Vector2(previousVert[3].position.x, previousVert[3].position.y);
				v2 = new Vector2(previousVert[2].position.x, previousVert[2].position.y);
			} else {
				v1 = start - offset;
				v2 = start + offset;
			}

			Vector2 v3 = end + offset;
			Vector2 v4 = end - offset;
			//Return the VDO with the correct uvs
			switch (type) {
				case SegmentType.Start:
					return SetVbo(new[] { v1, v2, v3, v4 }, _startUvs);
				case SegmentType.End:
					return SetVbo(new[] { v1, v2, v3, v4 }, _endUvs);
				case SegmentType.Full:
					return SetVbo(new[] { v1, v2, v3, v4 }, _fullUvs);
				default:
					return SetVbo(new[] { v1, v2, v3, v4 }, _middleUvs);
			}
		}

		protected override void GeneratedUVs() {
			if (activeSprite != null) {
				Vector4 outer = Sprites.DataUtility.GetOuterUV(activeSprite);
				Vector4 inner = Sprites.DataUtility.GetInnerUV(activeSprite);
				_uvTopLeft = new Vector2(outer.x, outer.y);
				_uvBottomLeft = new Vector2(outer.x, outer.w);
				_uvTopCenterLeft = new Vector2(inner.x, inner.y);
				_uvTopCenterRight = new Vector2(inner.z, inner.y);
				_uvBottomCenterLeft = new Vector2(inner.x, inner.w);
				_uvBottomCenterRight = new Vector2(inner.z, inner.w);
				_uvTopRight = new Vector2(outer.z, outer.y);
				_uvBottomRight = new Vector2(outer.z, outer.w);
			} else {
				_uvTopLeft = Vector2.zero;
				_uvBottomLeft = new Vector2(0, 1);
				_uvTopCenterLeft = new Vector2(0.5f, 0);
				_uvTopCenterRight = new Vector2(0.5f, 0);
				_uvBottomCenterLeft = new Vector2(0.5f, 1);
				_uvBottomCenterRight = new Vector2(0.5f, 1);
				_uvTopRight = new Vector2(1, 0);
				_uvBottomRight = Vector2.one;
			}


			_startUvs = new[] { _uvTopLeft, _uvBottomLeft, _uvBottomCenterLeft, _uvTopCenterLeft };
			_middleUvs = new[] { _uvTopCenterLeft, _uvBottomCenterLeft, _uvBottomCenterRight, _uvTopCenterRight };
			_endUvs = new[] { _uvTopCenterRight, _uvBottomCenterRight, _uvBottomRight, _uvTopRight };
			_fullUvs = new[] { _uvTopLeft, _uvBottomLeft, _uvBottomRight, _uvTopRight };
		}

		protected override void ResolutionToNativeSize(float distance) {
			if (UseNativeSize) {
				m_Resolution = distance / (activeSprite.rect.width / pixelsPerUnit);
				_lineThickness = activeSprite.rect.height / pixelsPerUnit;
			}
		}
	}
}