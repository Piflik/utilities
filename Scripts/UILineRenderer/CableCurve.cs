﻿// Credit Farfarer
// Sourced from - https://gist.github.com/Farfarer/a765cd07920d48a8713a0c1924db6d70
// Updated for UI / 2D - SimonDarksideJ

using System;
// ReSharper disable UnusedMember.Global

namespace UnityEngine.UI.Extensions {
	[Serializable]
	public class CableCurve {
		[SerializeField] private Vector2 _start = default;
		[SerializeField] private Vector2 _end = default;
		[SerializeField] private float _slack = default;
		[SerializeField] private int _steps = default;
		[SerializeField] private bool _regen = default;

		private static readonly Vector2[] _emptyCurve = { new Vector2(0.0f, 0.0f), new Vector2(0.0f, 0.0f) };
		[SerializeField] private Vector2[] _points = default;

		public bool regenPoints {
			get => _regen;
			set => _regen = value;
		}

		public Vector2 start {
			get => _start;
			set {
				if (value != _start)
					_regen = true;
				_start = value;
			}
		}

		public Vector2 end {
			get => _end;
			set {
				if (value != _end)
					_regen = true;
				_end = value;
			}
		}
		public float slack {
			get => _slack;
			set {
				if (!value.Approximates(_slack))
					_regen = true;
				_slack = Mathf.Max(0.0f, value);
			}
		}
		public int steps {
			get => _steps;
			set {
				if (value != _steps)
					_regen = true;
				_steps = Mathf.Max(2, value);
			}
		}

		public Vector2 midPoint {
			get {
				Vector2 mid = Vector2.zero;
				if (_steps == 2) {
					return (_points[0] + _points[1]) * 0.5f;
				} else if (_steps > 2) {
					int m = _steps / 2;
					if (_steps % 2 == 0) {
						mid = (_points[m] + _points[m + 1]) * 0.5f;
					} else {
						mid = _points[m];
					}
				}
				return mid;
			}
		}

		public CableCurve() {
			_points = _emptyCurve;
			_start = Vector2.up;
			_end = Vector2.up + Vector2.right;
			_slack = 0.5f;
			_steps = 20;
			_regen = true;
		}

		public CableCurve(Vector2[] inputPoints) {
			_points = inputPoints;
			_start = inputPoints[0];
			_end = inputPoints[1];
			_slack = 0.5f;
			_steps = 20;
			_regen = true;
		}

		public CableCurve(CableCurve v) {
			_points = v.Points();
			_start = v.start;
			_end = v.end;
			_slack = v.slack;
			_steps = v.steps;
			_regen = v.regenPoints;
		}

		public Vector2[] Points() {
			if (!_regen)
				return _points;

			if (_steps < 2)
				return _emptyCurve;

			float lineDist = Vector2.Distance(_end, _start);
			float lineDistH = Vector2.Distance(new Vector2(_end.x, _start.y), _start);
			float l = lineDist + Mathf.Max(0.0001f, _slack);
			const float r = 0.0f;
			float s = _start.y;
			float u = lineDistH;
			float v = end.y;

			if (u.Approximates(r))
				return _emptyCurve;

			float ztarget = Mathf.Sqrt(Mathf.Pow(l, 2.0f) - Mathf.Pow(v - s, 2.0f)) / (u - r);

			const int loops = 30;
			int iterationCount = 0;
			const int maxIterations = loops * 10; // For safety.
			bool found = false;

			float z = 0.0f;
			float zstep = 100.0f;
			for (int i = 0; i < loops; i++) {
				for (int j = 0; j < 10; j++) {
					iterationCount++;
					float ztest = z + zstep;
					float ztesttarget = (float)Math.Sinh(ztest) / ztest;

					if (float.IsInfinity(ztesttarget))
						continue;

					if (ztesttarget.Approximates(ztarget)) {
						found = true;
						z = ztest;
						break;
					}

					if (ztesttarget > ztarget) {
						break;
					}

					z = ztest;

					if (iterationCount > maxIterations) {
						found = true;
						break;
					}
				}

				if (found)
					break;

				zstep *= 0.1f;
			}

			float a = (u - r) / 2.0f / z;
			float p = (r + u - a * Mathf.Log((l + v - s) / (l - v + s))) / 2.0f;
			float q = (v + s - l * (float)Math.Cosh(z) / (float)Math.Sinh(z)) / 2.0f;

			_points = new Vector2[_steps];
			float stepsf = _steps - 1;
			for (int i = 0; i < _steps; i++) {
				float stepf = i / stepsf;
				Vector2 pos = Vector2.zero;
				pos.x = Mathf.Lerp(start.x, end.x, stepf);
				pos.y = a * (float)Math.Cosh((stepf * lineDistH - p) / a) + q;
				_points[i] = pos;
			}

			_regen = false;
			return _points;
		}
	}
}