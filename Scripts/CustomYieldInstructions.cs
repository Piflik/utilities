using System;
using UnityEngine;

namespace Utilities {
	public class WaitForFrames : CustomYieldInstruction {
		private readonly int _targetFrameCount;

		public WaitForFrames(int numberOfFrames) {
			_targetFrameCount = Time.frameCount + numberOfFrames;
		}

		public override bool keepWaiting => Time.frameCount < _targetFrameCount;
	}

	class WaitWhile : CustomYieldInstruction {
		readonly Func<bool> _predicate;
		public override bool keepWaiting => _predicate();
		public WaitWhile(Func<bool> predicate) { _predicate = predicate; }
	}

	class WaitUntil : CustomYieldInstruction {
		readonly Func<bool> _predicate;
		public override bool keepWaiting => !_predicate();
		public WaitUntil(Func<bool> predicate) { _predicate = predicate; }
	}
}