﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedMember.Local

namespace Utilities {
	public class DisplayConfiguration : MonoBehaviour {
		private enum DisplayID {
			Display1,
			Display2,
			Display3,
			Display4,
			Display5,
			Display6,
			Display7,
			Display8,
		}

		[Serializable]
		private struct Configuration {
			[SerializeField, FormerlySerializedAs("display")] DisplayID _display;
			[SerializeField, FormerlySerializedAs("resolution")] Vector2Int _resolution;


			public Vector2Int resolution => _resolution;
			public int displayIndex => (int)_display;
		}

		[SerializeField] private Configuration[] _configurations = default;

		private void Awake() {
			if (_configurations == null) return;

			Display[] displays = Display.displays;
			foreach (Configuration configuration in _configurations) {
				int index = configuration.displayIndex;
				if (index < 0 || index >= displays.Length) continue;
				Display display = displays[index];
				if (!display.active)
					display.Activate();
				display.SetRenderingResolution(configuration.resolution.x, configuration.resolution.y);
			}
		}
	}
}