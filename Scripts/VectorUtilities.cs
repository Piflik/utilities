﻿using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static partial class VectorUtilities {

		/// <summary>
		/// Calculate a 2D directional Vector in the XZ-Plane fron an angle in radians
		/// </summary>
		public static Vector3 DirectionFromAngle(float radians) {
			return new Vector3(Mathf.Sin(radians), 0, Mathf.Cos(radians));
		}

		/// <summary>
		/// Compose a vector from a set of vectors that uses the minimum value in each dimension
		/// </summary>
		public static Vector2 MinComp(params Vector2[] vecs) {

			float xMin = Mathf.Infinity, yMin = Mathf.Infinity;

			foreach (Vector2 vec in vecs) {
				xMin = Mathf.Min(xMin, vec.x);
				yMin = Mathf.Min(yMin, vec.y);
			}

			return new Vector2(xMin, yMin);
		}

		/// <summary>
		/// Compose a vector from a set of vectors that uses the maximum value in each dimension
		/// </summary>
		public static Vector2 MaxComp(params Vector2[] vecs) {

			float xMax = -Mathf.Infinity, yMax = -Mathf.Infinity;

			foreach (Vector2 vec in vecs) {
				yMax = Mathf.Max(yMax, vec.y);
				xMax = Mathf.Max(xMax, vec.x);
			}

			return new Vector2(xMax, yMax);
		}


		/// <summary>
		/// Compose two vectors from a set of vectors that use the minimum and maximum value respectively in each dimension
		/// </summary>
		public static void MinMaxComp(out Vector2 min, out Vector2 max, params Vector2[] vecs) {

			min.x = Mathf.Infinity;
			min.y = Mathf.Infinity;
			max.x = -Mathf.Infinity;
			max.y = -Mathf.Infinity;

			foreach (Vector2 vec in vecs) {
				min.x = Mathf.Min(min.x, vec.x);
				min.y = Mathf.Min(min.y, vec.y);
				max.x = Mathf.Max(max.x, vec.x);
				max.y = Mathf.Max(max.y, vec.y);
			}
		}

		/// <summary>
		/// Dot product for Vector2Int
		/// </summary>
		public static float Dot(Vector2Int a, Vector2Int b) {
			return a.x * b.x + a.y * b.y;
		}

		/// <summary>
		/// Dot product for Vector3Int
		/// </summary>
		public static float Dot(Vector3Int a, Vector3Int b) {
			return a.x * b.x + a.y * b.y + a.z * b.z;
		}

		/// <summary>
		/// Cross product for Vector3Int
		/// </summary>
		public static Vector3Int Cross(Vector3Int a, Vector3Int b) {
			return new Vector3Int(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
		}

		/// <summary>
		/// Calculate the angle between two Vector2Int
		/// </summary>
		public static float Angle(Vector2Int from, Vector2Int to) {
			return Mathf.Acos(Mathf.Clamp(Vector2.Dot(from.Normalized(), to.Normalized()), -1f, 1f)) * Mathf.Rad2Deg;
		}

		/// <summary>
		/// Calculate the angle between two Vector3Int
		/// </summary>
		public static float Angle(Vector3Int from, Vector3Int to) {
			return Mathf.Acos(Mathf.Clamp(Vector3.Dot(from.Normalized(), to.Normalized()), -1f, 1f)) * Mathf.Rad2Deg;
		}

		/// <summary>
		/// Compose a Vector2Int from 2 Vector2Ints that uses the minimum value in each dimension
		/// </summary>
		public static Vector2Int Min(Vector2Int a, Vector2Int b) {
			return new Vector2Int(Mathf.Min(a.x, b.x), Mathf.Min(a.y, b.y));
		}

		/// <summary>
		/// Compose a Vector2Int from 2 Vector2Ints that uses the maximum value in each dimension
		/// </summary>
		public static Vector2Int Max(Vector2Int a, Vector2Int b) {
			return new Vector2Int(Mathf.Max(a.x, b.x), Mathf.Max(a.y, b.y));
		}

		/// <summary>
		/// Compose a Vector3Int from 2 Vector3Ints that uses the minimum value in each dimension
		/// </summary>
		public static Vector3Int Min(Vector3Int a, Vector3Int b) {
			return new Vector3Int(Mathf.Min(a.x, b.x), Mathf.Min(a.y, b.y), Mathf.Min(a.z, b.z));
		}

		/// <summary>
		/// Compose a Vector3Int from 2 Vector3Ints that uses the maximum value in each dimension
		/// </summary>
		public static Vector3Int Max(Vector3Int a, Vector3Int b) {
			return new Vector3Int(Mathf.Max(a.x, b.x), Mathf.Max(a.y, b.y), Mathf.Max(a.z, b.z));
		}

		public static Vector2 RandomPointInAnnulus(float minRadius, float maxRadius) {
			float angle = Random.Range(-Mathf.PI, Mathf.PI);
			float radius = Random.Range(minRadius, maxRadius);
			Vector2 direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
			return direction * radius;
		}

		public static Vector3 RandomPointInVolume(Vector3 min, Vector3 max) {
			return new Vector3(
				Random.Range(min.x, max.x),
				Random.Range(min.y, max.y),
				Random.Range(min.z, max.z)
			);
		}

		public static Vector2 RandomPointInRect(Rect rect) {
			return new Vector2(
				rect.x + Random.Range(-rect.width * 0.5f, rect.width * 0.5f),
				rect.y + Random.Range(-rect.height * 0.5f, rect.height * 0.5f));
		}
	}
}
