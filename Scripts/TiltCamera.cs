﻿using UnityEngine;
using System.Linq;

namespace Utilities {
	/// <summary>
	/// tilt projection matrix of camera; requires tilted Graphics Raycaster for UI input to work correctly
	/// </summary>
	[RequireComponent(typeof(Camera))]
	public class TiltCamera : MonoBehaviour {

		public float tiltAmount;

		public static float amount;

		public const float BACKGROUND_DISTANCE = 5;

		// Use this for initialization
		void Start() {
			amount = tiltAmount;

			Canvas[] canvases = FindObjectsOfType<Canvas>();
			Camera cam = GetComponent<Camera>();

			cam.ResetProjectionMatrix();

			if (canvases != null) {
				foreach (Canvas c in canvases.Where(c => c.renderMode == RenderMode.ScreenSpaceCamera)) {
					c.worldCamera = cam;
					c.planeDistance = BACKGROUND_DISTANCE;
				}
			}

			cam.farClipPlane = BACKGROUND_DISTANCE + 1;


			cam.projectionMatrix *= new Matrix4x4() {
				m00 = 1, m01 = 0, m02 = tiltAmount, m03 = BACKGROUND_DISTANCE * tiltAmount,
				m10 = 0, m11 = 1, m12 = tiltAmount, m13 = BACKGROUND_DISTANCE * tiltAmount,
				m20 = 0, m21 = 0, m22 = 1, m23 = 0,
				m30 = 0, m31 = 0, m32 = 0, m33 = 1,
			};
		}

#if UNITY_EDITOR

		void OnValidate() {
			Start();
		}

#endif
	}
}