﻿using System;
using System.Collections.Generic;

namespace Utilities {
	public class GeneralPropertyComparer<T, TKey> : IEqualityComparer<T> {
		private Func<T, TKey> _expr { get; }
		public GeneralPropertyComparer(Func<T, TKey> expr) {
			_expr = expr;
		}
		public bool Equals(T left, T right) {
			TKey leftProp = _expr.Invoke(left);
			TKey rightProp = _expr.Invoke(right);
			if (leftProp == null && rightProp == null)
				return true;
			if (leftProp == null ^ rightProp == null)
				return false;
			return leftProp.Equals(rightProp);
		}
		public int GetHashCode(T obj) {
			TKey prop = _expr.Invoke(obj);
			return prop == null ? 0 : prop.GetHashCode();
		}
	}
}