﻿using UnityEngine;
using UnityEngine.UI;

namespace Utilities {
	public class FPSCounter : MonoBehaviour {

		private const float DESIRED_FPS = 60;
		private const float MINIMAL_FPS = 30;
	
		// Use this for initialization
		private void Start() {
			_text = GetComponent<Text>();
		}

		private Text _text;

		// Attach this to a GUIText to make a frames/second indicator.
		//
		// It calculates frames/second over each updateInterval,
		// so the display does not keep changing wildly.
		//
		// It is also fairly accurate at very low FPS counts (<10).
		// We do this not by simply counting frames per interval, but
		// by accumulating FPS for each frame. This way we end up with
		// correct overall FPS even if the interval renders something like
		// 5.5 frames.

		public float updateInterval = 0.5F;

		private float _accumulatedFrames; // FPS accumulated over the interval
		private int _frames; // Frames drawn over the interval
		private float _lastTime; // Left time for current interval


		private void Update() {
			_accumulatedFrames += Time.timeScale / Time.deltaTime;
			_frames++;

			// Interval ended - update GUI text and start new interval
			if (Time.time - _lastTime < updateInterval) return;

			_lastTime = Time.time;

			// display two fractional digits (f2 format)
			float fps = _accumulatedFrames / _frames;
			string format = System.String.Format("{0:F2} FPS", fps);
			_text.text = format;

			if (fps < DESIRED_FPS)
				_text.material.color = Color.yellow;
			else if (fps < MINIMAL_FPS)
				_text.material.color = Color.red;
			else
				_text.material.color = Color.green;
			//	DebugConsole.Log(format,level);
			_accumulatedFrames = 0.0F;
			_frames = 0;
		}
	}
}