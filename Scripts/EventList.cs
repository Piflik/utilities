﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Utilities {
	public enum ListChangeType {
		Add,
		Set,
		Remove,
		RemoveAt,
		Insert,
		Cleared,
		Changed
	}

	[Serializable]
	public class EventList<T> : IList<T>, IEquatable<EventList<T>> where T : IComparable {
		
		public delegate void ListChangedEventDelegate(EventList<T> sender, ListChangeType type, T newValue = default, int index = default);

		private readonly List<T> _list;

		public event ListChangedEventDelegate listChanged;

		public EventList() {
			_list = new List<T>();
		}

		public EventList(List<T> initialVal) {
			items = initialVal;
		}

		public void Add(T item) {
			_list.Add(item);
			listChanged?.Invoke(this, ListChangeType.Add, item, _list.Count - 1);
		}

		public List<T> items {
			get => _list;
			set {
				_list.Clear();
				_list.AddRange(value);
				listChanged?.Invoke(this, ListChangeType.Set);
			}
		}

		public int IndexOf(T item) => _list.IndexOf(item);

		public void Insert(int index, T item) {
			_list.Insert(index, item);
			listChanged?.Invoke(this, ListChangeType.Insert, item, index);

		}

		public void RemoveAt(int index) {
			_list.RemoveAt(index);
			listChanged?.Invoke(this, ListChangeType.RemoveAt, default, index);
		}

		public void Clear() {
			_list.Clear();
			listChanged?.Invoke(this, ListChangeType.Cleared);
		}

		public bool Contains(T item) => _list.Contains(item);
		public void CopyTo(T[] array, int arrayIndex) => _list.CopyTo(array, arrayIndex);
		public IEnumerator<T> GetEnumerator() => _list.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
		public bool IsReadOnly => ((IList<T>)_list).IsReadOnly;

		public bool Remove(T item) {
			bool b = _list.Remove(item);
			listChanged?.Invoke(this, ListChangeType.Remove, item);
			return b;
		}

		public int Count => _list.Count;

		public T this[int index] {
			get => _list[index];
			set {
				T val = _list[index];
				_list[index] = value;
				if (value.CompareTo(val) == 0) {
					listChanged?.Invoke(this, ListChangeType.Changed, _list[index], index);
				}
			}
		}

		public bool Equals(EventList<T> other) {
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(_list, other._list);
		}

		public override bool Equals(object obj) {
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((EventList<T>) obj);
		}

		public override int GetHashCode() {
			return (_list != null ? _list.GetHashCode() : 0);
		}

		public static bool operator ==(EventList<T> a, EventList<T> b) => a != null && a.Equals(b) || b == null;
		public static bool operator !=(EventList<T> a, EventList<T> b) => a != null && !a.Equals(b) || b != null;
	}
}