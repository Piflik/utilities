﻿using UnityEngine;
using System;

// ReSharper disable UnusedMember.Global

namespace Utilities {
	public class Fade3DObject : MonoBehaviour {
		private Color _objColor;
		private Tweener _tween;
		private MeshRenderer _mr;

		public float changeRenderModeOnAlpha = -1;

		[SerializeField]
		private float _alpha = 1f;

		private float _lastAlpha;
		private float _targetAlpha;

		// Init
		private void Start() {
			_tween = Tweener.Create(0, 0, false, onUpdate: HandleAlphaTween);

			_mr = gameObject.GetComponent<MeshRenderer>();
			if (_mr != null) {
				_objColor = _mr.materials[0].color;
				_alpha = _objColor.a;
			}

			for (int childIndex = 0; childIndex < gameObject.transform.childCount; childIndex++) {
				Transform child = gameObject.transform.GetChild(childIndex);
				child.gameObject.AddComponent<Fade3DObject>();
			}
		}

		public void FadeToAlpha(float a, float dur = 0, EaseType easeType = EaseType.SineIn) {
			if (dur.Approximates(0)) {
				_lastAlpha = _targetAlpha = a;
				SetAlpha(a);
			}

			_tween.Pause();
			_tween.duration = dur;
			_tween.easeType = easeType;
			_lastAlpha = _alpha;
			_targetAlpha = a;
			_tween.Start(reset: true);
		}

		public void SetAlpha(float a) {
			_alpha = a;

			Renderer[] tempMeshRenderer = gameObject.transform.GetComponentsInChildren<Renderer>();

			if (tempMeshRenderer != null) {
				foreach (Renderer rend in tempMeshRenderer) {
					foreach (Material m in rend.materials) {
						try {
							Color tempCol = m.GetColor("_Color");
							tempCol.a = _alpha;
							m.SetColor("_Color", tempCol);

							if (changeRenderModeOnAlpha >= 0) {
								if (_alpha < changeRenderModeOnAlpha) m.SetFloat("_Mode", 2); // 2: Fade, 3: Transparent
								if (_alpha >= changeRenderModeOnAlpha) m.SetFloat("_Mode", 3); // 2: Fade, 3: Transparent
							}
						} catch (Exception e) {
							Utilities.Debug.LogError(e.Message);
						}
					}
				}
			}
		}

		private void HandleAlphaTween(float a) {
			SetAlpha(a * _targetAlpha + (1 - a) * _lastAlpha);
		}
	}
}
