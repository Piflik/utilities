﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public class CircularBuffer<T> : IEnumerable<T> {
		private T[] _buffer;
		private int _stackPointer;

		public CircularBuffer(int capacity) {
			if (capacity < 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), "must be positive");
			_buffer = new T[capacity];
		}

		public int count { get; private set; }

		public int capacity {
			get => _buffer.Length;
			set {
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value), "must be positive");

				if (value == _buffer.Length)
					return;

				T[] buffer = new T[value];
				int c = 0;
				while (count > 0 && c < value)
					buffer[c++] = Pop();

				_buffer = buffer;
				count = c;
				_stackPointer = 0;
			}
		}

		public void Push(T item) {
			bool overwritten = Push(item, out T overwrittenElement);

			if (overwritten && overwrittenElement is IDisposable dispElement) {
				dispElement.Dispose();
			}
		}

		public bool Push(T item, out T overwrittenElement) {
			overwrittenElement = _buffer[_stackPointer];
			_buffer[_stackPointer] = item;

			bool overwritten = count == capacity;

			if (count < capacity) {
				++count;
			}

			_stackPointer = (_stackPointer + 1) % capacity;

			return overwritten;
		}

		public T Pop() {
			if (count == 0)
				throw new InvalidOperationException("queue exhausted");

			_stackPointer = (_stackPointer - 1 + capacity) % capacity;

			T dequeued = _buffer[_stackPointer];
			_buffer[_stackPointer] = default;

			--count;
			return dequeued;
		}

		public T Peek() {
			if (count == 0)
				throw new InvalidOperationException("queue exhausted");

			return _buffer[(_stackPointer - 1 + capacity) % capacity];
		}

		public void Clear() {
			_stackPointer = 0;
			count = 0;

			if (typeof(IDisposable).IsAssignableFrom(typeof(T))) {
				foreach (IDisposable item in _buffer.Where(i => i != null).Select(i => (IDisposable) i)) {
					item.Dispose();
				}
			}
		}

		public T this[int index] {
			get {
				if (index < 0 || index >= count)
					throw new ArgumentOutOfRangeException(nameof(index));

				return _buffer[(_stackPointer - count + index + capacity) % capacity];
			}
			set {
				if (index < 0 || index >= count)
					throw new ArgumentOutOfRangeException(nameof(index));

				_buffer[(_stackPointer - count + index + capacity) % capacity] = value;
			}
		}

		public int IndexOf(T item) {
			for (int i = 0; i < count; ++i)
				if (Equals(item, this[i]))
					return i;
			return -1;
		}

		public void Insert(int index, T item) {
			if (index < 0 || index > count)
				throw new ArgumentOutOfRangeException(nameof(index));

			if (count == index)
				Push(item);
			else if (count < capacity) {
				T last = this[count - 1];
				for (int i = count - 1; i > index; --i)
					this[i] = this[i - 1];
				this[index] = item;
				Push(last);
			} else {
				for (int i = 1; i < index; ++i) {
					this[i - 1] = this[i];
				}

				this[index] = item;
			}
		}

		public void RemoveAt(int index) {
			if (index < 0 || index >= count)
				throw new ArgumentOutOfRangeException(nameof(index));

			for (int i = index; i < count - 1; ++i)
				this[i] = this[i + 1];
			Pop();
		}

		public IEnumerator<T> GetEnumerator() {
			if (count == 0 || capacity == 0)
				yield break;

			for (int i = 0; i < count; ++i)
				yield return this[i];
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
	}
}
