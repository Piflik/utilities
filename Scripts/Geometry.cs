﻿using System;
using System.Collections.Generic;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public static class Geometry {

		private enum PointLineRelation {
			On,
			RightOf,
			LeftOf
		}

		public static bool TryIntersectRay(Ray ray, Vector3 planeCenter, Vector3 planeNormal, out Vector3 intersection) {
			float denom = Vector3.Dot(ray.direction, planeNormal);
			if (Mathf.Abs(denom) > Mathf.Epsilon) {
				float t = Vector3.Dot(planeCenter - ray.origin, planeNormal) / denom;
				if (t >= Mathf.Epsilon) {
					intersection = ray.GetPoint(t);
					return true;
				}
			}

			intersection = default;
			return false;
		}

		public static bool DoSplinesIntersect(Vector2[] splineA, Vector2[] splineB, float onLineThreshold = float.Epsilon) {
			for (int i = 0; i < splineA.Length - 1; i++) {
				LineSegment segA = new LineSegment(splineA[i], splineA[i + 1]);
				for (int j = 0; j < splineB.Length - 1; j++) {
					if (DoLinesIntersect(segA, new LineSegment(splineB[j], splineB[j + 1]), onLineThreshold)) {
						return true;
					}
				}
			}
			return false;
		}

		public static bool DoLinesIntersect(Vector2 lineAStart, Vector2 lineAEnd, Vector2 lineBStart, Vector2 lineBEnd, float onLineThreshold = float.Epsilon) {
			return DoLinesIntersect(new LineSegment(lineAStart, lineAEnd), new LineSegment(lineBStart, lineBEnd), onLineThreshold);
		}

		public static bool DoBoundingBoxesIntersect(Vector2[] a, Vector2[] b) {
			return a[0].x <= b[1].x && a[1].x >= b[0].x && a[0].y <= b[1].y && a[1].y >= b[0].y;
		}

		private static bool DoLinesIntersect(LineSegment a, LineSegment b, float onLineThreshold) {
			return DoBoundingBoxesIntersect(a.boundingBox, b.boundingBox) && TouchesOrCrosses(a, b, onLineThreshold) && TouchesOrCrosses(b, a, onLineThreshold);
		}

		private static float CrossProduct(Vector2 p1, Vector2 p2) {
			return p1.x * p2.y - p1.y * p2.x;
		}

		private static PointLineRelation GetRelation(LineSegment line, Vector2 point, float onLineThreshold) {
			if (onLineThreshold < 0) {
				onLineThreshold = -onLineThreshold;
			}
			// Move the image, so that a.first is on (0|0)
			LineSegment tempLine = new LineSegment(new Vector2(0, 0), new Vector2(line.end.x - line.start.x, line.end.y - line.start.y));
			Vector2 tempPoint = new Vector2(point.x - line.start.x, point.y - line.start.y);
			float r = CrossProduct(tempLine.end, tempPoint);
			if (Math.Abs(r) < onLineThreshold) {
				return PointLineRelation.On;
			}
			if (r < 0) {
				return PointLineRelation.RightOf;
			}
			return PointLineRelation.LeftOf;
		}


		private static bool TouchesOrCrosses(LineSegment a, LineSegment b, float onLineThreshold) {
			PointLineRelation relOfStart = GetRelation(a, b.start, onLineThreshold);
			PointLineRelation relOfEnd = GetRelation(a, b.end, onLineThreshold);
			return relOfStart == PointLineRelation.On || relOfEnd == PointLineRelation.On || relOfStart == PointLineRelation.RightOf ^ relOfEnd == PointLineRelation.RightOf;
		}

		private struct LineSegment {
			public readonly Vector2 start;
			public readonly Vector2 end;

			public readonly Vector2[] boundingBox;

			public LineSegment(Vector2 a, Vector2 b) {
				start = a;
				end = b;

				boundingBox = new[] {
				new Vector2(Mathf.Min(start.x, end.x), Mathf.Min(start.y, end.y)),
				new Vector2(Mathf.Max(start.x, end.x), Mathf.Max(start.y, end.y))
			};
			}

			public override bool Equals(object obj) {
				if (!(obj is LineSegment)) {
					return false;
				}

				LineSegment segment = (LineSegment)obj;
				return start.Equals(segment.start) && end.Equals(segment.end);
			}

			public override int GetHashCode() {
				int hashCode = 405212230;
				hashCode = hashCode * -1521134295 + base.GetHashCode();
				hashCode = hashCode * -1521134295 + EqualityComparer<Vector2>.Default.GetHashCode(start);
				hashCode = hashCode * -1521134295 + EqualityComparer<Vector2>.Default.GetHashCode(end);
				return hashCode;
			}
		}
	}
}
