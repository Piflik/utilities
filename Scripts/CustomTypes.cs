﻿// ReSharper disable UnusedMember.Global
using System;

namespace Utilities {
	public enum DispatchMode {
		NoDispatch,
		Dispatch,
		ForceDispatch,
	}

	public enum Direction {
		Undefined,
		LeftRight,
		RightLeft,
		TopDown,
		BottomUp
	}

	public enum Rotation {
		Clockwise,
		CounterClockwise,
		Auto
	}

	public enum Orientation {
		Undefined,
		Vertical,
		Horizontal
	}

	/// <summary>
	/// Same values as TextAnchor!! + 9 = undefined
	/// </summary>
	public enum Alignment {
		TopLeft = 0,
		Top = 1,
		TopRight = 2,
		Left = 3,
		Center = 4,
		Right = 5,
		BottomLeft = 6,
		Bottom = 7,
		BottomRight = 8,
		None = 9
	}

	public enum EaseType {
		SineIn,
		SineOut,
		SineInOut,
		QuadIn,
		QuadOut,
		QuadInOut,
		CubicIn,
		CubicOut,
		CubicInOut,
		QuartIn,
		QuartOut,
		QuartInOut,
		QuintIn,
		QuintOut,
		QuintInOut,
		Linear,
		OvershootIn,
		OvershootOut,
		OvershootInOut,
		ElasticIn,
		ElasticOut,
		ElasticInOut,
	}

	public enum AdustUV {
		Auto,
		Width,
		Height,
		TargetSize
	}

	public enum UnitLength {
		Cm,
		Inch,
		Px
	}

	public enum DebugType {
		Default,
		Configurator,
		Warning,
		Error,
		Header,
		Fail,
		Success,
		Info,
	}

	[Flags]
	public enum Axis {
		X = 1,
		Y = 2,
		Z = 4,

		XY = X|Y,
		XZ = X|Z,
		YZ = Y|Z,

		XYZ = X|Y|Z,
	}
}