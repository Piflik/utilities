#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN

using System;
using Utilities;
using UnityEngine;

namespace Windows {
	public static partial class Console {
		private class ConsoleInput : IDisposable {

			private readonly CircularBuffer<string> _previousInputs = new CircularBuffer<string>(50);
			private int _inputIndex;
			private string _inputBuffer = string.Empty;

			private readonly Action<string> _onInputText;
			private string _inputString = "";

			public ConsoleInput(Action<string> onInputText) {
				UnityClock.instance.onUpdate += Update;
				_onInputText = onInputText;
			}

			private void ClearLine() {
				System.Console.CursorLeft = 0;
				System.Console.Write(new string(' ', System.Console.BufferWidth));
				System.Console.CursorTop--;
				System.Console.CursorLeft = 0;
			}

			private void RedrawInputLine() {
				if (System.Console.CursorLeft > 0)
					ClearLine();

				System.Console.ForegroundColor = ConsoleColor.Green;
				System.Console.Write($"> {_inputString}");
			}

			private void OnBackspace() {
				if (_inputString.Length < 1) return;

				_inputString = _inputString.Substring(0, _inputString.Length - 1);
				RedrawInputLine();
			}

			private void OnEscape() {
				ClearLine();
				_inputString = "";
				_inputIndex = 0;
			}

			private void OnEnter() {
				ClearLine();
				System.Console.ForegroundColor = ConsoleColor.Green;
				System.Console.WriteLine($"> {_inputString}");

				if (!string.IsNullOrWhiteSpace(_inputString)) {
					_previousInputs.Push(_inputString);
				}

				_inputIndex = 0;

				string strtext = _inputString;
				_inputString = "";

				HandleInput(strtext);
			}

			private void DrawPreviousInput(int index) {

				int newIndex = Mathf.Clamp(_inputIndex + index, 0, _previousInputs.count);

				if (newIndex == _inputIndex) return;

				_inputIndex = newIndex;

				if (_inputIndex == 0) {
					_inputString = _inputBuffer;
					_inputBuffer = string.Empty;
				} else {
					if (string.IsNullOrWhiteSpace(_inputBuffer)) _inputBuffer = _inputString;
					_inputString = _previousInputs[_previousInputs.count - _inputIndex];
				}

				RedrawInputLine();
			}

			private void Update() {
				if (!System.Console.KeyAvailable) return;
				ConsoleKeyInfo key = System.Console.ReadKey();

				switch (key.Key) {
					case ConsoleKey.Enter:
						OnEnter();
						return;
					case ConsoleKey.Backspace:
						OnBackspace();
						return;
					case ConsoleKey.Escape:
						OnEscape();
						return;
					case ConsoleKey.UpArrow:
						DrawPreviousInput(1);
						return;
					case ConsoleKey.DownArrow:
						DrawPreviousInput(-1);
						return;
				}

				if (key.KeyChar != '\u0000') {
					_inputString += key.KeyChar;
					RedrawInputLine();
				}
			}

			public void Dispose() {
				UnityClock.instance.onUpdate -= Update;
			}

			void HandleInput(string inputString) {
				switch (inputString.ToUpperInvariant()) {
					case "EXIT":
					case "SHUTDOWN":
						Close();
						break;
					case "LIST":
					case "LS":
					case "HELP":
					case "?":
						ListAllCommands();
						break;
					case "CLS":
						Clear();
						break;
					default:
						string[] parts = inputString.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
						if (!HandleCommand(parts[0], parts.Slice(1, parts.Length))) {
							_onInputText?.Invoke(inputString);
						}
						break;
				}

			}
		}
	}
}
#endif