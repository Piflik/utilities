#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading;
using Utilities;

namespace Windows {
	[SuppressUnmanagedCodeSecurity]
	public static partial class Console {

		interface ICommand {
			string description { get; }
			void Invoke();
		}

		interface ICommand1 {
			string description { get; }
			void Invoke(string p1);
			string parameters { get; }
		}

		interface ICommand2 {
			string description { get; }
			void Invoke(string p1, string p2);
			string parameters { get; }
		}

		interface ICommand3 {
			string description { get; }
			void Invoke(string p1, string p2, string p3);
			string parameters { get; }
		}

		interface ICommand4 {
			string description { get; }
			void Invoke(string p1, string p2, string p3, string p4);
			string parameters { get; }
		}

		interface ICommand5 {
			string description { get; }
			void Invoke(string p1, string p2, string p3, string p4, string p5);
			string parameters { get; }
		}

		private struct Command : ICommand {
			public string description { get; set; }
			public Action action;
			public void Invoke() => action?.Invoke();
		}

		private struct Command<T> : ICommand1 where T : struct, IConvertible {
			public string description { get; set; }
			public Action<T> action;
			public void Invoke(string p) {
				if (p.TryParse(out T v, ignoreCase: true) || p.TryConvert(out v)) {
					action?.Invoke(v);
				} else {
					WriteLine($"Unable to parse paramter {p} to type {typeof(T)}");
				}
			}

			public string parameters => $"[{typeof(T)}]";
		}

		private struct Command<T1, T2> : ICommand2
			where T1 : struct, IConvertible
			where T2 : struct, IConvertible {
			public string description { get; set; }
			public Action<T1, T2> action;
			public void Invoke(string p1, string p2) {
				if (!(p1.TryParse(out T1 v1, ignoreCase: true) || p1.TryConvert(out v1))) {
					WriteLine($"Unable to parse paramter {p1} to type {typeof(T1)}");
				} else if (!(p2.TryParse(out T2 v2, ignoreCase: true) || p2.TryConvert(out v2))) {
					WriteLine($"Unable to parse paramter {p2} to type {typeof(T2)}");
				} else {
					action?.Invoke(v1, v2);
				}
			}

			public string parameters => $"[{typeof(T1)}] [{typeof(T2)}]";
		}

		private struct Command<T1, T2, T3> : ICommand3
			where T1 : struct, IConvertible
			where T2 : struct, IConvertible
			where T3 : struct, IConvertible {
			public string description { get; set; }
			public Action<T1, T2, T3> action;
			public void Invoke(string p1, string p2, string p3) {
				if (!(p1.TryParse(out T1 v1, ignoreCase: true) || p1.TryConvert(out v1))) {
					WriteLine($"Unable to parse paramter {p1} to type {typeof(T1)}");
				} else if (!(p2.TryParse(out T2 v2, ignoreCase: true) || p2.TryConvert(out v2))) {
					WriteLine($"Unable to parse paramter {p2} to type {typeof(T2)}");
				} else if (!(p3.TryParse(out T3 v3, ignoreCase: true) || p3.TryConvert(out v3))) {
					WriteLine($"Unable to parse paramter {p3} to type {typeof(T3)}");
				} else {
					action?.Invoke(v1, v2, v3);
				}
			}

			public string parameters => $"[{typeof(T1)}] [{typeof(T2)}] [{typeof(T3)}]";
		}

		private struct Command<T1, T2, T3, T4> : ICommand4
			where T1 : struct, IConvertible
			where T2 : struct, IConvertible
			where T3 : struct, IConvertible
			where T4 : struct, IConvertible {
			public string description { get; set; }
			public Action<T1, T2, T3, T4> action;
			public void Invoke(string p1, string p2, string p3, string p4) {
				if (!(p1.TryParse(out T1 v1, ignoreCase: true) || p1.TryConvert(out v1))) {
					WriteLine($"Unable to parse paramter {p1} to type {typeof(T1)}");
				} else if (!(p2.TryParse(out T2 v2, ignoreCase: true) || p2.TryConvert(out v2))) {
					WriteLine($"Unable to parse paramter {p2} to type {typeof(T2)}");
				} else if (!(p3.TryParse(out T3 v3, ignoreCase: true) || p3.TryConvert(out v3))) {
					WriteLine($"Unable to parse paramter {p3} to type {typeof(T3)}");
				} else if (!(p4.TryParse(out T4 v4, ignoreCase: true) || p4.TryConvert(out v4))) {
					WriteLine($"Unable to parse paramter {p4} to type {typeof(T4)}");
				} else {
					action?.Invoke(v1, v2, v3, v4);
				}
			}

			public string parameters => $"[{typeof(T1)}] [{typeof(T2)}] [{typeof(T3)}] [{typeof(T4)}]";
		}

		private struct Command<T1, T2, T3, T4, T5> : ICommand5
			where T1 : struct, IConvertible
			where T2 : struct, IConvertible
			where T3 : struct, IConvertible
			where T4 : struct, IConvertible
			where T5 : struct, IConvertible {
			public string description { get; set; }
			public Action<T1, T2, T3, T4, T5> action;
			public void Invoke(string p1, string p2, string p3, string p4, string p5) {
				if (!(p1.TryParse(out T1 v1, ignoreCase: true) || p1.TryConvert(out v1))) {
					WriteLine($"Unable to parse paramter {p1} to type {typeof(T1)}");
				} else if (!(p2.TryParse(out T2 v2, ignoreCase: true) || p2.TryConvert(out v2))) {
					WriteLine($"Unable to parse paramter {p2} to type {typeof(T2)}");
				} else if (!(p2.TryParse(out T3 v3, ignoreCase: true) || p3.TryConvert(out v3))) {
					WriteLine($"Unable to parse paramter {p3} to type {typeof(T3)}");
				} else if (!(p2.TryParse(out T4 v4, ignoreCase: true) || p4.TryConvert(out v4))) {
					WriteLine($"Unable to parse paramter {p4} to type {typeof(T4)}");
				} else if (!(p2.TryParse(out T5 v5, ignoreCase: true) || p5.TryConvert(out v5))) {
					WriteLine($"Unable to parse paramter {p5} to type {typeof(T5)}");
				} else {
					action?.Invoke(v1, v2, v3, v4, v5);
				}
			}

			public string parameters => $"[{typeof(T1)}] [{typeof(T2)}] [{typeof(T3)}] [{typeof(T4)}] [{typeof(T5)}]";
		}



		[Flags]
		public enum LogMode {
			None = 0,
			Error = 1,
			Warning = 2,
			Log = 4,
			All = ~None,
		}

		private delegate bool ConsoleEventDelegate(int eventType);

		private static TextWriter _oldOutput;
		private static TextReader _oldInput;
		private static TextWriter _oldError;

		private static ConsoleEventDelegate _eventHandler;

		private static readonly InvokePump _mainThreadInvoker = new InvokePump();

		private static LogMode _logMode = LogMode.All;
		private static LogMode _stackTraceLogMode = LogMode.Error | LogMode.Warning;

		private static ConsoleInput _input;

		private static int _commandLength = 8;
		private static int _descriptionLength = 12;

		private const int STD_OUTPUT_HANDLE = -11;

		private static bool _consoleExists;

		public static bool isOpen { get; private set; }

		private static readonly Dictionary<string, ICommand> _noArgumentActions = new Dictionary<string, ICommand>(StringComparer.OrdinalIgnoreCase);
		private static readonly Dictionary<string, ICommand1> _singleArgumentActions = new Dictionary<string, ICommand1>(StringComparer.OrdinalIgnoreCase);
		private static readonly Dictionary<string, ICommand2> _doubleArgumentActions = new Dictionary<string, ICommand2>(StringComparer.OrdinalIgnoreCase);
		private static readonly Dictionary<string, ICommand3> _tripleArgumentActions = new Dictionary<string, ICommand3>(StringComparer.OrdinalIgnoreCase);
		private static readonly Dictionary<string, ICommand4> _quadrupleArgumentActions = new Dictionary<string, ICommand4>(StringComparer.OrdinalIgnoreCase);
		private static readonly Dictionary<string, ICommand5> _quintupleArgumentActions = new Dictionary<string, ICommand5>(StringComparer.OrdinalIgnoreCase);

		public static bool Initialize(bool showCloseButton = true, Action<string> onInputText = null) {

			_commandLength = 8;
			_descriptionLength = 12;

			if (_consoleExists) {
				ShowWindow(GetConsoleWindow(), 4);
				isOpen = true;
				return false;
			}

			if (!AttachConsole(-1)) {
				AllocConsole();
			}

			_oldOutput = System.Console.Out;
			_oldInput = System.Console.In;
			_oldError = System.Console.Error;

			try {
				System.Text.Encoding encoding = System.Text.Encoding.UTF8;

				System.Console.OutputEncoding = encoding;

				IntPtr stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
				Microsoft.Win32.SafeHandles.SafeFileHandle safeFileHandle = new Microsoft.Win32.SafeHandles.SafeFileHandle(stdHandle, true);
				FileStream fileStream = new FileStream(safeFileHandle, FileAccess.Write);

				StreamWriter standardOutput = new StreamWriter(fileStream, encoding) {
					AutoFlush = true
				};
				System.Console.SetOut(standardOutput);
				System.Console.SetIn(new StreamReader(System.Console.OpenStandardInput()));
				System.Console.SetError(new StreamWriter(System.Console.OpenStandardError()) {
					AutoFlush = true,
				});

				_eventHandler = ConsoleEventCallback;

				if (showCloseButton) {
					SetConsoleCtrlHandler(_eventHandler, true);
				} else {
					DisableCloseButton();
				}

				System.Console.Clear();
				_consoleExists = true;
			} catch (Exception e) {
				Utilities.Debug.Log("Couldn't redirect output: " + e.Message);
			}

			//if (onInputText != null) {
			_input = new ConsoleInput(onInputText);
			//}

			Application.wantsToQuit += () => {
				Shutdown();
				return true;
			};

			isOpen = true;
			return true;
		}

		public static void RegisterForStandardLog(LogMode logMode, LogMode stackTraceLogMode) {
			_logMode = logMode;
			_stackTraceLogMode = stackTraceLogMode;

			Application.logMessageReceived += HandleLog;
		}

		public static void HandleLog(string message, string stackTrace, LogType type) {
			switch (type) {
				case LogType.Warning when (_logMode & LogMode.Warning) != 0:
					System.Console.ForegroundColor = ConsoleColor.Yellow;
					System.Console.WriteLine(message);
					if ((_stackTraceLogMode & LogMode.Warning) == 0) break;
					System.Console.ForegroundColor = ConsoleColor.DarkYellow;
					System.Console.WriteLine(stackTrace);

					break;
				case LogType.Assert:
				case LogType.Error:
				case LogType.Exception:
					if ((_logMode & LogMode.Error) != 0) {
						System.Console.ForegroundColor = ConsoleColor.Red;
						System.Console.WriteLine(message);
						if ((_stackTraceLogMode & LogMode.Error) == 0) break;
						System.Console.ForegroundColor = ConsoleColor.DarkRed;
						System.Console.WriteLine(stackTrace);
					}

					break;
				default:
					if ((_logMode & LogMode.Log) != 0) {
						System.Console.ForegroundColor = ConsoleColor.White;
						System.Console.WriteLine(message);
						if ((_stackTraceLogMode & LogMode.Log) == 0) break;
						System.Console.ForegroundColor = ConsoleColor.Gray;
						System.Console.WriteLine(stackTrace);
					}

					break;
			}

		}

		public static void RegisterCommand(Action action, string command, string description = "") {
			_noArgumentActions[command] = new Command {
				action = action,
				description = description,
			};

			if (command.Length > _commandLength) _commandLength = command.Length;
			if (description.Length > _descriptionLength) _descriptionLength = description.Length;
		}

		public static void RegisterCommand<T>(Action<T> action, string command, string description = "") where T : struct, IConvertible {
			_singleArgumentActions[command] = new Command<T> {
				action = action,
				description = description,
			};

			if (command.Length > _commandLength) _commandLength = command.Length;
			if (description.Length > _descriptionLength) _descriptionLength = description.Length;
		}

		public static void RegisterCommand<T1, T2>(Action<T1, T2> action, string command, string description = "")
			where T1 : struct, IConvertible
			where T2 : struct, IConvertible {
			_doubleArgumentActions[command] = new Command<T1, T2> {
				action = action,
				description = description,
			};

			if (command.Length > _commandLength) _commandLength = command.Length;
			if (description.Length > _descriptionLength) _descriptionLength = description.Length;
		}

		public static void RegisterCommand<T1, T2, T3>(Action<T1, T2, T3> action, string command, string description = "")
			where T1 : struct, IConvertible
			where T2 : struct, IConvertible
			where T3 : struct, IConvertible {
			_tripleArgumentActions[command] = new Command<T1, T2, T3> {
				action = action,
				description = description,
			};

			if (command.Length > _commandLength) _commandLength = command.Length;
			if (description.Length > _descriptionLength) _descriptionLength = description.Length;
		}

		public static void RegisterCommand<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action, string command, string description = "")
			where T1 : struct, IConvertible
			where T2 : struct, IConvertible
			where T3 : struct, IConvertible
			where T4 : struct, IConvertible {
			_quadrupleArgumentActions[command] = new Command<T1, T2, T3, T4> {
				action = action,
				description = description,
			};

			if (command.Length > _commandLength) _commandLength = command.Length;
			if (description.Length > _descriptionLength) _descriptionLength = description.Length;
		}

		public static void RegisterCommand<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action, string command, string description = "")
			where T1 : struct, IConvertible
			where T2 : struct, IConvertible
			where T3 : struct, IConvertible
			where T4 : struct, IConvertible
			where T5 : struct, IConvertible {
			_quintupleArgumentActions[command] = new Command<T1, T2, T3, T4, T5> {
				action = action,
				description = description,
			};

			if (command.Length > _commandLength) _commandLength = command.Length;
			if (description.Length > _descriptionLength) _descriptionLength = description.Length;
		}

		private static bool HandleCommand(string command, params string[] parameters) {
			switch (parameters.Length) {
				case 0 when _noArgumentActions.TryGetValue(command, out ICommand action):
					action.Invoke();
					return true;
				case 1 when _singleArgumentActions.TryGetValue(command, out ICommand1 action):
					action?.Invoke(parameters[0]);
					return true;
				case 2 when _doubleArgumentActions.TryGetValue(command, out ICommand2 action):
					action?.Invoke(parameters[0], parameters[1]);
					return true;
				case 3 when _tripleArgumentActions.TryGetValue(command, out ICommand3 action):
					action?.Invoke(parameters[0], parameters[1], parameters[2]);
					return true;
				case 4 when _quadrupleArgumentActions.TryGetValue(command, out ICommand4 action):
					action?.Invoke(parameters[0], parameters[1], parameters[2], parameters[3]);
					return true;
				case 5 when _quintupleArgumentActions.TryGetValue(command, out ICommand5 action):
					action?.Invoke(parameters[0], parameters[1], parameters[2], parameters[3], parameters[4]);
					return true;
			}

			WriteLine($"No command \"{command}\" found with {parameters.Length} parameters");
			return false;
		}

		private static void ListAllCommands() {
			int firstColumn = _commandLength + 5;
			int secondColumn = _descriptionLength + 5;

			List<string> commands = new List<string>();

			commands.Add($"{"cls".PadRight(firstColumn)}{"Clear console".PadRight(secondColumn)}-");
			commands.Add($"{"exit".PadRight(firstColumn)}{"Close console".PadRight(secondColumn)}-");
			commands.Add($"{"help".PadRight(firstColumn)}{"Show this list".PadRight(secondColumn)}-");

			foreach (KeyValuePair<string, ICommand> command in _noArgumentActions) {
				commands.Add($"{command.Key.PadRight(firstColumn)}{command.Value.description.PadRight(secondColumn)}-");
			}
			foreach (KeyValuePair<string, ICommand1> command in _singleArgumentActions) {
				commands.Add($"{command.Key.PadRight(firstColumn)}{command.Value.description.PadRight(secondColumn)}{command.Value.parameters}");
			}
			foreach (KeyValuePair<string, ICommand2> command in _doubleArgumentActions) {
				commands.Add($"{command.Key.PadRight(firstColumn)}{command.Value.description.PadRight(secondColumn)}{command.Value.parameters}");
			}
			foreach (KeyValuePair<string, ICommand3> command in _tripleArgumentActions) {
				commands.Add($"{command.Key.PadRight(firstColumn)}{command.Value.description.PadRight(secondColumn)}{command.Value.parameters}");
			}
			foreach (KeyValuePair<string, ICommand4> command in _quadrupleArgumentActions) {
				commands.Add($"{command.Key.PadRight(firstColumn)}{command.Value.description.PadRight(secondColumn)}{command.Value.parameters}");
			}
			foreach (KeyValuePair<string, ICommand5> command in _quintupleArgumentActions) {
				commands.Add($"{command.Key.PadRight(firstColumn)}{command.Value.description.PadRight(secondColumn)}{command.Value.parameters}");
			}

			WriteLine($"{"Command:".PadRight(firstColumn)}{"Description:".PadRight(secondColumn)}Parameters:");
			WriteLine(new string('=', firstColumn+secondColumn+11));

			foreach (string command in commands.OrderBy(c => c)) {
				WriteLine(command);
			}
		}

		public static void WriteLine(string message, ConsoleColor color = ConsoleColor.White) {
			System.Console.ForegroundColor = color;
			System.Console.WriteLine(message);
		}

		public static void Clear() {
			System.Console.Clear();
		}

		public static void Close() {
			ShowWindow(GetConsoleWindow(), 0);
			isOpen = false;
			Shutdown();
		}

		private static void Shutdown() {
			System.Console.SetOut(_oldOutput);
			System.Console.SetIn(_oldInput);
			System.Console.SetError(_oldError);
			_input?.Dispose();
			_input = null;
			FreeConsole();
			_consoleExists = false;

			isOpen = false;
		}

		public static void SetTitle(string strName) {
			SetConsoleTitleA(strName);
		}

		private static bool ConsoleEventCallback(int eventType) {
			switch (eventType) {
				case 0:
				case 1:
				case 2:
					_mainThreadInvoker.Invoke(Application.Quit);
					break;
			}

			Thread.Sleep(2000);
			return false;
		}

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool AttachConsole(int dwProcessId);

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool AllocConsole();

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool FreeConsole();

		[DllImport("kernel32.dll", EntryPoint = "GetStdHandle", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
		private static extern IntPtr GetStdHandle(int nStdHandle);

		[DllImport("kernel32.dll")] private static extern bool SetConsoleTitleA(string lpConsoleTitle);

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);

		private const int MF_BYCOMMAND = 0x00000000;
		public const int SC_CLOSE = 0xF060;

		[DllImport("user32.dll")]
		public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

		[DllImport("user32.dll")]
		private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

		[DllImport("kernel32.dll", ExactSpelling = true)]
		private static extern IntPtr GetConsoleWindow();

		[DllImport("user32")]
		static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

		private static void DisableCloseButton() {
			DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
		}
	}
}
#endif