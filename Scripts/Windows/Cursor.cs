﻿#if UNITY_STANDALONE_WIN || UNITY_EDITOR
using System.Runtime.InteropServices;

namespace Windows {
	public static class Cursor {

		[StructLayout(LayoutKind.Sequential)]
		public struct Point {
			public int x;
			public int y;
		}

		[DllImport("user32.dll")]
		private static extern bool GetCursorPos(out Point lpPoint);

		[DllImport("user32.dll")]
		private static extern long SetCursorPos(int x, int y);

		public static Point position {
			get {
				GetCursorPos(out Point point);
				return point;
			}
			set => SetCursorPos(value.x, value.y);
		}
	}
}

#endif
