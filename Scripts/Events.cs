﻿using System;
using UnityEngine.Events;

namespace Utilities {
	/// <summary>
	/// Container class for Serializable Events in UnityEditor.
	/// </summary>
	public static class Events {
		[Serializable]
		public class Bool : UnityEvent<bool> {}

		[Serializable]
		public class Int : UnityEvent<int> {}

		[Serializable]
		public class Float : UnityEvent<float> {}

		[Serializable]
		public class String : UnityEvent<string> {}

		[Serializable]
		public class Double : UnityEvent<double> {}

		[Serializable]
		public class Transform : UnityEvent<UnityEngine.Transform> {}

		[Serializable]
		public class GameObject : UnityEvent<UnityEngine.GameObject> {}

		[Serializable]
		public class Vector3 : UnityEvent<UnityEngine.Vector3> {}

		[Serializable]
		public class Vector2 : UnityEvent<UnityEngine.Vector2> {}

		[Serializable]
		public class Quaternion : UnityEvent<Quaternion> {}

	}
}