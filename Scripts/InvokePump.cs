﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Utilities {

	/// <summary>
	/// Queue actions up to be called during some other threads update pump.
	/// </summary>
	public class InvokePump : WaitHandle {

		private int _threadId;

		private readonly ConcurrentQueue<Action> _invoking;
		private readonly EventWaitHandle _waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);

		public InvokePump() : this(null) { }

		public InvokePump(Thread ownerThread) {
			_threadId = ownerThread?.ManagedThreadId ?? Thread.CurrentThread.ManagedThreadId;

			_invoking = new ConcurrentQueue<Action>();
			UnityClock.instance.onUpdate += Update;
		}

		/// <summary>
		/// Returns true if on a thread other than the one that owns this pump.
		/// </summary>
		public bool invokeRequired => _threadId != 0 && Thread.CurrentThread.ManagedThreadId != _threadId;

		/// <summary>
		/// Queues an action to be invoked next time Update is called. This method will block until that occurs.
		/// </summary>
		/// <param name="action"></param>
		public void Invoke(Action action) {
			if (_threadId == 0) throw new InvalidOperationException("InvokePump has been closed.");
			if (Thread.CurrentThread.ManagedThreadId == _threadId) throw new InvalidOperationException("Never call WaitOne on an InvokePump from the thread that owns it, this will freeze that thread indefinitely.");
			if (action == null) throw new ArgumentNullException(nameof(action));

			_invoking.Enqueue(action);
			_waitHandle.WaitOne(); //block until it's called
		}

		/// <summary>
		/// Queues an action to be invoked next time Update is called. This method does not block.
		/// </summary>
		/// <param name="action"></param>
		public void BeginInvoke(Action action) {
			if (_threadId == 0) throw new InvalidOperationException("InvokePump has been closed.");
			if (action == null) throw new ArgumentNullException(nameof(action));

			_invoking.Enqueue(action);
		}

		/// <summary>
		/// Can only be called by the thread that owns this InvokePump, this will run all queued actions.
		/// </summary>
		public void Update() {
			if (_threadId == 0) return; //we're destroyed
			if (invokeRequired) throw new InvalidOperationException("InvokePump.Update can only be updated on the thread that was designated its owner.");

			//record the current length so we only activate those actions added at this point
			//any newly added actions should wait until NEXT update
			while (_invoking.TryDequeue(out Action act))
				act?.Invoke();

			//release waits
			_waitHandle.Set();
		}

		public override void Close() {
			base.Close();
			if (UnityClock.isRunning) UnityClock.instance.onUpdate -= Update;

			if (_threadId == 0) return; //already was destroyed

			_waitHandle.Close();
			_threadId = 0;
		}

		protected override void Dispose(bool explicitDisposing) {
			base.Dispose(explicitDisposing);
			if(UnityClock.isRunning) UnityClock.instance.onUpdate -= Update;

			if (_threadId == 0) return; //already was destroyed

			(_waitHandle as IDisposable).Dispose();
			_threadId = 0;
		}

		public override bool WaitOne() {
			if (_threadId == 0) throw new InvalidOperationException("InvokePump has been closed.");
			if (Thread.CurrentThread.ManagedThreadId == _threadId) throw new InvalidOperationException("Never call WaitOne on an InvokePump from the thread that owns it, this will freeze that thread indefinitely.");
			return _waitHandle.WaitOne();
		}

		public override bool WaitOne(int millisecondsTimeout) {
			if (_threadId == 0) throw new InvalidOperationException("InvokePump has been closed.");
			if (Thread.CurrentThread.ManagedThreadId == _threadId) throw new InvalidOperationException("Never call WaitOne on an InvokePump from the thread that owns it, this will freeze that thread indefinitely.");
			return _waitHandle.WaitOne(millisecondsTimeout);
		}

		public override bool WaitOne(int millisecondsTimeout, bool exitContext) {
			if (_threadId == 0) throw new InvalidOperationException("InvokePump has been closed.");
			if (Thread.CurrentThread.ManagedThreadId == _threadId) throw new InvalidOperationException("Never call WaitOne on an InvokePump from the thread that owns it, this will freeze that thread indefinitely.");
			return _waitHandle.WaitOne(millisecondsTimeout, exitContext);
		}

		public override bool WaitOne(TimeSpan timeout) {
			if (_threadId == 0) throw new InvalidOperationException("InvokePump has been closed.");
			if (Thread.CurrentThread.ManagedThreadId == _threadId) throw new InvalidOperationException("Never call WaitOne on an InvokePump from the thread that owns it, this will freeze that thread indefinitely.");
			return _waitHandle.WaitOne(timeout);
		}

		public override bool WaitOne(TimeSpan timeout, bool exitContext) {
			if (_threadId == 0) throw new InvalidOperationException("InvokePump has been closed.");
			if (Thread.CurrentThread.ManagedThreadId == _threadId) throw new InvalidOperationException("Never call WaitOne on an InvokePump from the thread that owns it, this will freeze that thread indefinitely.");
			return _waitHandle.WaitOne(timeout, exitContext);
		}
	}

}