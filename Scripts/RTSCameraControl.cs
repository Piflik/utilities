﻿using System.Collections;
using UnityEngine;

namespace Utilities {
	public class RTSCameraControl : MonoBehaviour {

		private static RTSCameraControl _instance;

		public static RTSCameraControl instance {
			get { return _instance ?? (_instance = Camera.main.gameObject.AddComponent<RTSCameraControl>()); }
		}

		//TODO modify via options menu
		[SerializeField]
		[Range(0, 1)]
		private float _panSpeed = 1;
		private const float PAN_SPEED_MULTIPLIER = 25;
		private const float PAN_SEEP_MULTIPLIER_MOUSE = 5;

		[SerializeField]
		[Range(0, 1)]
		private float _rotateSpeed = 1;
		private const float ROTATE_SPEED_MULTIPLIER = 100;
		private const float ROTATE_SPEED_MULTIPLIER_MOUSE = 250;
		private const float MIN_ROTATION = -25;
		private const float MAX_ROTATION = 85;

		[SerializeField]
		[Range(0, 1)]
		private float _zoomSpeed = 1;
		private const float ZOOM_SPEED_MULTIPLIER = 1000;
		private const float MIN_ZOOM = 0.5f;
		private const float MAX_ZOOM = 25f;

		void Awake() {
			if (_instance != null && _instance != this) {
				Destroy(gameObject);
			} else {

				_instance = this;
				_desiredPosition = transform.position;
				_desiredRotation = transform.rotation.eulerAngles;
			}
		}

		private Vector3 _desiredPosition;
		private Vector3 _desiredRotation;
		void Update() {

			Move(Input.GetAxis("Horizontal") * transform.right + Input.GetAxis("Vertical") * Vector3.Cross(transform.right, Vector3.up), _panSpeed * Time.deltaTime * PAN_SPEED_MULTIPLIER);
			Move(Input.GetAxis("Zoom") * transform.forward, _zoomSpeed * Time.deltaTime * ZOOM_SPEED_MULTIPLIER); //TODO zoom to cursor?
			Rotate(0, Input.GetAxisRaw("Rotate") * Time.deltaTime * _rotateSpeed * ROTATE_SPEED_MULTIPLIER);

			//TODO fix mouse Pan/Rotate to always keep cursor over same point in space
			if (Input.GetButton("Pan")) {
				if (Input.GetButton("Modify")) {
					Rotate(Input.GetAxis("Mouse Y") * Time.deltaTime * _rotateSpeed * ROTATE_SPEED_MULTIPLIER_MOUSE, Input.GetAxis("Mouse X") * Time.deltaTime * _rotateSpeed * ROTATE_SPEED_MULTIPLIER_MOUSE);
				} else {
					Move(Input.GetAxis("Mouse X") * transform.right + Input.GetAxis("Mouse Y") * transform.up, Time.deltaTime * PAN_SEEP_MULTIPLIER_MOUSE * _desiredPosition.y);
				}
			}

			ApplyTransformation();
		}

		private void Move(Vector3 dir, float speed) {
			_desiredPosition += speed * dir;
			_desiredPosition.y = Mathf.Clamp(_desiredPosition.y, MIN_ZOOM, MAX_ZOOM);
		}


		//private void Zoom(float amount) {
		//	_desiredPosition.y = Mathf.Clamp(_desiredPosition.y + amount, MIN_ZOOM, MAX_ZOOM);
		//}

		private void Rotate(float x, float y) {

			_desiredRotation.x = Mathf.Clamp(_desiredRotation.x + x, MIN_ROTATION, MAX_ROTATION);
			_desiredRotation.y += y;
		}

		private void ApplyTransformation() {
			transform.position = _desiredPosition + _posDisplacement;
			transform.rotation = Quaternion.Euler(_desiredRotation);
		}

		private bool _shaking = false;
		public void Shake(float strength, float duration) {
			if (_shaking) return;
			StartCoroutine(CamShake(strength, duration));
		}

		private Vector3 _posDisplacement = Vector3.zero;
		private IEnumerator CamShake(float strength, float duration) {
			_shaking = true;
			float timer = 0;

			while (timer < duration) {

				float displacement = FloatUtilities.Gaussian(timer, strength, 0, duration * 0.3f);
				_posDisplacement = displacement * Random.onUnitSphere;

				timer += Time.deltaTime;
				yield return null;
			}

			_posDisplacement = Vector3.zero;
			_shaking = false;
		}
	}
}