﻿using System;
using System.Linq;
using UnityEngine;
// ReSharper disable UnusedMember.Global

namespace Utilities {
	public class Matrix3 : IEquatable<Matrix3> {
		public Matrix3() { }

		public Matrix3(float v00, float v01, float v02, float v10, float v11, float v12, float v20, float v21, float v22) {

			data[0] = v00;
			data[1] = v01;
			data[2] = v02;
			data[3] = v10;
			data[4] = v11;
			data[5] = v12;
			data[6] = v20;
			data[7] = v21;
			data[8] = v22;
		}

		/// <summary>
		/// Takes first 9 values from input array. creates zero-matrix if array length smaller than 9
		/// </summary>
		public Matrix3(float[] values) {
			data = values.Length < 9 ? new float[9] : values.Take(9).ToArray();
		}

		public Matrix3(Quaternion q) {
			float num1 = q.x * 2f;
			float num2 = q.y * 2f;
			float num3 = q.z * 2f;
			float num4 = q.x * num1;
			float num5 = q.y * num2;
			float num6 = q.z * num3;
			float num7 = q.x * num2;
			float num8 = q.x * num3;
			float num9 = q.y * num3;
			float num10 = q.w * num1;
			float num11 = q.w * num2;
			float num12 = q.w * num3;

			data[0] = (float)(1.0 - (num5 + (double)num6));
			data[1] = num7 - num12;
			data[2] = num8 + num11;
			data[3] = num7 + num12;
			data[4] = (float)(1.0 - (num4 + (double)num6));
			data[5] = num9 - num10;
			data[6] = num8 - num11;
			data[7] = num9 + num10;
			data[8] = (float)(1.0 - (num4 + (double)num5));
		}

		public float this[int i, int j] {
			get {
				if (i < 0 || i > 2 || j < 0 || j > 2) throw new IndexOutOfRangeException("Invalid index");

				return data[i + 3 * j];
			}
			set {
				if (i < 0 || i > 2 || j < 0 || j > 2) throw new IndexOutOfRangeException("Invalid index");

				data[i + 3 * j] = value;
			}
		}

		public static Matrix3 one = new Matrix3(1, 0, 0, 0, 1, 0, 0, 0, 1);

		public static readonly Matrix3 zero = new Matrix3(0, 0, 0, 0, 0, 0, 0, 0, 0);

		public static Matrix3 FromQuaternion(Quaternion q) => new Matrix3(q);

		public static Matrix3 Rotation(float radians, Axis axis) {
			float c = Mathf.Cos(radians);
			float s = Mathf.Sin(radians);

			switch (axis) {
				case Axis.X:
					return new Matrix3(1, 0, 0, 0, c, -s, 0, s, c);
				case Axis.Z:
					return new Matrix3(c, -s, 0, s, c, 0, 0, 0, 1);
				default:
					return new Matrix3(c, 0, s, 0, 1, 0, -s, 0, c);

			}
		}

		public static Matrix3 Rotation(float radians, Vector3 axis) {
			float c = Mathf.Cos(radians);
			float s = Mathf.Sin(radians);

			Vector3 n = axis.normalized;

			return new Matrix3(
				c + n.x * n.x * (1 - c), n.x * n.y * (1 - c) - n.z * s, n.x * n.z * (1 - c) + n.y * s,
				n.y * n.x * (1 - c) + n.z * s, c + n.y * n.y * (1 - c), n.y * n.z * (1 - c) - n.x * s,
				n.z * n.x * (1 - c) - n.y * s, n.z * n.y * (1 - c) + n.x * s, c + n.z * n.z * (1 - c)
			);
		}

		public static Matrix3 Scale(float x, float y, float z) {
			return new Matrix3(x, 0, 0, 0, y, 0, 0, 0, z);
		}

		public static Matrix3 Scale(Vector3 scale) {
			return new Matrix3(scale.x, 0, 0, 0, scale.y, 0, 0, 0, scale.z);
		}

		public static Matrix3 ScaleUniform(float scale) {
			return new Matrix3(scale, 0, 0, 0, scale, 0, 0, 0, scale);
		}

		public override string ToString() {
			return $"{v00} {v01} {v02}\n{v10} {v11} {v12}\n{v20} {v21} {v22}";
		}

		public void Set(float val00, float val01, float val02, float val10, float val11, float val12, float val20, float val21, float val22) {
			data[0] = val00;
			data[1] = val01;
			data[2] = val02;
			data[3] = val10;
			data[4] = val11;
			data[5] = val12;
			data[6] = val20;
			data[7] = val21;
			data[8] = val22;
		}


		//rows and columns
		public void SetRow(int n, Vector3 newValues) {
			if (n > 2 || n < 0) return;

			data[n] = newValues.x;
			data[n + 1] = newValues.y;
			data[n + 2] = newValues.z;
		}

		public void SetColumn(int n, Vector3 newValues) {
			if (n > 2 || n < 0) return;

			data[n] = newValues.x;
			data[n + 3] = newValues.y;
			data[n + 6] = newValues.z;
		}

		public void SetRows(Vector3 x, Vector3 y, Vector3 z) {
			data[0] = x.x;
			data[1] = x.y;
			data[2] = x.z;
			data[3] = y.x;
			data[4] = y.y;
			data[5] = y.z;
			data[6] = z.x;
			data[7] = z.y;
			data[8] = z.z;
		}

		public void SetColumns(Vector3 x, Vector3 y, Vector3 z) {
			data[0] = x.x;
			data[1] = y.x;
			data[2] = z.x;
			data[3] = x.y;
			data[4] = y.y;
			data[5] = z.y;
			data[6] = x.z;
			data[7] = y.z;
			data[8] = z.z;
		}

		public Vector3 GetRow(int n) {
			if (n > 2 || n < 0) return default;
			return new Vector3(data[n], data[n + 1], data[n + 2]);
		}

		public Vector3 GetColumn(int n) {
			if (n > 2 || n < 0) return default;
			return new Vector3(data[n], data[n + 3], data[n + 6]);
		}

		//elements
		public float v00 {
			get => data[0];
			set => data[0] = value;
		}

		public float v01 {
			get => data[1];
			set => data[1] = value;
		}

		public float v02 {
			get => data[2];
			set => data[2] = value;
		}

		public float v10 {
			get => data[3];
			set => data[3] = value;
		}

		public float v11 {
			get => data[4];
			set => data[4] = value;
		}

		public float v12 {
			get => data[5];
			set => data[5] = value;
		}

		public float v20 {
			get => data[6];
			set => data[6] = value;
		}

		public float v21 {
			get => data[7];
			set => data[7] = value;
		}

		public float v22 {
			get => data[8];
			set => data[8] = value;
		}

		public float[] data { get; } = new float[9];

		public float determinant => v00 * v11 * v22 + v01 * v12 * v20 + v02 * v10 * v21 - v02 * v11 * v20 - v01 * v10 * v22 - v00 * v12 * v21;

		public Vector3 diagonal => new Vector3(v00, v11, v22);

		public float trace => v00 + v11 + v22;

		public Matrix3 transpose {
			get {
				Matrix3 output = new Matrix3();

				output.SetRows(GetColumn(0), GetColumn(1), GetColumn(2));

				return output;
			}
		}

		public Matrix3 inverse {
			get {
				float det = determinant;
				if (Mathf.Approximately(det, 0)) throw new ArithmeticException("Determinant is 0");

				return new Matrix3(
					(data[4] * data[8] - data[5] * data[7]) / det, (data[2] * data[7] - data[1] * data[8]) / det, (data[1] * data[5] - data[2] * data[4]) / det,
					(data[5] * data[6] - data[3] * data[8]) / det, (data[0] * data[8] - data[2] * data[6]) / det, (data[2] * data[3] - data[0] * data[5]) / det,
					(data[3] * data[7] - data[4] * data[6]) / det, (data[1] * data[6] - data[0] * data[7]) / det, (data[0] * data[4] - data[1] * data[3]) / det
				);
			}
		}

		/// <summary>
		/// A quaternion that represents the same Rotation as the Matrix (if it is a valid Rotation matrix; undefined if not)
		/// </summary>
		public Quaternion quaternion {
			get {

				float w = Mathf.Sqrt(Mathf.Max(0, 1.0f + v00 + v11 + v22)) * 0.5f;
				float x = Mathf.Sqrt(Mathf.Max(0, 1.0f + v00 - v11 - v22)) * 0.5f;
				float y = Mathf.Sqrt(Mathf.Max(0, 1.0f - v00 + v11 - v22)) * 0.5f;
				float z = Mathf.Sqrt(Mathf.Max(0, 1.0f - v00 - v11 + v22)) * 0.5f;

				x = Mathf.Sign(v21 - v12) * Mathf.Abs(x);
				y = Mathf.Sign(v02 - v20) * Mathf.Abs(y);
				z = Mathf.Sign(v10 - v01) * Mathf.Abs(z);

				return new Quaternion(x, y, z, w);
			}
		}

		//operators
		public static Matrix3 operator +(Matrix3 a, Matrix3 b) {
			Matrix3 m = new Matrix3();

			for (int i = 0; i < 9; ++i) {
				m.data[i] = a.data[i] + b.data[i];
			}

			return m;
		}

		public static Matrix3 operator -(Matrix3 a, Matrix3 b) {
			Matrix3 m = new Matrix3();

			for (int i = 0; i < 9; ++i) {
				m.data[i] = a.data[i] - b.data[i];
			}

			return m;
		}

		public static Matrix3 operator -(Matrix3 a) {
			Matrix3 m = new Matrix3();

			for (int i = 0; i < 9; ++i) {
				m.data[i] = -a.data[i];
			}

			return m;
		}

		public static Matrix3 operator *(Matrix3 a, float b) {
			Matrix3 m = new Matrix3();

			for (int i = 0; i < 9; ++i) {
				m.data[i] = a.data[i] * b;
			}

			return m;
		}

		public static Matrix3 operator *(float b, Matrix3 a) {
			Matrix3 m = new Matrix3();

			for (int i = 0; i < 9; ++i) {
				m.data[i] = a.data[i] * b;
			}

			return m;
		}

		public static Matrix3 operator *(Matrix3 a, Matrix3 b) {
			Matrix3 m = new Matrix3();

			for (int i = 0; i < 9; ++i) {
				m.data[i] = Vector3.Dot(a.GetRow(i / 3), b.GetColumn(i % 3));
			}

			return m;
		}

		public static Matrix3 operator /(Matrix3 a, float b) {
			Matrix3 m = new Matrix3();

			for (int i = 0; i < 9; ++i) {
				m.data[i] = a.data[i] / b;
			}

			return m;
		}

		public override int GetHashCode() {
			return data != null ? data.GetHashCode() : 0;
		}

		public bool Equals(Matrix3 other) {
			return !(other is null) && (ReferenceEquals(this, other) || data.SequenceEqual(other.data));
		}

		public override bool Equals(object obj) {
			return !(obj is null) && (ReferenceEquals(this, obj) || obj.GetType() == GetType() && Equals((Matrix3)obj));
		}

		public static bool operator ==(Matrix3 a, Matrix3 b) {
			return a is null && b is null || !(a is null) && a.Equals(b);
		}

		public static bool operator !=(Matrix3 a, Matrix3 b) {
			return a is null && !(b is null) || !(a is null) && !a.Equals(b);
		}
	}
}