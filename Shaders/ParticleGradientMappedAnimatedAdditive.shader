﻿//By Florian Smolka, with the help of Simon Truempler's(Simon Schreibt) Blog post on the explosions in Fallout4  
Shader "Particles/GradientMapped-Animated Additive" {
	Properties{
		_TintColor("Tint Color", Color) = (1,1,1,1)
		_ShapesTex("Shapes Texture (R)", 2D) = "white" {}
		_GradientMap("Gradient Map (RGB)", 2D) = "white" {}
		_Settings("X: Ramp Rows, Y: , Z: , W: ", Vector) = (4,0,0,0)
		_Multiplier("Output Multiplier", Range(0, 10)) = 1
	}

	SubShader{

		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }

		Blend SrcAlpha One
		AlphaTest Greater .01

		Cull Off Lighting Off ZWrite Off

		Pass{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_particles

			#include "UnityCG.cginc"

			uniform sampler2D _ShapesTex;
			uniform sampler2D _GradientMap;
			uniform fixed4 _TintColor;
			uniform fixed4 _Settings;
			uniform half _Multiplier;

			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 customData : TEXCOORD1;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				fixed3 rampData : TEXCOORD1;
			};

			uniform float4 _ShapesTex_ST;
			uniform float4 _GradientMap_ST;

			v2f vert(appdata_t v) {
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;

				o.texcoord.xy = TRANSFORM_TEX(v.texcoord.xy, _ShapesTex);
		
				o.rampData.x = 1 / _Settings.x; //just makes it easier to just put in the rows of the ramp textures
				float offset = o.rampData.x * 0.5;

				o.rampData.y = (floor(v.customData.x * _Settings.x) * o.rampData.x) + offset;
				o.rampData.z = frac(v.customData.x/o.rampData.x);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target {
				half4 shapeTex = tex2D(_ShapesTex, i.texcoord.xy);

				//x is the current value of the pixel on the shapes texture
				//y is the current ramp texture row, depending on the red color assigned in the PS
				half2 rampColorUV;
				rampColorUV.x = shapeTex.r;
				rampColorUV.y = i.rampData.y;
				rampColorUV = saturate(rampColorUV);

				//remap the colors once for current row and once for the next and then lerp between them
				half3 reColored1 = tex2D(_GradientMap, rampColorUV);
				half3 reColored2 = tex2D(_GradientMap, half2(rampColorUV.x, rampColorUV.y + i.rampData.x));

				half3 reColorLerp = lerp(reColored1.rgb, reColored2.rgb, i.rampData.z);
	
				half4 col;
				col.rgb = reColorLerp * _TintColor.rgb * _Multiplier * i.color.rgb;
				col.a = shapeTex.r * i.color.a;

				return col;
			}

			ENDCG

		}
	}
}
