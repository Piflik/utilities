Shader "Custom/Distortion" {
	Properties {
		_MainColor ("Color", Color) = (1,1,1,1)
		_Normal ("Normal Map", 2D) = "normal" {}
		_IOR("Refraction", Range(-5, 5)) = 1
		_NormalMapStrength("Normal Map Strength)", Range(0, 1)) = 1
	}
	
	SubShader {
		
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		
		GrabPass {}
		
		Pass {
						
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};
			
			struct v2f {
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
				float4 grabUv : TEXCOORD3;
				float3 viewNormal : TEXCOORD4;
			};
			
			uniform sampler2D _GrabTexture;
			
			uniform half4 _MainColor;
			uniform sampler2D _Normal;
			uniform float4 _Normal_ST;
			uniform half _IOR;
			uniform half _NormalMapStrength;
			
			v2f vert (appdata IN) {
				v2f OUT;
							
				OUT.position = UnityObjectToClipPos(IN.vertex);
				OUT.uv = IN.uv;
				
				float3 worldPos = mul(unity_ObjectToWorld, IN.vertex).xyz;
				
				OUT.viewDir = normalize(UnityWorldSpaceViewDir(worldPos));
				OUT.normal = UnityObjectToWorldNormal(IN.normal);
			   
				OUT.grabUv = ComputeGrabScreenPos(OUT.position);
			   
				OUT.viewNormal = UnityObjectToClipPos(IN.normal);

				return OUT;
			}
			
			//TODO fix distortion of geometry in front of object
			fixed4 frag (v2f IN) : SV_Target {
			
				IN.normal = normalize(IN.normal);
				IN.viewDir = normalize(IN.viewDir);
				IN.viewNormal = normalize(IN.viewNormal);

				fixed3 normalMap = normalize(UnpackNormal(tex2D(_Normal, _Normal_ST * IN.uv))) * _NormalMapStrength;

				float3 normProj = IN.normal - dot(IN.normal, IN.viewDir) * IN.viewDir;
				float fresnel = pow(length(cross(IN.normal, IN.viewDir)),4);
				
				IN.grabUv.x += _IOR * IN.viewNormal.x * fresnel;
				IN.grabUv.y -= _IOR * IN.viewNormal.y * fresnel;

				IN.grabUv.xy += _IOR * normalMap;

				fixed4 refraction = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(IN.grabUv));
				
				return _MainColor * refraction;
			}
			
			ENDCG
		
		}
	}
}