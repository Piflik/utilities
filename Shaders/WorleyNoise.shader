﻿Shader "Procedural/Worley Noise Unlit"
{
	Properties {
		_MainTex ("Gradient Color (RGB), Illumination Strength (A)", 2D) = "white" {}
		_Size("Size", Range(0.1, 10)) = 1
		_Speed("Animation Speed", Range(0,2)) = 0
		_Multiplier("Output Multiplier", Range(0, 10)) = 1

		_CellSpread("Cell Spread", Range(0.001, 5)) = 1

		_Thresh("Threshold", Range(0,1)) = 0.5
		_Steep("Steepness", Range(1,10)) = 2

		_Angularity("Angularity", Range(0, 1)) = 0
		_Randomness("Randomness", Range(0,10)) = 5

		_Levels("Iterations", Range(1,4)) = 1
		[MaterialToggle] _Invert("Invert", Float) = 0
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			#include "Noise.cginc"

			struct appdata {
				float4 vertex : POSITION;
			};

			struct v2f {
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float3 posLoc : TEXCOORD0;
			};

			uniform sampler2D _MainTex;
			uniform float _Size;
			uniform float _Multiplier;
			uniform float _Speed;
			uniform float _CellSpread;
			uniform float _Steep;
			uniform float _Thresh;
			uniform float _Angularity;
			uniform float _Randomness;
			uniform float _Levels;
			uniform float _Invert;
			
			v2f vert (appdata v) {
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.posLoc = v.vertex.xyz / (0.1*_Size);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target {

				float noiseCoord = worley(i.posLoc, _CellSpread, _Angularity, _Randomness, _Time.y * _Speed, _Levels);

				noiseCoord = sigmoid(noiseCoord - _Thresh, 0, 1, _Steep * _Steep);
				noiseCoord = abs(_Invert - noiseCoord);

				// sample the texture
				float4 col = tex2D(_MainTex, half2(noiseCoord, 0));
				col *= (1 + col.a * _Multiplier);

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				
				return col;
			}
			ENDCG
		}
	}
}
