﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Animated Spritesheet" {
	Properties{
		_TintColor("Tint Color", Color) = (1,1,1,1)
		_Sprites("Spritesheet (RGBA)", 2D) = "white" {}
		_Settings("X: Columns, Y: Rows, Z: Timescale, W: ", Vector) = (4,0,0,0)
		_Multiplier("Output Multiplier", Range(0, 10)) = 1
	}

	SubShader{

		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }

		Blend SrcAlpha OneMinusSrcAlpha
		AlphaTest Greater .01

		Cull Off Lighting Off ZWrite Off

		Pass{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_particles

			#include "UnityCG.cginc"

			uniform sampler2D _Sprites;
			uniform fixed4 _TintColor;
			uniform fixed4 _Settings;
			uniform half _Multiplier;

			struct appdata_t {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			uniform float4 _Sprites_ST;

			v2f vert(appdata_t v) {
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);

				o.uv = TRANSFORM_TEX(v.uv, _Sprites);
		
				int numFrames = _Settings.x * _Settings.y;
				float currentTime = _Time.y * _Settings.z;

				float2 offset;
				offset.x = (int)fmod(currentTime, _Settings.x);
				offset.y = _Settings.y - 1 - (int)fmod(currentTime / _Settings.x, _Settings.y);

				o.uv = (o.uv + offset) / _Settings.xy;


				return o;
			}

			fixed4 frag(v2f i) : SV_Target {
			
				half4 col = tex2D(_Sprites, i.uv);

				col.rgb = col * _TintColor.rgb * _Multiplier;
				col.a = col * _TintColor.a;

				return col;
			}

			ENDCG

		}
	}
}
