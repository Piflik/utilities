﻿Shader "Procedural/Worley Noise Textured" {
	Properties {
		[Header(Cells)]
		_CellColor("Color", Color) = (1,1,1,1)
		_CellTex("Texture", 2D) = "white" {}
		_CellSpec("Spec (RGB), Gloss (A)", 2D) = "black" {}
		_CellGloss("Gloss Multiplier", Range(0,1)) = 1
		_CellNorm("Normals", 2D) = "normal" {}
		[HDR]_CellIllum("Emission", Color) = (0,0,0,0)

		[Header(Gaps)]
		_SepColor("Color", Color) = (1,1,1,1)
		_SepTex("Texture", 2D) = "white" {}
		_SepSpec("Spec (RGB), Gloss (A)", 2D) = "black" {}
		_SepGloss("Gloss Multiplier", Range(0,1)) = 1
		_SepNorm("Normals", 2D) = "normal" {}
		[HDR]_SepIllum("Emission", Color) = (0,0,0,0)

		[Header(Noise Properties)]
		_Size("Size", Range(0.1, 10)) = 1
		_Speed("Animation Speed", Range(0,2)) = 0
		_CellSpread("Cell Spread", Range(0.001, 5)) = 1
		_Thresh("Threshold", Range(0,1)) = 0.5
		_Steep("Steepness", Range(1,10)) = 2
		_Angularity("Angularity", Range(0, 1)) = 0
		_Randomness("Randomness", Range(0,10)) = 5

		_Levels("Iterations", Range(1,4)) = 1
		[MaterialToggle] _Invert("Invert", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
			
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf StandardSpecular fullforwardshadows vertex:vert alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		#include "Noise.cginc"

		struct Input {
			float2 uv_CellTex;
			float2 uv_SepTex;
			float3 locPos;
		};

		uniform half4 _CellColor;
		uniform sampler2D _CellTex;
		uniform sampler2D _CellSpec;
		uniform sampler2D _CellNorm;
		uniform float _CellGloss;
		uniform half4 _CellIllum;

		uniform half4 _SepColor;
		uniform sampler2D _SepTex;
		uniform sampler2D _SepSpec;
		uniform sampler2D _SepNorm;
		uniform float _SepGloss;
		uniform half4 _SepIllum;

		uniform float _Size;
		uniform float _Steep;
		uniform float _Thresh;
		uniform float _Speed;
		uniform float _CellSpread;
		uniform float _Angularity;
		uniform float _Randomness;
		uniform float _Levels;
		uniform float _Invert;

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.locPos = v.vertex.xyz / (0.1*_Size);
		}

		void surf (Input IN, inout SurfaceOutputStandardSpecular o) {

			float noiseCoord = worley(IN.locPos, _CellSpread, _Angularity, _Randomness, _Time.y * _Speed, _Levels);

			noiseCoord = sigmoid(noiseCoord - _Thresh, 0, 1, _Steep * _Steep); //TODO fix
			noiseCoord = abs(_Invert - noiseCoord);
			
			fixed4 cell = tex2D(_CellTex, IN.uv_CellTex) * _CellColor;
			fixed4 sep = tex2D(_SepTex, IN.uv_SepTex) * _SepColor;
			o.Albedo = noiseCoord * cell.rgb + (1 - noiseCoord) * sep.rgb;
			o.Alpha = noiseCoord * cell.a + (1 - noiseCoord) * sep.a;

			o.Emission = noiseCoord * _CellIllum + (1 - noiseCoord) * _SepIllum;

			cell = tex2D(_CellSpec, IN.uv_CellTex);
			sep = tex2D(_SepSpec, IN.uv_SepTex);

			o.Specular = noiseCoord * cell.rgb + (1 - noiseCoord) * sep.rgb;
			o.Smoothness = noiseCoord * cell.a * _CellGloss + (1 - noiseCoord) * sep.a *_SepGloss;

			o.Normal = noiseCoord * UnpackNormal(tex2D(_CellNorm, IN.uv_CellTex)) + (1 - noiseCoord) * UnpackNormal(tex2D(_SepNorm, IN.uv_SepTex));
		}
		ENDCG
	}
	FallBack "Diffuse"
}
